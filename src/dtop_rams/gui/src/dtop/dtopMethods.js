export const getDomain = () => {
  if (process.env.VUE_APP_TESTING === 'testing') {
    return 'dto.test';
  } else {
    return window.location.hostname.replace(`${process.env.VUE_APP_DTOP_MODULE_SHORT_NAME}.`, '');
  }
}

export const getProtocol = () => {
  if (process.env.VUE_APP_TESTING === 'testing') {
    return '';
  } else {
    return window.location.protocol + '//'; // 'http://'
  }
}

export const getAuth = () => {
  return window.location.protocol === 'https:' ? {
    headers: {
      "Authorization": process.env.VUE_APP_DTOP_BASIC_AUTH
    },
    withCredentials: true
  } : {};
}
