import axios from "axios";

export default {
  data () {
    return {
      form: null
    }
  },
  created: function () {
     console.log("Printing from the Mixin tested", this.studyId)
  },
  computed: {
    studyId() {
      return this.$store.getters.studyId
    }
  },
  methods: {
  }
}
