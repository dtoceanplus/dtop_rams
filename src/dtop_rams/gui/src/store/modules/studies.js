const state = {
  studyId: null,
  studyTitle: null,
  task_id_uls: null,
  task_id_fls: null,
  task_id_relaibility: null,
  project_structure: [{projectId: null,
                      studyId: null,
                      moduleId: null,
                      entityId: null}]
}

const mutations = {
  SET_ID: (state, studyId) => {
    state.studyId = studyId
  },
  SET_TITLE: (state, studyTitle) => {
    state.studyTitle = studyTitle
  },
  SET_PROJECT_STRUCTURE: (state, project_structure) => {
    state.project_structure = project_structure
  },
  SET_TASK_RELIABILITY: (state, task_id_relaibility) => {
    state.task_id_relaibility = task_id_relaibility
  },
  SET_TASK_FLS: (state, task_id_fls) => {
    state.task_id_fls = task_id_fls
  },
  SET_TASK_ULS: (state, task_id_uls) => {
    state.task_id_uls = task_id_uls
  }
}

const actions = {
  setId({ commit }, { studyId }) {
    commit('SET_ID', studyId)
  },
  setTitle({ commit }, { studyTitle }) {
    commit('SET_TITLE', studyTitle)
  },
  setProjectStructure({ commit }, { project_structure }) {
    commit('SET_PROJECT_STRUCTURE', project_structure)
  },
  setTaskReliability({ commit }, { task_id_relaibility }) {
    commit('SET_TASK_RELIABILITY', task_id_relaibility)
  },
  setTaskFLS({ commit }, { task_id_fls }) {
    commit('SET_TASK_FLS', task_id_fls)
  },
  setTaskULS({ commit }, { task_id_uls }) {
    commit('SET_TASK_ULS', task_id_uls)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
