const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  studyId: state => state.studies.studyId,
  studyTitle: state => state.studies.studyTitle,
  project_structure: state => state.studies.project_structure,
  task_id_uls: state => state.studies.task_id_uls,
  task_id_fls: state => state.studies.task_id_fls,
  task_id_relaibility: state => state.studies.task_id_relaibility
}
export default getters
