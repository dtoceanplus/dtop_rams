module.exports = {
    moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
    transform: {
        '^.+\\.vue$': 'vue-jest',
        '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
        '^.+\\.jsx?$': 'babel-jest'
    },
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1'
    },
    snapshotSerializers: ['jest-serializer-vue'],
    testMatch: [
        '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
    ],
    coverageThreshold: {
        global: {
            statements: 93.84,
            branches: 82.68,
            functions: 97.3,
            lines: 93.83
        }
    },
    collectCoverageFrom: ['src/utils/**/*.{js,vue}', '!src/utils/auth.js', '!src/utils/request.js', 'src/components/**/*.{js,vue}', 'src/views/**/*.{js,vue}', '!src/views/404.vue', '!src/views/dashboard/*', '!src/views/form/*', '!src/views/login/*', '!src/views/table/*', '!src/views/tree/*'],
    coverageDirectory: '<rootDir>/tests/unit/coverage',
    'collectCoverage': true,
    'coverageReporters': [
        'html',
        'text-summary',
        'clover'
    ],

    reporters: [
        'default',
        '<rootDir>/custom-reporter.js'
    ],
    testURL: 'http://localhost/',
    setupFiles: [
      "core-js",
      '<rootDir>/jest.stub.js'
    ]
}
