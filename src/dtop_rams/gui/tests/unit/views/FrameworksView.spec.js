import { shallowMount } from '@vue/test-utils'

import FrameworksView from '@/views/frameworks/view/index.vue'

import ElementUI from 'element-ui';
import Vue from 'vue'
import axios from 'axios'

import FrameworkViewJson from '../json/FrameworkViewJson.json'
import StageDataJson from '../json/StageDataJson.json'
import StageEvalDataJson from '../json/StageEvalDataJson.json'
import StageGateDataJson from '../json/StageGateDataJson.json'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let framework_id = 1
    let wrapper = shallowMount(FrameworksView, {
        mocks: {
            $route: {
                params: {
                    framework_id: framework_id,
                }
            }
        }
    })

    it('getFramework', async() => {
        axios.resolveWith(FrameworkViewJson)
        await wrapper.vm.getFramework(wrapper.vm.frameworkID);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.framework_data).toEqual(FrameworkViewJson.data);
    })

    it('activateData gates', () => {
        let tab_name = 'stage_gates_1';
        wrapper.vm.activateData(tab_name);
        expect(wrapper.vm.idx).toEqual("1");
        expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stage_gates[wrapper.vm.idx]);
    })

    it('activateData gates else', () => {
        let tab_name = 'stages_0';
        wrapper.vm.activateData(tab_name);
        expect(wrapper.vm.idx).toEqual("0");
        expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stages[wrapper.vm.idx]);
    })

    it('activateData stages category', () => {
        let tab_name = 'stages_0';
        wrapper.vm.category = true
        wrapper.vm.activeName = 'stages_1'
        wrapper.vm.activateData(tab_name);
        expect(wrapper.vm.idx).toEqual("0");
    })

    it('activateData stages', () => {
        let tab_name = 'stages_0';
        wrapper.vm.category = false
        wrapper.vm.activeName = 'stages_1'
        wrapper.vm.activateData(tab_name);
        expect(wrapper.vm.idx).toEqual("0");
    })

    it('activateData else', () => {
        let tab_name = 'stages_0';
        wrapper.vm.category = false
        wrapper.vm.activeName = 'stage_gates_1'
        wrapper.vm.activateData(tab_name);
        expect(wrapper.vm.idx).toEqual("0");
    })

    it('getStageData', async() => {
        axios.resolveWith(StageDataJson)
        let uri = "/api/stages/1";
        await wrapper.vm.getStageData(uri);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.data).toEqual(StageDataJson.data.activity_categories);
    })

    it('getStageData error', async() => {
      axios.resolveWith(StageDataJson, true)
      let uri = "/api/stages/1";
      await wrapper.vm.getStageData(uri);
      await wrapper.vm.$nextTick()
  })

    it('getStageEvalData', async() => {
        axios.resolveWith(StageEvalDataJson, false)
        await wrapper.vm.getStageEvalData("/api/stages/1");
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.data).toEqual(StageEvalDataJson.data.evaluation_areas);
    })

    it('getStageEvalData error', async() => {
      axios.resolveWith(StageEvalDataJson, true)
      await wrapper.vm.getStageEvalData(wrapper.vm.framework_data.stages[0]);
      await wrapper.vm.$nextTick();
  })

    it('getStageGateData', async() => {
        axios.resolveWith(StageGateDataJson)
        await wrapper.vm.getStageGateData(wrapper.vm.framework_data.stage_gates[0]);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.data).toEqual(StageGateDataJson.data.question_categories);
    })

    it('getStageGateData', async() => {
      axios.resolveWith(StageGateDataJson, true)
      await wrapper.vm.getStageGateData(wrapper.vm.framework_data.stage_gates[0]);
      await wrapper.vm.$nextTick()
  })

    it('handleClick', () => {
        let tab = {
            name: "stages_2"
        }
        wrapper.vm.handleClick(tab);
    })

    it('test emit of reloadData', async() => {
      wrapper.vm.$root.$emit('reloadData')
    })
})
