import { shallowMount } from '@vue/test-utils'

import ChecklistInput from '@/views/stage_gate_studies/checklists/input/index.vue'
import ElementUI from 'element-ui';
import FrameworkViewJson from '../json/FrameworkViewJson.json'
import StageDataJson from '../json/StageDataJson.json'
import StageEvalDataJson from '../json/StageEvalDataJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  const $router = {
    push: jest.fn(),
  }
  let study_data = {
    "_links": {
      "collection": "/api/stage-gate-studies/",
      "self": "/api/stage-gate-studies/1"
    },
    "checklist_study_complete": true,
    "description": "test",
    "framework": "DTOceanPlus Framework template",
    "framework_id": 1,
    "id": 1,
    "name": "test"
  }
  const wrapper = shallowMount(ChecklistInput, {
    mocks: {
      $route: {
          params: {
              study_data: study_data
          }
      },
      $router
    }
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": study_data.id}});
  })

  it('initialiseData', async() => {
    axios.resolveWith(FrameworkViewJson)
    await wrapper.vm.initialiseData(wrapper.vm.studyData.framework_id)
    expect(wrapper.vm.framework_data).toEqual(FrameworkViewJson.data)
    expect(wrapper.vm.end_idx).toEqual(wrapper.vm.framework_data.stages.length - 1)
    expect(wrapper.vm.stage_uri).toEqual(FrameworkViewJson.data.stages[wrapper.vm.stage_idx])
  })

  it('getStageData', async() => {
    axios.resolveWith(StageDataJson)
    await wrapper.vm.getStageData(wrapper.vm.stage_uri);
    expect(wrapper.vm.stageName).toEqual(StageDataJson.data.name);
    expect(wrapper.vm.stageData).toEqual(StageDataJson.data.activity_categories);
  })

  it('getStageData error', async() => {
    axios.resolveWith(StageDataJson, true)
    await wrapper.vm.getStageData(wrapper.vm.stage_uri);
  })

  it('getChecklistData', async() => {
    let ChecklistDataJson = { "data": { "completed_activities": [1] } }
    axios.resolveWith(ChecklistDataJson)
    await wrapper.vm.getChecklistData(wrapper.vm.studyData.id);
    expect(wrapper.vm.selectedActivities).toEqual(ChecklistDataJson.data.completed_activities);
  })

  it('getChecklistData error', async() => {
    let ChecklistDataJson = { "data": { "completed_activities": [1] } }
    axios.resolveWith(ChecklistDataJson, true)
    await wrapper.vm.getChecklistData(wrapper.vm.studyData.id);
  })

  it('getStageEvalData', async() => {
    axios.resolveWith(StageEvalDataJson)
    await wrapper.vm.getStageEvalData(wrapper.vm.stage_uri);
    expect(wrapper.vm.stageName).toEqual(StageEvalDataJson.data.name);
    expect(wrapper.vm.stageData).toEqual(StageEvalDataJson.data.evaluation_areas);
  })

  it('getStageEvalData error', async() => {
    axios.resolveWith(StageEvalDataJson, true)
    await wrapper.vm.getStageEvalData(wrapper.vm.stage_uri);
  })

  it('activateData true', () => {
    wrapper.vm.category = true
    wrapper.vm.activateData()
  })

  it('activateData false', () => {
    wrapper.vm.category = false
    wrapper.vm.activateData()
  })

  it('previousDisabled stage_idx == 0', () => {
    wrapper.vm.stage_idx = 0
    let result = wrapper.vm.previousDisabled()
    expect(result).toEqual(true)
  })

  it('previousDisabled stage_idx != 0', () => {
    wrapper.vm.stage_idx = 1
    let result = wrapper.vm.previousDisabled()
    expect(result).toEqual(false)
  })

  it('nextDisabled stage_idx == end_idx', () => {
    wrapper.vm.stage_idx = 0
    wrapper.vm.end_idx = 0
    let result = wrapper.vm.nextDisabled()
    expect(result).toEqual(true)
  })

  it('nextDisabled stage_idx != end_idx', () => {
    wrapper.vm.stage_idx = 0
    wrapper.vm.end_idx = 1
    let result = wrapper.vm.nextDisabled()
    expect(result).toEqual(false)
  })

  it('loadStage', () => {
    let result = wrapper.vm.loadStage()
    expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stages[wrapper.vm.stage_idx])
  })

  it('previousStage', () => {
    wrapper.vm.stage_idx = 1
    wrapper.vm.previousStage()
    expect(wrapper.vm.stage_idx).toEqual(0)
  })

  it('nextStage', () => {
    wrapper.vm.stage_idx = 1
    wrapper.vm.nextStage()
    expect(wrapper.vm.stage_idx).toEqual(2)
  })

  it('onSaveChecklist', () => {
    wrapper.vm.onSaveChecklist()
  })

  it('onSubmitChecklist', () => {
    wrapper.vm.onSubmitChecklist()
  })

  it('saveChecklist', async() => {
    const payload = {
        completed_activities: wrapper.vm.selectedActivities
    }
    let success_msg = {
      data: {
        message: 'fair job',
        type: 'success'
      }
    }
    axios.resolveWith(success_msg)
    await wrapper.vm.saveChecklist(payload, wrapper.vm.studyData.id)
  })

  it('openResultsPage', async() => {
    await wrapper.vm.openResultsPage()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ChecklistOutput", "params": {"study_data": study_data}});
  })
})
