import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import StageGateStudies from '@/views/stage_gate_studies/index.vue'
import ElementUI from 'element-ui';
import StageGateStudiesJson from '../json/StageGateStudiesJson.json'
import FrameworksJson from '../json/FrameworksJson.json'

import Vue from 'vue'
import axios from 'axios'

/* Layout */
import Layout from '@/layout'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    const localVue = createLocalVue()
    localVue.use(VueRouter)
    const routes = [{
        path: '/stage-gate-studies',
        component: Layout,
        redirect: '/stage-gate-studies/index',
        name: 'StageGateStudies',
        meta: { title: 'Stage Gate Studies', icon: 'table' },
        children: [{
                path: 'index',
                name: 'StageGateStudies',
                component: () =>
                    import ('@/views/stage_gate_studies/index'),
                meta: { title: 'Index' },
                hidden: true
            },
            {
                path: ':study_id',
                name: 'SgStudiesHome',
                component: () =>
                    import ('@/views/stage_gate_studies/home/index'),
                meta: { title: 'Home' },
                hidden: true
            }
        ]
    }];
    const router = new VueRouter({ routes })
    const wrapper = shallowMount(StageGateStudies, {
        localVue,
        router
    })

    let row = { id: 1, name: "name", description: "desc", framework: "framework", framework_id: 1 }

    it('getStudies', async() => {
        axios.resolveWith(StageGateStudiesJson)
        await wrapper.vm.getStudies();
        expect(wrapper.vm.sg_studies).toEqual(StageGateStudiesJson.data.stage_gate_studies);
    })

    it('getFrameworks', async() => {
        axios.resolveWith(FrameworksJson)
        await wrapper.vm.getFrameworks();
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.frameworks).toEqual(FrameworksJson.data.frameworks);
    })

    it('onSelectStudy', async() => {
        wrapper.vm.onSelectStudy(row);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$route.path).toBe('/stage-gate-studies/' + row.id)
    })

    it('addStudy', async() => {
        axios.resolveWith(StageGateStudiesJson, false)
        await wrapper.vm.addStudy(StageGateStudiesJson);
    })

    it('addStudy error', async() => {
        axios.resolveWith(StageGateStudiesJson, true)
        await wrapper.vm.addStudy(StageGateStudiesJson);
    })

    it('updateStudy', async() => {
        axios.resolveWith(StageGateStudiesJson, false)
        await wrapper.vm.updateStudy(StageGateStudiesJson);
    })

    it('updateStudy error', async() => {
        axios.resolveWith(StageGateStudiesJson, true)
        await wrapper.vm.updateStudy(StageGateStudiesJson);
    })

    it('editStudy', () => {
        wrapper.vm.editStudy(row);
        expect(wrapper.vm.editForm.id).toEqual(row.id);
        expect(wrapper.vm.editForm.name).toEqual(row.name);
        expect(wrapper.vm.editForm.description).toEqual(row.description);
        expect(wrapper.vm.dialogUpdateFormVisible).toEqual(true);
    })

    it('removeStudy', async() => {
        axios.resolveWith(FrameworksJson, false)
        await wrapper.vm.removeStudy(FrameworksJson);
    })

    it('removeStudy error', async() => {
        axios.resolveWith(FrameworksJson, true)
        await wrapper.vm.removeStudy(FrameworksJson);
    })

    it('onDeleteStudy', () => {
        wrapper.vm.onDeleteStudy(row);
        expect(wrapper.vm.deleteForm.id).toEqual(row.id);
        expect(wrapper.vm.deleteForm.name).toEqual(row.name);
        expect(wrapper.vm.deleteForm.description).toEqual(row.description);
        expect(wrapper.vm.dialogDeleteVisible).toEqual(true);

        expect(wrapper.vm.deleteForm.framework).toEqual(row.framework);
        expect(wrapper.vm.deleteForm.framework_id).toEqual(row.framework_id);

    })
    it('initForm', () => {
        wrapper.vm.initForm();
        expect(wrapper.vm.addStudyForm.name).toEqual('');
        expect(wrapper.vm.addStudyForm.description).toEqual('');
        expect(wrapper.vm.addStudyForm.framework).toEqual('');
        expect(wrapper.vm.addStudyForm.framework_id).toEqual('');

        expect(wrapper.vm.editForm.id).toEqual('');
        expect(wrapper.vm.editForm.name).toEqual('');
        expect(wrapper.vm.editForm.description).toEqual('');

        expect(wrapper.vm.deleteForm.id).toEqual('');
        expect(wrapper.vm.deleteForm.name).toEqual('');
        expect(wrapper.vm.deleteForm.description).toEqual('');
        expect(wrapper.vm.deleteForm.framework).toEqual('');
        expect(wrapper.vm.deleteForm.framework_id).toEqual('');
    })
    it('onSubmit', () => {
        wrapper.vm.onSubmit();
        expect(wrapper.vm.dialogFormVisible).toEqual(false);
    })

    it('onCancel', () => {
        wrapper.vm.onCancel();
        expect(wrapper.vm.dialogFormVisible).toEqual(false);
        expect(wrapper.vm.dialogUpdateFormVisible).toEqual(false);
        expect(wrapper.vm.dialogDeleteVisible).toEqual(false);
    })

    it('onSubmitUpdate', () => {
        wrapper.vm.onSubmitUpdate();
        expect(wrapper.vm.dialogUpdateFormVisible).toEqual(false);
    })

})
