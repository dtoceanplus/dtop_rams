import { shallowMount } from '@vue/test-utils'

import AssessorOutput from '@/views/stage_gate_studies/assessor/output/index'
import ElementUI from 'element-ui';
import AssessorResultsJson from '../json/AssessorResultsJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")


describe('index.vue', () => {
  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate_data = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(AssessorOutput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate_data: app_stage_gate_data
            }
        },
        $router
    }
  })

  it('getAssessorResults', async() => {
    axios.resolveWith(AssessorResultsJson)
    await wrapper.vm.getAssessorResults(wrapper.vm.appStageGateData.id)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.stageResults).toEqual(AssessorResultsJson.data.overall)
    expect(wrapper.vm.questionCategoryResults).toEqual(AssessorResultsJson.data.question_categories)
    expect(wrapper.vm.evaluationAreaResults).toEqual(AssessorResultsJson.data.evaluation_areas)
    expect(wrapper.vm.assessorStageGateData).toEqual(AssessorResultsJson.data.responses)
  })

  it('goBack', async() => {
    await wrapper.vm.goBack();
    await wrapper.vm.$nextTick();
    expect($router.push).toHaveBeenCalledWith({"name": "AssessorHome", "params": {"study_data": study_data}});
  })

  it('customColorMethod', () => {
    let red = wrapper.vm.customColorMethod(30);
    expect(red).toEqual('#f56c6c');
    let grey = wrapper.vm.customColorMethod(59);
    expect(grey).toEqual('#909399');
    let orange = wrapper.vm.customColorMethod(99);
    expect(orange).toEqual('#e6a23c');
    let green = wrapper.vm.customColorMethod(100);
    expect(green).toEqual('#5cb87a');
  })

  it('isThresholdApplied', () => {
    let q_true = {
      question_metric: {
        threshold_bool: true
      }
    };
    let q_threshold_true = wrapper.vm.isThresholdApplied(q_true);
    expect(q_threshold_true).toBeTruthy()
    let q_false = {
      question_metric: {
        threshold_bool: false
      }
    };
    let q_threshold_false = wrapper.vm.isThresholdApplied(q_false);
    expect(q_threshold_false).toBeFalsy()
    let q_no_qm = {
      error: true
    };
    let q_no_qm_false = wrapper.vm.isThresholdApplied(q_no_qm);
    expect(q_no_qm_false).not.toBeTruthy()
  })

  it('isThresholdFailed', () => {
    let error = {
      error: true
    };
    let error_out = wrapper.vm.isThresholdFailed(error);
    expect(error_out).toBeFalsy()
    let threshold = {
      question_metric: {
        threshold_bool: true
      },
      applicant_results: {
        threshold_passed: false
      }
    };
    let threshold_out = wrapper.vm.isThresholdFailed(threshold);
    expect(threshold_out).toBeTruthy()
  })

  it('returnThresholdPassed', () => {
    let q_yes = {
      applicant_results:{
        threshold_passed: true
      }
    }
    let q_yes_out = wrapper.vm.returnThresholdPassed(q_yes);
    expect(q_yes_out).toEqual("Yes")
    let q_no = {
      applicant_results:{
        threshold_passed: false
      }
    }
    let q_no_out = wrapper.vm.returnThresholdPassed(q_no);
    expect(q_no_out).toEqual("No")
  })
})
