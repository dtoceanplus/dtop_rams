import { shallowMount } from '@vue/test-utils'

import ApplicantHome from '@/views/stage_gate_studies/applicant/home/index'
import ElementUI from 'element-ui';
import StageGateAssessmentsJson from '../json/StageGateAssessmentsJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ApplicantHome, {
      mocks: {
          $route: {
              params: {
                  study_data: study_data
              }
          },
          $router
      }
  })

  it('getStageGateData', async() => {
    axios.resolveWith(StageGateAssessmentsJson)
    await wrapper.vm.getStageGateData(wrapper.vm.studyData.id);
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.stageGateData).toEqual(StageGateAssessmentsJson.data.stage_gate_assessments)
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": 1}});
  })

  let app_stage_gate = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  it('openStageGateInput', async() => {
    await wrapper.vm.openStageGateInput(app_stage_gate)
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({
      "name": "ApplicantInput",
      "params": {
        "study_data": study_data,
        "app_stage_gate": app_stage_gate
      }
    });
  })

  it('openStageGateOutput', async() => {
    await wrapper.vm.openStageGateOutput(app_stage_gate)
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({
      "name": "ApplicantOutput",
      "params": {
        "study_data": study_data,
        "app_stage_gate_data": app_stage_gate
      }
    });
  })
})
