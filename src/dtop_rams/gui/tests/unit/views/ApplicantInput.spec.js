import { shallowMount } from '@vue/test-utils'

import ApplicantInput from '@/views/stage_gate_studies/applicant/input/index'
import ElementUI from 'element-ui';
import ApplicantAnswersJson from '../json/ApplicantAnswersJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ApplicantInput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate: app_stage_gate
            }
        },
        $router
    }
  })

  it('getApplicantAnswers', async() => {
    axios.resolveWith(ApplicantAnswersJson)
    await wrapper.vm.getApplicantAnswers(wrapper.vm.appStageGateData.id)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.applicantAnswers).toEqual(ApplicantAnswersJson.data)
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ApplicantHome", "params": {"study_data": study_data}});
  })

  it('getApplicantAnswerPayload', async() => {
    let payload = wrapper.vm.getApplicantAnswerPayload();
    await wrapper.vm.$nextTick()
    let app_answers = [
      {
        "id": 1,
        "justification": "Is this working? Yes it is.",
        "response": null,
        "result": 40
      },
      {
        "id": 2,
        "justification": null,
        "response": null,
        "result": null
      },
      {
        "id": 3,
        "justification": null,
        "response": null,
        "result": null
      },
      {
        "id": 4,
        "justification": null,
        "response": null,
        "result": 5
      }
    ]
    expect(payload).toEqual(app_answers)
  })

  it('onSaveApplicantInputs', async() => {
    await wrapper.vm.onSaveApplicantInputs();
  })

  it('saveApplicantInputs', async() => {
    axios.resolveWith(ApplicantAnswersJson, false)
    await wrapper.vm.saveApplicantInputs();
  })

  it('saveApplicantInputs error', async() => {
    axios.resolveWith(ApplicantAnswersJson, true)
    await wrapper.vm.saveApplicantInputs();
  })

  it('onSubmitApplicantInputs', async() => {
    await wrapper.vm.onSubmitApplicantInputs();
  })

  it('openApplicantOutputs', async() => {
      await wrapper.vm.openApplicantOutputs();
      await wrapper.vm.$nextTick;
      expect($router.push).toHaveBeenCalledWith({
        "name": "ApplicantOutput",
        "params": {
          "study_data": study_data,
          "app_stage_gate_data": app_stage_gate
        }
      });
  })

  it('updateApplicantStudyStatus', async() => {
    axios.resolveWith(ApplicantAnswersJson, false)
    await wrapper.vm.updateApplicantStudyStatus();
  })

  it('updateApplicantStudyStatus error', async() => {
    axios.resolveWith(ApplicantAnswersJson, true)
    await wrapper.vm.updateApplicantStudyStatus();
  })
})
