import { shallowMount } from '@vue/test-utils'

import AssessorInput from '@/views/stage_gate_studies/assessor/input/index'
import ElementUI from 'element-ui';
import AssessorScoresJson from '../json/AssessorScoresJson.json'

import Vue from 'vue'
import axios from 'axios'
import { _ } from 'core-js';

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(AssessorInput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate: app_stage_gate
            }
        },
        $router
    }
  })

  it('getAssessorScores', async() => {
    axios.resolveWith(AssessorScoresJson)
    await wrapper.vm.getAssessorScores(wrapper.vm.appStageGateData.id)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.assessorScores).toEqual(AssessorScoresJson.data)
    let s_list = [
      {
        "id": 1,
        "score": 2
      },
      {
        "id": 2,
        "score": 3
      }
    ]
    expect(wrapper.vm.score_list).toEqual(s_list)
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "AssessorHome", "params": {"study_data": study_data}});
  })

  it('isThresholdApplied', () => {
    let q_true = {
      question_metric: {
        threshold_bool: true
      }
    };
    let q_threshold_true = wrapper.vm.isThresholdApplied(q_true);
    expect(q_threshold_true).toBeTruthy()
    let q_false = {
      question_metric: {
        threshold_bool: false
      }
    };
    let q_threshold_false = wrapper.vm.isThresholdApplied(q_false);
    expect(q_threshold_false).toBeFalsy()
    let q_no_qm = {
      error: true
    };
    let q_no_qm_false = wrapper.vm.isThresholdApplied(q_no_qm);
    expect(q_no_qm_false).toBeFalsy()
  })

  it('isThresholdFailed', () => {
    let error = {
      error: true
    };
    let error_out = wrapper.vm.isThresholdFailed(error);
    expect(error_out).toBeFalsy()
    let threshold = {
      question_metric: {
        threshold_bool: true
      },
      applicant_results: {
        threshold_passed: false
      }
    };
    let threshold_out = wrapper.vm.isThresholdFailed(threshold);
    expect(threshold_out).toBeTruthy()
  })

  it('returnThresholdPassed', () => {
    let q_yes = {
      applicant_results:{
        threshold_passed: true
      }
    }
    let q_yes_out = wrapper.vm.returnThresholdPassed(q_yes);
    expect(q_yes_out).toEqual("Yes")
    let q_no = {
      applicant_results:{
        threshold_passed: false
      }
    }
    let q_no_out = wrapper.vm.returnThresholdPassed(q_no);
    expect(q_no_out).toEqual("No")
  })

  it('getAssessorScoresPayload', () => {
    let payload = wrapper.vm.getAssessorScoresPayload();
    let assessor_scores = {
      'assessor_scores': [
        {
          "id": 1,
          "score": 2
        },
        {
          "id": 2,
          "score": 3
        },
      ],
      'assessor_comments': [
        {
          "id": 1,
          "comment": "this is a test comment"
        },
        {
          "id": 2,
          "comment": "asdrfasdf"
        },
        {
          "id": 3,
          "comment": "asdfwqer"
        },
        {
          "id": 4,
          "comment": "fasderqwrqwe"
        }
      ]
    };
    expect(payload).toEqual(assessor_scores)
  })

  it('checkAssessorInputPayload', () => {
    wrapper.vm.checkAssessorInputPayload()
    expect(wrapper.vm.defaultScoreDialogVisible).toBe(false)
    wrapper.setData({ score_list: [ {id: 1, score: 2}, {id: 2, score: null} ] })
    wrapper.vm.checkAssessorInputPayload();
    expect(wrapper.vm.defaultScoreDialogVisible).toBe(true)
  })

  it('onCancel', () => {
    wrapper.setData({ defaultScoreDialogVisible: true })
    wrapper.vm.onCancel()
    expect(wrapper.vm.defaultScoreDialogVisible).toBe(false)
  })

  it('onFillScores', () => {
    wrapper.setData({ score_list: [ {id: 1, score: "2"}, {id: 2, score: null} ] })
    wrapper.vm.onFillScores()
    expect(wrapper.vm.score_list).toEqual([{"id": 1, "score": "2"}, {"id": 2, "score": "2"}])
  })

  it('onSaveAssessorScores', async() => {
    await wrapper.vm.onSaveAssessorScores();
  })

  it('saveAssessorScores', async() => {
    axios.resolveWith(AssessorScoresJson, false)
    await wrapper.vm.saveAssessorScores();
  })

  it('saveAssessorScores error', async() => {
    axios.resolveWith(AssessorScoresJson, true)
    await wrapper.vm.saveAssessorScores();
  })

  it('onSubmitAssessorScores', async() => {
    await wrapper.vm.onSubmitAssessorScores();
  })

  it('openAssessorOutputs', async() => {
      await wrapper.vm.openAssessorOutputs();
      await wrapper.vm.$nextTick;
      expect($router.push).toHaveBeenCalledWith({
        "name": "AssessorOutput",
        "params": {
          "study_data": study_data,
          "app_stage_gate_data": app_stage_gate
        }
      });
  })

  it('updateAssessorStudyStatus', async() => {
    axios.resolveWith(AssessorScoresJson, false)
    await wrapper.vm.updateAssessorStudyStatus();
  })

  it('updateAssessorStudyStatus error', async() => {
    axios.resolveWith(AssessorScoresJson, true)
    await wrapper.vm.updateAssessorStudyStatus();
  })
})
