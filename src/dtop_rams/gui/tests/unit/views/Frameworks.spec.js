import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import Frameworks from '@/views/frameworks/index.vue'
import ElementUI from 'element-ui';
import Vue from 'vue'
import FrameworksJson from '../json/FrameworksJson.json'
import axios from 'axios'
/* Layout */
import Layout from '@/layout'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    const localVue = createLocalVue()
    localVue.use(VueRouter)
    const routes = [{
        path: '/frameworks',
        component: Layout,
        redirect: '/frameworks/index',
        name: 'Frameworks',
        meta: { title: 'Frameworks', icon: 'nested' },
        children: [{
                path: 'index',
                name: 'Index',
                component: () =>
                    import ('@/views/frameworks/index'),
                meta: { title: 'Index' },
                hidden: true
            },

            {
                path: ':framework_id',
                name: 'View',
                component: () =>
                    import ('@/views/frameworks/view/index'),
                meta: { title: 'View' },
                hidden: true
            }
        ]
    }, ];
    const router = new VueRouter({ routes })
    const wrapper = shallowMount(Frameworks, {
        localVue,
        router
    })

    let row = { id: 1, name: "name", description: "desc" }

    it('getFrameworks', async() => {
        axios.resolveWith(FrameworksJson)
        await wrapper.vm.getFrameworks();
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.frameworks).toEqual(FrameworksJson.data.frameworks);
    })

    it('viewFramework', async() => {
        wrapper.vm.viewFramework(row);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$route.path).toBe('/frameworks/' + row.id)
    })

    it('addFramework', async() => {
        axios.resolveWith(FrameworksJson, false)
        await wrapper.vm.addFramework(FrameworksJson);
    })

    it('addFramework error', async() => {
        axios.resolveWith(FrameworksJson, true)
        await wrapper.vm.addFramework(FrameworksJson);
    })

    it('updateFramework', async() => {
        axios.resolveWith(FrameworksJson, false)
        await wrapper.vm.updateFramework(FrameworksJson);
    })

    it('updateFramework error', async() => {
        axios.resolveWith(FrameworksJson, true)
        await wrapper.vm.updateFramework(FrameworksJson);
    })

    it('editFramework', () => {
        wrapper.vm.editFramework(row);
        expect(wrapper.vm.editForm.id).toEqual(row.id);
        expect(wrapper.vm.editForm.name).toEqual(row.name);
        expect(wrapper.vm.editForm.description).toEqual(row.description);
        expect(wrapper.vm.dialogUpdateFormVisible).toEqual(true);
    })

    it('removeFramework', async() => {
        axios.resolveWith(FrameworksJson, false)
        await wrapper.vm.removeFramework(FrameworksJson);
    })

    it('removeFramework error', async() => {
        axios.resolveWith(FrameworksJson, true)
        await wrapper.vm.removeFramework(FrameworksJson);
    })

    it('onDeleteFramework', () => {
        wrapper.vm.onDeleteFramework(row);
        expect(wrapper.vm.deleteForm.id).toEqual(row.id);
        expect(wrapper.vm.deleteForm.name).toEqual(row.name);
        expect(wrapper.vm.deleteForm.description).toEqual(row.description);
        expect(wrapper.vm.dialogDeleteVisible).toEqual(true);
    })
    it('initForm', () => {
        wrapper.vm.initForm();
        expect(wrapper.vm.addFrameworkForm.name).toEqual('');
        expect(wrapper.vm.addFrameworkForm.description).toEqual('');
        expect(wrapper.vm.editForm.id).toEqual('');
        expect(wrapper.vm.editForm.name).toEqual('');
        expect(wrapper.vm.editForm.description).toEqual('');
        expect(wrapper.vm.deleteForm.id).toEqual('');
        expect(wrapper.vm.deleteForm.name).toEqual('');
        expect(wrapper.vm.deleteForm.description).toEqual('');
    })

    it('onSubmit', () => {
        wrapper.vm.onSubmit();
        expect(wrapper.vm.dialogFormVisible).toEqual(false);

    })

    it('onSubmitUpdate', () => {
        wrapper.vm.onSubmitUpdate();
        expect(wrapper.vm.dialogUpdateFormVisible).toEqual(false);
    })

    it('onCancel', () => {
        wrapper.vm.onCancel();
        expect(wrapper.vm.dialogFormVisible).toEqual(false);
        expect(wrapper.vm.dialogUpdateFormVisible).toEqual(false);
        expect(wrapper.vm.dialogDeleteVisible).toEqual(false);
    })
    it('isDisabled', () => {
        let row = { name: "DTOceanPlus Framework template" }
        const result = wrapper.vm.isDisabled(row);
        expect(result).toEqual(true);
    })
    it('isDisabled else', () => {
        let row = { name: "" }
        const result = wrapper.vm.isDisabled(row);
        expect(result).toEqual(false);
    })
})
