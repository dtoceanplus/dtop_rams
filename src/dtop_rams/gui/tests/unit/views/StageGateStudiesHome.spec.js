import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import StageGateStudiesHome from '@/views/stage_gate_studies/home/index.vue'
import ElementUI from 'element-ui';
import getSgStudy from '../json/getSgStudy.json'

import Vue from 'vue'
import axios from 'axios'

/* Layout */
import Layout from '@/layout'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    const localVue = createLocalVue()
    localVue.use(VueRouter)
    let study_id = 1

    const $router = {
      push: jest.fn(),
    }
    const wrapper = shallowMount(StageGateStudiesHome, {
      mocks: {
          $route: {
              params: {
                  study_id: study_id
              }
          },
          $router
      }
    })

    it('getSgStudy', async() => {
      axios.resolveWith(getSgStudy)
      await wrapper.vm.getSgStudy();
      expect(wrapper.vm.studyData).toEqual(getSgStudy.data);
    })

    it('openChecklist', async() => {
      await wrapper.vm.openChecklist()
      await wrapper.vm.$nextTick()
      expect($router.push).toHaveBeenCalledWith({"name": "ChecklistInput", "params": {"study_data": getSgStudy.data}});
    })

    it('openChecklistResults', async() => {
      await wrapper.vm.openChecklistResults()
      await wrapper.vm.$nextTick()
      expect($router.push).toHaveBeenCalledWith({"name": "ChecklistOutput", "params": {"study_data": getSgStudy.data}});
    })

    it('openApplicantHome', async() => {
      await wrapper.vm.openApplicantHome()
      await wrapper.vm.$nextTick()
      expect($router.push).toHaveBeenCalledWith({"name": "ApplicantHome", "params": {"study_data": getSgStudy.data}});
    })

    it('openAssessorHome', async() => {
      await wrapper.vm.openAssessorHome()
      await wrapper.vm.$nextTick()
      expect($router.push).toHaveBeenCalledWith({"name": "AssessorHome", "params": {"study_data": getSgStudy.data}});
    })
})
