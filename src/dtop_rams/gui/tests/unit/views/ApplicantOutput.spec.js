import { shallowMount } from '@vue/test-utils'

import ApplicantOutput from '@/views/stage_gate_studies/applicant/output/index'
import ElementUI from 'element-ui';
import ApplicantResultsJson from '../json/ApplicantAnswersJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate_data = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ApplicantOutput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate_data: app_stage_gate_data
            }
        },
        $router
    }
  })

  it('getApplicantResults', async() => {
    axios.resolveWith(ApplicantResultsJson)
    await wrapper.vm.getApplicantResults(wrapper.vm.appStageGateData.id)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.applicantResults).toEqual(ApplicantResultsJson.data)
    expect(wrapper.vm.summaryData).toEqual(ApplicantResultsJson.data.summary_data)
    expect(wrapper.vm.responses).toEqual(ApplicantResultsJson.data.responses)
    expect(wrapper.vm.metricResults).toEqual(ApplicantResultsJson.data.metric_results)
  })

  it('goBack', async() => {
    await wrapper.vm.goBack();
    await wrapper.vm.$nextTick();
    expect($router.push).toHaveBeenCalledWith({"name": "ApplicantHome", "params": {"study_data": study_data}});
  })

  it('customColorMethod', () => {
    let red = wrapper.vm.customColorMethod(30);
    expect(red).toEqual('#f56c6c');
    let grey = wrapper.vm.customColorMethod(59);
    expect(grey).toEqual('#909399');
    let orange = wrapper.vm.customColorMethod(99);
    expect(orange).toEqual('#e6a23c');
    let green = wrapper.vm.customColorMethod(100);
    expect(green).toEqual('#5cb87a');
  })


  it('tableRowClassName', () => {
    let no_threshold_row = {
      threshold: false
    }
    let no_threshold = wrapper.vm.tableRowClassName({row: no_threshold_row, rowIndex: 0});
    expect(no_threshold).toEqual('')
    let warning_row = {
      threshold: 50,
      threshold_passed: false
    }
    let warning = wrapper.vm.tableRowClassName({row: warning_row, rowIndex: 0});
    expect(warning).toEqual('warning-row')
    let success_row = {
      threshold: 50,
      threshold_passed: true
    }
    let success = wrapper.vm.tableRowClassName({row: success_row, rowIndex: 0});
    expect(success).toEqual('success-row')
  })

  it('passFailFormatter', () => {
    let no_threshold_row = {
      threshold: false
    }
    let no_threshold = wrapper.vm.passFailFormatter(no_threshold_row);
    expect(no_threshold).toEqual('')
    let warning_row = {
      threshold: 50,
      threshold_passed: false
    }
    let warning = wrapper.vm.passFailFormatter(warning_row);
    expect(warning).toEqual('FAIL')
    let success_row = {
      threshold: 50,
      threshold_passed: true
    }
    let success = wrapper.vm.passFailFormatter(success_row);
    expect(success).toEqual('PASS')
  })

  it('isThresholdApplied', () => {
    let q_true = {
      question_metric: {
        threshold_bool: true
      }
    };
    let q_threshold_true = wrapper.vm.isThresholdApplied(q_true);
    expect(q_threshold_true).toBeTruthy()
    let q_false = {
      question_metric: {
        threshold_bool: false
      }
    };
    let q_threshold_false = wrapper.vm.isThresholdApplied(q_false);
    expect(q_threshold_false).toBeFalsy()
    let q_no_qm = {
      error: true
    };
    let q_no_qm_false = wrapper.vm.isThresholdApplied(q_no_qm);
    expect(q_no_qm_false).not.toBeTruthy()
  })

  it('isThresholdFailed', () => {
    let error = {
      error: true
    };
    let error_out = wrapper.vm.isThresholdFailed(error);
    expect(error_out).toBeFalsy()
    let threshold = {
      question_metric: {
        threshold_bool: true
      },
      applicant_results: {
        threshold_passed: false
      }
    };
    let threshold_out = wrapper.vm.isThresholdFailed(threshold);
    expect(threshold_out).toBeTruthy()
  })

  it('returnThresholdPassed', () => {
    let q_yes = {
      applicant_results:{
        threshold_passed: true
      }
    }
    let q_yes_out = wrapper.vm.returnThresholdPassed(q_yes);
    expect(q_yes_out).toEqual("Yes")
    let q_no = {
      applicant_results:{
        threshold_passed: false
      }
    }
    let q_no_out = wrapper.vm.returnThresholdPassed(q_no);
    expect(q_no_out).toEqual("No")
  })

  it('checkThresholdSuccessRate', () => {
    wrapper.setData({ summaryData: { threshold_success_rate: 0 } })
    expect(wrapper.vm.checkThresholdSuccessRate()).toBeTruthy()
    wrapper.setData({ summaryData: { threshold_success_rate: 50 } })
    expect(wrapper.vm.checkThresholdSuccessRate()).toBeTruthy()
    wrapper.setData({ summaryData: { threshold_success_rate: null } })
    expect(wrapper.vm.checkThresholdSuccessRate()).toBeFalsy()
  })
})
