import { shallowMount } from '@vue/test-utils'

import ChecklistOutput from '@/views/stage_gate_studies/checklists/output/index.vue'
import ElementUI from 'element-ui';
import CheckListOutputsJson from '../json/CheckListOutputsJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  let study_data = { "study_data": { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" } }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ChecklistOutput, {
    mocks: {
      $route: {
        params: study_data
      },
      $router
    }
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": study_data.study_data.id}});
  })

  it('getChecklistOutputs', async() => {
    axios.resolveWith(CheckListOutputsJson)
    await wrapper.vm.getChecklistOutputs()
    expect(wrapper.vm.summary_stage_data).toEqual(CheckListOutputsJson.data.results)
  })

  it('customColorMethod < 33', () => {
    let result = wrapper.vm.customColorMethod(10)
    expect(result).toEqual('#f56c6c')
  })

  it('customColorMethod < 66', () => {
    let result = wrapper.vm.customColorMethod(45)
    expect(result).toEqual('#909399')
  })

  it('customColorMethod < 100', () => {
    let result = wrapper.vm.customColorMethod(75)
    expect(result).toEqual('#e6a23c')
  })

  it('customColorMethod else', () => {
    let result = wrapper.vm.customColorMethod(110)
    expect(result).toEqual('#5cb87a')
  })

  it('openChecklistStage', async() => {
    await wrapper.vm.openChecklistStage(0)
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ChecklistStage", "params": {"study_data": study_data.study_data, stage_data: CheckListOutputsJson.data.results[0]}});
  })
})
