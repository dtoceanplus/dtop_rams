import { shallowMount } from '@vue/test-utils'

import ChecklistOutputStage from '@/views/stage_gate_studies/checklists/output/stage/index.vue'
import ElementUI from 'element-ui';

import Vue from 'vue'

Vue.use(ElementUI);

describe('index.vue', () => {
  let study_stage_data = { "study_data": { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }, "stage_data": { "activity_categories": [{ "complete": 1, "name": "Activity Category A", "outstanding": [{ "activity_category": 1, "activity_eval_areas": [2], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 2, "name": "Activity A2" }], "percent": 50, "total": 2 }, { "complete": 0, "name": "Activity Category B", "outstanding": [{ "activity_category": 2, "activity_eval_areas": [3], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 3, "name": "Activity B1" }, { "activity_category": 2, "activity_eval_areas": [4], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 4, "name": "Activity B2" }], "percent": 0, "total": 2 }, { "complete": 0, "name": "Activity Category C", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 0, "total": 1 }], "evaluation_areas": [{ "complete": 0, "name": "Acceptability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Installability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Availability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Maintainability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Reliability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Survivability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 1, "name": "Affordability", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 50, "total": 2 }, { "complete": 0, "name": "Manufacturability", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Energy Transformation", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Energy Delivery", "outstanding": [], "percent": 0, "total": 0 }, { "complete": 0, "name": "Energy Capture", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 0, "total": 1 }, { "complete": 0, "name": "Other", "outstanding": [{ "activity_category": 1, "activity_eval_areas": [2], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 2, "name": "Activity A2" }, { "activity_category": 2, "activity_eval_areas": [3], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 3, "name": "Activity B1" }, { "activity_category": 2, "activity_eval_areas": [4], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 4, "name": "Activity B2" }], "percent": 0, "total": 3 }], "name": "Stage 1", "percent_complete": 20 } }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ChecklistOutputStage, {
    mocks: {
      $route: {
        params: study_stage_data
      },
      $router
    }
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ChecklistOutput", "params": {"study_data": study_stage_data.study_data}});
  })

  it('getActiveEvalAreas', () => {
    wrapper.vm.getActiveEvalAreas()
    expect(wrapper.vm.activeEvalAreas).toEqual([{ "complete": 1, "name": "Affordability", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 50, "total": 2 }, { "complete": 0, "name": "Energy Capture", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 0, "total": 1 }, { "complete": 0, "name": "Other", "outstanding": [{ "activity_category": 1, "activity_eval_areas": [2], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 2, "name": "Activity A2" }, { "activity_category": 2, "activity_eval_areas": [3], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 3, "name": "Activity B1" }, { "activity_category": 2, "activity_eval_areas": [4], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 4, "name": "Activity B2" }], "percent": 0, "total": 3 }])
  })

  it('activateData true', () => {
    wrapper.vm.category = true
    wrapper.vm.activateData()
    expect(wrapper.vm.outstanding_activities).toEqual([{ "complete": 1, "name": "Affordability", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 50, "total": 2 }, { "complete": 0, "name": "Energy Capture", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 0, "total": 1 }, { "complete": 0, "name": "Other", "outstanding": [{ "activity_category": 1, "activity_eval_areas": [2], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 2, "name": "Activity A2" }, { "activity_category": 2, "activity_eval_areas": [3], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 3, "name": "Activity B1" }, { "activity_category": 2, "activity_eval_areas": [4], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 4, "name": "Activity B2" }], "percent": 0, "total": 3 }])
  })

  it('activateData false', () => {
    wrapper.vm.category = false
    wrapper.vm.activateData()
    expect(wrapper.vm.outstanding_activities).toEqual([{ "complete": 1, "name": "Activity Category A", "outstanding": [{ "activity_category": 1, "activity_eval_areas": [2], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 2, "name": "Activity A2" }], "percent": 50, "total": 2 }, { "complete": 0, "name": "Activity Category B", "outstanding": [{ "activity_category": 2, "activity_eval_areas": [3], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 3, "name": "Activity B1" }, { "activity_category": 2, "activity_eval_areas": [4], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 4, "name": "Activity B2" }], "percent": 0, "total": 2 }, { "complete": 0, "name": "Activity Category C", "outstanding": [{ "activity_category": 3, "activity_eval_areas": [5, 6], "description": "Cupidatat dolore irure in sint minim in non.", "further_details": "Further details describing the activity; e.g. required duration of testing.", "id": 5, "name": "Activity C1" }], "percent": 0, "total": 1 }])
  })

  it('customColorMethod < 33', () => {
    let result = wrapper.vm.customColorMethod(10)
    expect(result).toEqual('#f56c6c')
  })

  it('customColorMethod < 66', () => {
    let result = wrapper.vm.customColorMethod(45)
    expect(result).toEqual('#909399')
  })

  it('customColorMethod < 100', () => {
    let result = wrapper.vm.customColorMethod(75)
    expect(result).toEqual('#e6a23c')
  })

  it('customColorMethod else', () => {
    let result = wrapper.vm.customColorMethod(110)
    expect(result).toEqual('#5cb87a')
  })
})
