import { shallowMount } from '@vue/test-utils'

import BarChart from '@/components/BarChart/index.vue'
import ElementUI from 'element-ui';
// import StageGateStudiesJson from '../json/StageGateStudiesJson.json'

import Vue from 'vue'
import { isIterable } from 'core-js';

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let bar_chart_prop_data = {
      id: 0,
      title: "Stage Gate score",
      x: [
        2.25,
        2.4499999999999997
      ],
      y: [
        "Average",
        "Weighted average"
      ]
    }
    const wrapper = shallowMount(BarChart, {
      propsData: {
        bar_chart_data: bar_chart_prop_data
      }
    })

    it('test bar chart data', () => {
      expect(wrapper.vm.title).toBe("Stage Gate score");
      expect(wrapper.vm.x).toEqual([
        2.25,
        2.4499999999999997
      ]);
      expect(wrapper.vm.y).toEqual([
        "Average",
        "Weighted average"
      ]);
      expect(wrapper.vm.layout.margin.l).toBe(200);
      expect(wrapper.vm.layout.xaxis.tickmode).toBe('array');
      expect(wrapper.vm.layout.xaxis.tickvals).toEqual([0, 1, 2, 3, 4])
    })

})
