import { shallowMount } from '@vue/test-utils'

import StageGates from '@/components/StageGates/index.vue'
import ElementUI from 'element-ui';
import StageGateStudiesJson from '../json/StageGateStudiesJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    const wrapper = shallowMount(StageGates)
    let row = { id: 1, threshold: "threshold", threshold_bool: true }

    it('updateMetricThreshold', async() => {
        let questionMetricId = 1
        axios.resolveWith(StageGateStudiesJson, false)
        await wrapper.vm.updateMetricThreshold(StageGateStudiesJson, questionMetricId);
    })
    it('updateMetricThreshold error', async() => {
        let questionMetricId = 1
        axios.resolveWith(StageGateStudiesJson, true)
        await wrapper.vm.updateMetricThreshold(StageGateStudiesJson, questionMetricId);
    })

    it('initForm', () => {
        wrapper.vm.initForm();
        expect(wrapper.vm.updateMetricForm.id).toEqual('');
        expect(wrapper.vm.updateMetricForm.threshold).toEqual('');
        expect(wrapper.vm.updateMetricForm.threshold_bool).toEqual(true);
    })

    it('editMetricThreshold', () => {
        wrapper.vm.editMetricThreshold(row);
        expect(wrapper.vm.updateMetricForm.id).toEqual(row.id);
        expect(wrapper.vm.updateMetricForm.threshold).toEqual(row.threshold);
        expect(wrapper.vm.updateMetricForm.threshold_bool).toEqual(row.threshold_bool);
        expect(wrapper.vm.metricUpdateFormVisible).toEqual(true);
    })
    it('onCancel', () => {
        wrapper.vm.onCancel();
        expect(wrapper.vm.metricUpdateFormVisible).toEqual(false);
    })

    it('onSubmitUpdate', () => {
        wrapper.vm.onSubmitUpdate();
        expect(wrapper.vm.metricUpdateFormVisible).toEqual(false);
    })

    it('isDisabled', () => {
        let framework_id = 1
        let result = wrapper.vm.isDisabled(framework_id);
        expect(result).toEqual(true);
    })

    it('isDisabled false', () => {
        let framework_id = 2
        let result = wrapper.vm.isDisabled(framework_id);
        expect(result).toEqual(false);
    })
})
