###                                                                        ###
#  DTOceanPlus -- Module of Reliability, Availability, Maintainability and   #
#                             Reliability (RAMS)                             #
###                                                                        ###

import os    
import scipy.stats
import statistics 
import numpy as np
import math
import random
import pandas as pd
import xlrd
import json
import matplotlib.pyplot as fig
from operator import add, sub
from collections import Counter
from itertools import *
from itertools import chain


class RamsReliabilityCpx3():
    """ This class is used for assessing reliability for complexity 3.
    
    The reliability is assessed for the basic components.

    Attributes:
      Public method(s) include(s) 'sim_failure_event', 
        'calc_pof_system', 'unique', 'k_out_of_n', 'combination',
        'pre_process' and 'system_failure_event'.
    """

    def sim_failure_event(self, component_id, failure_rate, t_life,
                          pd = 'Exponential'):
        """ assesses the component_level reliability for basic components
       
        This function simulates the failure events of basic components
          in parallel.

        Args:
          component_id: a 1D list containing the Ids of 
          components (list - int), unit [-]         
          diff_ts: a list containing the records of time to failure (list) 
          failure_rate: a 1D list including failure rates of
            basic components (list - float), unit [1/hour]
          t_life: the lifetime (scaler - int), unit [year]  
          nr_seed: a random number uniformally sampled in (0, 1) 
            (scaler - float), unit [-]
          num_time: the no. of discrtized time (scaler - int), unit [-]
          num_comp: the no. of components (scaler - int), unit [-]
          t: the randomly simulated failure time (scaler - int), unit [hour] 
          pd: the probabilitstic distribution of time to failure
            (only 'exponential' considered) (string), unit [-]            
          temp1: a temporary list storing the failure times of a component
            (list), unit [-]
          t_round: the time of failure events rounded to hour (scaler - int)
            , unit [-]     

        Returns:  
          max_ttf: a list containing the maximum time to failure of all the 
            components (list - float), unit [hour]
          mttf: a list containing the theoretical mean time to failure of
            all the components (list - float), i.e. 1/failure rate, unit [hour]
          std_ttf: a list containing the standard deviation of time to failure 
            of all the components (list - float), unit [hour]
          ts_failure_event: a 2D list including the failure time  (list - int)
            , unit [hour]

        Raises:
          TypeError: An error occurs, if 'pd' is other than 'Exponential'.     
          ValueError: An error occurs, if 'ts_failure_event' is empty.       
        """        
        self.output_reliability_component = dict()
        ts_failure_event = []
        self.output_reliability_component["ts_failure_event"] = ts_failure_event
        self.output_reliability_component["component_id"] = component_id           
        try:
            if ((pd == 'Exponential')) and\
               (all((0 < i <1) for i in failure_rate)):
                num_time = int(t_life*365*24) 
                num_comp = len(failure_rate)  
                ts_failure_event = [[0] * 1] * num_comp
                for i in range(num_comp):
                    temp1 = []
                    t = 0
                    t0 = 0
                    t_round = 0
                    while ((t_round < num_time)):  
                        nr_seed = random.random()  
                        t = -(1/failure_rate[i])*math.log(1-nr_seed)
                        t_round = t_round + math.floor(t)
                        if (t_round > t0):
                            temp1.append(t_round)
                            t0 = t_round
                    num = len(temp1)
                    del temp1[num-1]
                    ts_failure_event[i] = temp1   
                self.output_reliability_component["ts_failure_event"] = \
                    ts_failure_event
            else:
                raise TypeError 
        except TypeError:
            print('"Exponential" distribution is the only available option\
                  for TTF at Stage 1! Failure rates must be in (0, 1)!')    

        max_ttf = []
        mttf = []
        std_ttf = []
        self.output_reliability_component["max_ttf"] = max_ttf
        self.output_reliability_component["mttf"] = mttf
        self.output_reliability_component["std_ttf"] = std_ttf  
        try:
            if (ts_failure_event == []):
                raise ValueError
            else:
                for i in range(len(ts_failure_event)):
                    temp2 = []
                    temp2.append(0)
                    mttf.append(1/failure_rate[i])
                    if (len(ts_failure_event[i]) == 0):
                        max_ttf.append(-1.0)
                        std_ttf.append(-1.0)
                    else:
                        max_ttf.append(max(ts_failure_event[i]))
                        std_ttf.append(statistics.stdev(ts_failure_event[i]))
            self.output_reliability_component["max_ttf"] = max_ttf
            self.output_reliability_component["mttf"] = mttf
            self.output_reliability_component["std_ttf"] = std_ttf    
        except ValueError:
            print('The ts_failure_event is empty!')                

        return self.output_reliability_component


    def calc_pof_system(self, level, node, table_hierarchy, t_life,
                        dump_figure = 'no'):
        """ assesses the system-level reliability 

        This function assesses the reliability for the units at all levels.
        !!Warning!! 
        The logic gates defined in the hierarchy are opposite to those in
        fault tree analysis. The conversion is explained as follows:
        a logic gate in the hierarchy the corresponding logic gate
        in fault tree
            'OR'                 'AND'
            'AND'                'OR'
 
        Args:
          table_hierarchy: the information of the hierarchy (array)
          basic_component: a 1D list containing all the basic components (list)
          dump_figure: there are two options (string) 
            - 'no': no figure of pof as a function of time shown (default)
            - 'yes': a figure of pof as a function of time shown
          max_column: the maximum number of children for a parenet node
            (scalar)
          max_row: the maximum number of intermediate nodes (scalar)
          node: a 1D array containing the types of nodes (array)
          num_node: the number of nodes in the hierarchy tree (scalar)
          num_field: the number of columns in a panda data frame (scalar)
          num_level: the number of levels of a hierarchy (scalar)
          table_hierarchy: a 2D array containing the hierarchy (array)
          t_lifetime: the lifetime of time series of basic components  (scalar)
          temp_hierarchy: a temporary panda dataframe containing
            the hierarchy tree (array)
          temp: a temporary varialbe (scalar)
          temp1: a 1D temporary array containing the states of child nodes of 
            a parent node (array)
          temp2: a 1D temporary array containing the states of the parent node
            during the lifetime (array)
             
        Returns:
          output_reliability_system: a dictionary containing "pof_system", which
            contains two fields, "pof_accumulate" and "pof_annual"
            {"pof_system_item: {
                "pof_accumulate": [0.1, 0.2, ...],
                "pof_annual": [1.02e-5, 1.265e-5, ...]
            }} 

        Raises:
          None
        """        
        self.output_reliability_system = []
        #self.output_reliability_system = dict()

        num_node = list(np.shape(table_hierarchy))[0]
        num_level = len(level)
        basic_component = []
        for i in range(num_node):
            if (table_hierarchy[i,2] == 'Level 0'):
                basic_component.append(table_hierarchy[i,0])
        max_column = 0
        max_row = 0
        for i in range(num_node):
            if (table_hierarchy[i,1] != node[num_level-1]):
                max_row = max_row+1
        for i in range(max_row):
            l = table_hierarchy[i,4].split(',')
            if (max_column < len(l)):
                max_column = len(l)
        
        time_line = np.linspace(0, 365*t_life*24, 365*t_life+1)
        # To calculate the failure probabilities of systems (the 1st intermediate level)
        num_level1 = 0
        pof_system = dict()
        for i in range(num_level-2, num_level-3, -1):
            for j in range(max_row):
                if (table_hierarchy[j,1] == node[i]):
                    num_level1 = num_level1+1
                    pof_system[table_hierarchy[j,0]] = ' '
                    child = table_hierarchy[j,4].split(',')
                    num_column = len(child)
                    if (table_hierarchy[j,5] == 'AND'):  # !! it should be an 'OR' gate in the fault tree
                        temp = []
                        for t in range(len(time_line)):
                            lambda_system = 0.0
                            for s in range(num_column):
                                child[s] = child[s].replace(' ','')
                                for k in range(num_node):
                                    if (table_hierarchy[k,0] == child[s]):
                                        lambda_system = lambda_system+float(table_hierarchy[k,6])
                                        break
                            temp.append(1-np.exp(-lambda_system*time_line[t]))
                        pof_system[table_hierarchy[j,0]] = temp
                    if (table_hierarchy[j,5] == 'OR'):  # !! it should be an 'AND' gate in the fault tree
                        R = []
                        for t in range(len(time_line)):
                            temp = 1.0
                            for s in range(num_column):
                                child[s] = child[s].replace(' ','')
                                for k in range(num_node):
                                    if (table_hierarchy[k,0] == child[s]):
                                        temp = temp*(1-np.exp(-float(table_hierarchy[k,6])*time_line[t]))
                                        break
                            R.append(temp)
                        pof_system[table_hierarchy[j,0]]  = R
                    if ((table_hierarchy[j,5] != 'OR') and (table_hierarchy[j,5] != 'AND')):
                        k = int(table_hierarchy[j,5][0])
                        n_total = int(table_hierarchy[j,5][2])
                        failure_rate = []
                        id = np.arange(0, num_column, step = 1)
                        for s in range(num_column):
                            child[s] = child[s].replace(' ','')
                            for k in range(num_node):
                                if (table_hierarchy[k,0] == child[s]):
                                    failure_rate.append(table_hierarchy[k,6])
                        reliability_kn = [0.0]*len(time_line)
                        for h in range(k,n_total+1):
                            temp = self.k_out_of_n(id, failure_rate, h, t_life)
                            reliability_kn = list(map(add, reliability_kn, temp))
                        pof_kn = list(map(sub, [1.0]*len(time_line), reliability_kn))
                        pof_system[table_hierarchy[j,0]]  = pof_kn
                         
        # To calculate the failure probabilities of systems (other levels)
        for i in range(num_level-3, -1, -1):
            for j in range(max_row-num_level1):
                if (table_hierarchy[j,1] == node[i]):
                    num_level1 = num_level1+1
                    pof_system[table_hierarchy[j,0]] = ' '
                    child = table_hierarchy[j,4].split(',')
                    num_column = len(child)
                    if (table_hierarchy[j,5] == 'AND'):  # !! it should be an 'OR' gate in the fault tree
                        temp2 = []
                        for t in range(len(time_line)):
                            temp1 = 1.0
                            for s in range(num_column):
                                child[s] = child[s].replace(' ','')
                                if (basic_component.count(child[s]) >= 1):
                                    for p in range(num_node):
                                        if (table_hierarchy[p,0] == child[s]):
                                            temp1 = temp1*np.exp(-float(table_hierarchy[p,6])*time_line[t])
                                            break
                                else:
                                    for key in pof_system.keys():
                                        if (key == child[s]):
                                            temp1 = temp1*(1-pof_system[key][t])
                                            break  
                            temp2.append(1-temp1)
                        pof_system[table_hierarchy[j,0]] = temp2
                    if (table_hierarchy[j,5] == 'OR'):  # !! it should be an 'AND' gate in the fault tree
                        R = []
                        for t in range(len(time_line)):
                            temp = 1.0
                            for s in range(num_column):
                                child[s] = child[s].replace(' ','')
                                if (basic_component.count(child[s]) >= 1):
                                    for p in range(num_node):
                                        if (table_hierarchy[p,0] == child[s]):
                                            temp = temp*(1-np.exp(-float(table_hierarchy[p,6])*time_line[t]))
                                            break                            
                                else:
                                    for key in pof_system.keys():
                                        if (key == child[s]):
                                            temp = temp*pof_system[key][t]
                                            break
                            R.append(temp)
                        pof_system[table_hierarchy[j,0]] = R
                    if ((table_hierarchy[j,5] != 'OR') and (table_hierarchy[j,5] != 'AND')):
                        k = int(table_hierarchy[j,5][0])
                        n_total = int(table_hierarchy[j,5][2])
                        failure_rate = []
                        id = np.arange(0, num_column, step = 1)     
                        print(child)                 
                        for s in range(num_column):
                            child[s] = child[s].replace(' ','')
                            for k in range(num_node):
                                if (table_hierarchy[k,0] == child[s]):
                                    failure_rate.append(table_hierarchy[k,6])
                        reliability_kn = [0.0]*len(time_line)
                        for h in range(k,n_total+1):
                            temp = self.k_out_of_n(id, failure_rate, h, t_life)
                            reliability_kn = list(map(add, reliability_kn, temp))
                        pof_kn = list(map(sub, [1.0]*len(time_line), reliability_kn))
                        pof_system[table_hierarchy[j,0]]  = pof_kn

        list_units = []
        for key in pof_system:
            list_units.append(key)
        for i in range(len(list_units)):
            pof_system_item = dict()
            pof_system_item = {
                'name': '',
                'pof_accumulate': [],
                'pof_annual': []
                }
            pof_system_item['name'] = list_units[i]
            pof_system_item['pof_accumulate'] = pof_system[list_units[i]]
            t = np.linspace(0, 365*t_life*24, t_life+1)
            pof_accumulate_year = []
            for k in range(len(t)):
                pof_accumulate_year.append(pof_system_item['pof_accumulate'][365*k])
            pof_system_item['pof_annual'] = np.diff(pof_accumulate_year)
            self.output_reliability_system.append(pof_system_item)

        reliability = self.output_reliability_system
        max_annual_pof = max(reliability[len(reliability)-1]['pof_annual'])

        return max_annual_pof


    def unique(self, array): 
        """
        This function gets unique values. 
        """      
        # intilize a null list 
        self.unique_array = [] 
          
        # traverse for all elements 
        for x in array: 
            # check if exists in unique_list or not 
            if x not in self.unique_array: 
                self.unique_array.append(x) 

        return self.unique_array
    
    def k_out_of_n(self, id, failure_rate, k, t_life):
        """ 
        This function calculates the probability of failure for a k/N logic
          gate.

        Args:
          id - a list of node ids (list)
          failure_rate - the failure rates of the nodes in id (list)
          k - the k in the 'k/N' logic gate (scalar)
          t_life - the design lifetime (scalar)
          basic_component: a 1D list (list)
          list_combination: a 2D list storing the combinations of nodes, 
            with each row representing one conbimation (list)
          list_complement: a 2D list storing the nodes that are not in 
            list_combination (list)
          summary_conbimation.txt: a text file storing the intermediate 
            combinations of nodes (text)
          time_line: a 1D list containing the time which (list)

        Returns:  
          reliability: a 1D list containing the time-dependent (list)
          reliability of the node (i.e. parent) at the top level

        Raises:
          None       
        """  
        # to remove the intermediate text file, if it exists
        f_exist = os.path.isfile('./summary_conbimation.txt')
        if f_exist:
            os.remove('summary_conbimation.txt')
            print('the existing file has been removed!')
        
        # to load each line to a list
        num = len(id)  
        data = [0.0]*k
        self.combination(id, data, 0, num-1, 0, k)
        delete_list = ['[', ']', '\n', ' ']
        list_combination = []
        with open('summary_conbimation.txt','r') as f:
            for line in f: 
                for word in delete_list:
                    if (word in line): 
                        line = line.replace(word, '')
                list_combination.append(line)        
        f.close()
        n = len(list_combination)  
        for i in range(n):
            list_combination[i] = list_combination[i].split(',')
        
        # to find the entries that are not in the list_combination
        list_complement = []
        for i in range(n): 
            temp = []
            for k in range(num):  
                if (str(id[k]) in list_combination[i]): 
                    continue
                else:
                    temp.append(str(id[k]))
                    break
            list_complement.append(temp)
        # 
        reliability = []
        time_line = np.linspace(0, 365*t_life*24, 365*t_life+1)
        for time in range(len(time_line)):
            sum = 0.0
            for i in range(n): 
                temp = 1.0
                for j in list(map(int,list_combination[i])):
                    temp = temp * math.exp(-failure_rate[j]*time_line[time])
                for k in list(map(int,list_complement[i])): 
                    temp = temp * (1-math.exp(-failure_rate[id[k]]*time_line[time]))
                sum = sum + temp
            reliability.append(sum)
    
        return reliability
    
 
    def combination(self, array, data, start, end, index, k):
        # array - the list of items to be combined
        # data - a temporary list
        # start - the first label of the list
        # ends - the last label of the list
        # index - the starting label of the list
        # k - the k of the k-out-of-N
        if (index == k):
            f = open('summary_conbimation.txt','a+')
            f.write(str(data))
            f.write("\n")
            f.close()
            return
            
        i = start  
        while(i <= end and end - i + 1 >= k - index): 
            data[index] = array[i] 
            self.combination(array, data, i + 1,  end, index + 1, k) 
            i += 1    


    def pre_process(self, temp_hierarchy):
        """ 
        pre-processes the temp_hierarchy containing the information of 
          the hierarchy of a specific subsystem.
            
        Args:
          temp_hierarchy- the information of the hierarchy (array) 
            column [0] - Design_id
            column [1] - Type
            column [2] - Category
            column [3] - Parent 
            column [4] - Child
            column [5] - Gate_Type (refer to the warnings in calc_pof_system)
            column [6] - Failure_Rate_Minor [1/hour]
            column [7] - Failure_Rate_Replacement [1/hour]
          basic_component: a list containing all the basic component (list)
          level: a list containing the categories of the hierarchy (list)
            , e.f level = ['Level 2', 'Level 1', 'Level 0']
          max_column: the number of maximum children in the column of 'Child'
            (scalar)
          max_row: the number of maximum intermediate units (scalar)
          node: a list containing all the units in the hierarchy (list)
          num_inter_level: the number of intermediate nodes on the 2nd toppest 
            hierarchical level (scalar)
          num_level: the number of levels (scalar)
          num_node: the number of nodes (records) in the temp_hierarchy
            (scalar)
          temp_level: a temporary list containing the categories of the 
            hierarchy (list)
          type_inter_node: the type of the nodes on the 2nd toppest hierarchy
            (string)
             
        Returns:
          table_hierarchy: the sorted hierarcy array (array)

        Raises:
          None
        """
        
        temp_hierarchy[:,2] = temp_hierarchy[:,1]
        temp_hierarchy[:,3] = temp_hierarchy[:,5]
        temp_hierarchy = np.delete(temp_hierarchy, [0, 1, 4], 1)
        table_hierarchy = []
        num_node = list(np.shape(temp_hierarchy))[0]
        level = self.unique(temp_hierarchy[:,2])
        for i in range(num_node):
            temp = ''
            if (type(temp_hierarchy[:,4][i]) == list):
                for k in range(len(temp_hierarchy[:,4][i])):
                    temp = temp + temp_hierarchy[:,4][i][k] + ', '
                temp_hierarchy[:,4][i] = temp[0:len(temp)-2]
            else: 
                temp_hierarchy[:,4][i] = temp_hierarchy[:,4][i]
            
        num_level = len(level)
        temp_level = []
        for i in range(len(level)):
            temp_level.append(level[i].split())
        for i in range(0, len(level)-1):
            for j in range(0, len(level)-1-i):
                if (int(temp_level[j][1]) < int(temp_level[j+1][1])):
                    temp_level[j+1], temp_level[j] = temp_level[j], temp_level[j+1]
        for i in range(len(level)):
            temp_level[i] = temp_level[i][0] + ' ' + temp_level[i][1]
        level = temp_level  
        
        p = 0
        for i in range(num_level):
            for k in range(num_node):
                if (temp_hierarchy[k,2] == level[i]):
                    table_hierarchy.append(temp_hierarchy[k,:])
                    p = p + 1
        table_hierarchy = np.asarray(table_hierarchy)
        node = self.unique(table_hierarchy[:,1])
        #for i in range(num_node):
        #    table_hierarchy[i,4] = table_hierarchy[i,4].replace('[','')
        #    table_hierarchy[i,4] = table_hierarchy[i,4].replace(']','')
        type_inter_node = '' # the type of the nodes on the 2nd toppest hierarchical level 
        for i in range(num_node): 
            if (table_hierarchy[i,1] == node[1]):
                type_inter_node = table_hierarchy[i,2]
                break    
        num_inter_level = 0 # the number of intermediate nodes on the 2nd toppest hierarchical level
        node_inter = []
        for i in range(num_node):
            if (table_hierarchy[i,2] == type_inter_node):
                num_inter_level = num_inter_level + 1
                node_inter.append(table_hierarchy[i,0])
        for i in range(num_node): # to ensure the data format of basic components (Name_of_Node) is string
            if (type(table_hierarchy[i,0]) == int):
                table_hierarchy[i,0] = str(table_hierarchy[i,0])
        number_na = Counter(table_hierarchy[:,6])
        if (len(table_hierarchy[:,6]) == number_na['NA']):
            table_hierarchy[:,6] = table_hierarchy[:,7]
        for i in range(len(table_hierarchy[:,6])):
	        if (table_hierarchy[i,6] != 'NA'):
	    	    table_hierarchy[i,6] = float(table_hierarchy[i,6])        

        unit_ids = []
        unit_type = []
        unit_ids = list(table_hierarchy[:,0])
        unit_type = list(table_hierarchy[:,1])
        pointer1 = 0
        for i in range(len(unit_type)):
            if (unit_type[i] == 'Level 0'):
                pointer1 = i # the number of the first basic component
                break 
        failure_rate = list(table_hierarchy[:,6])[pointer1:len(unit_ids)]       
        for i in range(len(failure_rate)):
            failure_rate[i] = failure_rate[i]/(365*24)
        component_ids = unit_ids[pointer1:len(unit_ids)]

        for i in range(len(table_hierarchy[:,6])):
            if (table_hierarchy[i,6] != "NA"):
                table_hierarchy[i,6] = table_hierarchy[i,6]/(365*24)

        # identify the shared components at all levels
        start_basic = table_hierarchy[:,1].tolist().index('Level 0')
        start_level1 = table_hierarchy[:,1].tolist().index('Level 1')
        shared_component = dict()
        child_level1 = []
    
        shared_component['trunk'] = []
        for i in range(start_level1, start_basic): 
            temp = []
            child = table_hierarchy[i,4].split(',')
            for j in range(len(child)):
                temp.append(child[j].replace(' ',''))
            child_level1.append(temp)
        temp = set.intersection(*[set(list) for list in child_level1])
        for k in temp:
            shared_component['trunk'].append(k)
    
        filtered_level1 = []
        for i in range(len(child_level1)): 
            main = set(child_level1[i])
            subset = set(shared_component['trunk'])
            remaining = main - subset
            temp = []
            for k in remaining:
                temp.append(k)
            filtered_level1.append(temp)
        chain_level1 = list(chain.from_iterable(filtered_level1))
        counts = Counter(chain_level1) 
        label_share_component = []
        repeat_no_share_component = []
        for key in counts:
            if counts[key] > 1:
                label_share_component.append(key)
                repeat_no_share_component.append(counts[key])
    
        if (len(child_level1) >= 2):
            group_repeat_no = np.linspace(2, len(child_level1)-1, len(child_level1)-2)
            for i in range(len(group_repeat_no)):
                group_repeat_no[i] = int(group_repeat_no[i])
        else:
            group_repeat_no = []

        if len(group_repeat_no) == 0 or len(label_share_component) == 0:
            shared_component["other branches"] = [[]]
        else:
            group_share_component = []
            for s in range(len(group_repeat_no)):
                temp = []
                for i in range(len(label_share_component)):
                    if (repeat_no_share_component[i] == group_repeat_no[s]):
                        temp.append(label_share_component[i])
                if (temp != []):
                    group_share_component.append(temp)
                    shared_component["other branches"] = group_share_component

        return component_ids, failure_rate, level, node, shared_component, table_hierarchy


    def system_failure_event(self, shared_component, t_life, t_wait, table_hierarchy):
        """ 
        predicts the system/subsystem states through Monte Carlo  Simulation
          , based upon the following assumptions:
          - the failures of basic components are statistically 
            independent;
          - the failure sequence is predicted according to the hierarchy;
          - the corrective maintenance is the adopted maintnenance 
            streategy.
        Nomenclature:
          - TTF: time to failure
          - MTTF: mean time to failure

        Args:
          t_life: the design lifetime (scalar)
          t_wait: the waiting time (scalar), e.g. inlcuding
            - the time for preparing spare parts
            - the time for chartering a vessel
            - the time for waiting an appropriate weather window
          table_hierarchy: the hierarchy for each subsystem

          children: a 1D list containing the data in the 4-th column
          of the table_hierarchy (list)           
          component_id: a 1D list containing the basic component ids (list)
          failure_rate: a 1D list containing the failure rates of the basic 
            components (list)
          failure_time: a 1D list containing the simulated TTFs of the basic 
            components (list)
          failure_time_reorder: a 1D list containing the failure rates of 
            the basic components in an ascending order (chronologic time) 
            (list)
          failure_sequence: a 1D list containing the labels of failed basic 
            components (list)
          gate: a 1D list containing the data in the 5-th column of the 
            table_hierarchy (list)
          n_node: the number of units in the hierarchy (scalar)
          pointer: indicates the current intermediate unit (string)
          pointer_index: the index of the pointer (scalar)
          rec_fail: a 2D list, each sub-list contains the records of failed 
            basic components affiliated to the corresponding intermetiate
            unit (list)
          t_sim_start: the start-up time of a simulation (scalar)
          time_system_failure: a 1D list containing the TTFs of the subsystem 
            failures (list)
          zero_index: the index of the first basic component, for which 
            the simulation of TTF is performed (scalar).
             
        Returns:
          mttf: the mean time to failure of the system in a lifetime simulation (scalar)
        """
        mttf = 0.0
        component_id = []
        children = []
        failure_rate = []
        failure_sequence = []   
        failure_time = []
        failure_time_reorder = []
        gate = []        
        n_node = np.size(table_hierarchy, 0)
        rec_fail = []
        seq_fail_component = []
        seq_fail_time = []
        time_system_failure = []
        for i in range(n_node): 
            if (table_hierarchy[i,2] == 'Level 0'):
                component_id.append(table_hierarchy[i,0])
                failure_rate.append(table_hierarchy[i,6])
            if (table_hierarchy[i,2] != 'Level 0'):
                children.append(table_hierarchy[i,4])
                gate.append(table_hierarchy[i,5])
        for j in range(0, n_node-len(component_id)):
            temp = ['']
            rec_fail.append(temp)   
        state = [1] * (n_node-len(component_id))
        n_fail = [0] * (n_node-len(component_id))
        failure_sequence = []
        for i in range(len(component_id)):
            failure_sequence.append(component_id[i])
            failure_time.append(0)
            failure_time_reorder.append(0)
    
        t_sim_start = 0
        while (t_sim_start < t_life*365*24):
            if ((0 in failure_time) == False):
                zero_index = 0
            else:
                zero_index = failure_time.index(0)
            if (zero_index == 0):
                for i in range(zero_index,len(component_id)):
                    t_round = pow(10,15)
                    if (i>0):
                        while (t_round > t_life*365*24) or (t_round == failure_time[i-1]): 
                            nr_seed = random.random() 
                            t = -(1/failure_rate[i])*math.log(1-nr_seed) 
                            t_round = math.floor(t) + t_sim_start
                    else:
                        while (t_round > t_life*365*24): 
                            nr_seed = random.random() 
                            t = -(1/failure_rate[i])*math.log(1-nr_seed)
                            t_round = math.floor(t) + t_sim_start
                    failure_time[i] = t_round                    
            else:
                for i in range(zero_index,len(component_id)):
                    t_round = pow(10,15)
                    while (t_round > t_life*365*24): 
                        nr_seed = random.random() 
                        for s in range(len(component_id)):
                            if (failure_sequence[i] == component_id[s]):
                                failure_rate1 = failure_rate[s]
                        t = -(1/failure_rate1)*math.log(1-nr_seed)
                        t_round = math.floor(t) + t_sim_start
                    failure_time[i] = t_round
            failure_time_reorder = failure_time        
            for i in range(len(failure_time_reorder)-1):
                for j in range(0, len(failure_time_reorder)-1-i):
                    if (failure_time_reorder[j] > failure_time_reorder[j+1]):
                        failure_sequence[j+1], failure_sequence[j] = \
                            failure_sequence[j], failure_sequence[j+1]
                        failure_time_reorder[j+1],  failure_time_reorder[j] =\
                            failure_time_reorder[j],  failure_time_reorder[j+1]
            
            pointer_index = 0
            pointer = failure_sequence[pointer_index]
    
            if failure_sequence[pointer_index] in shared_component['trunk']:
                state[0] = 0
                t_sim_start = failure_time_reorder[pointer_index-1] + t_wait  
                time_system_failure.append(failure_time_reorder[pointer_index-1])
                temp_fail_time = []                
                temp_fail_time.append(failure_time_reorder[0])
                failure_sequence_previous = []
                for i in range(len(failure_sequence)):
                    failure_sequence_previous.append(failure_sequence[i])
                del(failure_sequence[0:(pointer_index)])
                del(failure_time[0:(pointer_index)])
                
                temp_fail_seq = []
                failure_sequence.append(failure_sequence_previous[0])
                temp_fail_seq.append(failure_sequence_previous[0])
                failure_time_reorder.append(0)
                
                seq_fail_component.append(temp_fail_seq)
                seq_fail_time.append(temp_fail_time)               
                state = [1] * (n_node-len(component_id))
                n_fail = [0] * (n_node-len(component_id))
                rec_fail = []
                for j in range(0, n_node-len(component_id)):
                    temp = ['']
                    rec_fail.append(temp)  
            else: 
                while (pointer_index <= len(failure_time)-1):
                    while (pointer != table_hierarchy[0,0]):
                        if failure_sequence[pointer_index] in shared_component['trunk']:
                            state[0] = 0
                            pointer = table_hierarchy[0,0]
                        else:
                            if any(pointer in sublist for sublist in shared_component['other branches']):
                                for kk in range(0, n_node-len(component_id)):
                                    child = children[kk].split(',')
                                    num_column = len(child)
                                    for s in range(num_column):
                                        child[s] = child[s].replace(' ','')
                                        if (pointer in child):
                                            state[kk] = 0
    
                                for kk in range(n_node-len(component_id)-1, -1, -1): 
                                    child = children[kk].split(',')
                                    num_column = len(child)
                                    if (state[kk] == 0):
                                        for ss in range(kk):
                                            # it is an 'OR' gate in the fault tree.
                                            if (gate[kk] == 'AND'):
                                                if (table_hierarchy[kk,0] in children[ss]):
                                                    state[ss] = 0
                                                else:
                                                    for hh in range(0, n_node-len(component_id)):
                                                        if (pointer == table_hierarchy[hh,0]):
                                                            if (state[hh] == 0):
                                                                state[kk] = 0 
                                            # it is an 'AND' gate in the fault tree.
                                            if (gate[kk] == 'OR'):
                                                sum = 0
                                                for hh in range(num_column):
                                                    for q in range(0, n_node-len(component_id)):
                                                        if (child[hh] == table_hierarchy[q,0]):
                                                            if (state[q] == 0):
                                                                sum = sum +1 
                                                if (sum == num_column):
                                                    state[kk] = 0  
                                            # it is an 'k/N' gate in the fault tree.    
                                            if (gate[kk] != 'OR') and (gate[kk] != 'AND'):  
                                                k = int(table_hierarchy[kk,5][0]) 
                                                if ((len(self.unique(rec_fail[kk]))) == k):
                                                    state[kk] = 0
                                pointer = table_hierarchy[kk,0]
                            else: 
                                for j in range(0, n_node-len(component_id)):
                                    child = children[j].split(',')
                                    num_column = len(child)
                                    for s in range(num_column):
                                        child[s] = child[s].replace(' ','')
                                    for k in range(num_column):
                                        if (child[k] == pointer):
                                            rec_fail[j].append(pointer)
                                            # it is an 'OR' gate in the fault tree.
                                            if (gate[j] == 'AND'):  
                                                if (pointer in component_id):
                                                    state[j] = 0
                                                else:
                                                    for h in range(n_node-len(component_id)):
                                                        if (pointer == table_hierarchy[h,0]):
                                                            if (state[h] == 0):
                                                                state[j] = 0
                                                n_fail[j] = n_fail[j] + 1
                                            # it is an 'AND' gate in the fault tree.
                                            if (gate[j] == 'OR'):   
                                                sum = 0
                                                for h in range(num_column):
                                                    for s in range(n_node-len(component_id)):
                                                        if (child[h] == table_hierarchy[s,0]):
                                                            if (state[s] == 0):
                                                                sum = sum +1 
                                                if (sum == num_column):
                                                    state[j] = 0
                                                    n_fail[j] = num_column
                                            # it is an 'k/N' gate in the fault tree.    
                                            if (gate[j] != 'OR') and (gate[j] != 'AND'):  
                                                k = int(table_hierarchy[j,5][0]) 
                                                if ((len(self.unique(rec_fail[j]))-1) == k):
                                                    state[j] = 0
	    	    		        				                                        
                                            pointer = table_hierarchy[j,0]
                                        else:
                                            continue
                
                    pointer_index = pointer_index + 1
                    if (pointer_index < len(failure_time)):
                        pointer = failure_sequence[pointer_index]
                    if (state[0] == 0):  
                        t_sim_start = failure_time_reorder[pointer_index-1] + t_wait  
                        time_system_failure.append(failure_time_reorder[pointer_index-1])
        
                        temp_fail_time = []                
                        for t in range(0,pointer_index):
                            temp_fail_time.append(failure_time_reorder[t])
        
                        failure_sequence_previous = []
                        for i in range(len(failure_sequence)):
                            failure_sequence_previous.append(failure_sequence[i])
                        del(failure_sequence[0:(pointer_index)])
                        del(failure_time[0:(pointer_index)])
                        
                        temp_fail_seq = []
                        for t in range(0,pointer_index):
                            failure_sequence.append(failure_sequence_previous[t])
                            temp_fail_seq.append(failure_sequence_previous[t])
                            failure_time_reorder.append(0)
                        
                        seq_fail_component.append(temp_fail_seq)
                        seq_fail_time.append(temp_fail_time)              
        
                        state = [1] * (n_node-len(component_id))
                        n_fail = [0] * (n_node-len(component_id))
                        rec_fail = []
                        for j in range(0, n_node-len(component_id)):
                            temp = ['']
                            rec_fail.append(temp)  
                        break
        time_system_failure.insert(0,0)
        mttf = np.mean((np.diff(time_system_failure)))

        return mttf


    def pof_mttf(self, downtime, level_ed, level_et, level_sk, node_ed,
        node_et, node_sk, n_sim, shared_component_ed, shared_component_et, 
        shared_component_sk, t_life, table_hierarchy_ed, table_hierarchy_et,
        table_hierarchy_sk):
        """ 
        calculates the mttf of the system.
        Nomenclature:
          - MTTF: mean time to failure

        Args:
          downtime: the down time for estimating the waiting time (list)
          level_ed: a list containing the levels of nodes in the ED hierarchy
            (list)
          level_et: a list containing the levels of nodes in the ET hierarchy
            (list)
          level_sk: a list containing the levels of nodes in the SK hierarchy 
            (list)
          node_ed: a list containing the unique nodes in the ED hierarchy
            (list)        
          node_et: a list containing the unique nodes in the ET hierarchy
            (list)
          node_sk: a list containing the unique nodes in the SK hierarchy 
            (list)
          n_sim: the number of simulations (scalar)
          shared_component_ed: the shared componenets in different branches 
            in the fault tree of the ED subsystem (list)
          shared_component_et: the shared componenets in different branches 
            in the fault tree of the ET subsystem (list)
          shared_component_sk: the shared componenets in different branches 
            in the fault tree of the SK subsystem (list)
          t_life: the design lifetime (scalar)
          t_wait: the waiting time (scalar), e.g. inlcuding
            - the time for preparing spare parts
            - the time for chartering a vessel
            - the time for waiting an appropriate weather window
          table_hierarchy_ed: the converted hierarchy of the ED subsystem
            (2D list)
          table_hierarchy_et: the converted hierarchy of the ET subsystem
            (2D list)
          table_hierarchy_sk: the converted hierarchy of the SK subsystem
            (2D list)
             
        Returns:
          max_annual_pof_ed: the maximum annual probability of failure of
            the ED subsystem, (scalar - float)
          max_annual_pof_et: the maximum annual probability of failure of
            the ET subsystem, (scalar - float)
          max_annual_pof_sk: the maximum annual probability of failure of
            the SK subsystem, (scalar - float)
          max_annual_pof_array: the maximum annual probability of failure of
            the array, (scalar - float)
          max_ttf_ed: the maximum time to failure of the ED subsystem 
            (scalar - float)
          max_ttf_et: the maximum time to failure of the ET subsystem 
            (scalar - float)
          max_ttf_sk: the maximum time to failure of the SK subsystem 
            (scalar - float)
          max_ttf_array: the maximum time to failure of the array 
            (scalar - float)
          mttf_ed: the mean time to failure of the ED subsystem
            (scalar - float)
          mttf_et: the mean time to failure of the ET subsystem
            (scalar - float)
          mttf_sk: the mean time to failure of the SK subsystem
            (scalar - float)
          mttf_array: the mean time to failure of the array
            (scalar - float)
          std_ttf_ed: the standard deviation of time to failure of
            the ED subsystem (scalar - float)
          std_ttf_et: the standard deviation of time to failure of
            the ET subsystem (scalar - float)
          std_ttf_sk: the standard deviation of time to failure of
            the SK subsystem (scalar - float)
          std_ttf_array: the standard deviation of time to failure of
            the array (scalar - float)
        """
        n_downtime = len(downtime)
        wait_time = []
        for i in range(n_downtime):
            if not (isinstance(downtime[i],str)):
                wait_time.append(downtime[i])
        t_wait = statistics.mean(wait_time)

        reliability_summary = dict()
        mttf_lifetime_ed = 0.0
        mttf_lifetime_et = 0.0
        mttf_lifetime_sk = 0.0
        rec_ttf_ed = []
        rec_ttf_et = []
        rec_ttf_sk = []
        max_annual_pof_ed = self.calc_pof_system(level_ed, node_ed, table_hierarchy_ed, t_life)
        max_annual_pof_et = self.calc_pof_system(level_et, node_et, table_hierarchy_et, t_life)
        max_annual_pof_sk = self.calc_pof_system(level_sk, node_sk, table_hierarchy_sk, t_life)
        for i in range(n_sim): 
            ttf = self.system_failure_event(shared_component_ed, t_life, t_wait, table_hierarchy_ed)
            rec_ttf_ed.append(ttf)
        mttf_lifetime_ed = statistics.mean(rec_ttf_ed)   
        std_ttf_ed = statistics.stdev(rec_ttf_ed)      
        max_ttf_ed = max(rec_ttf_ed)         

        for i in range(n_sim): 
            ttf = self.system_failure_event(shared_component_et, t_life, t_wait, table_hierarchy_et)
            rec_ttf_et.append(ttf)
        mttf_lifetime_et = statistics.mean(rec_ttf_et)   
        std_ttf_et = statistics.stdev(rec_ttf_et)      
        max_ttf_et = max(rec_ttf_et) 

        for i in range(n_sim): 
            ttf = self.system_failure_event(shared_component_sk, t_life, t_wait, table_hierarchy_sk)
            rec_ttf_sk.append(ttf)
        mttf_lifetime_sk = statistics.mean(rec_ttf_sk)   
        std_ttf_sk = statistics.stdev(rec_ttf_sk)      
        max_ttf_sk = max(rec_ttf_sk)

        reliability_summary["max_annual_pof_ed"] = max_annual_pof_ed
        reliability_summary["max_annual_pof_et"] = max_annual_pof_et
        reliability_summary["max_annual_pof_sk"] = max_annual_pof_sk
        reliability_summary["max_annual_pof_array"] = 1-(1-max_annual_pof_ed)*\
            (1-max_annual_pof_et)*(1-max_annual_pof_sk)
        reliability_summary["mttf_ed"] = mttf_lifetime_ed
        reliability_summary["mttf_et"] = mttf_lifetime_et
        reliability_summary["mttf_sk"] = mttf_lifetime_sk
        reliability_summary["max_ttf_ed"] = max_ttf_ed
        reliability_summary["max_ttf_et"] = max_ttf_et
        reliability_summary["max_ttf_sk"] = max_ttf_sk
        reliability_summary["std_ttf_ed"] = std_ttf_ed
        reliability_summary["std_ttf_et"] = std_ttf_et
        reliability_summary["std_ttf_sk"] = std_ttf_sk

        ttf_array = []
        for i in range(len(rec_ttf_ed)):
            ttf_array.append(min(rec_ttf_ed[i], rec_ttf_et[i], rec_ttf_sk[i]))

        reliability_summary["mttf_array"] = statistics.mean(ttf_array)
        reliability_summary["max_ttf_array"] = max(ttf_array)
        reliability_summary["std_ttf_array"] = statistics.stdev(ttf_array)
        
        return reliability_summary