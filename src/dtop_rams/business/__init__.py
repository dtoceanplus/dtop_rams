from dtop_rams.business.cpx1 import (RamsReliabilityCpx1,
                                     RamsAvailabilityCpx1,
                                     RamsMaintainabilityCpx1,
                                     RamsSurvivabilityCpx1)

from dtop_rams.business.cpx2 import (RamsReliabilityCpx2,
                                     RamsAvailabilityCpx2,
                                     RamsMaintainabilityCpx2,
                                     RamsSurvivabilityCpx2)

from dtop_rams.business.cpx3 import (RamsReliabilityCpx3,
                                     RamsAvailabilityCpx3,
                                     RamsMaintainabilityCpx3,
                                     RamsSurvivabilityCpx3)


class ArrayRams():
    """
    This init file defines the framework of the analysis procedure for
      Complexity 1, 2 & 3. 
    """

    def calc_reliability(cpx):
        """ calculates the reliability metric.
        """
        try: 
            if cpx == "1":
                return RamsReliabilityCpx1()
            elif cpx == "2":
                return RamsReliabilityCpx2()
            elif cpx == "3":
                return RamsReliabilityCpx3()
            else:
                raise TypeError
        except TypeError:
            print('There are only Complexity level 1, 2 and 3!')

    def calc_availability(cpx):
        """ calculates the availability metric.
        """
        try: 
            if cpx == "1":
                return RamsAvailabilityCpx1()
            elif cpx == "2":
                return RamsAvailabilityCpx2()
            elif cpx == "3":
                return RamsAvailabilityCpx3()
            else:
                raise TypeError
        except TypeError:
            print('There are only Complexity level 1, 2 and 3!')

    def calc_maintainability(cpx):
        """ calculates the maintainability metric.
        """
        try: 
            if cpx == "1":
                return RamsMaintainabilityCpx1()
            elif cpx == "2":
                return RamsMaintainabilityCpx2()
            elif cpx == "3":
                return RamsMaintainabilityCpx3()
            else:
                raise TypeError
        except TypeError:
            print('There are only Complexity level 1, 2 and 3!')

    def calc_survivability(cpx):
        """ calculates the survivability metric.
        """
        try: 
            if cpx == "1":
                return RamsSurvivabilityCpx1()
            elif cpx == "2":
                return RamsSurvivabilityCpx2()
            elif cpx == "3":
                return RamsSurvivabilityCpx3()
            else:
                raise TypeError
        except TypeError:
            print('There are only Complexity level 1, 2 and 3!')
