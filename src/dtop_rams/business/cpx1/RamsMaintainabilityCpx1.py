###                                                                        ###
#  DTOceanPlus -- Module of Reliability, Availability, Maintainability and   #
#                           Maintainability (RAMS)                           #
###                                                                        ###

import scipy.stats
import math


class RamsMaintainabilityCpx1():
    """ This class is used for assessing maintainability for complexity 1.

    The maintainablity is assessed for the critical components.
    
    Attributes:
      Public method(s) include(s) 'calc_maintainability' and 'unique'.      
    """    

    def calc_maintainability(self, component_id, repair_time, std,
                             t_available, pd = 'Gaussian'):
        """ assesses the maintainability

        Args: 
          component_id: a 1D list containing the critical component Ids
            (list - int), unit [-]           
          pd: a string indicating the probabilistic distribution of 
            maintainability (list - string)
            'Gaussian' - Gaussian distribution
            'LogNormal' - LogNormal distribution
            'Exponential' - Exponential distribution  
          repair_time: a 1D list containing the mean repair time of components
            (list - int), unit [hour]
          std : a 1D list containing the standard deviation of repair time
            (list - int)   
          t_available: the available time for which maintainability is estimated
            (scaler - int), unit [hour]   
          maintainability_comp: a 1D list containing the maintainability
          (probability) at t_specifc for the critical components (list)  

        Returns:
          output_maintianability: a dictionary containing "critical_component" and
            "probability_maintenance" (dictionary)
            {"critical_component": ['Component 1', 'Component 2', '...'],
             "probability_maintenance": [0.923, 0.986, ...]} 

        Raises:
          TypeError: An error occurs, if 'component_id' is empty.      
        """
        maintainability_comp = []
        maintainability = []
        unique_component = []
        mttr = []
        self.output_maintainability = dict()
        self.output_maintainability["probability_maintenance"] = maintainability

        try:
            if (len(component_id) > 0):
                temp_array = self.unique(component_id)
                for k in range(len(temp_array)):
                    sum = 0.0
                    p = 0
                    unique_component.append(temp_array[k])
                    for j in range(len(component_id)):
                        if (unique_component[k] == component_id[j]):
							# test if the repair time is None/null
                            if (type(repair_time[j]) == int) or (type(repair_time[j]) == float):
                                sum = sum + repair_time[j]
                                p = p + 1
                    if (sum == 0.0):
                        mttr.append('NA')
                    else: 
                        mttr.append(sum/p)
                for i in range(len(unique_component)):
                    if (mttr[i] != 'NA'):
                        if (pd == 'Gaussian'):
                            maintainability_comp.append(scipy.stats.norm.cdf\
                                    (t_available, repair_time[i], std[i]))
                        if (pd == 'LogNormal'):
                            maintainability_comp.append(scipy.stats.lognorm.cdf\
                                    (t_available, std[i], 0, math.exp(repair_time[i])))
                        if (pd == 'Exponential'):
                            maintainability_comp.append(scipy.stats.expon.cdf\
                                    (t_available, 0, 1/repair_time[i]))
                    else:
                        maintainability_comp.append('NA')
                temp_actual = []
                number_na = 0
                for i in range(len(unique_component)):
                    if (maintainability_comp[i] != 'NA'):
                        temp_actual.append(maintainability_comp[i])
                    else:
                        number_na = number_na + 1
                if (number_na == len(unique_component)):
                    maintainability.append('NA')
                    self.output_maintainability["probability_maintenance"] = maintainability
                    self.output_maintainability["critical_component"] ='NA' 
                else:
                    maintainability.append(min(temp_actual))
                    index_component = temp_actual.index(maintainability, 0, len(temp_actual))  
                    self.output_maintainability["probability_maintenance"] = maintainability
                    self.output_maintainability["critical_component"] = unique_component[index_component]
            else:
                raise TypeError
        except TypeError:
            print('The component_id must not be empty!')

        return self.output_maintainability


    def unique(self, array): 
      
        # intilize a null list 
        self.unique_array = [] 
          
        # traverse for all elements 
        for x in array: 
            # check if exists in unique_list or not 
            if x not in self.unique_array: 
                self.unique_array.append(x) 

        return self.unique_array