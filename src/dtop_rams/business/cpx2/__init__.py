"""
This init file defines the framework of the analysis procedure for Complexity 2. 
"""
from dtop_rams.business.cpx2.RamsReliabilityCpx2 import RamsReliabilityCpx2
from dtop_rams.business.cpx2.RamsAvailabilityCpx2 import RamsAvailabilityCpx2
from dtop_rams.business.cpx2.RamsMaintainabilityCpx2 import RamsMaintainabilityCpx2
from dtop_rams.business.cpx2.RamsSurvivabilityCpx2 import RamsSurvivabilityCpx2
