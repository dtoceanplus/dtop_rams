###                                                                        ###
#  DTOceanPlus -- Module of Reliability, Availability, Maintainability and   #
#                            Survivability (RAMS)                            #
###                                                                        ###


import scipy.stats
import numpy as np
import math
import random
from scipy.stats import gumbel_r 


class RamsSurvivabilityCpx2():
    """ This class is used for assessing survivability for complexity 2.

    The survivability of the critical structural/ mechanical components
    is assessed using Monte Carlo Simulation.
    Note: a linear S-N curve is assumed to be consistent with the fundamental
          assumption made in the SK module 

    Attributes:
      nomenclature-
        a - the intercept of the chosen mean S-N curve with the log(N) axis
        h - the shape parameter of the 2-parameter Weibull probabilistic 
          distribuiton of the long-term stress ranges
        l - the ultimate stress/ load exerted upon the critical components
        m - the negative inverse slope of the chosen S-N curve
        n - the number of cycles of stress ranges
        q - the scale parameter of the 2-parameter Weibull probabilistic 
          distribuiton of the long-term stress ranges
        r - the resistance of the materials of the critical components
        ufl - the uncertain factor associated with the ultimate stress/ load
        urf - the uncertain factor associated with the resistance  
      -----------------------------note:------------------------------------ 
      The probabilistic distributions of  the following typical 
      probabilistic distributions:
        'Gaussian'     -> (in python, loc=mean; scale=std)
                          the input mu_l, mu_r, mu_ufl and mu_ufr
                          refer to loc
                          the input std_l, std_r, std_ufl and std_ufr
                          refer to scale
        'Exponential'  -> (in python, loc=0; scale=1/lambda)
                          the input mu_l, mu_r, mu_ufl and mu_ufr
                          refer to loc
                          the input std_l, std_r, std_ufl and std_ufr are
                          zero or left blank          
        'LogNormal'    -> (in python, loc=0; s=std_lnx; scale=exp(mu_lnx))
                          the input mu_l, mu_r, mu_ufl and mu_ufr
                          refer to scale (i.e. mu_ln(x))
                          the input std_l, std_r, std_ufl and std_ufr
                          refer to s (i.e. std_ln(x))                  
        'Weibull'      -> (in python, loc=0; s=shape parameter;
                           scale=scale parameter)
                          the input mu_l, mu_r, mu_ufl and mu_ufr refer to
                          s (shape parameter)
                          the input std_l, std_r, std_ufl and std_ufr
                          refer to scale (scale parameter)
        'Gumbel'       -> (in python, loc=loc; scale=scale parameter)
                          the input mu_l, mu_r, mu_ufl and mu_ufr refer to
                          s (loc parameter)
                          the input std_l, std_r, std_ufl and std_ufr
                          refer to scale (scale parameter)
        'constant'     -> only the mean value required.  
      -----------------------------note:------------------------------------ 
    """

    def calc_survivability_uls(self, device_id, mu_l, mu_r, mu_ufl, mu_ufr, 
                               n_sim, std_l, std_r, std_ufl, std_ufr,
                               option='Monte Carlo', pd_l='Gaussian', 
                               pd_r='Gaussian', pd_ufl='Gaussian', 
                               pd_ufr='Gaussian',error=1e-3, num_iteration=20):
        """ assesses the survivability for the ultimate limit state (ULS)

        Args:
          device_id: a 2D list, each row representing a device and the columns 
            in this row representing various componentid to be assessed
            (list - int), unit [-] 
          mu_l: a 1D list containing the mean values of the stresses/ loads
            of the critical components in the device (list - float)
            , unit [MPa] / [N]
          mu_r: a 1D list containing the mean values of the resistances of 
            the critical components in the device (list), unit [MPa] / [N]
          mu_ufl: the mean value of the uncertainty factor for the stress/load
            (scaler - float), unit [-]
          mu_ufr: the mean of the uncertainty factor for the resistance
            (scaler), unit [-]
          l_seed: a random realization of the stress/ load (scaler - float)
            , unit [MPa] / [N]
          r_seed: a random realization of the resistance (scaler - float)
            , unit [MPa] / [N]
          n_sim (scaler - int): the number of simulations (scaler - int)
            , unit [-]
          num_device: the number of devices (scaler - int), unit [-]
          n: a temporary number storing the number of failure events
            (scaler - int), unit [-]
          pd_l: the probabilitstic distribution of the stress/ load (string)
            , refer to the possible options in the note in Attributes
            , unit [-]
          pd_r: the probabilitstic distribution of the resistance (string),
            refer to the possible options in the note in Attributes
            , unit [-]
          pd_ufl: the probabilitstic distribution of the  uncertainty factor 
            for load/stress, refer to the possible options in the note
            in Attributes (string), unit [-]
          pd_ufr: the probabilitstic distribution of the uncertainty factor
            for resistance (string), refer to the possible options in the note
            in Attributes, unit [-]
          std_l: the standard deviations of the stresses/ loads of components
            (list - float), unit [MPa] / [N]
          std_r: the standard deviations of the resistances of components
            (list - float), unit [MPa] / [N]
          std_ufl: the standard deviation of the uncertainty factor for the 
            stress/ load (scaler), unit [-]
          std_ufr: the standard deviation of the uncertainty factor for the
            resistance (scaler), unit [-]
          ufl_seed: a random realization of the uncertainy factor for load/
            stress (scaler - float), unit [-]
          ufr_seed: a random realization of the uncertainy factor for 
            resistance (scaler - float), unit [-]

        Returns:
          output_survivability_uls:  a dictionary containing "device_id" and 
            "survival_uls" (dictionary)
            {"device_id": ['Device 1', 'Device 2', '...'],
             "survival_uls": [0.923, 0.986, ...]}       
     
        Raises:
          TypeError: An error occurs, if 'option' is not 'Monte Carlo'.  
        """
        survival_uls = []
        self.output_survivability_uls = dict()
        self.output_survivability_uls["device_id"] = device_id
        self.output_survivability_uls["survival_uls"] = survival_uls
        try:
            if (option == 'Monte Carlo') or (option == 'FORM'):
                if (option == 'Monte Carlo'):
                    num_device = len(device_id)
                    for i in range(num_device):
                        n = 0
                        for k in range(n_sim):
                            """sample a seed for load
                            """
                            if (pd_l == 'Gaussian'):
                                loc = mu_l
                                scale = std_l
                                l_seed = float(scipy.stats.truncnorm.rvs(0, 1e10, loc,
                                               scale,size=1))
                            if (pd_l == 'Exponential'):
                                scale = mu_l
                                l_seed = float(scipy.stats.expon.rvs(0, scale,
                                               size=1))
                            if (pd_l == 'LogNormal'):
                                std_lnl = math.sqrt(math.log(1+math.pow((std_l/mu_l),2)))
                                mu_lnl= math.log(mu_l)-0.5*math.pow(std_lnl,2)
                                s = std_lnl
                                scale = math.exp(mu_lnl)
                                l_seed = float(scipy.stats.lognorm.rvs(s, 0,
                                               scale, size=1))      
                            if (pd_l == 'Weibull'):
                                s = std_l
                                scale = mu_l
                                l_seed = float(scipy.stats.weibull_min.rvs(s, 0,
                                               scale, size=1))
                            if (pd_l == 'Gumbel'):
                                loc = mu_l
                                scale = std_l
                                l_seed = float(scipy.stats.gumbel_r.rvs(loc,
                                               scale, size=1))   
                            """sample a seed for resistance
                            """
                            if (pd_r == 'Gaussian'):
                                loc = mu_r
                                scale = std_r
                                r_seed = float(scipy.stats.truncnorm.rvs(0, 1e10, loc, scale,
                                               size=1))                                        
                            if (pd_r == 'Exponential'):
                                scale = mu_r
                                r_seed = float(scipy.stats.expon.rvs(0, scale, 
                                               size=1))
                            if (pd_l == 'LogNormal'):
                                std_lnr = math.sqrt(math.log(1+math.pow((std_r/mu_r),2)))
                                mu_lnr= math.log(mu_r)-0.5*math.pow(std_lnr,2)
                                s = std_lnr
                                scale = math.exp(mu_lnr)
                                r_seed = float(scipy.stats.lognorm.rvs(s, 0, scale,
                                               size=1))      
                            if (pd_r == 'Weibull'):
                                s = std_r
                                scale = mu_r
                                r_seed = float(scipy.stats.weibull_min.rvs(s, 0,
                                               scale, size=1))
                            if (pd_r == 'Gumbel'):
                                loc = mu_r
                                scale = std_r
                                r_seed = float(scipy.stats.gumbel_r.rvs(loc, scale,
                                               size=1))    
                            """sample a seed for the uncertainty factor for load
                            """
                            if (pd_ufl == 'Gaussian'):
                                loc = mu_ufl
                                scale = std_ufl
                                ufl_seed = float(scipy.stats.truncnorm.rvs(0, 2, loc, scale, 
                                                 size=1))                                          
                            if (pd_ufl == 'Exponential'):
                                scale = mu_ufl
                                ufl_seed = float(scipy.stats.expon.rvs(0, scale,
                                                 size=1))
                            if (pd_ufl == 'LogNormal'):
                                std_lnufl = math.sqrt(math.log(1+math.pow((std_ufl/mu_ufl),2)))
                                mu_lnufl= math.log(mu_ufl)-0.5*math.pow(std_lnufl,    2)                              
                                s = std_lnufl
                                scale = math.exp(mu_lnufl)
                                ufl_seed = float(scipy.stats.lognorm.rvs(s, 0, 
                                                 scale, size=1))   
                            if (pd_ufl == 'Weibull'):
                                s = std_ufl
                                scale = mu_ufl
                                ufl_seed = float(scipy.stats.weibull_min.rvs(s, 0,
                                                 scale, size=1))
                            if (pd_ufl == 'Gumbel'):
                                loc = mu_ufl
                                scale = std_ufl
                                ufl_seed = float(scipy.stats.gumbel_r.rvs(loc, 
                                                 scale, size=1))                                          
                            """sample a seed for the uncertainty factor for resistance
                            """
                            if (pd_ufr == 'Gaussian'):
                                loc = mu_ufr
                                scale = std_ufr
                                ufr_seed = float(scipy.stats.truncnorm.rvs(0, 2, loc, scale,
                                                 size=1))                                             
                            if (pd_ufr == 'Exponential'):
                                scale = mu_ufr
                                ufr_seed = float(scipy.stats.expon.rvs(0, scale,
                                                 size=1))
                            if (pd_ufr == 'LogNormal'):
                                std_lnufr = math.sqrt(math.log(1+math.pow((std_ufr/mu_ufr),2)))
                                mu_lnufr= math.log(mu_ufr)-0.5*math.pow(std_lnufr,    2)                                
                                s = std_lnufr
                                scale = math.exp(mu_lnufr)
                                ufr_seed = float(scipy.stats.lognorm.rvs(s, 0, scale,
                                                 size=1))      
                            if (pd_ufr == 'Weibull'):
                                s = std_ufr
                                scale = mu_ufr
                                ufr_seed = float(scipy.stats.weibull_min.rvs(s, 0,
                                                 scale, size=1))
                            if (pd_ufr == 'Gumbel'):
                                loc = mu_ufr
                                scale = std_ufr
                                ufr_seed = float(scipy.stats.gumbel_r.rvs(loc, 
                                                 scale, size=1))
                            g = ufr_seed*r_seed-ufl_seed*l_seed
                            if (g > 0.0):
                                n = n+1
                        survival_uls.append(float(n/n_sim))
                        self.output_survivability_uls["survival_uls"] = survival_uls

                if (option == 'FORM'):
                    print('FORM is used for now!')
                    num_device = len(device_id)
                    beta_index = []
                    alpha_u = []
                    u_design = []
    
                    beta_pre = 0
                    l_u_space = 1.0
                    r_u_space = 1.0
                    ufl_u_space = 0.1
                    ufr_u_space = 0.1
                    beta_tempt = []
                    alpha_tempt = []
                    u_design_tempt = []
                    num_converage = 0
                    for k in range(num_iteration):
                        l_u_space_cur = l_u_space
                        r_u_space_cur = r_u_space
                        ufl_u_space_cur = ufl_u_space
                        ufr_u_space_cur = ufr_u_space
                        """transform the load to the correponding U-space
                        """
                        if (pd_l == 'Gaussian'):
                            loc = mu_l
                            scale = std_l
                            l_x_space = l_u_space_cur*scale+loc
                            diff_l_u = std_l
                        if (pd_l == 'Exponential'):
                            lamda = 1/mu_l
                            l_x_space = -(1/lamda)*math.log(1.0-scipy.stats.norm.cdf\
                                        (l_u_space_cur, 0, 1))
                            diff_l_u = (1/lamda) * scipy.stats.norm.pdf(l_u_space_cur,\
                                        0, 1)/ (1-scipy.stats.norm.cdf\
                                        (l_u_space_cur, 0, 1)) 
                        if (pd_l == 'LogNormal'):
                            std_lnl = math.sqrt(math.log(1+math.pow((std_l/mu_l),2)))
                            mu_lnl= math.log(mu_l)-0.5*math.pow(std_lnl,2)
                            l_x_space = math.exp(l_u_space_cur*std_lnl+mu_lnl)
                            diff_l_u = math.exp(l_u_space_cur*std_lnl+mu_lnl)*std_lnl
                        if (pd_l == 'Gumbel'):
                            loc = mu_l
                            scale = std_l
                            a = math.pi/(math.sqrt(6)*scale)
                            b = loc-0.5772/a
                            l_x_space = b-(1/a)*math.log(-math.log\
                                    (scipy.stats.norm.cdf(l_u_space_cur,0,1)))
                            diff_l_u = -scipy.stats.norm.pdf(l_u_space_cur,\
                                        0, 1)/(a*scipy.stats.norm.cdf\
                                        (l_u_space_cur, 0, 1)*math.log\
                                        (scipy.stats.norm.cdf\
                                        (l_u_space_cur, 0, 1)))
                        """transform the resistance to the correponding U-space
                        """
                        if (pd_r == 'Gaussian'):
                            loc = mu_r
                            scale = std_r
                            r_x_space = r_u_space_cur*scale+loc
                            diff_r_u = std_r
                        if (pd_r == 'Exponential'):
                            lamda = 1/mu_r
                            r_x_space = -(1/lamda)*math.log(1.0-scipy.stats.norm.cdf\
                                        (r_u_space_cur, 0, 1))
                            diff_r_u = (1/lamda) * scipy.stats.norm.pdf(r_u_space_cur,\
                                        0, 1)/ (1-scipy.stats.norm.cdf\
                                        (r_u_space_cur, 0, 1)) 
                        if (pd_r == 'LogNormal'):
                            std_lnr = math.sqrt(math.log(1+math.pow((std_r/mu_r),2)))
                            mu_lnr= math.log(mu_r)-0.5*math.pow(std_lnr,2)
                            r_x_space = math.exp(r_u_space_cur*std_lnr+mu_lnr)
                            diff_r_u = math.exp(r_u_space_cur*std_lnl+mu_lnr)*std_lnr
                        if (pd_r == 'Gumbel'):
                            loc = mu_r
                            scale = std_r
                            a = math.pi/(math.sqrt(6)*scale)
                            b = loc-0.5772/a
                            r_x_space = b-(1/a)*math.log(-math.log(scipy.stats.\
                                        norm.cdf(r_u_space_cur,0,1)))
                            diff_r_u = -scipy.stats.norm.pdf(r_u_space, 0, 1)/\
                                       (a*scipy.stats.norm.cdf(r_u_space_cur,\
                                        0, 1)*math.log(scipy.stats.norm.cdf\
                                        (r_u_space_cur, 0, 1)))
                        """transform the uncertain factor for load to the correponding U-space
                        """
                        if (pd_ufl == 'Gaussian'):
                            loc = mu_ufl
                            scale = std_ufl
                            ufl_x_space = ufl_u_space_cur*scale+loc
                            diff_ufl_u = std_ufl
                        if (pd_ufl == 'Exponential'):
                            lamda = 1/mu_ufl
                            ufl_x_space = -(1/lamda)*math.log(1.0-scipy.stats.norm.cdf\
                                        (ufl_u_space_cur, 0, 1))
                            diff_ufl_u = (1/lamda) * scipy.stats.norm.pdf(ufl_u_space_cur,\
                                        0, 1)/ (1-scipy.stats.norm.cdf\
                                        (ufl_u_space_cur, 0, 1))
                        if (pd_ufl == 'LogNormal'):
                            std_ln_ufl = math.sqrt(math.log(1+math.pow((std_ufl/mu_ufl),2)))
                            mu_ln_ufl= math.log(mu_ufl)-0.5*math.pow(std_ln_ufl,2)
                            ufl_x_space = math.exp(ufl_u_space_cur*std_ln_ufl+mu_ln_ufl)
                            diff_ufl_u = math.exp(ufl_u_space_cur*std_ln_ufl+mu_ln_ufl)*std_ln_ufl
                        if (pd_ufl == 'Gumbel'):
                            loc = mu_ufl
                            scale = std_ufl
                            a = math.pi/(math.sqrt(6)*scale)
                            b = loc-0.5772/a
                            ufl_x_space = b-(1/a)*math.log(-math.log(scipy.\
                                          stats.norm.cdf(ufl_u_space_cur,0,1)))
                            diff_ufl_u = -scipy.stats.norm.pdf(ufl_u_space_cur,\
                                          0, 1)/(a*scipy.stats.norm.\
                                          cdf(ufl_u_space_cur, 0, 1)*math.log\
                                          (scipy.stats.norm.cdf(ufl_u_space_cur,\
                                          0, 1)))
                        """transform the uncertain factor for resistance to the correpondingU-space
                        """
                        if (pd_ufr == 'Gaussian'):
                            loc = mu_ufr
                            scale = std_ufr
                            ufr_x_space = ufr_u_space_cur*scale+loc
                            diff_ufr_u = std_ufl
                        if (pd_ufl == 'Exponential'):
                            lamda = 1/mu_ufr
                            ufr_x_space = -(1/lamda)*math.log(1.0-scipy.stats.norm.cdf\
                                        (ufr_u_space_cur, 0, 1))
                            diff_ufr_u = (1/lamda) * scipy.stats.norm.pdf(ufr_u_space_cur,\
                                        0, 1)/ (1-scipy.stats.norm.cdf\
                                        (ufr_u_space_cur, 0, 1))
                        if (pd_ufr == 'LogNormal'):
                            std_ln_ufr = math.sqrt(math.log(1+math.pow((std_ufr/mu_ufr),2)))
                            mu_ln_ufr= math.log(mu_ufr)-0.5*math.pow(std_ln_ufr,2)
                            ufr_x_space = math.exp(ufr_u_space_cur*std_ln_ufr+mu_ln_ufr)
                            diff_ufr_u = math.exp(ufr_u_space_cur*std_ln_ufr+mu_ln_ufr)*std_ln_ufr
                        if (pd_ufr == 'Gumbel'):
                            loc = mu_ufr
                            scale = std_ufr
                            a = math.pi/(math.sqrt(6)*scale)
                            b = loc-0.5772/a
                            ufr_x_space = b-(1/a)*math.log(-math.log(scipy.\
                                          stats.norm.cdf(ufr_u_space_cur,0,1)))
                            diff_ufr_u = -scipy.stats.norm.pdf(ufr_u_space_cur,\
                                          0, 1)/(a*scipy.stats.norm.cdf\
                                          (ufr_u_space_cur, 0, 1)*math.log\
                                          (scipy.stats.norm.cdf(ufr_u_space_cur,\
                                          0, 1)))
                        g = ufr_x_space * r_x_space - ufl_x_space * l_x_space 
                        # the order of partial differntiation: 
                        # [r_x_space, ufr_x_space, l_x_space, ufl_x_space]
                        diff_g = [ ufr_x_space*diff_r_u, r_x_space*diff_ufr_u,
                                  -ufl_x_space*diff_l_u, -l_x_space*diff_ufl_u]
                        r_u_space = ((sum(np.multiply(diff_g, [r_u_space_cur,\
                                     ufr_u_space_cur, l_u_space_cur, \
                                    ufl_u_space_cur]))-g)/sum(np.multiply\
                                    (diff_g,diff_g)))*diff_g[0]
                        ufr_u_space =((sum(np.multiply(diff_g, [r_u_space_cur,\
                                     ufr_u_space_cur, l_u_space_cur, \
                                    ufl_u_space_cur]))-g)/sum(np.multiply\
                                    (diff_g,diff_g)))*diff_g[1]
                        l_u_space = ((sum(np.multiply(diff_g, [r_u_space_cur,\
                                     ufr_u_space_cur, l_u_space_cur, \
                                    ufl_u_space_cur]))-g)/sum(np.multiply\
                                    (diff_g,diff_g)))*diff_g[2]
                        ufl_u_space = ((sum(np.multiply(diff_g, [r_u_space_cur,\
                                     ufr_u_space_cur, l_u_space_cur, \
                                    ufl_u_space_cur]))-g)/sum(np.multiply\
                                    (diff_g,diff_g)))*diff_g[3]
                        beta_tempt.append(math.sqrt(sum(np.multiply\
                                         ([l_u_space, ufl_u_space, r_u_space,\
                                         ufr_u_space],[l_u_space, ufl_u_space,\
                                         r_u_space, ufr_u_space]))))
                        num_converage = num_converage + 1
                        temp = []
                        for h in range(4): 
                            temp.append(-diff_g[h]/sum(np.multiply(diff_g,diff_g)))
                        alpha_tempt.append(temp)
                        u_design_tempt.append([l_u_space,ufl_u_space,r_u_space,ufr_u_space])
                        if (abs(beta_tempt[k]-beta_pre) < error):
                            print(num_converage)
                            break  
                        else:
                            beta_pre = beta_tempt[k]
                            continue   
                    beta_index.append(beta_tempt)
                    alpha_u.append(alpha_tempt)
                    u_design.append(u_design_tempt)  
    
                    for i in range(len(beta_index[0])):
                        survival_uls.append(1.0 - scipy.stats.norm.cdf(-beta_index[0][i], 0, 1))
                    self.output_survivability_uls["survival_uls"] = survival_uls
            else:
                raise TypeError
        except TypeError:
            self.output_survivability_uls["survival_uls"] = []
            print('An invalid method found. Please recheck!')
        
        return self.output_survivability_uls


    def calc_survivability_fls(self, device_id, mu_a, mu_h, mu_m, mu_n, mu_q, 
                          n_sim, pd_a, pd_q, std_a, std_h, std_m, std_n, 
                          std_q, option='Monte Carlo', pd_h='constant',
                          pd_m='constant', pd_n='constant'):
        """ assesses the survivability for the fatigue limit state (FLS)

        Args:
          a_seed: a random realization of a (scalar - float)
          h_seed: a random realization of h (scalar - float)
          m_seed: a random number of m (scalar -integer)
          mu_a: the mean value of a (scalar - float), unit [-]
          mu_h: the shape parameter in a 2-parameter Weibull distribution of 
            the long-term stress ranges (scalar - float)   
          mu_m: the mean value of the negative inverse slope of the chosen
            S - N curve (scalar - integer)        
          mu_n: the mean value of stress cycles (scalar - integer), unit [-]
          mu_q: the mean value of the scale parameter in  Weibull distribuiton
            of the long-term stress/load ranges (scalar - flaot)
          n_seed: a random realization of n (scalar - integer)
          n_simm: the number of simulation (scalar - integer), unit [-]
          num_device: the number of devices (scalar - integer)
          pd_a: the probabilitstic distribution of the intercept of the chosen
            mean S-N curve with the log(N) axis (string), refer to the 
            possible options in the note in the note in Attributes, unit [-]
          pd_h: the probabilitstic distribution of the 2-parameter Weibull
            distribution assumed for the long-term probabilistic distribution 
            of the stress ranges (string), refer to the possible options
            in the note in Attributes, unit [-]
          pd_m: the probabilitstic distribution of the negative inverse slope 
            of the chosen S-N curve, refer to the possible options in the note
            in Attributes (string), unit [-]
          pd_n: the probabilitstic distribution of the number of cycles of
            stress ranges (string), refer to the possible options in the note
            in Attributes, unit [-]    
          pd_q: the probabilitstic distribution of the scale parameter of the 
            2-parameter Weibull distribution assumed for the long-term
            probabilistic distribution of the stress ranges (string),
            refer to the possible options in the note in Attributes, unit [-]
          q_seed: a random realization of q (scalar - float)
          std_a: the standard deviation of a (scalar - float), unit [-]
          std_h: the standard deviation of h (scalar - float), unit [-]
          std_m: the standard deviation of m (scalar - integer), unit [-]
          std_n: the standard deviation of n (scalar - integer), unit [-]
          std_q: the standard deviation of q (scalar - float), unit [-]   

        Returns:
          output_survivability_fls:  a dictionary containing "device_id" and 
            "survival_fls" (dictionary)
            {"device_id": ['Device 1', 'Device 2', '...'],
             "survival_fls": [0.923, 0.986, ...]}       
     
        Raises:
          TypeError: An error occurs, if 'option' is not 'Monte Carlo'.    
        """
        survival_fls = []
        self.output_survivability_fls = dict()
        self.output_survivability_fls["device_id"] = device_id
        self.output_survivability_fls["survival_fls"] = survival_fls
        try:
            if (option == 'Monte Carlo') or (option == 'FORM'):
                if (option == 'Monte Carlo'):
                    print('Monte Carlo Method is used for now!')
                    num_device = len(device_id)
                    for i in range(num_device):
                        n = 0
                        for k in range(n_sim):
                            """sample a seed for a
                            """
                            if (pd_a == 'Gaussian'):
                                loc = mu_a
                                scale = std_a
                                a_seed = float(scipy.stats.truncnorm.rvs(0, 100, loc, scale,
                                size=1)) 
                            if (pd_a == 'LogNormal'):
                                s = std_a
                                scale = math.exp(mu_a)
                                a_seed = float(scipy.stats.lognorm.rvs(s, 0, 
                                scale, size=1))     
                            if (pd_a == 'Weibull'):
                                s = std_a
                                scale = mu_a
                                a_seed = float(scipy.stats.weibull_min.rvs(s, 0,
                                 scale, size=1))  
    
                            """sample a seed for h
                            """
                            # h is considered constant, based upon the experience 
                            if (pd_h == 'constant'):
                                h_seed = mu_h
    
    
                            """sample a seed for m
                            """
                            # m is considered constant, based upon the experience 
                            if (pd_m == 'constant'):
                                m_seed = mu_m
                            """sample a seed for n
                            """
                            # n is considered constant, based upon the experience 
                            if (pd_n == 'constant'):
                                n_seed = mu_n
                            """sample a seed for q
                            """
                            if (pd_q == 'Gaussian'):
                                loc = mu_q
                                scale = std_q
                                q_seed = float(scipy.stats.truncnorm.rvs(0, 10, loc, scale,
                                size=1))
                            if (pd_q == 'LogNormal'):
                                s = std_q
                                scale = math.exp(mu_q)
                                q_seed = float(scipy.stats.lognorm.rvs(s, 0, scale,
                                 size=1))     
                            if (pd_q == 'Weibull'):
                                s = std_q
                                scale = mu_q
                                q_seed = float(scipy.stats.weibull_min.rvs(s, 0,
                                 scale, size=1))                                                 
                            g = -math.log(n_seed) + math.log(a_seed) - m_seed * \
                                math.log(q_seed)-math.log(math.gamma(1+m_seed/
                                h_seed))
                            if (g > 0.0):
                                n = n+1
                        survival_fls.append(float(n/n_sim))
                        self.output_survivability_fls["survival_fls"] = survival_fls
                if (option == 'FORM'):
                    if (pd_q == 'LogNormal'):
                        std_lnq = math.sqrt(math.log(1+math.pow((std_q/mu_q),2)))
                        mu_lnq = math.log(mu_q)-0.5*math.pow(std_lnq,2)
                    else:
                        std_lnq = None
                        mu_lnq = None
                        raise TypeError("LongNormal is the only option for q, if the option is FORM!")     
                    if (pd_a == 'Gaussian'):
                        std_lna = math.sqrt(math.log(1+math.pow((std_a/mu_a),2)))
                        mu_lna = math.log(mu_a)-0.5*math.pow(std_lna,2)
                    else:
                        std_lna = None
                        mu_lna = None
                        raise TypeError("LongNormal is the only option for a, if the option is FORM!")
                    if ((mu_lna != None) and (std_lna != None) and (mu_lnq != None) and (std_lnq != None)):					
                        mu_lnn = np.log(mu_n)
                        std_lnn = 0.0									
                        numerator = -mu_lnn + mu_lna - mu_m*mu_lnq - np.log(math.gamma(1+mu_m/mu_h))
                        denominator = math.sqrt(pow(std_lnn,2) + pow(std_lna,2) + pow(mu_m,2)*pow(std_lnq,2))
                        beta_index = numerator/denominator
                        self.output_survivability_fls["survival_fls"] = [1.0 -scipy.stats.norm.cdf(-beta_index, 0, 1)]
                    else: 
                        self.output_survivability_fls["survival_fls"] = None
            else:
                raise TypeError
        except TypeError:
            self.output_survivability_fls["survival_fls"] = None
            print('An invalid method found. Please recheck!')
        return  self.output_survivability_fls

    
    def pre_process_sk(self, a, cdf, cycles, device_id, m, stress_range):
        """ prepcrosses the inputs for the SK module

        Note: see the dtosk_outputs_DesignAssessment.json in the SK OpenAPI
        example for more details regarding the data structure of inputs used 
        in this method. 
        cdf - cumulative distribution function

        Args:
          a: a multi-dimension list, with each entry representing the parameter
            a in the chosen S-N curve (list - int), unit [-]
          cdf: a multi-dimension list, the cumulative distribution function
            (list - float), unit [-]
          cycles: a multi-dimension list, the number of stress cycles 
            (list - int), see the SK example for more details, unit [-]
          device_id: the device ids (list - int), unit [-]
          m: a multi-dimension list, with each entry representing the parameter
            m in the chosen S-N curve (list - int), unit [-]      
          stress_range: a multi-dimension list, with each entry representing
            the stress ranges (list - int), unit [MPa]   

        Returns:
          a_unique: a 1D list containing the unique parameter a (list - float)
            , unit [-]
          cdf_unique: a 1D list containing the unique cdf (list - float), 
            unit [-]
          cycles_unique: a 1D list containing the unique number of stress 
            ranges (list - int), unit [-]
          device_unique: a 1D list containing the unique device ids 
            (list - sring), unit [-]
          m_unique: a 1D list containing the unique parameter m (list - float)
            , unit [-]           
          moorline_unique: a 1D list containing the unique mooring line ids
            with unique stress ranges (list - string), unit [-]
          number_moorline: a 1D list containing the number of mooring lines for
            each unique device (list - int)
          number_sea_state: a 1D list containing the number of  sea states for
            each unique mooring line (list - int)
          scale: a 2D list containing the scale parameter of the Weibull
            distriuuion of the long-term stress ranges, with each row 
			correponding to each device and each entry in a row  
            representing one unique mooring line (list - float)
          shape: a 2D list containing the shape parameter of the Weibull
            distriuuion of the long-term stress ranges, with each row 
			correponding to each device and each entry in a row  
            representing one unique mooring line (list - float)    

        Raises:
          None       
        """
        a_unique = []
        cdf_unique = []
        stress_range_unique = []
        cycles_unique = []
        m_unique = []
        device_unique = []
        number_moorline = []     

        a_unique.append(a[0][0])
        cdf_unique.append(cdf[0])
        stress_range_unique.append(stress_range[0])
        cycles_unique.append(cycles[0])
        m_unique.append(m[0][0])
        device_unique.append(device_id[0])
        number_moorline.append(len(stress_range[0][0]))
        number_sea_states = len(cdf[0])

        for i in range(len(device_id)):
            for k in range(len(a[i])):
                if (not(a[i][k] in a_unique)):
                     a_unique.append(a[i][k])
                if (not(m[i][k] in m_unique)):                     
                     m_unique.append(m[i][k])

                if (not(stress_range[i] in stress_range_unique)): 
                    device_unique.append(device_id[i])
                    cdf_unique.append(cdf[i])
                    cycles_unique.append(cycles[i])
                    stress_range_unique.append(stress_range[i])
                    number_moorline.append(len(stress_range[i]))
                for s in range(len(cdf_unique)):
                    for h in range(len(cdf_unique[s])):
                        if all(i == 1 for i in cdf_unique[s][h]):
                            for k in range(len(cdf_unique[s][h])):
                                cdf_unique[s][h][k] = 1.0 - 1e-8
        shape = []
        scale = []
        for i in range(len(device_unique)):
            temp_shape = []
            temp_scale = []
            for j in range(number_moorline[0]):
                temp_stress_range = []
                temp_cdf = []
                y = []
                x = []
                for k in range(number_sea_states):
                    temp_stress_range.append(stress_range_unique[i][k][j])
                    temp_cdf.append(cdf_unique[i][k][j])
                    y.append(math.log(-math.log(1-temp_cdf[k])))
                    x.append(math.log(temp_stress_range[k]))
                p = np.polyfit(x,y,1)
                temp_shape.append(p[0])
                temp_scale.append(math.exp(-p[1]/p[0]))
            shape.append(temp_shape)
            scale.append(temp_scale)

        return a_unique, m_unique, cdf_unique, cycles_unique, device_unique,\
               stress_range_unique, shape, scale


    def pre_process_et(self, device_id, fls_load, fls_prob, process_type, 
	                   uls_load, uls_prob):
        """ prepcrosses the inputs for the ET module

        Args:
          device_id: the device ids (list - int), unit [-]
          fls_load: a 1D list containing the fatigue stress ranges for 
		    pre-chosen sea states (list - float), unit [-]
          fls_prob: a 1D list containing the occurrence probabilities 
		    corresponding to the fatigue stress ranges in fls_load 
			(list - float), unit [-]
		  process_type: the pre-processing type for the ET subsystem (string)
		    , unit[-]
          uls_load: a 1D list containing the ultimate stresses for 
		    pre-chosen sea states (list - float), unit [-]
          uls_prob: a 1D list containing the occurrence probabilities 
		    corresponding to the ultimate stresses in uls_load 
			(list - float), unit [-] 

        Returns:
          mu_load_uls: the mean ULS stress exerted in the components in the ET
		    subsystem (scalar - float), unit [-]
          std_load_uls: the standard deviation of the ULS stress exerted
		    in the components in the ET subsystem (scalar - float), unit [-]
          scale: a 2D list containing the scale parameter of the Weibull
            distriuuion of the long-term stress ranges, with each row 
			correponding to each device and each entry in a row  
            representing one unique mooring line (list - float)
          shape: a 2D list containing the shape parameter of the Weibull
            distriuuion of the long-term stress ranges, with each row 
			correponding to each device and each entry in a row  
            representing one unique mooring line (list - float)    

        Raises:
          None       
        """
		# pre-processing the input for ULS
        if (process_type == "ULS"): 
            num_uls = len(uls_load)
            temp_uls = 0.0
            for i in range(num_uls):
            	temp_uls = temp_uls + uls_load[i] * uls_prob[i]
            mu_load_uls = temp_uls / float(num_uls)
            temp_uls = 0.0
            for i in range(num_uls):
            	temp_uls = temp_uls + pow((uls_load[i]-mu_load_uls),2) 
            std_load_uls = math.sqrt(temp_uls/float(num_uls-1))
            shape = []
            scale = []

		# pre-processing the input for FLS
        if (process_type == "FLS"):
            num_fls = len(fls_load)
            cdf_fls_prob = list(np.cumsum(fls_prob))
            for i in range(num_fls):
                y = []
                x = []
                y.append(math.log(-math.log(1-cdf_fls_prob[i])))
                x.append(math.log(fls_load[i]))
            p = np.polyfit(x,y,1)
            shape = p[0]
            scale = math.exp(-p[1]/p[0])
            mu_load_uls = None
            std_load_uls = None

        return mu_load_uls, std_load_uls, scale, shape


    def survivability_uls(self, device_id_et, device_id_sk, mu_mbl_et, 
	                      mu_mbl_sk, mu_tension_sk, mu_ufl, mu_ufr, n_sim,
						  std_mbl_et, std_mbl_sk, std_tension_sk, std_ufl, 
						  std_ufr, uls_load_et, uls_prob_et, option_uls, pd_l,
						  pd_r, pd_ufl, pd_ufr, process_type):
        """ calculates the surivival probability for the ULS state

        Args:
          device_id_et: a 1D list containing the critical device id for the ET
		    subsystem (list - string), unit [-] 
          device_id_sk: a 1D list containing the critical device id for the SK
		    subsystem (list - string), unit [-]
          mu_mbl_et: the mean maximum resistance of components 
		    in the ET subsystem (scalar - float), unit [MPa] / [N]
          mu_mbl_sk: the mean maximum breaking load (mbl) of mooring lines
		    in the SK subsystem (scalar - float), unit [MPa] / [N]
          mu_tension_sk: the mean tension of mooring lines in the SK subsystem
            (scalar - float), unit [MPa] / [N]
          mu_ufl: the mean value of the uncertainty factor for the stress/load
            (scaler - float), unit [-]
          mu_ufr: the mean of the uncertainty factor for the resistance
            (scaler - float), unit [-]
          n_sim: the number of simulations (scaler - int), unit [-]
          std_mbl_et: the standard deviation of resistance of components 
		    in the ET subsystem (scalar - float), unit [MPa] / [N]
          mu_mbl_sk: the standnard deviation of breaking load (mbl) of mooring
		    lines in the SK subsystem (scalar - float), unit [MPa] / [N]
          std_tension_sk: the standard deviation of tension of mooring lines
		    in the SK subsystem (scalar - float), unit [MPa] / [N]
          option_uls: the option of method for the ULS survivability assessment
		    (string), unit [-]
          pd_l: the probabilitstic distribution of the stress/ load (string)
            , unit [-]
          pd_r: the probabilitstic distribution of the resistance (string)
            , unit [-]
          pd_ufl: the probabilitstic distribution of the  uncertainty factor 
            for load/stress (string), unit [-]
          pd_ufr: the probabilitstic distribution of the uncertainty factor
            for resistance (string), unit [-]
		  process_type: the pre-processing type for the ET subsystem (string)
		    , unit[-]
		  uls_load_et: the ultimate stress exerted on the components 
		    in the ET subsystem (1D list - float), unit [MPa] / [N]
		  uls_prob_et: the occurrence probabilities corresponding to 
		    the ultimate stress exerted on the components 
		    in the ET subsystem (1D list - float), unit [-]

        Returns:
          survivability_summary_uls: a dictionary including the survival 
		    probabilities of the ET and SK subsystems

        Raises:
          None       
        """
        survivability_summary_uls = dict()

        # to calculate the survival probabilities - the SK subsystem
        device_sk = f'Device{device_id_sk}'
        survivability_uls_sk = self.calc_survivability_uls(device_sk,
        mu_tension_sk, mu_mbl_sk, mu_ufl, mu_ufr, n_sim, std_tension_sk, 
        std_mbl_sk, std_ufl, std_ufr, option_uls, pd_l,
        pd_r, pd_ufl, pd_ufr)
        
        # to calculate the survival probabilities - the ET subsystem
        mu_load_et, std_load_et, shape_et, scale_et = self.pre_process_et(device_id_et, 
		"", "", process_type, uls_load_et, uls_prob_et)
        survivability_uls_et = self.calc_survivability_uls([device_id_et],
        mu_load_et, mu_mbl_et, mu_ufl, mu_ufr, n_sim, std_load_et, 
		std_mbl_et, std_ufl, std_ufr, option_uls, pd_l, 
        pd_r, pd_ufl, pd_ufr)

        # dump the results
        if (option_uls == 'Monte Carlo'):
            survivability_summary_uls['SK Subsystem'] = {"survival_uls": [min(survivability_uls_sk["survival_uls"])],
                                                     "device_id": device_sk}
            survivability_summary_uls['ET Subsystem'] = {"survival_uls": [min(survivability_uls_et["survival_uls"])],
                                                    "device_id": device_id_et}
        if (option_uls == 'FORM'):
            if (survivability_uls_sk != []) and (survivability_uls_et != []):
                index_last_sk = len(survivability_uls_sk['survival_uls'])
                index_last_et = len(survivability_uls_et['survival_uls'])
                survivability_uls_sk['survival_uls'] = [survivability_uls_sk['survival_uls']      [index_last_sk-1]]
                survivability_uls_et['survival_uls'] = [survivability_uls_et['survival_uls']      [index_last_et-1]]
                survivability_summary_uls['SK Subsystem'] = {"survival_uls": [min(survivability_uls_sk["survival_uls"])],
				                                            "device_id": device_sk}
                survivability_summary_uls['ET Subsystem'] = {"survival_uls": [min(survivability_uls_et["survival_uls"])],
				                                            "device_id": device_id_et}
            else:
                survivability_summary_uls['SK Subsystem'] = {"survival_uls": [-1],
                                                         "device_id": "NA"}                 
                survivability_summary_uls['ET Subsystem'] = {"survival_uls": [-1],
                                                         "device_id": "NA"}

        return survivability_summary_uls


    def survivability_fls(self, a, cdf, cov_a, cov_q, cycles_sk, cycles_et, 
	    device_id_et, device_id_sk, fatigue_flag_sk, fls_load_et, fls_prob_et, m, mu_a_et,  
	    m_et, n_sim, option_fls, pd_a, pd_q, pd_h, pd_m, pd_n, process_type, 
		std_a_et, std_h, std_m, std_n, stress_range):
        """ calculates the surivival probability for the FLS state

        Args:
          a: a 1D list containing the S-N cuvre parameter a (list - float)
		    , unit [-] 
		  cdf: a 1D list containing the cumulative probabilities (list - float)
		    , unit [-] 
		  cov_a: the coefficient of variance of the S-N curve parameter a 
		    (scalar - float), unit [-]
		  cov_q: the coefficient of variance of the scale parameter in 
		    a 2-parameter Weibull probability distribution of 
			the long-term stress ranges (scalar - float), unit [-]
		  cycles: a 1D list containing the number of stress cycles
		    (list - integer), unit [-]
          device_id_et: a 1D list containing the critical device id for the ET
		    subsystem (list - string), unit [-]
          device_id_sk: a 1D list containing the critical device id for the SK
		    subsystem (list - string), unit [-]
		  fatigue_flag_sk: a 1D list containing the indicator of fatigue caculation
		    '0' indicates without fatigue calculation, unit [-]
          fls_load_et: a 1D list containing the fatigue stress ranges for 
		    pre-chosen sea states for the ET subsystem (list - float), unit [-]
          fls_prob_et: a 1D list containing the occurrence probabilities 
		    corresponding to the fatigue stress ranges in fls_load for the ET 
			subsystem (list - float), unit [-]
          m: a 1D list containing the S-N cuvre parameter m (list - float)
		    , unit [-]
          mu_a_et: the mean of the S-N curve parametre a (scalar - float)
		    , unit [-]
          m_et: the mean of the S-N cuvre parameter m (scalar - float)
		    , unit [-]	
          n_sim: the number of simulations (scaler - int), unit [-]	
          option_fls: the option of method for the FLS survivability assessment
		    (string), unit [-]
          pd_a: the probabilitstic distribution of the S-N curve parameter a
		    (string), unit [-]
          pd_q: the probabilitstic distribution of the scale parameter in 
		    the 2-parameter Weibull probability distribution of 
			the long-term stress ranges (string), unit [-]
          pd_h: the probabilitstic distribution of the shape parameter in 
		    the 2-parameter Weibull probability distribution of 
			the long-term stress ranges (string), unit [-]
          pd_m: the probabilitstic distribution of the S-N curve parameter m
		    (string), unit [-]
          pd_n: the probabilitstic distribution of the number of stress cycles
		    (string), unit [-]
		  process_type: the pre-processing type for the ET subsystem (string)
		    , unit[-]
		  std_a_et: the standard deviation of the S-N curve parameter a for 
		    the ET subsystem (scalar - float), unit[-]
		  std_h: the standard deviation of the shape parameter of a 2-parameter
		    Weibull distribution of the long-term stress ranges 
			(scalar - float), unit[-]		   
		  std_m: the standard deviation of the S-N curve parameter m 
		    (scalar - float), unit[-]
		  std_n: the standard deviation of the stress range cycles 
		    (scalar - float), unit[-]
		  stress_range: a 1D list containing the stress ranges of the critical 
		    componnets in the SK subsystem (list - float), unit [MPa]

        Returns:
          survivability_summary_fls: a dictionary including the survival 
		    probabilities of the ET and SK subsystems

        Raises:
          None       
        """
        survivability_summary_fls = dict()

        # to calculate the survival probabilities - for the SK subsystem
        if 0 in fatigue_flag_sk:
            survivability_summary_fls['SK Subsystem'] = {"survival_fls": [-1],
                                                         "device_id": "NA"}
        else:
            device_sk = f'{device_id_sk}'
            a_unique, m_unique, cdf_unique, cycles_unique, device_unique,\
            stress_range_unique, shape, scale = self.pre_process_sk(a, cdf,\
		    cycles_sk, device_id_sk, m,stress_range)
            survivability_fls_sk = []
            for i in range(len(device_unique)):
                for j in range(len(a_unique)): 
                    for k in range(len(shape[i])): 
                        temp = self.calc_survivability_fls([device_unique[i]], 
                        a_unique[j], shape[i][k], m_unique[j], 
                        max(cycles_unique[i]), scale[i][k], n_sim, pd_a, pd_q, 
                        cov_a*a_unique[j], std_h, std_m, std_n, cov_q*scale[i][k], 
		    			option_fls, pd_h, pd_m, pd_n)
                survivability_fls_sk.append(temp["survival_fls"][0])
            if not (None in survivability_fls_sk):
                label_sk = survivability_fls_sk.index(min(survivability_fls_sk))
                survivability_summary_fls['SK Subsystem'] = {"survival_fls":[min(survivability_fls_sk)],
                                                         "device_id": f'Device{label_sk}'}

        # to calculate the survival probabilities - for the ET subsystem
        if mu_a_et != -1.0 and std_a_et != -1.0 and m_et != -1.0:
            mu_load_et, std_load_et, mu_scale_et, shape_et = self.pre_process_et(device_id_et, 
            fls_load_et, fls_prob_et, process_type, "", "")
            std_scale_et = cov_q * mu_scale_et
            temp = self.calc_survivability_fls([device_id_et], mu_a_et, shape_et, m_et, 
                        int(cycles_et), mu_scale_et, n_sim, pd_a, pd_q, std_a_et, std_h, 
                        std_m, std_n, std_scale_et, option_fls, pd_h, pd_m, pd_n)
            if temp["survival_fls"] != None:
                survivability_summary_fls['ET Subsystem'] = {"survival_fls": temp["survival_fls"],
			                                             "device_id": f'{device_id_et}'}
        else:
            survivability_summary_fls["ET Subsystem"] = {"survival_fls": [-1],
                                                         "device_id": "NA"}

        return survivability_summary_fls