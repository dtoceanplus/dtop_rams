###                                                                        ###
#  DTOceanPlus -- Module of Reliability, Availability, Maintainability and   #
#                             Availability (RAMS)                            #
###                                                                        ###          

import scipy.stats
import numpy as np
import math
import random


class RamsAvailabilityCpx2():
    """ This class is used for assessing availability for complexity 2.
    
    The time-based availability is assessed for the devices.
    Ref. IEC 61400-25-1

    Attributes:
      Public method(s) include(s) 'calc_availability'.
    """

    def calc_availability(self, device_id, downtime, t_life):
        """ assesses the availability

        Args:
          device_id: a 1D list containing the device ids (list - string)
          downtime: a 1D list containing the downtime of devices
            (list - int), unit [hour]
          num_device: the no. of devices (scalar)
          num_time: the no. of discrtized time (scalar)
          t_life (caler - int): the lifetime, unit [year]  
        
        Returns: 
          output_availability: a dictionary containing "device_id" and
            "availability_tb" (dictionary)
            {"device_id": ['Device 1', 'Device 2', '...'],
             "availability_tb": [0.923, 0.986, ...]} 
            
        Raises:
          ValueError: An error occurs, if 'downtime' is empty.            
        """
        self.output_availability = dict()
        availability_timebase = []
        self.output_availability["availability_tb"] = []
        self.output_availability["device_id"] = device_id
        try:
            if ((t_life > 0) and (len(downtime) != 0)):
                num_time = int(t_life*365*24)
                num_device = len(downtime)
                for i in range(num_device):
                    availability_timebase.append((num_time-downtime[i])
                        /num_time)
                self.output_availability["availability_tb"] =\
                 availability_timebase
                self.output_availability["availability_array"] = \
                  sum(self.output_availability["availability_tb"])/\
                    len(self.output_availability["availability_tb"])
            else:
                raise ValueError
        except ValueError:
            print('The donwtime contains zero entry!')        
        return self.output_availability
