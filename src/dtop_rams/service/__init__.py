###                                                                       ###
#  DTOceanPlus -- Module of Reliability, Availability, Maintainability and  #
#                              Survivability (RAMS)                         #
###                                                                       ### 
# This function is used to create an object of a project. 

import os
from flask import Flask, request, jsonify
from flask_babel import Babel
from flask_cors import CORS
from dtop_rams.service.config import Config


babel = Babel()

def create_app(test_config=None):

    # creates an app
    app = Flask(__name__)
    app.config.from_object(Config)
    
    CORS(app)
    babel.init_app(app)
	
    from . import db
    db.init_app(app)

    # register blue print - check the status of input data
    from . import api as api_core
    app.register_blueprint(api_core.bp, url_prefix='/')
	
    from .api import assessment
    app.register_blueprint(assessment.bp, url_prefix='/') 

    from .api import representation
    app.register_blueprint(representation.bp)

    if os.environ.get('FLASK_ENV') == 'development':
        from .api.integration import provider_states
        app.register_blueprint(provider_states.bp)

    return app
    
@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])
