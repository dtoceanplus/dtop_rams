import sqlite3
import click
from flask import current_app, g
from flask.cli import with_appcontext
from dtop_rams.service.models import db


def close_db(e=None):

    db = g.pop("db", None)

    if db is not None:
        db.close()


def init_db():
    
    db.drop_all()
    db.create_all()


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


def init_app(app):

    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)

    db.init_app(app)
