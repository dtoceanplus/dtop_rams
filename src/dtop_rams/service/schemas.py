from dtop_rams.service.models import RamsProject, ResultsRQ , Results

from flask_marshmallow import Marshmallow
ma = Marshmallow()

class ProjectSchema(ma.ModelSchema):  
    class Meta:
        model = RamsProject
project_schema = ProjectSchema()


class ResultsSchemaRQ(ma.ModelSchema):
    class Meta:
        model = ResultsRQ
results_schema_rq = ResultsSchemaRQ()


class ResultsSchema(ma.ModelSchema):
    class Meta:
        model = Results
results_schema = ResultsSchema()