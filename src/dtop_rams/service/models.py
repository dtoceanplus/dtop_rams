from sqlalchemy.ext import mutable
from flask_sqlalchemy import SQLAlchemy
import json


db = SQLAlchemy()


class JsonEncodedDict(db.TypeDecorator):
    """Decode and encode the json data"""
    impl = db.Text

    def process_bind_param(self, value, dialect):
        if value is None:
            return '{}'
        else:
            return json.dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return {}
        else:
            return json.loads(value) 

mutable.MutableDict.associate_with(JsonEncodedDict)


class RamsProject(db.Model):

    __tablename__ = 'project'
    # id refers to the current RAMS project/application
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    desc = db.Column(db.String(500))
    complexity = db.Column(db.Integer)
    status = db.Column(db.Integer)
    tags = db.Column(db.String(50))
    lifetime = db.Column(db.Integer)
    # The following fields used to store the actual json data
    cov_a = db.Column(db.Float)
    cov_l = db.Column(db.Float)
    cov_q = db.Column(db.Float)
    cov_r = db.Column(db.Float)
    cov_ufl = db.Column(db.Float)
    cov_ufr = db.Column(db.Float)
    downtime = db.Column(db.Float)
    hierarchy_ed = db.Column(JsonEncodedDict, default={})
    hierarchy_et = db.Column(JsonEncodedDict, default={})
    hierarchy_sk = db.Column(JsonEncodedDict, default={})
    mu_ufl = db.Column(db.Float)
    mu_ufr = db.Column(db.Float)  
    n_sim_fls = db.Column(db.Integer)
    n_sim_uls = db.Column(db.Integer)  
    n_sim_reliability = db.Column(db.Integer)
    option_fls = db.Column(db.String(200)) 
    option_uls = db.Column(db.String(200)) 
    pd_a = db.Column(db.String(200))
    pd_h = db.Column(db.String(200))
    pd_l = db.Column(db.String(200))  
    pd_m = db.Column(db.String(200))  
    pd_n = db.Column(db.String(200))
    pd_q = db.Column(db.String(200))   
    pd_r = db.Column(db.String(200)) 
    pd_t_repair = db.Column(db.String(200)) 
    pd_ufl = db.Column(db.String(200)) 
    pd_ufr = db.Column(db.String(200))   
    std_t_repair = db.Column(db.Float)
    stress_sk = db.Column(JsonEncodedDict, default={})
    stress_et = db.Column(JsonEncodedDict, default={})
    t_ava_repair = db.Column(db.Float)
    lmo_downtime = db.Column(JsonEncodedDict, default={})  
    lmo_maintenance = db.Column(JsonEncodedDict, default={})  
    results = db.relationship('Results',
                           backref='RamsProject',
                           cascade="all, delete-orphan")
    results_rq = db.relationship('ResultsRQ',
                           backref='RamsProject',
                           cascade="all, delete-orphan")
    
    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class ResultsRQ(db.Model):
    __tablename__ = 'results_rq'
    id = db.Column(db.Integer, primary_key=True) 
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False) 
    reliability_system = db.Column(JsonEncodedDict, default={})
    survivability_uls = db.Column(JsonEncodedDict, default={}) 
    survivability_fls = db.Column(JsonEncodedDict, default={})

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                setattr(self, key, value)


class Results(db.Model):
    __tablename__ = 'results'
    id = db.Column(db.Integer, primary_key=True) 
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    reliability_component = db.Column(JsonEncodedDict, default={})  
    availability = db.Column(JsonEncodedDict, default={}) 
    maintainability = db.Column(JsonEncodedDict, default={}) 

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key) and key!='id':
                setattr(self, key, value)


class Task(db.Model):
    __tablename__ = 'tasks'
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    rq_job_id = db.Column(db.String, nullable=False)
