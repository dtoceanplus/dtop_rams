from os import environ, path


basedir = path.abspath(path.dirname(__file__))


class Config:
    """Set Flask configuration vars from .env file."""

    ###### General Config
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_APP = environ.get('FLASK_APP')
    FLASK_ENV = environ.get('FLASK_ENV')

    # Database
    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URL') or \
        'sqlite:///' + path.join(basedir, '..', '..', 'rams_db', 'rams.db')
    # SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URL') 
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    REDIS_URL = environ.get("REDIS_URL", "redis://redis:6379/0")
    QUEUES = ["default"]