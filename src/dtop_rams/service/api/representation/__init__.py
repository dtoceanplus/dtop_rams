import redis
import rq
from flask import Blueprint, current_app, jsonify, make_response, request
from dtop_rams.service.models import *
from dtop_rams.service.schemas import *
from dtop_rams.service import config
from dtop_rams.service.api.assessment.save_results import *
from dtop_rams import business

bp = Blueprint('representation', __name__)
headers = { 'Access-Control-Allow-Headers': 'Content-Type' }

@bp.route('/representation/<id>', methods=['GET'])
def get_representation(id):
    try: 
        if (RamsProject.query.get(id) is None):
            raise IndexError
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404
    
    try:
        id = int(id)
        project = RamsProject.query.get(id)
        # get the results internally
        results = dict()
        results["reliability_component"] = {}
        results["availability"] = {}
        results["maintainability"] = {}
        results["reliability_system"] = {}
        results["survivability_uls"] = {}
        results["survivability_fls"] = {}

        if not project.results:
            pass
        else:
            # records = [el for el in project.results if el.project_id==id]
            for row in project.results:
                if row.reliability_component != {}:
                    results["reliability_component"] = row.reliability_component
                if row.availability != {}:
                    results["availability"] = row.availability
                if row.maintainability != {}:
                    results["maintainability"] = row.maintainability

        if not project.results_rq:
            pass
        else:
            # records_rq = [el for el in project.results_rq if el.project_id==id]
            for row in project.results_rq:
                if row.reliability_system != {}:
                    results["reliability_system"] = row.reliability_system
                if row.survivability_uls != {}:
                    temp_dict = dict()
                    temp_dict["ET_Subsystem"] = row.survivability_uls["ET Subsystem"]
                    temp_dict["SK_Subsystem"] = row.survivability_uls["SK Subsystem"]
                    results["survivability_uls"] = temp_dict
                if row.survivability_fls != {}:
                    temp_dict = dict()
                    temp_dict["ET_Subsystem"] = row.survivability_fls["ET Subsystem"]
                    temp_dict["SK_Subsystem"] = row.survivability_fls["SK Subsystem"]
                    results["survivability_fls"] = temp_dict 
        
        # Dump the results in the format of digital representation 
        results_dr = dict()
        # RAMS results in 'array'
        results_dr['array'] = dict()
        results_dr['array']['assessment'] = dict()
        results_dr['array']['assessment']['availability'] = dict()
        results_dr['array']['assessment']['maintainability'] = dict()
        results_dr['array']['assessment']['max_pof_annual'] = dict()
        results_dr['array']['assessment']['max_ttf'] = dict()
        results_dr['array']['assessment']['mttf'] = dict()
        results_dr['array']['assessment']['std_ttf'] = dict()
        results_dr['array']['assessment']['availability']['description'] = "the availaiblity of the array" 
        results_dr['array']['assessment']['availability']['value'] = results['availability']['availability_array']
        results_dr['array']['assessment']['availability']['unit'] = "-" 
        results_dr['array']['assessment']['availability']['origin'] = "RAMS" 
        results_dr['array']['assessment']['max_pof_annual']['description'] = "the maximum annual probability of failure of the array"
        results_dr['array']['assessment']['max_pof_annual']['value'] = results['reliability_system']["max_annual_pof_array"]
        results_dr['array']['assessment']['max_pof_annual']['unit'] = "-" 
        results_dr['array']['assessment']['max_pof_annual']['origin'] = "RAMS" 
        results_dr['array']['assessment']['max_ttf']['description'] = "the maximum time to failure of the array"
        results_dr['array']['assessment']['max_ttf']['value'] = results['reliability_system']["max_ttf_array"]
        results_dr['array']['assessment']['max_ttf']['unit'] = "-" 
        results_dr['array']['assessment']['max_ttf']['origin'] = "RAMS" 
        results_dr['array']['assessment']['mttf']['description'] = "the mean time to failure of the array"
        results_dr['array']['assessment']['mttf']['value'] = results['reliability_system']["mttf_array"]
        results_dr['array']['assessment']['mttf']['unit'] = "-" 
        results_dr['array']['assessment']['mttf']['origin'] = "RAMS" 
        results_dr['array']['assessment']['std_ttf']['description'] = "the standard deviation of the time to failure of the array"
        results_dr['array']['assessment']['std_ttf']['value'] = results['reliability_system']["std_ttf_array"]
        results_dr['array']['assessment']['std_ttf']['unit'] = "-" 
        results_dr['array']['assessment']['std_ttf']['origin'] = "RAMS"
        results_dr['array']['assessment']['maintainability']['description'] = "the maintainability of the array"
        results_dr['array']['assessment']['maintainability']['value'] = results['maintainability']['probability_maintenance']
        results_dr['array']['assessment']['maintainability']['unit'] = "-" 
        results_dr['array']['assessment']['maintainability']['origin'] = "RAMS"
        # RAMS results in 'device'
        results_dr['device'] = dict()
        results_dr['device']['assessment'] = dict()
        results_dr['device']['assessment']['availability'] = dict()
        results_dr['device']['assessment']['availability']['description'] = "the availaiblity of the devices"  
        results_dr['device']['assessment']['availability']['value'] = results['availability']['availability_tb']        
        results_dr['device']['assessment']['availability']['unit'] = "-" 
        results_dr['device']['assessment']['availability']['origin'] = "RAMS"
        # RAMS results in 'power_transmission'
        results_dr['power_transmission'] = dict()
        results_dr['power_transmission']['assessment'] = dict()         
        results_dr['power_transmission']['assessment']['max_pof_annual'] = dict()
        results_dr['power_transmission']['assessment']['max_ttf'] = dict()
        results_dr['power_transmission']['assessment']['mttf'] = dict()
        results_dr['power_transmission']['assessment']['std_ttf'] = dict()
        results_dr['power_transmission']['assessment']['max_pof_annual']['description'] = "the maximumannual probability of failure of the power transmission subsystem (ED)"
        results_dr['power_transmission']['assessment']['max_pof_annual']['value'] = results['reliability_system']["max_annual_pof_ed"]
        results_dr['power_transmission']['assessment']['max_pof_annual']['unit'] = '-'
        results_dr['power_transmission']['assessment']['max_pof_annual']['origin'] = 'RAMS'
        results_dr['power_transmission']['assessment']['max_ttf']['description'] = "the maximum time to failure of the power transmission subsystem (ED)"
        results_dr['power_transmission']['assessment']['max_ttf']['value'] = results['reliability_system']["max_ttf_ed"]
        results_dr['power_transmission']['assessment']['max_ttf']['unit'] = '-'
        results_dr['power_transmission']['assessment']['max_ttf']['origin'] = 'RAMS'            
        results_dr['power_transmission']['assessment']['mttf']['description'] = "the mean time to failure of the power transmission subsystem (ED)"
        results_dr['power_transmission']['assessment']['mttf']['value'] = results['reliability_system']["mttf_ed"]
        results_dr['power_transmission']['assessment']['mttf']['unit'] = '-'
        results_dr['power_transmission']['assessment']['mttf']['origin'] = 'RAMS' 
        results_dr['power_transmission']['assessment']['std_ttf']['description'] = "the standard deviation of the time to failure of the power transmission subsystem (ED)"
        results_dr['power_transmission']['assessment']['std_ttf']['value'] = results['reliability_system']["std_ttf_ed"]
        results_dr['power_transmission']['assessment']['std_ttf']['unit'] = '-'
        results_dr['power_transmission']['assessment']['std_ttf']['origin'] = 'RAMS'  
        # RAMS results in 'pto'
        results_dr['pto'] = dict()
        results_dr['pto']['assessment'] = dict()         
        results_dr['pto']['assessment']['max_pof_annual'] = dict()
        results_dr['pto']['assessment']['max_ttf'] = dict()
        results_dr['pto']['assessment']['mttf'] = dict()
        results_dr['pto']['assessment']['std_ttf'] = dict()
        results_dr['pto']['assessment']['survivability_uls'] = dict()
        results_dr['pto']['assessment']['survivability_fls'] = dict()
        results_dr['pto']['assessment']['max_pof_annual']['description'] = "the maximum annual probability of failure of the energy transformation subsystem (ET)"
        results_dr['pto']['assessment']['max_pof_annual']['value'] = results['reliability_system']["max_annual_pof_et"] 
        results_dr['pto']['assessment']['max_pof_annual']['unit'] = '-'
        results_dr['pto']['assessment']['max_pof_annual']['origin'] = 'RAMS'           
        results_dr['pto']['assessment']['max_ttf']['description'] = "the maximum time to failure of the energy transformation subsystem (ET)"
        results_dr['pto']['assessment']['max_ttf']['value'] = results['reliability_system']["max_ttf_et"] 
        results_dr['pto']['assessment']['max_ttf']['unit'] = '-'
        results_dr['pto']['assessment']['max_ttf']['origin'] = 'RAMS' 
        results_dr['pto']['assessment']['mttf']['description'] = "the mean time to failure of the energy transformation subsystem (ET)"
        results_dr['pto']['assessment']['mttf']['value'] = results['reliability_system']["mttf_et"] 
        results_dr['pto']['assessment']['mttf']['unit'] = '-'
        results_dr['pto']['assessment']['mttf']['origin'] = 'RAMS' 
        results_dr['pto']['assessment']['std_ttf']['description'] = "the standard deviation of the time to failure of the energy transformation subsystem (ET)"
        results_dr['pto']['assessment']['std_ttf']['value'] = results['reliability_system']["std_ttf_et"] 
        results_dr['pto']['assessment']['std_ttf']['unit'] = '-'
        results_dr['pto']['assessment']['std_ttf']['origin'] = 'RAMS' 
        results_dr['pto']['assessment']['survivability_uls']['description'] = "the probability that the energy transformation subsystem survies the ultimate loads"
        results_dr['pto']['assessment']['survivability_uls']['value'] = results['survivability_uls']["ET_Subsystem"]["survival_uls"]
        results_dr['pto']['assessment']['survivability_uls']['unit'] = '-'
        results_dr['pto']['assessment']['survivability_uls']['origin'] = 'RAMS'
        results_dr['pto']['assessment']['survivability_fls']['description'] = "the probability that the energy transformation subsystem survies the fatigue loads"
        results_dr['pto']['assessment']['survivability_fls']['value'] = results['survivability_fls']["ET_Subsystem"]["survival_fls"]
        results_dr['pto']['assessment']['survivability_fls']['unit'] = '-'
        results_dr['pto']['assessment']['survivability_fls']['origin'] = 'RAMS'
        # RAMS results in 'station_keeping'
        results_dr['station_keeping'] = dict()
        results_dr['station_keeping']['assessment'] = dict()  
        results_dr['station_keeping']['assessment']['max_pof_annual'] = dict()
        results_dr['station_keeping']['assessment']['max_ttf'] = dict()
        results_dr['station_keeping']['assessment']['mttf'] = dict()
        results_dr['station_keeping']['assessment']['std_ttf'] = dict()       
        results_dr['station_keeping']['assessment']['survivability_uls'] = dict()
        results_dr['station_keeping']['assessment']['survivability_fls'] = dict()
        results_dr['station_keeping']['assessment']['max_pof_annual']['description'] = "the maximum annual probability of failure of the SK subsystem (SK)"
        results_dr['station_keeping']['assessment']['max_pof_annual']['value'] = results['reliability_system']["max_annual_pof_sk"] 
        results_dr['station_keeping']['assessment']['max_pof_annual']['unit'] = '-'
        results_dr['station_keeping']['assessment']['max_pof_annual']['origin'] = 'RAMS'
        results_dr['station_keeping']['assessment']['max_ttf']['description'] = "the maximum time tofailure of the station keeping subsystem (SK)"
        results_dr['station_keeping']['assessment']['max_ttf']['value'] = results['reliability_system']["max_ttf_sk"] 
        results_dr['station_keeping']['assessment']['max_ttf']['unit'] = '-'
        results_dr['station_keeping']['assessment']['max_ttf']['origin'] = 'RAMS'
        results_dr['station_keeping']['assessment']['mttf']['description'] = "the mean time to failure of the station keeping subsystem (SK)"
        results_dr['station_keeping']['assessment']['mttf']['value'] = results['reliability_system']["mttf_sk"] 
        results_dr['station_keeping']['assessment']['mttf']['unit'] = '-'
        results_dr['station_keeping']['assessment']['mttf']['origin'] = 'RAMS'
        results_dr['station_keeping']['assessment']['std_ttf']['description'] = "the standard deviation of the time to failure of the station keeping subsystem (SK)"
        results_dr['station_keeping']['assessment']['std_ttf']['value'] = results['reliability_system']["std_ttf_sk"] 
        results_dr['station_keeping']['assessment']['std_ttf']['unit'] = '-'
        results_dr['station_keeping']['assessment']['std_ttf']['origin'] = 'RAMS'
        results_dr['station_keeping']['assessment']['survivability_uls']['description'] = "the probability that the station keeping subsystem survies the ultimate loads"
        results_dr['station_keeping']['assessment']['survivability_uls']['value'] = results['survivability_uls']["SK_Subsystem"]["survival_uls"]
        results_dr['station_keeping']['assessment']['survivability_uls']['unit'] = '-'
        results_dr['station_keeping']['assessment']['survivability_uls']['origin'] = 'RAMS'
        results_dr['station_keeping']['assessment']['survivability_fls']['description'] = "the probability that the station keeping subsystem survies the fatigue loads"
        results_dr['station_keeping']['assessment']['survivability_fls']['value'] = results['survivability_fls']["SK_Subsystem"]["survival_fls"]
        results_dr['station_keeping']['assessment']['survivability_fls']['unit'] = '-'
        results_dr['station_keeping']['assessment']['survivability_fls']['origin'] = 'RAMS'

        return jsonify(results_dr), 200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


# @bp.route('rams/representation', methods=['POST'])
# def post_representation(id):
#     try: 
#         if (RamsProject.query.get(id) is None):
#             raise TypeError
#         else:
#             project = RamsProject.query.get(id)
#             rams_dr = project.rams_dr
#             return jsonify(rams_dr), 200
#     except TypeError: 
#         return jsonify("The RAMS DR is emtpy!"), 400
