
from flask import Blueprint, render_template, request, url_for, jsonify, make_response
import os
from bson.objectid import ObjectId
import json
import sqlite3
from dtop_rams.service import db, config
from dtop_rams.service.models import *
from dtop_rams.service.schemas import *
import requests


# define a blue print
bp = Blueprint("rams_server", __name__)
headers = {'Access-Control-Allow-Headers': 'Content-Type'}


def get_url_and_headers(module):    
    protocol = os.getenv('DTOP_PROTOCOL', 'http')
    domain = os.environ['DTOP_DOMAIN']
    auth = os.getenv('DTOP_AUTH','')    

    header_auth = f'{auth}'
    headers = {}
    headers['Authorization'] = header_auth
    header_host = f'{module + "." + domain}'

    server_url = f'{protocol + "://" + header_host}'
    try:
        response = requests.get(server_url, headers=headers)
        module_api_url = f'{server_url}'
    except requests.ConnectionError as exception:
        docker_ws_ip = "http://172.17.0.1:80"
        module_api_url = f'{docker_ws_ip}'
        headers['Host'] = header_host

    return module_api_url, headers


def make_request(url, headers=None, type='GET'):
    try:
        response = requests.request(type, url, headers=headers)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err1:
        return jsonify({"status": "error", "message": f"Http Error: {err1}"}), 400
    except requests.exceptions.ConnectionError as err2:
        return jsonify({"status": "error", "message": f"Conection Error: {err2}"}), 400
    except requests.exceptions.Timeout as err3:
        return jsonify({"status": "error", "message": f"Timeout Error: {err3}"}), 400
    except Exception as e:
        return jsonify({"status": "error", "message": f"Uncaught Error: {str(e)}"}), 400

    return response.json(), 200


@bp.route('/rams', methods=['GET'])
def get_all_inputs():
    """gets all the active RAMS studies/projects/entities.

    Args:
        N.A.
    
    Returns:
        projects_schema (dict): The returning body containing the 
            information of all the RAMS studies/projects/entities or
            a HTTP 404/400 error.

    """

    try:
        all_projects = RamsProject.query.all()
        projects_schema = ProjectSchema(many=True)
        return make_response(jsonify(projects_schema.dump(all_projects)), 200,
                             headers)
    except:
        return jsonify({'message': 'RAMS inputs could not be loaded from DB!'}), 404


@bp.route('/rams', methods=['POST'])
def create_project():
    """Creates a RAMS RAMS study/project/entity.

    Args:
        N.A.
    
    Returns:
        new_schema (dict): The returned body containing the 
            created RAMS study/project/entity or a HTTP 404/400 error.

    """
    
    try:
        rams_prj = request.get_json()
        new_instance = RamsProject(**rams_prj)
        db.session.add(new_instance)
        db.session.commit()
        new_schema = ProjectSchema()
        return jsonify(new_schema.dump(new_instance)), 201  

    except:
        return jsonify({'message': 'RAMS inputs could not be saved to DB!'}), 400
            

@bp.route('/rams/<id>', methods=['GET'])
def get_single_project(id):
    """Flask blueprint route for getting a specific RAMS study/project/entity.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The returned body containing the 
            inputs for the current RAMS study or a HTTP 400 error.

    """

    try:
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        else:
            return jsonify(title = project.title,
                           desc = project.desc,
                           complexity = project.complexity,
                           status = project.status,
                           tags = project.tags,
                           lifetime = project.lifetime,
                           cov_a = project.cov_a,
                           cov_l = project.cov_l,
                           cov_q = project.cov_q,
                           cov_r = project.cov_r,
                           cov_ufl = project.cov_ufl,
                           cov_ufr = project.cov_ufr,
                           mu_ufl = project.mu_ufl,
                           mu_ufr = project.mu_ufr,
                           n_sim_fls = project.n_sim_fls,
                           n_sim_uls = project.n_sim_uls,
                           n_sim_reliability = project.n_sim_reliability,
                           option_fls = project.option_fls,
                           option_uls = project.option_uls,
                           pd_a = project.pd_a,
                           pd_h = project.pd_h,
                           pd_l = project.pd_l,
                           pd_m = project.pd_m,
                           pd_n = project.pd_n,
                           pd_q = project.pd_q,
                           pd_r = project.pd_r,
                           pd_t_repair = project.pd_t_repair,                           
                           pd_ufl = project.pd_ufl,
                           pd_ufr = project.pd_ufr,  
                           std_t_repair = project.std_t_repair,
                           t_ava_repair = project.t_ava_repair,
                           hierarchy_ed = project.hierarchy_ed,
                           hierarchy_et = project.hierarchy_et,
                           hierarchy_sk = project.hierarchy_sk,
                           stress_sk = project.stress_sk,
                           stress_et = project.stress_et,
                           lmo_downtime = project.lmo_downtime,
                           lmo_maintenance = project.lmo_maintenance,
                           downtime = project.downtime
                           ), 200
    except IndexError:
        return jsonify({'message': 'The Id does not exist!'}), 404    


@bp.route('/rams/<id>', methods=['PUT'])
def update_specific_inputs(id):
    """Flask blueprint route for moidifying a specific RAMS study/project/entity.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The returned body containing the 
            inputs for the modified RAMS study or a HTTP 400 error.

    """

    try:
        project = RamsProject.query.filter_by(id=id).first()
        if project is None:
            raise IndexError
        body = request.get_json()
        project.update(**body)
        db.session.add(project)
        db.session.commit()

        return jsonify({'message': 'DB is updated!'}), 201
    except IndexError:
        return jsonify({'message': 'The Id does not exist!'}), 404        


@bp.route('/rams/<id>', methods=['DELETE'])
def delete_rams_project(id):
    """Flask blueprint route for deleting a specific RAMS study/project/entity.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (str): The message indicating the specified RAMS study has been
            deleted or a HTTP 400 error.

    """

    try:
        project = RamsProject.query.filter_by(id=id).first()

        db.session.delete(project)
        db.session.commit()

        return jsonify({'message': 'The specified RAMS project has been deleted.'}), 200

    except:
        return jsonify({'message': 'The specified RAMS project can not be deleted.'}), 404 


@bp.route('/rams/<id>/inputs/ed/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/ed/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/ed/complexity3', methods=['POST'])
def post_ed_inputs(id):
    """Flask blueprint route for creating the inputs for the Energy Delivery module (hierarchy).

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Example of an ED hierarchy:
    {
        "hierarchy_new": {
            "system": ["ED","ED","ED","ED","ED","ED","ED"],
            "name_of_node": ["ED Subsystem","ED1","Route1_1","2","0","3","1"],
            "node_type": ["System","System","Energy route","Component","Component","Component","Component"],
            "node_subtype": ["NA","NA","NA"," umbilical"," export"," wet-mate"," wet-mate"],
            "category": ["Level 3","Level 2","Level 1","Level 0","Level 0","Level 0","Level 0"],
            "child": [["ED1"],["Route1_1"],["3","2","1","0"],"NA","NA","NA","NA"],
            "design_id": ["NA","NA","NA","2","0","3","1"],
            "failure_rate_repair": ["NA","NA","NA",1.0364220051484658,114.24413966779242,5.42244,5.42244],
            "failure_rate_replacement": ["NA","NA","NA",1.0364220051484658,114.24413966779242,5.42244,5.42244],
            "gate_type": ["OR","OR","AND","NA","NA","NA","NA"],
            "parent": ["NA","NA",["ED1"],["Route1_1"],["Route1_1"],["Route1_1"],["Route1_1"]]
        }
    }

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError
        
        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()
            required_keys = ["system", "name_of_node", "design_id", "node_type", "node_subtype",\
                             "category", "parent", "child", "gate_type",\
                             "failure_rate_repair", "failure_rate_replacement"] 
            if not("entity_ed_id" in body): 
			    # standalone mode
                data = body
            else: 
                # integration mode
                ed_api_url, headers = get_url_and_headers("ed")
                entity_ed_id = body["entity_ed_id"]
                base_url = f'{ed_api_url}/api/energy-deliv-studies'

                data, status = make_request(f"{base_url}/{entity_ed_id}/results",
                                headers=headers, type='GET')

                if status != 200:
                    return data, status

            if not("hierarchy_new" in data):
                raise TypeError("The 'hierarchy_new' containing the actual hierarchy data is missing!")
            # check the basic format of the hierarchy
            n_keys = 0
            for i in range(len(required_keys)):
                if (required_keys[i] in data["hierarchy_new"]) and (isinstance(data["hierarchy_new"]      [required_keys[i]], list)):
                    n_keys = n_keys + 1
            if (n_keys == len(required_keys)):
                hierarchy_ed = dict()
                hierarchy_ed["system"] = data["hierarchy_new"]["system"]
                hierarchy_ed["name_of_node"] = data["hierarchy_new"]["name_of_node"]
                hierarchy_ed["design_id"] = data["hierarchy_new"]["design_id"]
                hierarchy_ed["node_type"] = data["hierarchy_new"]["node_type"]
                hierarchy_ed["node_subtype"] = data["hierarchy_new"]["node_subtype"]
                hierarchy_ed["category"] = data["hierarchy_new"]["category"]
                hierarchy_ed["parent"] = data["hierarchy_new"]["parent"]
                hierarchy_ed["child"] = data["hierarchy_new"]["child"]
                hierarchy_ed["gate_type"] = data["hierarchy_new"]["gate_type"]
                hierarchy_ed["failure_rate_repair"] = data["hierarchy_new"]["failure_rate_repair"]
                hierarchy_ed["failure_rate_replacement"] = data["hierarchy_new"]["failure_rate_replacement"]
            else: 
                raise TypeError("The hierarchy structure is incorrect! Please check the required keys in the hierarchy!")

            # check the logic dependency defined in the hierarchy
            unique_levels = list(set(hierarchy_ed["category"]))
            unique_level_n = []
            for k in range(len(unique_levels)):
                unique_level_n.append(int(unique_levels[k].replace("Level ", "")))
            highest_level = max(unique_level_n)
            for k in range(highest_level+1):
                level_name = f"Level {k}"
                if not (level_name in hierarchy_ed["category"]):
                    raise TypeError(f"Level {k} is missing in the key 'category' in the ED hierarchy!") 
            for i in range(len(hierarchy_ed["category"])):
                if hierarchy_ed["category"][i] != "Level 0":                    
                    temp1 = hierarchy_ed["child"][i]
                    for k in range(len(temp1)):
                        temp2 = hierarchy_ed["child"][i][k]
                        if not (temp2 in hierarchy_ed["name_of_node"]):
                            raise TypeError(f"The error in the key 'child' - the unit {temp2} is not found in the key 'name_of_node' in the ED hierarchy!") 
            for i in range(len(hierarchy_ed["category"])):
                temp1 = hierarchy_ed["parent"][i]
                if type(temp1) == list:
                    if temp1[0] != "NA":
                        if not (temp1[0] in hierarchy_ed["name_of_node"]):
                            raise TypeError(f"The error in the key 'parent' - the unit {temp1[0]} is not found in the key 'name_of_node' in the ED hierarchy!")
                else:
                    if temp1 != "NA":
                        if not (temp1 in hierarchy_ed["name_of_node"]):
                            raise TypeError(f"The error in the key 'parent' - the unit {temp1} is not found in the key 'name_of_node' in the ED hierarchy!") 

            instance = RamsProject.query.filter_by(id=id).first()
            instance.hierarchy_ed = hierarchy_ed        
            db.session.add(instance)
            db.session.commit()

            return jsonify("The ED hierarchy has been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400
    except IndexError: 
        return jsonify({'message': 'The ID does not exist!'}), 404


@bp.route('/rams/<id>/inputs/et/hierarchy/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/et/hierarchy/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/et/hierarchy/complexity3', methods=['POST'])
def post_et_inputs_hierarchy(id):
    """Flask blueprint route for creating the inputs from the Energy Transformation module (hierarchy).

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Example of an ET hierarchy:
    {"Hierarchy":{
        "value": {
        "system": ["ET","ET","ET","ET","ET","ET"],
        "name_of_node": ["Array_01","DEV_0","DEV_0_PTO_0_0","DEV_0_PTO_0_0_MechT","DEV_0_PTO_0_0_ElectT", "DEV_0_PTO_0_0_GridC"],
        "design_id": ["Array_01","Array_01","Array_01","Array_01","Array_01","Array_01"],
        "node_type": ["System","Device","PTO","Component","Component","Component"],
        "node_subtype": ["NA","NA","NA","NA","NA","NA"],
        "category": ["Level 3","Level 2","Level 1","Level 0","Level 0","Level 0"],
        "parent": ["NA","Array_01","DEV_0","DEV_0_PTO_0_0","DEV_0_PTO_0_0","DEV_0_PTO_0_0"],
        "child": [ ["DEV_0"], ["DEV_0_PTO_0_0"], ["DEV_0_PTO_0_0_MechT","DEV_0_PTO_0_0_ElectT", "DEV_0_PTO_0_0_GridC"],"NA","NA","NA"],
        "gate_type": ["AND","AND","AND","NA","NA","NA"],
        "failure_rate_repair": ["NA","NA","NA","NA","NA","NA"],
        "failure_rate_replacement": ["NA","NA","NA",2.517894594637399e-4,1.2815349288344602e-3,5. 994451644910028e-4]
        }
    }
    }

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError
        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()
            required_keys = ["system", "name_of_node", "design_id", "node_type", "node_subtype",\
                             "category", "parent", "child", "gate_type",\
                             "failure_rate_repair", "failure_rate_replacement"] 
            if not("entity_et_id" in body): 
			    # standalone mode
                data = body
            else: 
                # integration mode
                et_api_url, headers = get_url_and_headers("et")
                entity_et_id = body["entity_et_id"]
                base_url = f'{et_api_url}/energy_transf'

                data, status = make_request(f"{base_url}/{entity_et_id}/array",
                                headers=headers, type='GET')
                if status != 200:
                    return data, status

            if not("Hierarchy" in data):
                raise TypeError("The 'Hierarchy' containing the actual hierarchy data is missing!")

            n_keys = 0
            for i in range(len(required_keys)):
                if (required_keys[i] in data["Hierarchy"]["value"]) and (isinstance(data["Hierarchy"]["value"][required_keys[i]], list)):
                    n_keys = n_keys + 1
            
            if (n_keys == len(required_keys)):
                hierarchy_et = dict()
                hierarchy_et["system"] = data["Hierarchy"]["value"]["system"]
                hierarchy_et["name_of_node"] = data["Hierarchy"]["value"]["name_of_node"]
                hierarchy_et["design_id"] = data["Hierarchy"]["value"]["design_id"]
                hierarchy_et["node_type"] = data["Hierarchy"]["value"]["node_type"]
                hierarchy_et["node_subtype"] = data["Hierarchy"]["value"]["node_subtype"]
                hierarchy_et["category"] = data["Hierarchy"]["value"]["category"]
                hierarchy_et["parent"] = data["Hierarchy"]["value"]["parent"]
                hierarchy_et["child"] = data["Hierarchy"]["value"]["child"]
                hierarchy_et["gate_type"] = data["Hierarchy"]["value"]["gate_type"]
                hierarchy_et["failure_rate_repair"] = data["Hierarchy"]["value"]["failure_rate_repair"]
                hierarchy_et["failure_rate_replacement"] = data["Hierarchy"]["value"]["failure_rate_replacement"]    
            else:
                raise TypeError("The hierarchy structure is incorrect! Please check the required keys in the hierarchy!")

            # check the logic dependency defined in the hierarchy
            unique_levels = list(set(hierarchy_et["category"]))
            unique_level_n = []
            for k in range(len(unique_levels)):
                unique_level_n.append(int(unique_levels[k].replace("Level ", "")))
            highest_level = max(unique_level_n)
            for k in range(highest_level+1):
                level_name = f"Level {k}"
                if not (level_name in hierarchy_et["category"]):
                    raise TypeError(f"Level {k} is missing in the key 'category' in the ET hierarchy!") 
            for i in range(len(hierarchy_et["category"])):
                if hierarchy_et["category"][i] != "Level 0":                    
                    temp1 = hierarchy_et["child"][i]
                    for k in range(len(temp1)):
                        temp2 = hierarchy_et["child"][i][k]
                        if not (temp2 in hierarchy_et["name_of_node"]):
                            raise TypeError(f"The error in the key 'child' - the unit {temp2} is not found in the key 'name_of_node' in the ET hierarchy!") 
            for i in range(len(hierarchy_et["category"])):
                temp1 = hierarchy_et["parent"][i]
                if temp1 != "NA":
                    if type(temp1) == list:
                        if not (temp1[0] in hierarchy_et["name_of_node"]):
                            raise TypeError(f"The error in the key 'parent' - the unit {temp1[0]} is not found in 'name_of_node' in the ET hierarchy!")
                    else:
                        if not (temp1 in hierarchy_et["name_of_node"]):
                            raise TypeError(f"The error in the key 'parent' - the unit {temp1} is not found in 'name_of_node' in the ET hierarchy!") 
            project = RamsProject.query.filter_by(id=id).first()
            project.hierarchy_et = hierarchy_et        
            db.session.add(project)
            db.session.commit()
            return jsonify("The ET hierarchy has been saved to the database!"), 201  
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400

    except IndexError: 
        return jsonify({'message': 'The ID does not exist!'}), 404


@bp.route('/rams/<id>/inputs/et/stress/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/et/stress/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/et/stress/complexity3', methods=['POST'])
def post_et_inputs_stress(id):
    """Flask blueprint route for creating the inputs from the Energy Transformation module (stress).

    If id provided does not exist in the database, returns a HTTP 404 error. 
    
    Example of ET stress:
    {
        "S_N": {
            "description": "SN curve",
            "label": "SN curve energy",
            "unit": "-",
            "value": EachLike(EachLike(10.97))
        },
        "fatigue_stress_probability": {
            "description": "Fatigue Stress Probability",
            "label": "Fatigue Stress Probability",
            "unit": "-",
            "value": {
                "probability": EachLike(1.5206049678508205e-08),
                "stress": EachLike(1.6468789096280935e-05)
            }
        },
        "maximum_stress_probability": {
            "description": "Stress Probability",
            "label": "Maximum stress Probability",
            "unit": "-",
            "value": {
                "probability": EachLike(1.2132672269276997e-06),
                "stress": EachLike(1.6468789096280935e-05)
            }
        },
        "number_cycles": {
            "description": "Fatigue Stress Probability",
            "label": "NUme",
            "unit": "-",
            "value": EachLike(4.1399967481973125e+3)
        },
        "ultimate_stress": {
            "description": "Ultimate Stress",
            "label": "Ultimate stress",
            "unit": "-",
            "value": 503.9925969532528
        }
    }

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError
        
        try:
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()

            index_max = None
            if not("entity_et_id" in body): 
                # standalone mode
                data = body
            else:
                # integration mode
                et_api_url, headers = get_url_and_headers("et")
                entity_et_id = body["entity_et_id"]
                base_url = f'{et_api_url}/energy_transf'

                data_raw, status = make_request(f"{base_url}/{entity_et_id}/devices",
                                headers=headers, type='GET')
                if status != 200:
                    return data_raw, status
        
                indicator_stress = []
                for i in range(len(data_raw)):
                    if data_raw[i]["Dev_E_Grid"] is None:
                        raise TypeError("The field 'Dev_E_Grid' is missing!")
                    else:
                        indicator_stress.append(data_raw[i]["Dev_E_Grid"]["value"])
                
                index_max = indicator_stress.index(max(indicator_stress))

                data, status = make_request(f"{base_url}/{entity_et_id}/devices/{index_max}/ptos",
                                headers=headers, type='GET')
                if status != 200:
                    return data, status

            stress = data[0]
            if index_max is None:
                stress["id_critical"] = 0
            else:
                stress["id_critical"] = index_max

            if not ('S_N' in stress):
                raise TypeError("The field 'S_N' is missing!")
            if (stress["S_N"]["value"] is None) or (len(stress["S_N"]["value"]) == 0):
                raise TypeError("The S-N curve parameters (a and m) are missing!")
            if not('fatigue_stress_probability' in stress):
                raise TypeError("The field 'fatigue_stress_probability' is missing!")

            if stress["fatigue_stress_probability"]["value"]["stress"] is None:
                raise TypeError("The fatigue stress ranges are missing!")  
            if stress["fatigue_stress_probability"]["value"]["probability"] is None:
                raise TypeError("The probabilities corresponding to fatigue stress ranges are missing!")  
            
            if not('maximum_stress_probability' in stress):
                raise TypeError("The field 'maximum_stress_probability' is missing!")
            if stress["maximum_stress_probability"]["value"]["stress"] is None:
                raise TypeError("The maximum stresses are missing!")  
            if stress["maximum_stress_probability"]["value"]["probability"] is None:
                raise TypeError("The probabilities corresponding to maximum stresses are missing!")   
                       
            if not('number_cycles' in stress):
                raise TypeError("The field 'number_cycles' is missing!")
            if not('ultimate_stress' in stress):
                raise TypeError("The field 'ultimate_stress' is missing!")

            instance = RamsProject.query.filter_by(id=id).first()
            instance.stress_et = stress
            db.session.add(instance)
            db.session.commit()

            return jsonify("The ET stress has been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/sk/hierarchy/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/sk/hierarchy/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/sk/hierarchy/complexity3', methods=['POST'])
def post_sk_inputs_hierarchy(id):
    """Flask blueprint route for creating the inputs from the Staiton Keeping module (hierarchy).

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Example of an SK hierarchy:
    {
        "system": ["SK","SK","SK"],
        "name_of_node": ["SK0_x","SK0_x_pile_foundation1","SK0"],
        "design_id": ["NA","SK0_x_pile_foundation1","NA"],
        "node_type": ["System","Component","System"],
        "node_subtype": ["stationkeeping","foundation","stationkeeping"],
        "category": ["Level 1","Level 0","Level 2"],
        "parent": ["NA","SK0_x","NA"],
        "child": [["SK0_x_pile_foundation1"],["NA"],["SK0_x"]],
        "gate_type": ["AND","NA","AND"],
        "failure_rate_repair": ["NA",0.001876,"NA"],
        "failure_rate_replacement": ["NA",0.001876,"NA"]
    }

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        if RamsProject.query.get(id) is None:
            raise IndexError

        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()
            required_keys = ["system", "name_of_node", "design_id", "node_type", "node_subtype",\
                             "category", "parent", "child", "gate_type",\
                             "failure_rate_repair", "failure_rate_replacement"] 
            if not("entity_sk_id" in body): 
			    # standalone mode
                data = body
            else: 
                # integration mode
                sk_api_url, headers = get_url_and_headers("sk")
                entity_sk_id = body["entity_sk_id"]
                base_url = f'{sk_api_url}/sk'

                data, status = make_request(f"{base_url}/{entity_sk_id}/hierarchy",
                                headers=headers, type='GET')
                if status != 200:
                    return data, status

            n_keys = 0
            for i in range(len(required_keys)):
                if (required_keys[i] in data) and (isinstance(data[required_keys[i]], list)):
                    n_keys = n_keys + 1
		
            if (n_keys == len(required_keys)):
                hierarchy_sk = dict()
                hierarchy_sk["system"] = data["system"]
                hierarchy_sk["name_of_node"] = data["name_of_node"]
                hierarchy_sk["design_id"] = data["design_id"]
                hierarchy_sk["node_type"] = data["node_type"]
                hierarchy_sk["node_subtype"] = data["node_subtype"]
                hierarchy_sk["category"] = data["category"]
                hierarchy_sk["parent"] = data["parent"]
                hierarchy_sk["child"] = data["child"]
                hierarchy_sk["gate_type"] = data["gate_type"]
                hierarchy_sk["failure_rate_repair"] = data["failure_rate_repair"]
                hierarchy_sk["failure_rate_replacement"] = data["failure_rate_replacement"] 
            else:
                raise TypeError("The hierarchy structure is incorrect! Please check the required keys in the hierarchy!")                

            # check the logic dependency defined in the hierarchy
            unique_levels = list(set(hierarchy_sk["category"]))
            unique_level_n = []
            for k in range(len(unique_levels)):
                unique_level_n.append(int(unique_levels[k].replace("Level ", "")))
            highest_level = max(unique_level_n)
            for k in range(highest_level+1):
                level_name = f"Level {k}"
                if not (level_name in hierarchy_sk["category"]):
                    raise TypeError(f"Level {k} is missing in the key 'category' in the SK hierarchy!") 
            for i in range(len(hierarchy_sk["category"])):
                if hierarchy_sk["category"][i] != "Level 0":                    
                    temp1 = hierarchy_sk["child"][i]
                    for k in range(len(temp1)):
                        temp2 = hierarchy_sk["child"][i][k]
                        if not (temp2 in hierarchy_sk["name_of_node"]):
                            raise TypeError(f"The error in the key 'child' - the unit {temp2} is not found in  the key 'name_of_node' in the SK hierarchy!")
            for i in range(len(hierarchy_sk["category"])):
                temp1 = hierarchy_sk["parent"][i]
                if temp1 != "NA":
                    if type(temp1) == list:
                        if not (temp1[0] in hierarchy_sk["name_of_node"]):
                            raise TypeError(f"The error in the key 'parent' - the unit {temp1[0]} is not found in the key 'name_of_node' in the SK hierarchy!")
                    else:
                        if not (temp1 in hierarchy_sk["name_of_node"]):
                            raise TypeError(f"The error in the key 'parent' - the unit {temp1} is not found in the key 'name_of_node' in the SK hierarchy!") 

            instance = RamsProject.query.filter_by(id=id).first()
            instance.hierarchy_sk = hierarchy_sk
            db.session.add(instance)
            db.session.commit()

            return jsonify("The SK hierarchy has been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400 
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/sk/stress/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/sk/stress/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/sk/stress/complexity3', methods=['POST'])
def post_sk_inputs_stress(id):
    """Flask blueprint route for creating the inputs from the Staiton Keeping module (stress).

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Example of SK stress:
    {
        "devices": [
            {
                "uls_results": {
                    "mooring_tension": [[  [ [37435.35787709276,64419.65516736676]  ] ] ],
		            "tension_versus_mbl": [
                        [  [0.01751654091243178,0.017516540783813574,0.020824072813175522,02082407244702446] ]],
                    "mbl_uls": [9650000.0]        
		        },
		        "fls_results":{
		            "cdf_stress_range": [  [0.03530433733043322,0.004113311227466497,0.03575261836716770.049570761686637314] ],
                    "cdf": [ [0.07928831669580277,0.03422811116375578,0.09096998709297957,009096998709297957] ],
                    "ad": [60000000000.0],
                    "m": [3.0],
                    "n_cycles_lifetime": [95559948.11638647]
		          }}
                ],
        "master_structure": {
            "uls_results": {
                "mooring_tension": [[[  [0.0,245013.04723266925  ], ]  ]  ],
		        "mbl_uls": [9650000.0]
		    }}
    }
   
    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError 

        try:
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()
            if not("entity_sk_id" in body): 
                # standalone mode
                stress = body
            else:
                # integration mode
                sk_api_url, headers = get_url_and_headers("sk")
                entity_sk_id = body["entity_sk_id"]
                base_url = f'{sk_api_url}/sk'

                data, status = make_request(f"{base_url}/{entity_sk_id}/design_assessment",
                                headers=headers, type='GET')
                if status != 200:
                    return data, status
                stress = data

            if not ('devices' in stress):
                raise TypeError("The field 'devices' is missing!")

            if not isinstance(stress['devices'], list):
                raise TypeError("The data format of the field 'device' should be a list!")
            
            n_device = len(stress['devices'])

            for i in range(n_device):
                if not('uls_results' in stress['devices'][i]):
                    raise TypeError("The field uls_results is missing!")

                if not('mooring_tension' in stress['devices'][i]['uls_results']):
                    raise TypeError("The field mooring_tension is missing!")
                
                if stress['devices'][i]['uls_results']['mooring_tension'] == [[[[0]]]]: 
                    raise TypeError("The foundation is fixed. RAMS cannot assess this type of the staion keeping (SK) subsystem!")

                if not('mbl_uls' in stress['devices'][i]['uls_results']):
                    raise TypeError("The field mbl is missing!")
    
                if not('fls_results' in stress['devices'][i]):
                    raise TypeError("The field fls_results is missing!")

                if not('ad' in stress['devices'][i]['fls_results']):
                    raise TypeError("The field ad is missing!")
    
                if not('m' in stress['devices'][i]['fls_results']):
                    raise TypeError("The field m is missing!")                
    
                if not('n_cycles_lifetime' in stress['devices'][i]['fls_results']):
                    raise TypeError("The field n_cycles_lifetime is missing!")  
    
                if not('cdf' in stress['devices'][i]['fls_results']):
                    raise TypeError("The field cdf is missing!")  
    
                if not('cdf_stress_range' in stress['devices'][i]['fls_results']):
                    raise TypeError("The field cdf_stress_range is missing!")  

            instance = RamsProject.query.filter_by(id=id).first()
            instance.stress_sk = stress
            db.session.add(instance)
            db.session.commit()

            return jsonify("The SK stress has been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400  
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/lmo/downtime/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/lmo/downtime/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/lmo/downtime/complexity3', methods=['POST'])
def post_lmo_inputs_downtime(id):
    """Flask blueprint route for creating the inputs from the LMO module (downtime).

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Example of LMO downtime:
    {
        "project_life": 20,
        "n_devices": 1,
        "downtime": [
            {
                "device_id": "device_1",
                "downtime_table": {
                "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                "jan": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
                "feb": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,176.409],
                "mar": [0.0,0.0,0.0,0.0,95.7045,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,197.409,0.0,0.0,0.0,0.0,0.0,135.409,22.182],
                "apr": [0.0,0.0,0.0,0.0,3.591,62.295500000000004,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.182,0.0,0.0,0.0,0.0,0.0,63.182,0.0],
                "may": [0.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0],
                "jun": [0.0,0.0,0.0,24.7045,0.0,0.0,0.0,0.0,0.0,124.59100000000001,0.0,0.0,124.59100000000001,0.0,0.0,0.0,0.0,0.0,124.59100000000001,0.0,0.0],
                "jul": [0.0,0.0,0.0,37.591,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,105.40899999999999,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
                "aug": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,19.182,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
                "sep": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
                "oct": [0.0,0.0,83.29549999999999,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,0.0,0.0,0.0,0.0,184.59099999999998,0.0],
                "nov": [0.0,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,39.409,0.0,56.409,166.59099999999998,0.0,0.0],
                "dec": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,127.182,0.0,110.182,186.59099999999998,0.0,0.0]
            }
            }
        ]
    }

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError

        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()
            required_keys = ["year", "jan", "feb", "mar", "apr", "may", "jun", \
                             "jul", "aug", "sep", "oct", "nov", "dec"]
            
            if not("entity_lmo_id" in body): 
                # standalone mode
                data = body
            else:
                # integration mode
                lmo_api_url, headers = get_url_and_headers("lmo")
                entity_lmo_id = body["entity_lmo_id"]
                base_url = f'{lmo_api_url}/api'

                data, status = make_request(f"{base_url}/{entity_lmo_id}/phases/maintenance/downtime",
                                headers=headers, type='GET')
                if status != 200:
                    return data, status

            if not ('downtime' in data):
                raise TypeError("The field 'downtime' is missing!")
            else:
                downtime = data
            
            if len(downtime['downtime']) == 0:
                # indicator = False 
                raise TypeError("The field 'downtime' is empty!")
            else:
                for i in range(len(downtime['downtime'])):
                    for key1 in downtime['downtime'][i]['downtime_table'].keys(): # at the level of'device11'
                        n_keys = 0
                        for j in range(len(required_keys)):
                            if (required_keys[j] in downtime['downtime'][i]['downtime_table']) and \
                                (isinstance(downtime['downtime'][i]['downtime_table'][required_keys[j]],list)):
                                n_keys = n_keys + 1                        
                        if n_keys != len(required_keys):
                            indicator = False
                            break                   

            project = RamsProject.query.filter_by(id=id).first()
            project.lmo_downtime = downtime
            db.session.add(project)
            db.session.commit()

            return jsonify("The downtime has been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400 
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/lmo/maintenance/complexity1', methods=['POST'])
@bp.route('/rams/<id>/inputs/lmo/maintenance/complexity2', methods=['POST'])
@bp.route('/rams/<id>/inputs/lmo/maintenance/complexity3', methods=['POST'])
def post_lmo_inputs_maintenance(id):
    """Flask blueprint route for creating the inputs for the LMO module (maintenance data).

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Example of LMO maintenance data:
    {
        "technologies": ["ed_2"],
        "downtime": [23],
        "mttr": [15]
    }

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError

        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()

            if not("entity_lmo_id" in body): 
                # standalone mode
                data = body
            else:
                # integration mode
                lmo_api_url, headers = get_url_and_headers("lmo")
                entity_lmo_id = body["entity_lmo_id"]
                base_url = f'{lmo_api_url}/api'

                data, status = make_request(f"{base_url}/{entity_lmo_id}/phases/maintenance/plan",
                                headers=headers, type='GET')
                if status != 200:
                    return data, status
            
            if ("mttr" in data) and ("downtime" in data) and ("technologies" in data):
                if not isinstance(data['mttr'], list):
                    raise TypeError("The data format of 'mttr' should be list!")
                if all(x is None for x in data['mttr']):
                    raise TypeError("The key mttr only contains None/Null. The maintainability assessment cannot be performed!")
                if not isinstance(data['downtime'], list):
                    raise TypeError("The data format of 'downtime' should be list!")
                if not isinstance(data['technologies'], list):
                    raise TypeError("The data format of 'technologies' should be list!")
                maintenance = data
            else:
                raise TypeError("Please check 'mttr', 'downtime' and 'technologies' in the request body!")
                
            project = RamsProject.query.filter_by(id=id).first()
            project.lmo_maintenance = maintenance
            db.session.add(project)
            db.session.commit()

            return jsonify("The maintenance-related data have been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400       
    except IndexError: 
        return jsonify({'message': 'The ID does not exist!'}), 404


@bp.route('/rams/<id>/inputs/reliability/user_defined', methods=['POST']) 
@bp.route('/rams/<id>/inputs/reliability/user_defined', methods=['PUT']) 
def reliability_user_defined(id):
    """Flask blueprint route for creating/modifying the user-defined inputs for reliability assessment.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The message indicating a successful post or a HTTP 400 error.

    """

    # to obtain the user-defined variables (reliability)
    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError

        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()
            project = RamsProject.query.filter_by(id=id).first()
            if not ("n_sim_reliability" in body):
                raise TypeError("The field 'n_sim_reliability' is missing!")
            if not ("downtime" in body):
                raise TypeError("The field 'downtime' is missing!")
            if type(body['n_sim_reliability']) != int:
                raise TypeError("The data format of n_sim_reliability should be integar!")
            if type(body['downtime']) != float:
                try:
                    body['downtime'] = float(body['downtime'])
                except:
                    raise TypeError("The data format of downtime should be float!")
            if (body['n_sim_reliability'] < 2):
                raise ValueError("The simulaitn no. should not be less than 2!")
            project.n_sim_reliability = body['n_sim_reliability']    
            project.downtime = body['downtime'] 

            db.session.add(project)
            db.session.commit()

            return jsonify("The user-defined data for reliability assessment have been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400        
    except IndexError: 
        return jsonify({'message': 'The ID does not exist!'}), 404


@bp.route('/rams/<id>/inputs/reliability/user_defined', methods=['GET']) 
def get_reliability_user_defined(id):
    """Flask blueprint route for getting the user-defined inputs for reliability assessment.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The returning body containing the user-defined inputs
            for reliability assessment  or a HTTP 400 error.

    """

    # to obtain the user-defined variables (reliability)
    try:
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        else:
            new_schema = ProjectSchema()
            return jsonify(n_sim_reliability = project.n_sim_reliability,
                           downtime = project.downtime), 200         
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/maintainability/user_defined', methods=['POST']) 
@bp.route('/rams/<id>/inputs/maintainability/user_defined', methods=['PUT']) 
def maintainability_user_defined(id):
    """Flask blueprint route for creating/modifying the user-defined inputs maintainability assessment.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (spring): The message indicating a successful post or a HTTP 400 error.

    """

    # to post the user-defined variables (maintainability)
    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError

        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()

            if not ("pd_t_repair" in body):
                raise TypeError("'pd_t_repair'is missing!")
            if not ("std_t_repair" in body):
                raise TypeError("'std_t_repair'is missing!")            
            if not ("t_ava_repair" in body):
                raise TypeError("'t_ava_repair'is missing!")  

            if type(body["pd_t_repair"]) != str:
                raise TypeError("The data format of 'pd_t_repair' should be string!")
            if not(body["pd_t_repair"] in ["Gaussian", "LogNormal", "Exponential"]):
                raise TypeError("The probability distribution of the available time can only be Gaussian, LogNormal or Exponential!")                
            if type(body["std_t_repair"]) != float:
                try:
                    body['std_t_repair'] = float(body['std_t_repair'])
                except:
                    raise TypeError("The data format of 'std_t_repair' should be float!")
            if type(body["t_ava_repair"]) != float:
                try:
                    body['t_ava_repair'] = float(body['t_ava_repair'])
                except:
                    raise TypeError("The data format of 't_ava_repair' should be float!")

            project = RamsProject.query.filter_by(id=id).first()
            project.pd_t_repair = body['pd_t_repair'] 
            project.std_t_repair = body['std_t_repair']     
            project.t_ava_repair = body['t_ava_repair']
            if not (project.pd_t_repair in ['Gaussian', 'LogNormal', 'Exponential']):
                db.session.add(project)
                db.session.commit()
                raise NameError("There are only three options to choose from, namely Gaussian, LogNormal and Exponential!")
			
            db.session.add(project)
            db.session.commit()

            return jsonify("The user-defined data for maintainability assessment have been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400        
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/maintainability/user_defined', methods=['GET']) 
def get_maintainability_user_defined(id):
    """Flask blueprint route for getting the user-defined inputs for maintainability assessment.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The returning body containing the user-defined inputs
            for maintainability assessment  or a HTTP 400 error.

    """

    # to obtain the user-defined variables (maintainability)
    try:
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        else:
            new_schema = ProjectSchema()
            return jsonify(pd_t_repair = project.pd_t_repair,
                           std_t_repair = project.std_t_repair,
                           t_ava_repair = project.t_ava_repair), 200         
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/survivability/user_defined', methods=['POST']) 
@bp.route('/rams/<id>/inputs/survivability/user_defined', methods=['PUT']) 
def survivability_user_defined(id):
    """Flask blueprint route for creating/modifying the user-defined inputs survivability assessment.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (string): The message indicating a successful post or a HTTP 400 error.

    """

    # to post the user-defined variables (survivability)
    try:
        # to check the existence of the RAMS project with the specified id
        if RamsProject.query.get(id) is None:
            raise IndexError

        try: 
            if request.get_json() is None:
                raise TypeError("The request body is empty!")
            else:
                body = request.get_json()

            project = RamsProject.query.filter_by(id=id).first()
            complexity = project.complexity

            if not("cov_a" in body):
                try:
                    body['cov_a'] = float(body['cov_a'])
                except:
                    raise TypeError("The data format of cov_a should be float!")
            if not("cov_l" in body):
                try:
                    body['cov_l'] = float(body['cov_l'])
                except:
                    raise TypeError("The data format of cov_l should be float!")
            if not("cov_q" in body):
                try:
                    body['cov_q'] = float(body['cov_q'])
                except:
                    raise TypeError("The data format of cov_q should be float!")
            if not("cov_r" in body):
                try:
                    body['cov_r'] = float(body['cov_r'])
                except:
                    raise TypeError("The data format of cov_r should be float!")
            if not("cov_ufl" in body):
                try:
                    body['cov_ufl'] = float(body['cov_ufl'])
                except:
                    raise TypeError("The data format of cov_ufl should be float!")
            if not("cov_ufr" in body):
                try:
                    body['cov_ufr'] = float(body['cov_ufr'])
                except:
                    raise TypeError("The data format of cov_ufr should be float!")

            if not("mu_ufl" in body):
                try:
                    body['mu_ufl'] = float(body['mu_ufl'])
                except:
                    raise TypeError("The data format of mu_ufl should be float!")
            if not("mu_ufr" in body):
                try:
                    body['mu_ufr'] = float(body['mu_ufr'])
                except:
                    raise TypeError("The data format of mu_ufr should be float!")

            if not("n_sim_uls" in body):
                raise TypeError("The field n_sim_uls is missing!")
            if not("n_sim_fls" in body):
                raise TypeError("The field n_sim_fls is missing!")

            if not("option_fls" in body):
                raise TypeError("The field option_fls is missing!")
            if not("option_uls" in body):
                raise TypeError("The field option_uls is missing!")

            if not("pd_a" in body):
                raise TypeError("The field pd_a is missing!")
            if not("pd_h" in body):
                raise TypeError("The field pd_h is missing!")
            if not("pd_l" in body):
                raise TypeError("The field pd_l is missing!")
            if not("pd_m" in body):
                raise TypeError("The field pd_m is missing!")   
            if not("pd_n" in body):
                raise TypeError("The field pd_n is missing!")         
            if not("pd_q" in body):
                raise TypeError("The field pd_q is missing!")
            if not("pd_r" in body):
                raise TypeError("The field pd_r is missing!")
            if not("pd_ufl" in body):
                raise TypeError("The field pd_ufl is missing!")
            if not("pd_ufr" in body):
                raise TypeError("The field pd_ufr is missing!")

            if type(body["cov_a"]) != float:
                try:
                    body['cov_a'] = float(body['cov_a'])
                except:
                    raise TypeError("The data format of cov_a should be float!")
            if type(body["cov_l"]) != float:
                try:
                    body['cov_l'] = float(body['cov_l'])
                except:
                    raise TypeError("The data format of cov_l should be float!")
            if type(body["cov_q"]) != float:
                try:
                    body['cov_q'] = float(body['cov_q'])
                except:
                    raise TypeError("The data format of cov_q should be float!")
            if type(body["cov_r"]) != float:
                try:
                    body['cov_r'] = float(body['cov_r'])
                except:
                    raise TypeError("The data format of cov_r should be float!")
            if type(body["cov_ufl"]) != float:
                try:
                    body['cov_ufl'] = float(body['cov_ufl'])
                except:
                    raise TypeError("The data format of cov_ufl should be float!")
            if type(body["cov_ufr"]) != float:
                try:
                    body['cov_ufr'] = float(body['cov_ufr'])
                except:
                    raise TypeError("The data format of cov_ufr should be float!")

            if type(body["mu_ufl"]) != float:
                try:
                    body['mu_ufl'] = float(body['mu_ufl'])
                except:
                    raise TypeError("The data format of mu_ufl should be float!")
            if type(body["mu_ufr"]) != float:
                try:
                    body['mu_ufr'] = float(body['mu_ufr'])
                except:
                    raise TypeError("The data format of mu_ufr should be float!")

            if type(body["n_sim_uls"]) != int:
                raise TypeError("The data format of n_sim_uls should be integar!")
            if type(body["n_sim_fls"]) != int:
                raise TypeError("The data format of n_sim_fls should be integar!")

            if type(body["option_uls"]) != str:
                raise TypeError("The data format of option_uls should be string!")
            if type(body["option_fls"]) != str:
                raise TypeError("The data format of option_fls should be string!")

            if type(body["pd_a"]) != str:
                raise TypeError("The data format of pd_a should be string!")
            if type(body["pd_h"]) != str:
                raise TypeError("The data format of pd_h should be string!")
            if type(body["pd_l"]) != str:
                raise TypeError("The data format of pd_l should be string!")    
            if type(body["pd_m"]) != str:
                raise TypeError("The data format of pd_m should be string!")                    
            if type(body["pd_n"]) != str:
                raise TypeError("The data format of pd_n should be string!")
            if type(body["pd_q"]) != str:
                raise TypeError("The data format of pd_q should be string!")
            if type(body["pd_r"]) != str:
                raise TypeError("The data format of pd_r should be string!")
            if type(body["pd_ufl"]) != str:
                raise TypeError("The data format of pd_ulf should be string!")
            if type(body["pd_ufr"]) != str:
                raise TypeError("The data format of pd_ulr should be string!")

            if body["cov_a"] > 1.0:
                raise TypeError("The cofficient of a should be less than 1.0!")
            if body["cov_l"] > 1.0:
                raise TypeError("The cofficient of l should be less than 1.0!")
            if body["cov_q"] > 1.0:
                raise TypeError("The cofficient of q should be less than 1.0!")            
            if body["cov_r"] > 1.0:
                raise TypeError("The cofficient of r should be less than 1.0!")
            if body["cov_ufl"] > 1.0:
                raise TypeError("The cofficient of ufl should be less than 1.0!")
            if body["cov_ufr"] > 1.0:
                raise TypeError("The cofficient of ufr should be less than 1.0!")

            if (complexity == 1):
                if (body['option_fls'] != 'Monte Carlo' or body['option_uls'] != 'Monte Carlo'):
                    raise NameError("Monte Carlo is the only method for Complexity 1!")
            if (complexity == 2 or complexity == 3):
                if (body['pd_a'] != "Gaussian"):
                    raise NameError("Gaussian is the only option for log(a), if the option is FORM!")
                if ( body['pd_q'] != "LogNormal"):
                    raise NameError("LongNormal is the only option for q, if the option is FORM!")

            project.cov_a = body['cov_a']
            project.cov_l = body['cov_l']
            project.cov_q = body['cov_q']
            project.cov_r = body['cov_r']
            project.cov_ufl = body['cov_ufl']
            project.cov_ufr = body['cov_ufr']
            project.mu_ufl = body['mu_ufl']
            project.mu_ufr = body['mu_ufr']
            project.n_sim_fls = body['n_sim_fls']
            project.n_sim_uls = body['n_sim_uls']
            project.option_fls = body['option_fls']
            project.option_uls = body['option_uls']
            project.pd_a = body['pd_a']
            project.pd_h = body['pd_h']
            project.pd_l = body['pd_l']
            project.pd_m = body['pd_m']
            project.pd_n = body['pd_n']   
            project.pd_q = body['pd_q']                     
            project.pd_r = body['pd_r']
            project.pd_ufl = body['pd_ufl']
            project.pd_ufr = body['pd_ufr']                    

            db.session.add(project)
            db.session.commit()

            return jsonify("The user-defined data have been saved to the database!"), 201
        except Exception as e:
            return jsonify({"status": "error", 'message': str(e)}), 400         
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/inputs/survivability/user_defined', methods=['GET']) 
def get_survivability_user_defined(id):
    """Flask blueprint route for getting the user-defined inputs survivability assessment.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The returning body containing the user-defined inputs
            for survivability assessment or a HTTP 400 error.

    """

    # to obtain the user-defined variables (survivability)
    try:
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        else:
            new_schema = ProjectSchema()
            return jsonify(cov_a = project.cov_a,
                           cov_l = project.cov_l,
                           cov_q = project.cov_q,
                           cov_r = project.cov_r,
                           cov_ufl = project.cov_ufl,
                           cov_ufr = project.cov_ufr,
                           mu_ufl = project.mu_ufl,
                           mu_ufr = project.mu_ufr,
                           n_sim_fls = project.n_sim_fls,
                           n_sim_uls = project.n_sim_uls,
                           option_fls = project.option_fls,
                           option_uls = project.option_uls,
                           pd_a = project.pd_a,
                           pd_h = project.pd_h,
                           pd_l = project.pd_l,
                           pd_m = project.pd_m,
                           pd_n = project.pd_n,
                           pd_q = project.pd_q,
                           pd_r = project.pd_r,
                           pd_ufl = project.pd_ufl,
                           pd_ufr = project.pd_ufr), 200         
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404
