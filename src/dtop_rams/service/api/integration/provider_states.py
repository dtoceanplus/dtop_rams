from contextlib import suppress
import sqlalchemy.exc
from flask import Blueprint, request
import dtop_rams.service
from dtop_rams.service.db import init_db
from dtop_rams.service.models import RamsProject
from dtop_rams.service.schemas import *
from .data import rams_output, rams_prj
import requests
import json


bp = Blueprint("provider_states", __name__)


@bp.route("/provider_states/setup", methods=["POST"])
def provider_states_setup():
    """Flask blueprint route for setting up PACT provider tests."""

    init_db()
    consumer = request.json["consumer"]
    state = request.json["state"]

    if state == 'rams1 exists and has results summary':

        # with suppress(sqlalchemy.exc.IntegrityError):
        #     with db.session.begin_nested():
        #         project = ProjectSchema(id=1)
        #         db.session.add(project)

        response = requests.post(f'{request.url_root}rams', json=rams_prj)
        ramsId = response.get_json()['id']
        
        # response1 = requests.get(f'{request.url_root}rams/{ramsId}/reliability_component/20')
        # response2 = requests.get(f'{request.url_root}rams/{ramsId}/availability')
        # response3 = requests.get(f'{request.url_root}rams/{ramsId}/maintainability/12')
    
        # response = requests.post(f'{request.url_root}rams/{ramsId}/reliability_system/task/10', json={"id": 1})
        # response4 = requests.get(f'{request.url_root}rams/{ramsId}/reliability_system/1')
    
        # response = requests.post(f'{request.url_root}rams/{ramsId}/survivability_uls', json={'id': 2})
        # response5 = requests.get(f'{request.url_root}rams/{ramsId}/survivability_uls/2')
    
        # response = requests.post(f'{request.url_root}rams/{ramsId}/survivability_fls', json={'id': 3})
        # response3 = requests.get(f'{request.url_root}rams/{ramsId}/survivability_fls/3')

        # rams_assessment = requests.get(f"{request.url_root}rams/results_summary/1")

    return ""