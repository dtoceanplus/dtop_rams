from flask import Blueprint, current_app, jsonify, make_response, request
from dtop_rams.service.models import *
from dtop_rams.service.schemas import *
from dtop_rams.service import config
from dtop_rams.service.api.assessment.save_results import *
from dtop_rams import business
import math
import json
import os
import pandas as pd
import statistics
from itertools import chain
import numpy as np
import requests
import operator
from collections import OrderedDict
import redis
import rq


bp = Blueprint('rams_assessment', __name__)
headers = {'Access-Control-Allow-Headers': 'Content-Type'}


@bp.route('/rams/<id>/reliability_component/<int:lifetime>', methods=['GET'])
def get_reliability_component(id, lifetime:int):
    """Flask blueprint route for getting the compoment-level reliability assessment results.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
        lifetime (integer): The design lifetime (normally equal to the array design lifetime).
    
    Returns:
        Response (dict): The MTTF, maximum TTF and standard deviations of basic components
            or a HTTP 400 error.

    """

    try:
        # to check the existence of the specific project
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        complexity = project.complexity

        try:
            if (project.hierarchy_ed == {}) or (project.hierarchy_et == {}) or (project.hierarchy_sk == {}) or (project.hierarchy_ed is None) or (project.hierarchy_et is None) or (project.hierarchy_sk is None):
                raise TypeError
            else: 
                # to access the RAMS database to obtain the inputs and pre-processing
                app_rams = business.ArrayRams.calc_reliability(str(complexity))
                temp_hierarchy_ed = pd.DataFrame(project.hierarchy_ed).to_numpy()
                component_ids_ed, failure_rate_ed, level_ed, node_ed, shared_component_ed,     table_hierarchy_ed = app_rams.pre_process(temp_hierarchy_ed)
    
                app_rams = business.ArrayRams.calc_reliability(str(complexity))
                temp_hierarchy_et = pd.DataFrame(project.hierarchy_et).to_numpy()
                component_ids_et, failure_rate_et, level_et, node_et, shared_component_et,     table_hierarchy_et = app_rams.pre_process(temp_hierarchy_et)
    
                app_rams = business.ArrayRams.calc_reliability(str(complexity))
                temp_hierarchy_sk = pd.DataFrame(project.hierarchy_sk).to_numpy()
                component_ids_sk, failure_rate_sk, level_sk, node_sk, shared_component_sk,     table_hierarchy_sk = app_rams.pre_process(temp_hierarchy_sk)
    
            # to simulate the time series of failure events and save the results to DB
            reliability_ed = app_rams.sim_failure_event(component_ids_ed, failure_rate_ed, lifetime)
            reliability_et = app_rams.sim_failure_event(component_ids_et, failure_rate_et, lifetime)
            reliability_sk = app_rams.sim_failure_event(component_ids_sk, failure_rate_sk, lifetime)
            reliability_summary = dict()
      
            reliability_summary["ED_subsystem"] = reliability_ed
            reliability_summary["ET_subsystem"] = reliability_et
            reliability_summary["SK_subsystem"] = reliability_sk
            
            if db.session.query(Results).all() == []:
                result = Results(project_id=id)
            else:
                result = Results.query.filter_by(id=id).first() 

            result.reliability_component = reliability_summary
            
            db.session.add(result)
            db.session.commit()
    
            return jsonify(result.reliability_component), 200
        except TypeError:
            return jsonify({'message': 'Please firstly check the ED hierarchy, ET hierarchy and the SK hierarchy! If there is no error in these hierarchies, then check sim_failure_event.py!'}), 500
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404  


@bp.route('/rams/<id>/reliability_system/task/<int:lifetime>', methods=['POST'])
def add_task_reliability_system(id, lifetime:int):
    """Flask blueprint route for creating a system-level reliability assessment task.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
        lifetime (integer): The design lifetime (normally equal to the array design lifetime).
    
    Returns:
        Response (dict): The task id and status of the post task or a HTTP 400 error.

    """

    try:
        # to check the existence of the specific project
        project = RamsProject.query.get(id)
        complexity = project.complexity
        n_sim = project.n_sim_reliability
        downtime = project.downtime
        if project is None:
            raise IndexError("The Id does not exist!")
 
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404

    try:
        # to access the RAMS database to obtain the inputs and pre-processing
        app_rams = business.ArrayRams.calc_reliability(str(complexity))
        temp_hierarchy_ed = pd.DataFrame(project.hierarchy_ed).to_numpy()
        component_ids_ed, failure_rate_ed, level_ed, node_ed, shared_component_ed, table_hierarchy_ed = app_rams.pre_process(temp_hierarchy_ed)
        app_rams = business.ArrayRams.calc_reliability(str(complexity))
        temp_hierarchy_et = pd.DataFrame(project.hierarchy_et).to_numpy()
        component_ids_et, failure_rate_et, level_et, node_et, shared_component_et, table_hierarchy_et = app_rams.pre_process(temp_hierarchy_et)
        app_rams = business.ArrayRams.calc_reliability(str(complexity))
        temp_hierarchy_sk = pd.DataFrame(project.hierarchy_sk).to_numpy()
        component_ids_sk, failure_rate_sk, level_sk, node_sk, shared_component_sk, table_hierarchy_sk = app_rams.pre_process(temp_hierarchy_sk)
        tasks = Task.query.filter_by(project_id=id)
        for task in tasks:
            db.session.delete(task)
        db.session.commit()

        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            queue = rq.Queue(is_async=True)
            job = queue.enqueue(app_rams.pof_mttf, 
                  args = ([downtime], level_ed, level_et, level_sk, node_ed,
                          node_et, node_sk,n_sim, shared_component_ed, 
                          shared_component_et, shared_component_sk, lifetime, 
                          table_hierarchy_ed, table_hierarchy_et, table_hierarchy_sk),
                          job_timeout=6000)
            queue.enqueue(save_results_reliability, job.get_id(), depends_on=job)
            task = Task(project_id=id, rq_job_id=job.get_id())
            db.session.add(task)
            db.session.commit()
            response_object = {
                "status": "success",
                "data": {
                    "task_id": task.id,
                }
            }
        return jsonify(response_object), 202
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 500


@bp.route('/rams/<id>/reliability_system/<int:task_id>', methods=['GET'])
def get_reliability_system(id, task_id: int):
    """Flask blueprint route for getting the system-level reliability assessment results.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
        task_id (integer): The task id created in the POST method.
    
    Returns:
        Response (dict): The MTTF, maximum TTF and standard deviations of the Energy Delivery,
            Energy Transformation, Station Keeping subsystmes and the array; the maixmum annual
            probability of failure (PoF) of these subsystems and the array; or a HTTP 400 error.

    """

    if RamsProject.query.get(id) is None:
        raise IndexError("The Id does not exist!")

    task: Task = Task.query.filter_by(id=task_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)
            return jsonify({
                "task_id": task.id,
                "task_status": job.get_status(),
                "task_result": job.result,
            }), 200
        
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


@bp.route('/rams/<id>/availability', methods=['GET'])
def get_availability(id):
    """Flask blueprint route for getting the availability assessment results.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The time-based availability of devices and the array
            or a HTTP 400 error.

    """

    try:
        # to check the existence of the specific project
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        complexity = project.complexity

        try:
            # to obtain the downtime
            if (project.lmo_downtime == {}) or (project.lmo_downtime is None):
                raise TypeError
            else: 
                raw_data = project.lmo_downtime['downtime']
                for i in range(len(raw_data)):
                    data_downtime = raw_data[i]['downtime_table']
                    list_keys1 = []
                    list_keys2 = []
                    list_downtime = []
                    list_keys1.append(raw_data[i]['device_id'])
                    for key1 in data_downtime: 
                        temp1 = []
                        temp2 = []
                        # for key2 in data_downtime[key1]:
                        if (key1 != 'year'):
                            temp1.append(key1)
                            temp2.append(sum(data_downtime[key1]))
                        if (key1 == 'year'):
                            t_life = len(data_downtime[key1])
                        list_keys2.append(temp1)
                    list_downtime.append(sum(temp2))

            # to calculate and return the availability
            app_rams = business.ArrayRams.calc_availability(str(complexity))
            availability_summary = app_rams.calc_availability(list_keys1, list_downtime, t_life)

            if db.session.query(Results).all() == []:
                result = Results(project_id=id)
            else:
                result = Results.query.filter_by(id=id).first() 
            result.availability = availability_summary
            db.session.add(result)
            db.session.commit()

            return jsonify(result.availability), 200
        except TypeError:
            return jsonify({'message': 'Please first check lmo_downtime! If it is not empty, then check calc_availability.py!'}), 500
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/maintainability/<int:t_ava_repair>', methods=['GET'])
def get_maintainability(id, t_ava_repair):
    """Flask blueprint route for getting the maintainability assessment results.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
        t_ava_repair (float): The available time for repairing the damaged components.
    
    Returns:
        Response (dict): The minimal probability that the damaged components can be 
            successfully repaired within the given available time (t_ava_repair) 
            or a HTTP 400 error.

    """

    try:
        # to check the existence of the specific project
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        complexity = project.complexity

        # to obtain the required input data
        try:
            if (project.lmo_maintenance == {}) or (project.lmo_maintenance is None):
                raise TypeError
            else: 
                data_maintenance = project.lmo_maintenance
                std_t_repair = project.std_t_repair
                pd_t_repair = project.pd_t_repair
                list_mttr = data_maintenance['mttr']
                list_component = data_maintenance['technologies']
                list_std = []
                for i in range(len(list_component)):
                    list_std.append(std_t_repair)

            # to calculate and return the maintainability
            app_rams = business.ArrayRams.calc_maintainability(str(complexity))
            maintainability_summary = app_rams.calc_maintainability(list_component, 
            list_mttr, list_std, t_ava_repair, pd_t_repair)

            if db.session.query(Results).all() == []:
                result = Results(project_id=id)
            else:
                result = Results.query.filter_by(id=id).first()

            result.maintainability = maintainability_summary
            db.session.add(result)
            db.session.commit()
    
            return jsonify(result.maintainability), 200
        except:
            return jsonify(
                {'message': 'Please first check lmo_maintenance! If it is not empty, then check calc_maintainability.py!'}), 500
    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404


@bp.route('/rams/<id>/survivability_uls', methods=['POST'])
def add_task_survivability_uls(id):
    """Flask blueprint route for creating a ULS survivability assessment task.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The task id and status of the post or a HTTP 400 error.

    """

    try:
        project = RamsProject.query.get(id)
        if project is None:
            raise IndexError
        complexity = project.complexity

    except IndexError: 
        return jsonify({'message': 'The Id does not exist!'}), 404

    try:
        # general inputs
        cov_l = project.cov_l
        cov_r = project.cov_r
        cov_ufl = project.cov_ufl
        cov_ufr = project.cov_ufr
        mu_ufl = project.mu_ufl
        mu_ufr = project.mu_ufr
        n_sim = project.n_sim_uls
        option_uls = project.option_uls
        pd_l = project.pd_l 
        pd_r = project.pd_r
        pd_ufl = project.pd_ufl
        pd_ufr = project.pd_ufr
        std_ufl = cov_ufl * mu_ufl
        std_ufr = cov_ufr * mu_ufr
        
        # to obtain the required input data - SK subsystem
        result_sk = project.stress_sk
        device_number = len(result_sk['devices'])
        device_sk = []
        max_tension_sk = []
        for i in range(device_number):
            device_sk.append(f'Device{i}')
            temp1 = result_sk['devices'][i]['uls_results']['mooring_tension']
            temp_tension_sk = list(chain.from_iterable(list(chain.from_iterable
            (list(chain.from_iterable(temp1))))))
            max_tension_sk.append(max(temp_tension_sk))
        if ('master_structure' in result_sk):
            temp2 = result_sk['master_structure']['uls_results']['mooring_tension']
            temp_tension_sk = list(chain.from_iterable(list(chain.from_iterable
                (list(chain.from_iterable(temp2))))))
        max_tension_sk.append(max(temp_tension_sk))    
        mu_tension_sk = max(max_tension_sk)
        std_tension_sk = cov_l * mu_tension_sk
        device_id_sk = np.argmax(max_tension_sk)

        mbl_sk = []
        for i in range(device_number):
            device_sk.append(f'Device{i}')
            temp = result_sk['devices'][i]['uls_results']['mbl_uls']
            mbl_sk.append(max(temp))
        mu_mbl_sk = min(mbl_sk)
        std_mbl_sk = cov_r * mu_mbl_sk
        
        # to obtain the required input data - ET subsystem
        result_et = project.stress_et
        device_id_et = f'Device{result_et["id_critical"]}'
        mu_mbl_et = result_et['ultimate_stress']['value']
        std_mbl_et =  mu_mbl_et * cov_r
        uls_load_et = result_et['maximum_stress_probability']["value"]['stress']
        uls_prob_et = result_et['maximum_stress_probability']["value"]['probability']

        app_rams = business.ArrayRams.calc_survivability(str(complexity))
        tasks = Task.query.filter_by(project_id=id)
        for task in tasks:
            db.session.delete(task)
        db.session.commit()

        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            queue = rq.Queue(is_async=True)
            job = queue.enqueue(app_rams.survivability_uls, 
                  args = (device_id_et, device_id_sk, mu_mbl_et, mu_mbl_sk, 
                          mu_tension_sk, mu_ufl, mu_ufr, n_sim, std_mbl_et, 
                          std_mbl_sk, std_tension_sk, std_ufl, std_ufr, 
                          uls_load_et, uls_prob_et, option_uls, pd_l, pd_r, pd_ufl, 
                          pd_ufr, "ULS"),
                          job_timeout=6000)
            queue.enqueue(save_results_survivability_uls, job.get_id(), depends_on=job)
            task = Task(project_id=id, rq_job_id=job.get_id())
            db.session.add(task)
            db.session.commit()
            response_object = {
                "status": "success",
                "data": {
                    "task_id": task.id,
                }
            }
        return jsonify(response_object), 202
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 500


@bp.route('/rams/<id>/survivability_uls/<int:task_id>', methods=['GET'])
def get_survivability_uls(id, task_id: int):
    """Flask blueprint route for getting the ULS survivability assessment results.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
        task_id (integer): The task id created in the POST method.
    
    Returns:
        Response (dict): The probabilities that the Energy Transformation 
            and Station Keeping subsystems can survive the ultimate loads/stresses;
            or a HTTP 400 error.

    """

    if RamsProject.query.get(id) is None:
        raise IndexError("The Id does not exist!")

    task: Task = Task.query.filter_by(id=task_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)

            return jsonify({
                "task_id": task.id,
                "task_status": job.get_status(),
                "task_result": job.result,
            }), 200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


@bp.route('/rams/<id>/survivability_fls', methods=['POST'])
def add_task_survivability_fls(id):
    """Flask blueprint route for creating a FLS survivability assessment task.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
    
    Returns:
        Response (dict): The task ID and status of the post or a HTTP 400 error.

    """

    try:
        project = RamsProject.query.get(id) 
        if project is None:
            raise IndexError  
        complexity = project.complexity
    except IndexError:
        return jsonify({'message': 'The Id does not exist!'}), 404

    try: 
        # general inputs
        cov_a = project.cov_a
        cov_q = project.cov_q
        n_sim = project.n_sim_fls
        option_fls = project.option_fls
        pd_a = project.pd_a 
        pd_h = project.pd_h
        pd_m = project.pd_m
        pd_n = project.pd_n
        pd_q = project.pd_q
        # to obtain the required input data - SK subsystem
        result_sk = project.stress_sk
        device_number = len(result_sk['devices'])
        device_id_sk = []
        a = []
        m = []
        cycles_sk = []
        cdf = []
        stress_range = []
        fatigue_flag_sk = []
        for i in range(device_number):
            device_id_sk.append(f'Device{i}')
            a.append(result_sk['devices'][i]['fls_results']['ad'])
            m.append(result_sk['devices'][i]['fls_results']['m'])
            n_cycles = len(result_sk['devices'][i]['fls_results']
            ['n_cycles_lifetime'])
            fatigue_flag_sk.append(result_sk['devices'][i]['fls_results']['env_proba'][0])
            temp = []
            for k in range(n_cycles):
                temp.append(int(result_sk['devices'][i]['fls_results']
                ['n_cycles_lifetime'][k]))
            cycles_sk.append(temp)
            stress_range.append(result_sk['devices'][i]['fls_results']
            ['cdf_stress_range'])
            cdf.append(result_sk['devices'][i]['fls_results']['cdf'])

        # to obtain the required input data - ET subsystem
        result_et = project.stress_et
        device_id_et = f'Device{result_et["id_critical"]}'
        if result_et["S_N"]["value"] != [[0]]:
            mu_a_et = pow(10,result_et["S_N"]["value"][0][1])
            std_a_et = cov_a * mu_a_et
            m_et = result_et["S_N"]["value"][0][0]
        else:
            mu_a_et = -1.0
            std_a_et = -1.0
            m_et = -1.0
        cycles_et = sum(result_et['number_cycles']["value"])
        fls_load_et = result_et['fatigue_stress_probability']["value"]['stress']
        fls_prob_et = result_et['fatigue_stress_probability']["value"]['probability']

        # to perform the fatigue assessment
        app_rams = business.ArrayRams.calc_survivability(str(complexity))
        tasks = Task.query.filter_by(project_id=id)
        for task in tasks:
            db.session.delete(task)
        db.session.commit()

        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            queue = rq.Queue(is_async=True)
            job = queue.enqueue(app_rams.survivability_fls, 
                  args = (a, cdf, cov_a, cov_q, cycles_sk, cycles_et, 
	                      device_id_et, device_id_sk, fatigue_flag_sk, fls_load_et, fls_prob_et, m, 
                          mu_a_et, m_et, n_sim, option_fls, pd_a, pd_q, pd_h, pd_m, 
                          pd_n, "FLS", std_a_et, '', '', '', stress_range),
                          job_timeout=6000)
            queue.enqueue(save_results_survivability_fls, job.get_id(), depends_on=job)
            task = Task(project_id=id, rq_job_id=job.get_id())
            db.session.add(task)
            db.session.commit()
            response_object = {
                "status": "success",
                "data": {
                    "task_id": task.id,
                }
            }
        return jsonify(response_object), 202
    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 500


@bp.route('/rams/<id>/survivability_fls/<int:task_id>', methods=['GET'])
def get_survivability_fls(id, task_id: int):
    """Flask blueprint route for getting the FLS survivability assessment results.

    If id provided does not exist in the database, returns a HTTP 404 error. 

    Args:
        id (integer): The ID of the RAMS study.
        task_id (integer): The task id created in the POST method.
    
    Returns:
        Response (dict): The probabilities that the Energy Transformation 
            and Station Keeping subsystems can survive the fatigue stresses;
            or a HTTP 400 error.

    """

    if RamsProject.query.get(id) is None:
        raise IndexError("The Id does not exist!")

    task: Task = Task.query.filter_by(id=task_id).first()
    if task is None:
        return jsonify({"status": "error", "message": "Task not found."}), 404

    try:
        with rq.Connection(redis.from_url(current_app.config["REDIS_URL"])):
            job = rq.Queue().fetch_job(task.rq_job_id)

            return jsonify({
                "task_id": task.id,
                "task_status": job.get_status(),
                "task_result": job.result,
            }), 200

    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


@bp.route('/rams/results_summary/<id>', methods=['GET'])
def get_rams_results(id):
    try:
        if (RamsProject.query.get(id) is None):
            raise IndexError
    except IndexError: 
        return jsonify({'message': f'No entity is registered with the given ID (#{id})'}), 404

    try:
        id = int(id)
        project = RamsProject.query.get(id)
        results = dict()
        results["reliability_component"] = {}
        results["availability"] = {}
        results["maintainability"] = {}
        results["reliability_system"] = {}
        results["survivability_uls"] = {}
        results["survivability_fls"] = {}

        if not project.results:
            pass
        else:
            # records = [el for el in project.results if el.project_id==id]
            for row in project.results:
                if row.reliability_component != {}:
                    results["reliability_component"] = row.reliability_component
                if row.availability != {}:
                    results["availability"] = row.availability
                if row.maintainability != {}:
                    results["maintainability"] = row.maintainability

        if not project.results_rq:
            pass
        else:
            # records_rq = [el for el in project.results_rq if el.project_id==id]
            for row in project.results_rq:
                if row.reliability_system != {}:
                    results["reliability_system"] = row.reliability_system
                if row.survivability_uls != {}:
                    temp_dict = dict()
                    temp_dict["ET_Subsystem"] = row.survivability_uls["ET Subsystem"]
                    temp_dict["SK_Subsystem"] = row.survivability_uls["SK Subsystem"]
                    results["survivability_uls"] = temp_dict
                if row.survivability_fls != {}:
                    temp_dict = dict()
                    temp_dict["ET_Subsystem"] = row.survivability_fls["ET Subsystem"]
                    temp_dict["SK_Subsystem"] = row.survivability_fls["SK Subsystem"]
                    results["survivability_fls"] = temp_dict

        return jsonify(results), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


@bp.route('/rams/results_summary/<id>', methods=['DELETE'])
def clean_db(id):
    try:
        if (RamsProject.query.get(id) is None):
            raise IndexError
    except IndexError: 
        return jsonify({'message': f'No entity is registered with the given ID (#{id})'}), 404
    id = int(id)
    project = RamsProject.query.get(id)

    try:

        tasks = Task.query.filter_by(project_id=id)
        for task in tasks:
            db.session.delete(task)
        db.session.commit()

        results = Results.query.filter_by(project_id=id)
        for result in results:
            db.session.delete(result)
        db.session.commit()

        records_rq = ResultsRQ.query.filter_by(project_id=id).all()
        for record in records_rq:
            db.session.delete(record)
        db.session.commit()

        return jsonify({"status": "success", 'message': 'Project results deleted'}), 200

    except Exception as e:
        return jsonify({"status": "error", 'message': str(e)}), 404

