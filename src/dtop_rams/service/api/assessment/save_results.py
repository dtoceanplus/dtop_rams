import json
import redis
from flask import current_app
from rq import Connection, Queue, Worker
from rq.registry import FinishedJobRegistry
from dtop_rams.service import create_app
from dtop_rams.service.models import Task, db, ResultsRQ, RamsProject
import requests


def save_results_reliability(task_id):
    with Connection(redis.from_url(current_app.config["REDIS_URL"])):
        job = Queue().fetch_job(task_id)
        # Save results to database
        task = Task.query.filter_by(rq_job_id=job.id).first()
        assert task is not None

        if db.session.query(ResultsRQ).all() == []:
            result = ResultsRQ(project_id=task.project_id, reliability_system=job.result)
            db.session.add(result)
        else:
            record = ResultsRQ.query.filter_by(id=1).first()
            result = ResultsRQ(project_id=task.project_id, reliability_system=job.result)
            record.reliability_system = result.reliability_system
            db.session.add(record)
        db.session.commit()


def save_results_survivability_uls(task_id):
    with Connection(redis.from_url(current_app.config["REDIS_URL"])):
        job = Queue().fetch_job(task_id)
        # Save results to database
        task = Task.query.filter_by(rq_job_id=job.id).first()
        assert task is not None

        if db.session.query(ResultsRQ).all() == []:
            result = ResultsRQ(project_id=task.project_id, survivability_uls=job.result)
            db.session.add(result)
        else:
            record = ResultsRQ.query.filter_by(id=1).first() 
            result = ResultsRQ(project_id=task.project_id, survivability_uls=job.result)
            record.survivability_uls = result.survivability_uls
            db.session.add(record)
        db.session.commit()


def save_results_survivability_fls(task_id):
    with Connection(redis.from_url(current_app.config["REDIS_URL"])):
        job = Queue().fetch_job(task_id)
        # Save results to database
        task = Task.query.filter_by(rq_job_id=job.id).first()
        assert task is not None

        if db.session.query(ResultsRQ).all() == []:
            result = ResultsRQ(project_id=task.project_id, survivability_fls=job.result)
            db.session.add(result)
        else:
            record = ResultsRQ.query.filter_by(id=1).first() 
            result = ResultsRQ(project_id=task.project_id, survivability_fls=job.result)
            record.survivability_fls = result.survivability_fls
            db.session.add(record)
        db.session.commit()