import DashboardPage from '../pages/dashboard.page';
describe("dashboard", () => {
    const dashboard = new DashboardPage();

    it('Create RAMS project', () => {
        dashboard.visit();
        dashboard.createProject();
    })

    it.skip('Edit RAMS project', () => {
        dashboard.editProject()
    })

    it.skip('Open RAMS project', () => {
        dashboard.visit();
        dashboard.openProject()
    })

    it.skip('Remove RAMS project', () => {
        dashboard.visit();
        dashboard.removeProject()
    })
})