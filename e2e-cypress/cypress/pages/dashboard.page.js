class DashboardPage {
    static PROJECT_TITLE = 'Project title'
    static PROJECT_TAGS = 'tag'
    static PROJECT_DESC = 'Description'
    static PROJECT_TITLE_EDIT = 'Project title2'
    static PROJECT_TAGS_EDIT = 'tag2'
    static PROJECT_DESC_EDIT = 'Description2'

    visit() {
        cy.visit('/#/rams-studies/index')
    }

    createProject() {
        cy.get('[data-cy=createProjectBtn]').click()
        cy.get('[data-cy=projectTitle]').clear().type(DashboardPage.PROJECT_TITLE)
        cy.get('[data-cy=projectTags]').clear().type(DashboardPage.PROJECT_TAGS)
        // cy.get('[data-cy=projectComplexity]').click()
        // cy.get('.el-select-dropdown').contains("1").click()
        cy.get('[data-cy=projectDescription]').type(DashboardPage.PROJECT_DESC)
        cy.get('[data-cy="projectCreate"]').click()
        cy.contains(DashboardPage.PROJECT_TITLE)
    }

    editProject() {
        cy.get('[data-cy="projectEdit"]').eq(1).click()
        cy.get('[data-cy=projectTitle]').clear().type(DashboardPage.PROJECT_TITLE_EDIT)
        cy.get('[data-cy=projectTags]').clear().type(DashboardPage.PROJECT_TAGS_EDIT)
        cy.get('[data-cy=projectComplexity]').click()
        cy.get('.el-select-dropdown').contains('2').click()
        cy.get('[data-cy=projectMachineType]').click()
        cy.get('.el-select-dropdown').contains('Wave Converter').click()
        cy.get('[data-cy=projectDescription]').clear().type(DashboardPage.PROJECT_DESC_EDIT)
        cy.get('[data-cy="projectSave"]').click()
        cy.get('.el-message-box__btns > .el-button--primary').click()
        cy.contains(DashboardPage.PROJECT_TITLE_EDIT)
    }

    openProject() {
        cy.get('[data-cy="projectOpen"]').eq(1).click()
        cy.url().should('include', '/general')
    }

    removeProject() {
        cy.get('[data-cy="projectRemove"]').eq(1).click()
        cy.should('not.contain', DashboardPage.PROJECT_TITLE_EDIT)
    }

}
export default DashboardPage;