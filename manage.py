import json

import redis
from flask.cli import FlaskGroup
from rq import Connection, Queue, Worker
from rq.registry import FinishedJobRegistry

from dtop_rams.service import create_app, models
from dtop_rams.service.models import Task, db


app = create_app()
cli = FlaskGroup(create_app=create_app)
redis_url = app.config["REDIS_URL"]
redis_connection = redis.from_url(redis_url)


@cli.command("run_worker")
def run_worker():
    with Connection(redis_connection):
        worker = Worker(app.config["QUEUES"])
        worker.work()


if __name__ == "__main__":
    print('Set up the Worker run function')
    cli()
