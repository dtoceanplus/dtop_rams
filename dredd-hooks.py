import json
import os
import traceback
from functools import partial
import dredd_hooks as hooks
import requests
from dredd_utils import *
from dredd_data import *



##==================================== hooks for the path of /rams ====================================
# @hooks.before("/rams > Returns all the active RAMS projects > 200 > application/json")
@hooks.before("/rams > Returns all the active RAMS projects > 404")
# @hooks.before("/rams > Creates a new rams project > 201")
# @hooks.before("/rams > Creates a new rams project > 400")


##============================== hooks for the path of /rams/{ransId} =================================
# @hooks.before("/rams/{ramsId} > Returns the specific RAMS project > 200 > application/json")
# @hooks.before("/rams/{ramsId} > Returns the specific RAMS project > 404")
# @hooks.before("/rams/{ramsId} > Modifies a specific RAMS project > 201")
# @hooks.before("/rams/{ramsId} > Modifies a specific RAMS project > 404")
# @hooks.before("/rams/{ramsId} > Deletes a specific RAMS project > 200")
# @hooks.before("/rams/{ramsId} > Deletes a specific RAMS project > 404")


##=================================== hooks for the path of inputs  ===================================
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity1 > Creates the input data from the ED module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity1 > Creates the input data from the ED module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity1 > Creates the input data from the ED module for a complexity-1 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity2 > Creates the input data from the ED module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity2 > Creates the input data from the ED module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity2 > Creates the input data from the ED module for a complexity-2 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity3 > Creates the input data from the ED module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity3 > Creates the input data from the ED module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/ed/complexity3 > Creates the input data from the ED module for a complexity-3 assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity1 > Creates the hierarchy from the ET module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity1 > Creates the hierarchy from the ET module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity1 > Creates the hierarchy from the ET module for a complexity-1 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity2 > Creates the hierarchy from the ET module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity2 > Creates the hierarchy from the ET module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity2 > Creates the hierarchy from the ET module for a complexity-2 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity3 > Creates the hierarchy from the ET module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity3 > Creates the hierarchy from the ET module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity3 > Creates the hierarchy from the ET module for a complexity-3 assessment > 404")

@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity1 > Creates the stresses from the ET module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity1 > Creates the stresses from the ET module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity1 > Creates the stresses from the ET module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity2 > Creates the stresses from the ET module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity2 > Creates the stresses from the ET module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity2 > Creates the stresses from the ET module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity3 > Creates the stresses from the ET module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity3 > Creates the stresses from the ET module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity3 > Creates the stresses from the ET module for a complexity-3 assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity1 > Creates the hierarchy from the SK module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity1 > Creates the hierarchy from the SK module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity1 > Creates the hierarchy from the SK module for a complexity-1 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity2 > Creates the hierarchy from the SK module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity2 > Creates the hierarchy from the SK module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity2 > Creates the hierarchy from the SK module for a complexity-2 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity3 > Creates the hierarchy from the SK module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity3 > Creates the hierarchy from the SK module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity3 > Creates the hierarchy from the SK module for a complexity-3 assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity1 > Creates the stresses from the SK module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity1 > Creates the stresses from the SK module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity1 > Creates the stresses from the SK module for a complexity-1 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity2 > Creates the stresses from the SK module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity2 > Creates the stresses from the SK module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity2 > Creates the stresses from the SK module for a complexity-2 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity3 > Creates the stresses from the SK module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity3 > Creates the stresses from the SK module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity3 > Creates the stresses from the SK module for a complexity-3 assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity1 > Creates the downtime from the LMO module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity1 > Creates the downtime from the LMO module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity1 > Creates the downtime from the LMO module for a complexity-1 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity2 > Creates the downtime from the LMO module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity2 > Creates the downtime from the LMO module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity2 > Creates the downtime from the LMO module for a complexity-2 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity3 > Creates the downtime from the LMO module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity3 > Creates the downtime from the LMO module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity3 > Creates the downtime from the LMO module for a complexity-3 assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity1 > Creates the maintenance-related data from the LMO module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity1 > Creates the maintenance-related data from the LMO module for a complexity-1 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity1 > Creates the maintenance-related data from the LMO module for a complexity-1 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity2 > Creates the maintenance-related data from the LMO module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity2 > Creates the maintenance-related data from the LMO module for a complexity-2 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity2 > Creates the maintenance-related data from the LMO module for a complexity-2 assessment > 404")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity3 > Creates the maintenance-related data from the LMO module for a complexity-3 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity3 > Creates the maintenance-related data from the LMO module for a complexity-3 assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity3 > Creates the maintenance-related data from the LMO module for a complexity-3 assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Creates the user-defined inputs for reliability assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Creates the user-defined inputs for reliability assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Creates the user-defined inputs for reliability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Updates the user-defined inputs for reliability assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Updates the user-defined inputs for reliability assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Updates the user-defined inputs for reliability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Gets the user-defined inputs for reliability assessment > 200")
# @hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Gets the user-defined inputs for reliability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Creates the user-defined inputs for maintainability assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Creates the user-defined inputs for maintainability assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Creates the user-defined inputs for maintainability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Updates the user-defined inputs for maintainability assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Updates the user-defined inputs for maintainability assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Updates the user-defined inputs for maintainability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Gets the user-defined inputs for maintainability assessment > 200")
# @hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Gets the user-defined inputs for maintainability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Creates the user-defined inputs for survivability assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Creates the user-defined inputs for survivability assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Creates the user-defined inputs for survivability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Updates the user-defined inputs for survivability assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Updates the user-defined inputs for survivability assessment > 400")
# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Updates the user-defined inputs for survivability assessment > 404")

# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Gets the user-defined inputs for survivability assessment > 200")
# @hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Gets the user-defined inputs for survivability assessment > 404")


##==================== hooks for the path of /rams/{ransId}/reliability_component =====================
# @hooks.before("/rams/{ramsId}/reliability_component/{lifetime} > Returns the time to failure  and mean time to failure (mttf) of basic components in all subsystems > 200 > application/json")
# @hooks.before("/rams/{ramsId}/reliability_component/{lifetime} > Returns the time to failure  and mean time to failure (mttf) of basic components in all subsystems > 500")
# @hooks.before("/rams/{ramsId}/reliability_component/{lifetime} > Returns the time to failure  and mean time to failure (mttf) of basic components in all subsystems > 404")
##====================== hooks for the path of /rams/{ramsId}/reliability_system ======================
@hooks.before("/rams/{ramsId}/reliability_system/{taskId} > Returns the maximum annual probabilities of failure (pofs) of units at different levels in a system > 200 > application/json")
@hooks.before("/rams/{ramsId}/reliability_system/{taskId} > Returns the maximum annual probabilities of failure (pofs) of units at different levels in a system > 500")
# @hooks.before("/rams/{ramsId}/reliability_system/{taskId} > Returns the maximum annual probabilities of failure (pofs) of units at different levels in a system > 404")

# @hooks.before("/rams/{ramsId}/reliability_system/task/{lifetime} > Adds a task for the system-level reliability assessment > 202")
# @hooks.before("/rams/{ramsId}/reliability_system/task/{lifetime} > Adds a task for the system-level reliability assessment > 500")
# @hooks.before("/rams/{ramsId}/reliability_system/task/{lifetime} > Adds a task for the system-level reliability assessment > 404")
##========================= hooks for the path of /rams/{ransId}/availability =========================
# @hooks.before("/rams/{ramsId}/availability > Returns the availability of devices and array > 200 > application/json")
# @hooks.before("/rams/{ramsId}/availability > Returns the availability of devices and array > 500")
# @hooks.before("/rams/{ramsId}/availability > Returns the availability of devices and array > 404")
##======================= hooks for the path of /rams/{ransId}/maintainability ========================
# @hooks.before("/rams/{ramsId}/maintainability/{t_ava_repair} > Returns the probability that the damaged components can be  successfully repaired > 200 > application/json")
# @hooks.before("/rams/{ramsId}/maintainability/{t_ava_repair} > Returns the probability that the damaged components can be  successfully repaired > 500")
# @hooks.before("/rams/{ramsId}/maintainability/{t_ava_repair} > Returns the probability that the damaged components can be  successfully repaired > 404")
##====================== hooks for the path of /rams/{ransId}/survivability_uls =======================
@hooks.before("/rams/{ramsId}/survivability_uls/{taskId} > Returns the minimal probability that the critical component can survive the ultimate loads > 200 > application/json")
@hooks.before("/rams/{ramsId}/survivability_uls/{taskId} > Returns the minimal probability that the critical component can survive the ultimate loads > 500")
# @hooks.before("/rams/{ramsId}/survivability_uls/{taskId} > Returns the minimal probability that the critical component can survive the ultimate loads > 404")

@hooks.before("/rams/{ramsId}/survivability_uls > Adds a task for the uls survivability assessment > 202")
# @hooks.before("/rams/{ramsId}/survivability_uls > Adds a task for the uls survivability assessment > 500")
# @hooks.before("/rams/{ramsId}/survivability_uls > Adds a task for the uls survivability assessment > 404")
##====================== hooks for the path of /rams/{ransId}/survivability_fls =======================
@hooks.before("/rams/{ramsId}/survivability_fls/{taskId} > Returns the minimal probability that the critical component can survive the fatigue loads > 200 > application/json")
@hooks.before("/rams/{ramsId}/survivability_fls/{taskId} > Returns the minimal probability that the critical component can survive the fatigue loads > 500")
# @hooks.before("/rams/{ramsId}/survivability_fls/{taskId} > Returns the minimal probability that the critical component can survive the fatigue loads > 404")

@hooks.before("/rams/{ramsId}/survivability_fls > Adds a task for the fls survivability assessment > 202")
# @hooks.before("/rams/{ramsId}/survivability_fls > Adds a task for the fls survivability assessment > 500")
# @hooks.before("/rams/{ramsId}/survivability_fls > Adds a task for the fls survivability assessment > 404")


##====================================== hooks for the result summary ====================================
@hooks.before("/rams/results_summary/{ramsId} > Returns the summary of assessment results > 200 > application/json")
# @hooks.before("/rams/results_summary/{ramsId} > Returns the summary of assessment results > 404")
@hooks.before("/rams/results_summary/{ramsId} > Returns the summary of assessment results > 500")

@hooks.before("/rams/results_summary/{ramsId} > Deletes the result summary > 200")
# @hooks.before("/rams/results_summary/{ramsId} > Deletes the result summary > 404")


##================================ hooks for the Digital Represenataion  =================================
@hooks.before("/representation/{ramsId} > Gets the specific rams assessment results > 200 > application/json")
@hooks.before("/representation/{ramsId} > Gets the specific rams assessment results > 404")
@hooks.before("/representation/{ramsId} > Gets the specific rams assessment results > 500")
def skip(transaction):
    """
    Skip the test.
    """
    transaction['skip'] = True

@hooks.before_each
@if_not_skipped
def before_each(transaction):
    remove_all_projects(transaction)
    post_project(transaction)


##================================= the functions to test the paths /rams =================================
@hooks.before("/rams > Returns all the active RAMS projects > 200")
@if_not_skipped
def get_project_200(transaction):
    """
    """

# @hooks.before("/rams > Returns all the active RAMS projects > 404")
# @if_not_skipped
# def get_project_404(transaction):
#     """
#     Clear the db to triger 404
#     """
#     remove_all_projects(transaction)

@hooks.before("/rams > Creates a new rams project > 201")
@if_not_skipped
def post_project_201(transaction):
    """
    Create a RAMS project.
    """
    post_project(transaction)

@hooks.before("/rams > Creates a new rams project > 400")
@if_not_skipped
def post_project_400(transaction):
    """
    Remove one of required parameters when creating a RAMS project.
    """
    request = json.loads(transaction['request']['body'])
    request.pop('complexity', None)
    transaction['request']['body'] = ' '


##=========================== the functions to test the paths /rams/{ransId} ==============================
@hooks.before("/rams/{ramsId} > Returns the specific RAMS project > 200")
@if_not_skipped
def get_project_specific_200(transaction):
    """
    Get a specific RAMS project
    """
    get_a_project(transaction)

@hooks.before("/rams/{ramsId} > Returns the specific RAMS project > 404")
@if_not_skipped
def get_project_specific_404(transaction):
    """
    Try to add a wrong id to triger 404
    """
    transaction['request']['uri'] = transaction['request']['uri'].replace(
        '1', '-1')
    transaction['fullPath'] = transaction['fullPath'].replace('1', '-1')

@hooks.before("/rams/{ramsId} > Modifies a specific RAMS project > 201")
@if_not_skipped
def put_project_specific_201(transaction):
    """
    Update a specific RAMS project
    """
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']
    ramsId = 1
    r = requests.put(f'{protocol}//{host}:{port}/rams/{ramsId}', json=rams_example)

@hooks.before("/rams/{ramsId} > Modifies a specific RAMS project > 404")
@if_not_skipped
def put_project_specific_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = transaction['request']['uri'].replace(
        '1', '-1')
    transaction['fullPath'] = transaction['fullPath'].replace('1', '-1')

@hooks.before("/rams/{ramsId} > Deletes a specific RAMS project > 200")
@if_not_skipped
def delete_project_specific_200(transaction):
    """
    Delete a specific RAMS project
    """
    post_project(transaction)

@hooks.before("/rams/{ramsId} > Deletes a specific RAMS project > 404")
@if_not_skipped
def delete_project_specific_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = transaction['request']['uri'].replace(
        '1', '-1')
    transaction['fullPath'] = transaction['fullPath'].replace('1', '-1')


##======================= the functions to test the paths /rams/{ramsId}/inputs/ed/ =======================
@hooks.before("/rams/{ramsId}/inputs/ed/complexity1 > Creates the input data from the ED module for a complexity-1 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/ed/complexity2 > Creates the input data from the ED module for a complexity-2 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/ed/complexity3 > Creates the input data from the ED module for a complexity-3 assessment > 201")
def post_ed_input_201(transaction):
    """
    Create a post request to send the actual ED results
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/ed/complexity1 > Creates the input data from the ED module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/ed/complexity2 > Creates the input data from the ED module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/ed/complexity3 > Creates the input data from the ED module for a complexity-3 assessment > 400")
@if_not_skipped
def post_ed_input_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/ed/complexity1 > Creates the input data from the ED module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/ed/complexity2 > Creates the input data from the ED module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/ed/complexity3 > Creates the input data from the ED module for a complexity-3 assessment > 404")
@if_not_skipped
def post_ed_input_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/ed'
    transaction['fullPath'] = '/rams/200/inputs/ed'


##=================== the functions to test the paths /rams/{ramsId}/inputs/et/hierarchy/ =================
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity1 > Creates the hierarchy from the ET module for a complexity-1 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity2 > Creates the hierarchy from the ET module for a complexity-2 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity3 > Creates the hierarchy from the ET module for a complexity-3 assessment > 201")
@if_not_skipped
def post_et_hierarchy_201(transaction):
    """
    Create a post request to send the actual et hierarchy
    """
    data = json.loads(transaction["request"]["body"])
    data1 = dict()
    data1["Hierarchy"] = dict()
    data1["Hierarchy"]["value"] = data
    transaction["request"]["body"] = json.dumps(data1)

@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity1 > Creates the hierarchy from the ET module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity2 > Creates the hierarchy from the ET module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity3 > Creates the hierarchy from the ET module for a complexity-3 assessment > 400")
@if_not_skipped
def post_et_hierarchy_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity1 > Creates the hierarchy from the ET module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity2 > Creates the hierarchy from the ET module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/et/hierarchy/complexity3 > Creates the hierarchy from the ET module for a complexity-3 assessment > 404")
@if_not_skipped
def post_et_hierarchy_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/et/hierarchy'
    transaction['fullPath'] = '/rams/200/inputs/et/hierarchy'


##=================== the functions to test the paths /rams/{ramsId}/inputs/et/stress/ ====================
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity1 > Creates the stresses from the ET module for a complexity-1 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity2 > Creates the stresses from the ET module for a complexity-2 assessment > 201")
# @hooks.before("/rams/{ramsId}/inputs/et/stress/complexity3 > Creates the stresses from the ET module for a complexity-3 assessment > 201")
# @if_not_skipped
# def post_et_stress_201(transaction):
#     """
#     Create a post request to send the actual stress
#     """
#     data = json.loads(transaction["request"]["body"])
#     transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity1 > Creates the stresses from the ET module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity2 > Creates the stresses from the ET module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity3 > Creates the stresses from the ET module for a complexity-3 assessment > 400")
@if_not_skipped
def post_et_stress_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity1 > Creates the stresses from the ET module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity2 > Creates the stresses from the ET module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/et/stress/complexity3 > Creates the stresses from the ET module for a complexity-3 assessment > 404")
@if_not_skipped
def post_et_stress_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/et/stress'
    transaction['fullPath'] = '/rams/200/inputs/et/stress'


##================= the functions to test the paths /rams/{ramsId}/inputs/sk/hierarchy/ ===================
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity1 > Creates the hierarchy from the SK module for a complexity-1 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity2 > Creates the hierarchy from the SK module for a complexity-2 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity3 > Creates the hierarchy from the SK module for a complexity-3 assessment > 201")
@if_not_skipped
def post_sk_hierarchy_201(transaction):
    """
    Create a post request to send the actual SK hierarchy
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity1 > Creates the hierarchy from the SK module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity2 > Creates the hierarchy from the SK module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity3 > Creates the hierarchy from the SK module for a complexity-3 assessment > 400")
@if_not_skipped
def post_sk_hierarchy_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity1 > Creates the hierarchy from the SK module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity2 > Creates the hierarchy from the SK module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/sk/hierarchy/complexity3 > Creates the hierarchy from the SK module for a complexity-3 assessment > 404")
@if_not_skipped
def post_sk_hierarchy_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/sk/hierarchy'
    transaction['fullPath'] = '/rams/200/inputs/sk/hierarchy'


##=================== the functions to test the paths /rams/{ramsId}/inputs/sk/stress/ ====================
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity1 > Creates the stresses from the SK module for a complexity-1 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity2 > Creates the stresses from the SK module for a complexity-2 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity3 > Creates the stresses from the SK module for a complexity-3 assessment > 201")
@if_not_skipped
def post_sk_stress_201(transaction):
    """
    Create a post request to send the actual stress
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity1 > Creates the stresses from the SK module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity2 > Creates the stresses from the SK module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity3 > Creates the stresses from the SK module for a complexity-3 assessment > 400")
@if_not_skipped
def post_sk_stress_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity1 > Creates the stresses from the SK module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity2 > Creates the stresses from the SK module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/sk/stress/complexity3 > Creates the stresses from the SK module for a complexity-3 assessment > 404")
@if_not_skipped
def post_sk_stress_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/sk/stress'
    transaction['fullPath'] = '/rams/200/inputs/sk/stress'


##================== the functions to test the paths /rams/{ramsId}/inputs/lmo/downtime/ ==================
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity1 > Creates the downtime from the LMO module for a complexity-1 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity2 > Creates the downtime from the LMO module for a complexity-2 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity3 > Creates the downtime from the LMO module for a complexity-3 assessment > 201")
@if_not_skipped
def post_lmo_downtime_201(transaction):
    """
    Create a post request to send the actual downtime
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity1 > Creates the downtime from the LMO module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity2 > Creates the downtime from the LMO module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity3 > Creates the downtime from the LMO module for a complexity-3 assessment > 400")
@if_not_skipped
def post_lmo_downtime_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity1 > Creates the downtime from the LMO module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity2 > Creates the downtime from the LMO module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/lmo/downtime/complexity3 > Creates the downtime from the LMO module for a complexity-3 assessment > 404")
@if_not_skipped
def post_lmo_downtime_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/lmo/downtime'
    transaction['fullPath'] = '/rams/200/inputs/lmo/downtime'


##================ the functions to test the paths /rams/{ramsId}/inputs/lmo/maintenance/ =================
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity1 > Creates the maintenance-related data from the LMO module for a complexity-1 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity2 > Creates the maintenance-related data from the LMO module for a complexity-2 assessment > 201")
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity3 > Creates the maintenance-related data from the LMO module for a complexity-3 assessment > 201")
@if_not_skipped
def post_lmo_maintenance_201(transaction):
    """
    Create a post request to send the actual maintenance-related data
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity1 > Creates the maintenance-related data from the LMO module for a complexity-1 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity2 > Creates the maintenance-related data from the LMO module for a complexity-2 assessment > 400")
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity3 > Creates the maintenance-related data from the LMO module for a complexity-3 assessment > 400")
@if_not_skipped
def post_lmo_maintenance_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity1 > Creates the maintenance-related data from the LMO module for a complexity-1 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity2 > Creates the maintenance-related data from the LMO module for a complexity-2 assessment > 404")
@hooks.before("/rams/{ramsId}/inputs/lmo/maintenance/complexity3 > Creates the maintenance-related data from the LMO module for a complexity-3 assessment > 404")
@if_not_skipped
def post_lmo_maintenance_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/lmo/maintenance'
    transaction['fullPath'] = '/rams/200/inputs/lmo/maintenance'


##============ the functions to test the paths /rams/{ramsId}/inputs/reliability/user_defined =============
@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Creates the user-defined inputs for reliability assessment > 201")
@if_not_skipped
def post_ud_reliability_201(transaction):
    """
    Create a post request to send the user-defined variables for reliability assessment
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Creates the user-defined inputs for reliability assessment > 400")
@if_not_skipped
def post_ud_reliability_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Creates the user-defined inputs for reliability assessment > 404")
@if_not_skipped
def post_ud_reliability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/reliability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/reliability/user_defined'

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Updates the user-defined inputs for reliability assessment > 201")
@if_not_skipped
def update_ud_reliability_201(transaction):
    """
    Update the user-defined data for reliability assessment
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Updates the user-defined inputs for reliability assessment > 400")
@if_not_skipped
def update_ud_reliability_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Updates the user-defined inputs for reliability assessment > 404")
@if_not_skipped
def update_ud_reliability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/reliability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/reliability/user_defined'

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Gets the user-defined inputs for reliability assessment > 200")
def get_ud_reliability_200(transaction):
    """
    Get the user-defined data for reliability assessment
    """
    post_ud_reliability(transaction, 1)

@hooks.before("/rams/{ramsId}/inputs/reliability/user_defined > Gets the user-defined inputs for reliability assessment > 404")
@if_not_skipped
def get_ud_reliability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/reliability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/reliability/user_defined'


##========== the functions to test the paths /rams/{ramsId}/inputs/maintainability/user_defined ===========
@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Creates the user-defined inputs for maintainability assessment > 201")
@if_not_skipped
def post_ud_maintainability_201(transaction):
    """
    Create a post request to send the user-defined variables for maintainability assessment
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Creates the user-defined inputs for maintainability assessment > 400")
@if_not_skipped
def post_ud_maintainability_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Creates the user-defined inputs for maintainability assessment > 404")
@if_not_skipped
def post_ud_maintainability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/maintainability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/maintainability/user_defined'

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Updates the user-defined inputs for maintainability assessment > 201")
@if_not_skipped
def update_ud_maintainability_201(transaction):
    """
    Update the user-defined data for maintainability assessment
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Updates the user-defined inputs for maintainability assessment > 400")
@if_not_skipped
def update_ud_maintainability_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Updates the user-defined inputs for maintainability assessment > 404")
@if_not_skipped
def update_ud_maintainability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/maintainability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/maintainability/user_defined'

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Gets the user-defined inputs for maintainability assessment > 200")
@if_not_skipped
def get_ud_maintainability_200(transaction):
    """
    Get the user-defined data for maintainability assessment
    """
    post_ud_maintainability(transaction, 1)

@hooks.before("/rams/{ramsId}/inputs/maintainability/user_defined > Gets the user-defined inputs for maintainability assessment > 404")
@if_not_skipped
def get_ud_maintainability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/maintainability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/maintainability/user_defined'


##=========== the functions to test the paths /rams/{ramsId}/inputs/survivability/user_defined ============
@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Creates the user-defined inputs for survivability assessment > 201")
@if_not_skipped
def post_ud_survivability_201(transaction):
    """
    Create a post request to send the user-defined variables for survivability assessment
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Creates the user-defined inputs for survivability assessment > 400")
@if_not_skipped
def post_ud_survivability_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Creates the user-defined inputs for survivability assessment > 404")
@if_not_skipped
def post_ud_survivability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/survivability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/survivability/user_defined'

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Updates the user-defined inputs for survivability assessment > 201")
@if_not_skipped
def update_ud_survivability_201(transaction):
    """
    Update the user-defined data for survivability assessment
    """
    data = json.loads(transaction["request"]["body"])
    transaction["request"]["body"] = json.dumps(data)

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Updates the user-defined inputs for survivability assessment > 400")
@if_not_skipped
def update_ud_survivability_400(transaction):
    """
    Make an empty request body to trigger 400.
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Updates the user-defined inputs for survivability assessment > 404")
@if_not_skipped
def update_ud_survivability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/survivability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/survivability/user_defined'

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Gets the user-defined inputs for survivability assessment > 200")
@if_not_skipped
def get_ud_survivability_200(transaction):
    """
    Get the user-defined data for survivability assessment
    """
    post_ud_survivability(transaction, 1)

@hooks.before("/rams/{ramsId}/inputs/survivability/user_defined > Gets the user-defined inputs for survivability assessment > 404")
@if_not_skipped
def get_ud_survivability_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/200/inputs/survivability/user_defined'
    transaction['fullPath'] = '/rams/200/inputs/survivability/user_defined'


##==================== hooks for the path of /rams/{ransId}/reliability_component =====================
@hooks.before("/rams/{ramsId}/reliability_component/{lifetime} > Returns the time to failure  and mean time to failure (mttf) of basic components in all subsystems > 200 > application/json")
@if_not_skipped
def get_reliability_component_200(transaction):
    """
    Get the results of the component-level reliabiity assessment
    """
    reliability_assessment(transaction, 1)

@hooks.before("/rams/{ramsId}/reliability_component/{lifetime} > Returns the time to failure  and mean time to failure (mttf) of basic components in all subsystems > 500")
@if_not_skipped
def get_reliability_component_500(transaction):
    """
    Trigger 500
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/reliability_component/{lifetime} > Returns the time to failure  and mean time to failure (mttf) of basic components in all subsystems > 404")
@if_not_skipped
def get_reliability_component_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = '/rams/200/reliability_component/10.0'
    transaction['fullPath'] = '/rams/200/reliability_component/10.0'


##====================== hooks for the path of /rams/{ransId}/reliability_system ======================
@hooks.before("/rams/{ramsId}/reliability_system/task/{lifetime} > Adds a task for the system-level reliability assessment > 202")
@if_not_skipped
def post_task_reliability_system_202(transaction):
    """
    Add a task for system-level reliabiity assessment
    """
    reliability_assessment(transaction, 1)
    transaction['request']['body'] = json.dumps({"id": 1})

@hooks.before("/rams/{ramsId}/reliability_system/task/{lifetime} > Adds a task for the system-level reliability assessment > 500")
@if_not_skipped
def post_task_reliability_system_500(transaction):
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/reliability_system/task/{lifetime} > Adds a task for the system-level reliability assessment > 404")
@if_not_skipped
def post_task_reliability_system_404(transaction):
    transaction['request']['uri'] = transaction['request']['uri'].replace(
        '1', '-1')
    transaction['fullPath'] = transaction['fullPath'].replace('1', '-1')

# @hooks.before("/rams/{ramsId}/reliability_system/{taskId} > Returns the maximum annual probabilities of failure (pofs) of units at different levels in a system > 200")
# @if_not_skipped
# def get_reliability_system_200(transaction):
#     """
#     Get the results of the system-level reliabiity assessment
#     """
#     print('ok')
#     transaction['request']['body'] = json.dumps({"id": 1}) 
    # r = post_task_reliability(transaction, 1)
    # fake_tr = {'request': {'uri': "/rams/1/reliability_system/20"}}
    # fake_tr['protocol'] = transaction['protocol']
    # fake_tr['host'] = transaction['host']
    # fake_tr['port'] = transaction['port']
    # reliability_assessment(fake_tr, 1)

    # print(r)
    # task_id = r.json()["data"]["task_id"]
    # transaction['request']['uri'] = f'/rams/1/reliability_system/{task_id}'
    # transaction['fullPath'] = f'/rams/1/reliability_system/{task_id}'

# @hooks.before("/rams/{ramsId}/reliability_system/{taskId} >  Returns the maximum annual probabilities of failure (pofs) of units at different levels in a system > 500")
# @if_not_skipped
# def get_reliability_system_500(transaction):
#     """
#     Trigger 500
#     """
#     transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/reliability_system/{taskId} > Returns the maximum annual probabilities of failure (pofs) of units at different levels in a system > 404")
@if_not_skipped
def get_reliability_system_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = '/rams/1/reliability_system/10'
    transaction['fullPath'] = '/rams/1/reliability_system/10'


##========================= hooks for the path of /rams/{ransId}/availability =========================
@hooks.before("/rams/{ramsId}/availability > Returns the availability of devices and array > 200 > application/json")
@if_not_skipped
def get_availability_200(transaction):
    """
    Get the results of the availability assessment
    """
    availability_assessment(transaction, 1)

@hooks.before("/rams/{ramsId}/availability > Returns the availability of devices and array > 500")
@if_not_skipped
def get_availability_500(transaction):
    """
    Add  to trigger 500
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/availability > Returns the availability of devices and array > 404")
@if_not_skipped
def get_availability_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = '/rams/200/availability'
    transaction['fullPath'] = '/rams/200/availability'


##======================= hooks for the path of /rams/{ransId}/maintainability ========================
@hooks.before("/rams/{ramsId}/maintainability/{t_ava_repair} > Returns the probability that the damaged components can be  successfully repaired > 200 > application/json")
@if_not_skipped
def get_maintainability_200(transaction):
    """
    Get the results of the maintainability assessment
    """
    maintainability_assessment(transaction, 1)

@hooks.before("/rams/{ramsId}/maintainability/{t_ava_repair} > Returns the probability that the damaged components can be  successfully repaired > 500")
@if_not_skipped
def get_maintainability_500(transaction):
    """
    Add a wrong ramsId to trigger 500
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/maintainability/{t_ava_repair} > Returns the probability that the damaged components can be  successfully repaired > 404")
@if_not_skipped
def get_maintainability_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = '/rams/200/maintainability/1'
    transaction['fullPath'] = '/rams/200/maintainability/1'


##======================= hooks for the path of /rams/{ransId}/survivability_uls ========================
# @hooks.before("/rams/{ramsId}/survivability_uls > Adds a task for the uls survivability assessment > 202")
# @if_not_skipped
# def post_task_survivability_uls_202(transaction):
#     """
#     Add a task for the survivability assessment (uls)
#     """
#     survivability_assessment(transaction, 1)
#     transaction['request']['body'] = json.dumps({"id": 1})

@hooks.before("/rams/{ramsId}/survivability_uls > Adds a task for the uls survivability assessment > 500")
@if_not_skipped
def post_task_survivability_uls_500(transaction):
    """
    Add a wrong ramsId to trigger 500
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/survivability_uls > Adds a task for the uls survivability assessment > 404")
@if_not_skipped
def post_task_survivability_uls_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = transaction['request']['uri'].replace(
        '1', '-1')
    transaction['fullPath'] = transaction['fullPath'].replace('1', '-1')

# @hooks.before("/rams/{ramsId}/survivability_uls/{taskId} > Returns the minimal probability that the critical component can survive the ultimate loads > 200 > application/json")
# @if_not_skipped
# def get_surivability_uls_200(transaction):
#     """
#     Get the results of the survivability assessment (uls)
#     """
#     transaction['request']['body'] = json.dumps({"id": 1}) 
#     fake_tr = {'request': {'uri': "/rams/1/survivability_uls"}}
#     fake_tr['protocol'] = transaction['protocol']
#     fake_tr['host'] = transaction['host']
#     fake_tr['port'] = transaction['port']
#     survivability_assessment(fake_tr, 1)
#     r = post_task_survivability_uls(transaction, 1)
#     task_id = r.json()["data"]["task_id"]
#     transaction['request']['uri'] = f'/rams/1/survivability_uls/{task_id}'
#     transaction['fullPath'] = f'/rams/1/survivability_uls/{task_id}'

# @hooks.before("/rams/{ramsId}/survivability_uls/{taskId} > Returns the minimal probability that the critical component can survive the ultimate loads > 500")
# @if_not_skipped
# def get_surivability_uls_500(transaction):
#     """
#     Trigger 500
#     """
#     transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/survivability_uls/{taskId} > Returns the minimal probability that the critical component can survive the ultimate loads > 404")
@if_not_skipped
def get_surivability_uls_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = '/rams/1/survivability_uls/10'
    transaction['fullPath'] = '/rams/1/survivability_uls/10'


##======================= hooks for the path of /rams/{ransId}/survivability_fls ========================
# @hooks.before("/rams/{ramsId}/survivability_fls > Adds a task for the fls survivability assessment > 202")
# @if_not_skipped
# def post_task_survivability_fls_202(transaction):
#     """
#     Add a task for the survivability assessment (fls)
#     """
#     survivability_assessment(transaction, 1)
#     transaction['request']['body'] = json.dumps({"id": 1})

@hooks.before("/rams/{ramsId}/survivability_fls > Adds a task for the fls survivability assessment > 500")
@if_not_skipped
def post_task_survivability_fls_500(transaction):
    """
    Add a wrong ramsId to trigger 500
    """
    transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/survivability_fls > Adds a task for the fls survivability assessment > 404")
@if_not_skipped
def post_task_survivability_fls_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = transaction['request']['uri'].replace(
        '1', '-1')
    transaction['fullPath'] = transaction['fullPath'].replace('1', '-1')

# @hooks.before("/rams/{ramsId}/survivability_fls/{taskId} > Returns the minimal probability that the critical component can survive the fatigue loads > 200 > application/json")
# @if_not_skipped
# def get_surivability_fls_200(transaction):
#     """
#     Get the results of the survivability assessment (fls)
#     """
#     save_inputs_r(transaction, 1)
#     r = add_task_reliability_system(transaction,1)

# @hooks.before("/rams/{ramsId}/survivability_fls/{taskId} > Returns the minimal probability that the critical component can survive the fatigue loads > 500")
# @if_not_skipped
# def get_surivability_fls_500(transaction):
#     """
#     Trigger 500
#     """
#     transaction['request']['body'] = ''

@hooks.before("/rams/{ramsId}/survivability_fls/{taskId} > Returns the minimal probability that the critical component can survive the fatigue loads > 404")
@if_not_skipped
def get_surivability_fls_404(transaction):
    """
    Add a wrong ramsId to trigger 404
    """
    transaction['request']['uri'] = '/rams/1/survivability_fls/10'
    transaction['fullPath'] = '/rams/1/survivability_fls/10'


##====================================== hooks for the result summary ====================================
# @hooks.before("/rams/results_summary/{ramsId} > Returns the summary of assessment results > 200 > application/json")
# @if_not_skipped
# def get_results_summary_200(transaction):
#     """
#     Gets the summary of assessment results
#     """
#     reliability_assessment(transaction, 1)
#     availability_assessment(transaction, 1)
#     maintainability_assessment(transaction, 1)
#     survivability_assessment(transaction, 1)

@hooks.before("/rams/results_summary/{ramsId} > Returns the summary of assessment results > 404")
@if_not_skipped
def get_results_summary_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/results_summary/200'
    transaction['fullPath'] = '/rams/results_summary/200'

# @hooks.before("/rams/results_summary/{ramsId} > Deletes the result summary > 200")
# @if_not_skipped
# def delete_results_summary_200(transaction):

@hooks.before("/rams/results_summary/{ramsId} > Deletes the result summary > 404")
@if_not_skipped
def delete_results_summary_404(transaction):
    """
    Apply a wrong ramsId to trigger 404.
    """
    transaction['request']['uri'] = '/rams/results_summary/200'
    transaction['fullPath'] = '/rams/results_summary/200'


##================================= hooks for the Digital Represenataion  =================================
# @hooks.before("/representation/{ramsId} > Gets the specific rams assessment results > 200 > application/json")
# @if_not_skipped
# def get_representation_200(transaction):
#     """
#     Gets the specific rams assessment results
#     """
#     data = json.loads(transaction["request"]["body"])
#     transaction["request"]["body"] = json.dumps(data)
