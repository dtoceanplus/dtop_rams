.. _rams-home:

Reliability, Availability, Maintainability and Survivability
============================================================

Introduction 
------------

The Reliability, Availability, Maintainability and Survivability (RAMS) module is a software that assesses reliability, availability, maintainability, and survivability of marine energy conversion systems from the early concept stage to the commercial deployment. The definitions of reliability, availability, maintainability, and survivability, are given below:

- reliabiity is the ability of a structure or structural member to fulfil the specified requirements, during the working life, for which it has been designed. 
- availability theoretically refers to the probability that a system or component is performing its required function at a given point in time or over a stated period of time when operated and maintained in a prescribed manner. In most of the engineering applications, it is defined as the ratio of the uptime to the design lifetime.
- maintainability is the ability of a system to be repaired and restored to service when maintenance is conducted by personnel using specified skill levels and prescribed procedures and resources.
- survivability is the probability that the converter will stay on station over the stated operational life.


Structure
---------

This module's documentation is divided into four main sections:

- :ref:`rams-tutorials` to give step-by-step instructions on using RAMS for new users. 

- :ref:`rams-how-to` that show how to achieve specific outcomes using RAMS. 

- A section on :ref:`background, theory and calculation methods <rams-explanation>` that describes how RAMS works and aims to give confidence in the tools. 

- The :ref:`API reference <rams-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The Reliability, Availability, Maintainability and Survivability module has six major functionalities:  

#. :ref:`reliability assessment <reliability_tutorial>`– contains both component-level and system-level assessments. The component-level reliability assessment simulates the time to failure (TTF) of basic components; estimates the mean values, maximum and standard deviations of TTFs of basic components. The system-level reliability assessment simulates the time to failure (TTF) of subsystems (energy delivey, energy transformation and station keeping) and the array;  estimates the mean values, maximum and standard deviations of TTFs of these subsystems and the array; calculates the maximum annual probability of failure (PoF) of these subsystems and the array.

#. :ref:`availability assessment <availability_tutorial>`– calculates the availability of the individual devices and the average availability of the array.

#. :ref:`maintainability assessment <maintainability_tutorial>`– calculates the probability that the damaged components can be repaired within a specific period of time, given presrivbed resources and equipment.   


Workflow for using the RAMS module
----------------------------------

The four features, namely reliability, availability, maintainability and survivability, are assessed separately in the RAMS module. The generic workflows are the same, which includes collection of inputs, check the inputs, perform assessment and view the results.

.. image:: ./Flowchart_reliability.png
  :width: 350
  :alt: Alternative text

Figure 1 Workflow of RAMS Module -Reliability assessment

.. image:: ./Flowchart_availability.png
  :width: 350
  :alt: Alternative text

Figure 2 Workflow of RAMS Module - Availability assessment

.. image:: ./Flowchart_maintainability.png
  :width: 350
  :alt: Alternative text

Figure 3 Workflow of RAMS Module - Maintainability assessment

.. image:: ./Flowchart_survivability.png
  :width: 350
  :alt: Alternative text

Figure 4 Workflow of RAMS Module - Survivability assessment

Overview of RAMS data requirements
----------------------------------

Reliability assessment requires the hierarchies of the ED, ET and SK subsystems, the number of simulations and the waiting time, as summarized in the following table.

Table 1 Summary of Inputs for Reliability Assessment

+------------------------+----------+--------------------+---------+
| External module inputs | Default  |     Data origin    |  Units  |
+========================+==========+====================+=========+
|      ED hierarchy      | Required | ED or user-defined |    -    |
+------------------------+----------+--------------------+---------+
|      ET hierarchy      | Required | ET or user-defined |    -    |
+------------------------+----------+--------------------+---------+
|      SK hierarchy      | Required | SK or user-defined |    -    |
+------------------------+----------+--------------------+---------+
| Number of simulations  | Required |    User-defined    |    -    |
+------------------------+----------+--------------------+---------+
|     Waiting time       | Required |    User-defined    |   hour  |
+------------------------+----------+--------------------+---------+

Availability assessment requires the downtime of all the devices in an array, as summarized in the following table.

Table 2 Summary of Inputs for Availability Assessment

+------------------------+----------+---------------------+---------+
| External module inputs | Default  |      Data origin    |  Units  |
+========================+==========+=====================+=========+
|        Downtime        | Required | LMO or user-defined |    -    |
+------------------------+----------+---------------------+---------+

Maintainability assessment requires the downtime of all the devices in an array, as summarized in the following table.

Table 3 Summary of Inputs for Maintainability Assessment

+-----------------------------------------+----------+---------------------+---------+
|          External module inputs         | Default  |      Data origin    |  Units  |
+=========================================+==========+=====================+=========+
|              Available time             | Required |     User-defined    |   hour  |
+-----------------------------------------+----------+---------------------+---------+
| Probability distribution of repair time | Required |     User-defined    |    -    |
+-----------------------------------------+----------+---------------------+---------+
|    Standard deviation of repair time    | Required | SK or user-defined  |   hour  |
+-----------------------------------------+----------+---------------------+---------+
|                  MTTR                   | Required | LMO or user-defined |    -    |
+-----------------------------------------+----------+---------------------+---------+
|              Technologies               | Required | LMO or user-defined |    -    |
+-----------------------------------------+----------+---------------------+---------+

Survivability assessment requires the inputs in the following table.

Table 4 Summary of Inputs for Survivability Assessment

+-----------------------------------------+----------+-------------------------+---------+
|          External module inputs         | Default  |        Data origin      |  Units  |
+=========================================+==========+=========================+=========+
|              stress_sk.json             | Required |   SK or user-defined    |    -    |
+-----------------------------------------+----------+-------------------------+---------+
|              stress_et.json             | Required |   ET or user-defined    |    -    |
+-----------------------------------------+----------+-------------------------+---------+
|             Other parameters            | Required | Default or user-defined |    -    |
+-----------------------------------------+----------+-------------------------+---------+

The following table summarizes the other parameters and the explanations. 

Table 5 Explanation of Other Parameters for Survivability Assessment

+--------------------+----------+--------------------------------------------------------------------------------------------+
|     Parameters     |  Format  |                         Explanation                                                        |
+====================+==========+============================================================================================+
|        cov_a       |   float  |The coefficient of variance of the S-N curve parameter a                                    |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        cov_l       |   float  |The coefficient of variance of the extreme/ ultimate load                                   |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        cov_q       |   float  |See Note 1)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        cov_r       |   float  |See Note 2)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|       cov_ufl      |   float  |The coefficient of variance of the uncertainty factor associated with the load|             |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|       cov_ufr      |   float  |See Note 3)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|       mu_ufl       |   float  |The mean value of the uncertainty factor associated with the load                           |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|       mu_ufr       |   float  |The mean value of the uncertainty factor associated with the resistance                     |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|      n_sim_fls     |  integer |The number of simulations for the survivability assessment (fatigue limit state, FLS)       |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|      n_sim_uls     |  integer |The number of simulations for the survivability assessment (ultimate limit state, ULS)      |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|     option_fls     |  string  |See Note 4)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|     option_uls     |  string  |See Note 5)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_a        |  string  |The probability distribution of the S-N curve parameter a                                   |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_h        |  string  |See Note 6)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_l        |  string  |The probability distribution of the load                                                    |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_m        |  string  |The probability distribution of the S-N curve parameter m                                   |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_n        |  string  |The probability distribution of the number of stress range cycles                           |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_q        |  string  |See Note 7)                                                                                 |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|        pd_r        |  string  |The probability distribution of the resistance                                              |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|       pd_ufl       |  string  |The probability distribution of the uncertainty factor associated with the load             |
+--------------------+----------+--------------------------------------------------------------------------------------------+
|       pd_ufr       |  string  |The probability distribution of the uncertainty factor associated with the resistance       |
+--------------------+----------+--------------------------------------------------------------------------------------------+


| Notes:

| 1) The coefficient of variance of the scale parameter of the 2-parameter Weibull distribution (assumed that the long-term stress ranges follow the 2-parameter Weibull distribution).

| 2) The coefficient of variance of the resistance (maximum breaking load, MBL) of the mooring lines.

| 3) The coefficient of variance of the uncertainty factor associated with the resistance.

| 4) The method used for assessing the survivability (FLS), option 1 – ‘Monte Carlo’ (for complexity 1, 2 & 3);
   option 2 – ‘FORM’ (for complexity 2 & 3).

| 5) The method used for assessing the survivability (ULS), option 1 – ‘Monte Carlo’ (for complexity 1, 2 & 3); option 2 – ‘FORM’ (for complexity 2 & 3).

| 6) The probability distribution of the shape parameter of the 2-parameter Weibull distribution (assumed that the long-term stress ranges follow the 2-parameter Weibull distribution).

| 7) The probability distribution of the scale parameter of the 2-parameter Weibull distribution (assumed that the long-term stress ranges follow the 2-parameter Weibull distribution).

The data structure of stress_sk.json and stress_et.json are described as follows:

Table 6 Explanation of Data in stress_sk.json

+------------------------------------------------------------------+-------------------------------------------------+
|                               Data                               |                    Data origin                  |
+==================================================================+=================================================+
|The ultimate loads on the mooring lines                           |  devices[i]["uls_results"]["mooring_tension"]   |
+------------------------------------------------------------------+-------------------------------------------------+
|The maximum breaking loads (MBL) of the mooring lines             |       devices[i]["uls_results"]["mbl_uls"]      |
+------------------------------------------------------------------+-------------------------------------------------+
|The stress ranges on the mooring lines                            |  devices[i]["fls_results"]["cdf_stress_range"]  |
+------------------------------------------------------------------+-------------------------------------------------+
|The cumulative distribution functions (CDFs) of the stress ranges |          devices[i]["fls_results"]["cdf"]       |
+------------------------------------------------------------------+-------------------------------------------------+
|The S-N curve parameter a                                         |          devices[i]["fls_results"]["ad"]        |
+------------------------------------------------------------------+-------------------------------------------------+
|The S-N curve parameter m                                         |           devices[i]["fls_results"]["m"]        |
+------------------------------------------------------------------+-------------------------------------------------+
|The number of stress range cycles                                 | devices[i]["fls_results"]["n_cycles_lifetime"]  |
+------------------------------------------------------------------+-------------------------------------------------+

Notes: 
1) the key "devices" is inlcuded in the raw output json file from the SK module. 

Table 7 Explanation of Data in stress_et.json

+------------------------------------------------------------------+-------------------------------------------------+
|                               Data                               |                    Data origin                  |
+==================================================================+=================================================+
|The S-N curve                                                     |                    [i]["S_N"]                   |
+------------------------------------------------------------------+-------------------------------------------------+
|The ultimate stresses                                             |              [i]["ultimate_stress"]             |
+------------------------------------------------------------------+-------------------------------------------------+
|The maximum stresses and probability                              |         [i]["maximum_stress_probability"]       |
+------------------------------------------------------------------+-------------------------------------------------+
|The fatigue stresses and probability                              |         [i]["fatigue_stress_probability"]       |
+------------------------------------------------------------------+-------------------------------------------------+
|The number of cycles of stress ranges                             |                [i]["number_cycles"]             |
+------------------------------------------------------------------+-------------------------------------------------+

| Notes:

| 1) "S_N" is a dictionary, including four kesys, namely, "description", "lavel", "unit" and "value".

|  The key "value" contains the actual data, which is a 2D list. Each 1D list contains the S-N curve paramers. The first and second 1D lists contain the parameters corresponding to the first and second parts of a bilinear S-N curve. For example, ["value"][0] = [3.0, 10.97]. m=3.0 log10(a)=10.97.

| 2) "ultimate_stress" is a dictionary, including four kesys, namely, "description", "lavel", "unit" and "value". The key "value" contains the actual data, which is a float.

| 3) "maximum_stress_probability" is a dictionary, including four kesys, namely, "description", "lavel", "unit" and "value". The key "value" contains the actual data, namely "stress" and "proability". Both "stress" and "probability" contain 1D list.

| 4) "fatigue_stress_probability" is a dictionary, including four kesys, namely, "description", "lavel", "unit" and "value". The key "value" contains the actual data, namely "stress" and "proability". Both "stress" and "probability" contain 1D list.

| 5) "number_cycles" is a dictionary, including four kesys, namely, "description", "lavel", "unit" and "value".
   The key "value" contains the actual data, which is a 1D list. It should be stressed that the each entry must correspond to the correspnding stress range in the key "value" in "fatigue_stress_probability".