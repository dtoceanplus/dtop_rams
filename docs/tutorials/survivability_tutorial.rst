.. _survivability_tutorial: 

Assess the survivability
========================

Introduction and Background
---------------------------

The RAMS module only assesses the survivability from the perspective of structural integrity. The classi structural reliability analysis is the theoretical basis of the survivability assessment. 

The survivability assessment aims to estimate the probability of critical components surviving the stresses/ loads during the design lifetime from the perspectives of the ultimate and the fatigue limit states. The critical components can refer to the PTOs in the Energy transformation (ET) subsystem, the mooring lines in the Station keeping (SK) subsystem.

Both ulimtate limit state (ULS) and fatigue limit state (FLS) are the two only limit states considered. 

This section contains four tutorials:

#. `Upload the inputs`_

#. `Check the input sumary`_

#. `Perform the assessment`_

#. `Access the assessment results`_

Upload the inputs
-----------------

#. Navigate to Survivability by clicking ``R.A.M.S. Studies`` in the navigation pane on the left-hand side of the RAMS GUI.

#. Upload the stresses of the ET and SK subsystems by clicking the ``Upload SK stress`` and ``Upload ET stress`` buttons on the top of the RAMS GUI. A panel pops up, after clicking either of these buttons. Click the ``Browse`` button to find and upload the input file. Two messages, namely ``JSON file decoded successfully`` and ``JSON file saved to DB successfully``, pop up to indicate that the inputs are successfully uploaded.  

#. Go to ``Simultation Input``. Set up the ``Number of Simultation`` by clicking either plus or minus icons and choose the ``Simulation Method``. 

#. Go to ``ULS Input``. Set up the stochastic variables for ULS. Please refer to xxx for more details regarding these stochastic variables.

#. Go to ``FLS Input``. Set up the stochastic variables for ULS. Please refer to xxx for more details regarding these stochastic variables.

Check the input sumary
----------------------
#. The user can check the inputs to confirm, by clicking ``Input Summary``. 

Perform the assessment
----------------------

#. Click the ``Calculate`` button to start the calculation.


