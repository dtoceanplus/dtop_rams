.. _reliability_tutorial: 

Assess the reliability
======================

Introduction and Background
---------------------------

The fault tree (FT) is the theoretical basis of the reliability assessment. A FT is a traditional way representing failure events connected through logic gates. In the framework of FT, every element represents a failure event caused by a unit failure. ‘Unit’ is used, because a unit may refer to a basic component, a subassembly, a subsystem or a system based upon the complexity degree of a system hierarchy.

In most engineering applicaitons, the logical gate can be either ‘OR’, ‘AND’ or ‘k/N’ (also called vote gate). With no loss of generality, the nodes underneath the gate are called input, and the node above it is called output. ‘OR’ means that any input to the gate fails, the output fails. ‘AND’ means that if all the inputs to the gate fail, the output fails. ‘k/N’ means that if either k out of N inputs fail, the output fails. 

The nodes on the bottom most of a FT correspond to the failures of basic components. It is assumed that the time to failures (TTF) of basic components are statistically independent. Monte Carlo approach is used to randomly simulate the TTFs. The TTF of the units other than basic components is estimated based upon the logic dependencies. 

The RAMS module assesses reliability at both the component and the system levels. 

This section contains four tutorials:

#. `Upload the inputs`_

#. `Check the input sumary`_

#. `Perform the assessment`_

#. `Access the assessment results`_

Upload the inputs
-----------------

#. Navigate to Reliability by clicking ``R.A.M.S. Studies`` in the navigation pane on the left-hand side of the RAMS GUI.

#. Upload the hierarchies of the ED, ET and SK subsystems by clicking the ``Upload SK``,  ``Upload ED`` and ``Upload ET`` buttons on the top of the RAMS GUI. A panel pops up, after clicking either of these buttons. Click the ``Browse`` button to find and upload the input file. Two messages, namely ``JSON file decoded successfully`` and ``JSON file saved to DB successfully``, pop up to indicate that the inputs are successfully uploaded.  

#. Set up ``Average Waiting Time`` and the ``Number of Simulation`` by clicking either plus or minus icons. 

Check the input sumary
----------------------
#. The user can check the inputs to confirm, by clicking ``Input Summary``. 

Perform the assessment
----------------------

#. Click the ``Calculate`` button to start the calculation.


