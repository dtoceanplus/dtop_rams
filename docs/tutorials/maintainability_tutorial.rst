.. _maintainability_tutorial: 

Assess the maintainability
==========================

Introduction and Background
---------------------------

Maintainability refers to the probability that the damaged components can be repaired within a specific period of time, given presrivbed resources and equipment. The time to repair (TTR) is a stochastic variable. The mean TTR is the input provide by the LMO module. The probability distribution, the specific period of time (or called the available time period) and the standard devition of the mean TTR

This section contains four tutorials:

#. `Upload the inputs`_

#. `Check the input sumary`_

#. `Perform the assessment`_

#. `Access the assessment results`_

Upload the inputs
-----------------

#. Navigate to Maintainability by clicking ``R.A.M.S. Studies`` in the navigation pane on the left-hand side of the RAMS GUI.

#. Upload the downtime of the individual devices by clicking the ``Upload LMO`` button on the top of the RAMS GUI. A panel pops up, after clicking the ``Upload LMO`` button. Click the ``Browse`` button to find and upload the input file. Two messages, namely ``JSON file decoded successfully`` and ``JSON file saved to DB successfully``, pop up to indicate that the inputs are successfully uploaded.

#. Set up the ``Specific Time`` and the ``Standard Deviation`` by clicking either plus or minus icons. Choose the "Probability Distribution". 

Check the input sumary
----------------------
#. The user can check the inputs to confirm, by clicking ``Input Summary``. 

Perform the assessment
----------------------

#. Click the ``Calculate`` button to start the calculation.

Access the assessment results
-----------------------------

#. Click ``System maintainability`` to see the the probability that the damaged components can be repaired within a specific period of time. 