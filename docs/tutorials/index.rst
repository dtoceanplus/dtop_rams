.. _rams-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Reliability, Availability, Maintainability and Survivability tool.
They are intended for those who are new to the Reliability, Availability, Maintainability and Survivability tool.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

#. :ref:`reliability_tutorial`
#. :ref:`availability_tutorial`
#. :ref:`maintainability_tutorial`
#. :ref:`survivability_tutorial`

.. toctree::
   :maxdepth: 1
   :hidden:

   reliability_tutorial
   availability_tutorial
   maintainability_tutorial
   survivability_tutorial