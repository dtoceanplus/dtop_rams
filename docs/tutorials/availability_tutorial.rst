.. _availability_tutorial: 

Assess the availability
=======================

Introduction and Background
---------------------------

The RAMS module only assesses the time-based availability, which is the ratio of the uptime to the sum of uptime and downtime.   

This section contains four tutorials:

#. `Upload the inputs`_

#. `Check the input sumary`_

#. `Perform the assessment`_

#. `Access the assessment results`_

Upload the inputs
-----------------

#. Navigate to Availability by clicking ``R.A.M.S. Studies`` in the navigation pane on the left-hand side of the RAMS GUI.

#. Upload the downtime of the individual devices by clicking the ``Upload LMO`` button on the top of the RAMS GUI. A panel pops up, after clicking the ``Upload LMO`` button. Click the ``Browse`` button to find and upload the input file. Two messages, namely ``JSON file decoded successfully`` and ``JSON file saved to DB successfully``, pop up to indicate that the inputs are successfully uploaded.

Check the input sumary
----------------------
#. The user can check the inputs to confirm, by clicking ``Input Summary``. 

Perform the assessment
----------------------

#. Click the ``Calculate`` button to start the calculation.

Access the assessment results
-----------------------------

#. Click ``System availability`` to see the availability of the devices and the average availability of the array which are shown in a bar plot. The horizontal axis represents the availability in percentage. The vertical represents different items, for example, devices, array. 