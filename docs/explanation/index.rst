.. _rams-explanation:

*********************
Background and theory
*********************

This section describes the background, theoretical basis and fundamental assumptions behind the module.  

Background
==========

The Reliability Availability Maintainability, and Survivability (RAMS) module assesses the following metrics: 

#: Reliability refers to the ability of a structure or structural member to fulfil the specified requirements, during the working life, for which it has been designed. 
#: Availability refers to the probability that a system or component is performing its required function at a given point in time or over a stated period of time when operated and maintained in a prescribed manner. In engineering applications, the availability of a device is the ratio of the uptime to the sum of uptime and downtime during the design lifetime. The availability of the array is the arithmetic average of that of all devices in the array.
#: Maintainability refers to the ability of a system to be repaired and restored to service when maintenance is conducted by personnel using specified skill levels and prescribed procedures and resources.
#: Survivability refers to the probability that the critical structural and mechanical components can survive the ultimate and fatigue loads during the design lifetime.


Theoretical Basis
=================

Below is a set of background documents which help the user understand the theory behind reliabilty, availaiblity, maintainability and survivability assessments. 

#. :ref:`reliability_theory`

#. :ref:`availability_theory`

#. :ref:`maintainability_theory`

#. :ref:`survivability_theory`


Examples
========

Below is a set of examples which help the user understand how reliabilty, availaiblity, maintainability and survivability assessments are performed based upon the required inputs. 

#. :ref:`reliability_example`


.. toctree::
   :maxdepth: 1
   :hidden:

   reliability_theory
   reliability_example
   availability_theory
   availability_example
   maintainability_theory
   maintainability_example
   survivability_theory
   survivability_example