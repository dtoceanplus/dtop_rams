.. _survivability_theory: 

Theory - survivability
======================

Overview
--------

Survivability is only assessed for the structural and mechanical subsystems. The first order reliability method (FORM) and Monte Carlo simulation is the theoretical basis. Ultimate limit state (ULS) and fatigue limit state (FLS) are two most important design condistions for which the structural and mechanical subsystems are verified in design standards. Therefore, both ULS and FLS are the limit states to be assessed. 

This session is structured as follows:

#. Fundamental Case of Probabilty of Failure

#. Limit States

#. Limit State Functions

#. Main Steps


Fundamental Case of Probabilty of Failure 
-----------------------------------------

Consider a generic structural or mechnaical unit with load bearing capacity R. This unit is subjected to a load S. R and S are modeled by independent stochastic variables with density functions f and S f and distribution functions R F and S F , see Figure 1. 

.. image:: ./figure_survivability_1.png
  :width: 300
  :alt: Alternative text

The probability of failure (PoF) becomes:

.. math:: P_{F} = \int_{-\infty}^{\infty}{P(R \leq x)P(x < S \leq x + dx)dx}


Limit State 
-----------

The concept of limit state related to a specified requirement is defined as a state of the structure
including its loads at which the structure is just on the point of not satisfying the requirement.

usually the requirement will be interpreted and formulated within a mathematical model for the geometric and mechanical properties of the structure and for the actions on the structure. Let x1,x2, . . . ,xn be those variables that independently contribute to that part of the mathematical model that concerns geometry, strength properties and actions. The variables are free in the sense that their values can be chosen freely and independently within a given subset of the n-dimensional space. This subset is the domain of definition of the model in the same way as a function of n variables has a domain of definition.
To each choice of values corresponds a uniquely defined structure with uniquely defined loads. This structure with its loads is a pure mathematical object that does or does not satisfy a given limit state requirement. Possibly it cannot at all be realized as a physical object, for example because the load exceeds the carrying capacity of the structure.


Limit State Functions
---------------------

In general a limit state can be defined by an equation g(x1, . . . ,xn) = 0 in which g is some function of the input variables chosen such that g(x) > 0 for all x in the internal of the safe set, and g(x) < 0 for x in the internal of the adverse event (the failure set).

The limit state functions are only defined for the mechanical failure modes ude to damages, which are basically caused by both ultimate and cyclic loadings. Overstress and buckling are the common failures caused by the ultimate strength loadings (causing progressive degradation or sudden failure), while fatigue cracks are caused by cyclic loadings. 

In the following part of this sub-section, how to define limit state functions will be elabrated on.

#. ULS

The ultimate limit generally refers to the structural capacity againist the extreme loads. The material resistance refers to the yielding strength. 

The limit state function is given by: 

.. math:: g( X_{1},X_{2},\ldots,X_{n}) = X_{R}R(X_{1},X_{2},\ldots) - X_{S}S(\ldots,X_{n})
   :name: Equation 8

#. FLS

The classic S-N curve approach is used to model the fatigue strength of structural and mechanical components. This approach can cover the fatigue behaviour both on welded details and on mother materials. The cyclic stress ranges are caused by wave loadings exerted upon the structural and mechanical components. 

The limit state function is given by:

.. math:: g(A,B,N,a,m) = -\ln(N) + \ln(a) - m\ln{(B) - ln\left( \Gamma\left( 1 + \frac{m}{A} \right) \right)}
   :name: Equation 9


Main Steps
----------

#. Identify the significant failure modes of the structure, namely ULS and FLS.

#. Formulate failure functions (limit state functions) corresponding to each component in the failure
modes.

#. Identify the stochastic variables and the deterministic parameters in the failure functions. Further
specify the distribution types and statistical parameters for the stochastic variables and the
dependencies between them.

#. Estimate the probability of failure (PoF) of each failure mode.


First Order Reliability Method (FORM)
-------------------------------------

FORM is a Level II reliabilty method. The uncertain parameters are modelled by the mean values and the standard deviations, and by the correlation coefficients between the stochastic variables. The stochastic
variables are implicitly assumed to be normally distributed. 

