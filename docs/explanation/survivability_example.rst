.. _survivability_example: 

Example - survivability
=======================

Overview
--------

The critical structural components in the Staion Keeping (SK) module will be used as an example. It is assumed that there is a sample array containing four devices. Each device is kept in place through four mooring lines. Tension is assumed to be the dominating load for both ultimate and fatigue limit states. Unless otherwise specified, the load refers to tension and the stress ranges refer to those caused by the alternating tensions. Monte Carlo Method is used. The design lifetime is assumed to be 20 years in this subsection.
 

Ultimate Limit State
--------------------
According to the extreme strength analysis performed in the SK module, the four devices are subject to the identical ultimate loads. The ultimate loads exerted on the four mooring lines of one device are used to estimate the mean load. The standard deviation or coefficient of variance (CoV) is estimated based upon the engineering experience. The maximum breaking load (MBL) is the material resistance of the mooring lines, provided by the SK module. In nature, both load and MBL are stochastic variables. So, two uncertainty factors, which are also stochastic variables, are applied to load and MBL, respectively. See Table 1 for these inputs. 
Note: Since these variables are assumed to follow the LogNormal distribution, the mean values and standard deviations of the logged variables are given as follows: 

variables (to be added)

Table 1 For the Ultimate Survivability Assessment

+----------+------------------------------------------+---------------+----------+-----+
| variable |               Description                | Probabilistic |   Mean   | CoV |
|          |                                          |  Distribution |          |     |
+==========+==========================================+===============+==========+=====+
|    S     |The load exerted upon the mooring lines   |   LogNormal   |  83313 N | 0.3 |
+-----------------+--------------+--------------------+---------------+----------+-----+
|    R     |The material resistance the mooring lines |   LogNormal   | 270000 N | 0.2 |
+-----------------+--------------+--------------------+---------------+----------+-----+
|    Xs    |The uncertainty factor for the load       |   LogNormal   |    1.0   | 0.1 |
+-----------------+--------------+--------------------+---------------+----------+-----+
|    Xr    |The uncertainty factor for the resistance |   LogNormal   |    1.0   | 0.1 |
+-----------------+--------------+--------------------+---------------+----------+-----+


Fatigue Limit State
-------------------
As mentioned in the theory, the S-N curve-based approach is used to assess the survivability from the perspective of fatigue limit state. The variables include the number of cycles of stress ranges, the S-N curve parameters a and m, and the Weibull distribution parameters A and B. See Table 2 for these inputs. 

Table 2 For the Ultimate Survivability Assessment

+----------+---------------------------------------------------------------+---------------+----------+-----+
| variable |                         Description                           | Probabilistic |   Mean   | CoV |
|          |                                                               |  Distribution |          |     |
+==========+===============================================================+===============+==========+=====+
|    a     |log(a) is the intercept of log(N)-axis by the linear S-N curve |   LogNormal   |   27.09  | 0.25|
+----------+---------------------------------------------------------------+---------------+-----------+----+
|    m     |The negative inverse slope of the linear S-N curve.            |   Constant    |    3.0   |  -  |
+----------+---------------------------------------------------------------+---------------+----------+-----+
|    A     |The shape parameter of the 2-parameter Weibull distribution    |   Constant    |   1.778  |  -  |
|          |of the long-term stress ranges                                 |               |          |     |
+----------+---------------------------------------------------------------+---------------+----------+-----+
|    B     |The scale parameter of the 2-parameter Weibull distribution    |   LogNormal   |   0.115  | 0.5 |
|          |of the long-term stress ranges                                 |               |          |     |
+----------+---------------------------------------------------------------+---------------+----------+-----+
|    N     |The number of cycles of stress ranges                          |   Constant    |    1e8   |  -  |
+----------+---------------------------------------------------------------+---------------+----------+-----+
