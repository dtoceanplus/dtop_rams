.. _maintainability_theory: 

Theory - maintainability
========================

Overview
--------

Maintainability assessment is intended for calculating the probability that the damaged components can be successfully repaired within the given time. 


Probability Distribution of Repair Time
---------------------------------------

The time to repair (TTR) follows a probabilistic distribution, e.g. Gaussian distribution, LogNormal distribution, and Exponential distribution. The mean time to repair (MTTR) is only estimated for the components which need to be repaired. For one damaged component, MTTR is considered the mean value of the probabilistic distribution for the TTR. The user can choose one of these default probabilistic distributions, input a Std (or CoV) for the chosen probabilistic distribution and input an available time (t). 

RAMS should calculate these probabilities for all components and give the minimal probability as the criterion of maintainability for the array/ farm. 

The probability density functions (PDFs) for these default probabilistic distributions are as follows: 

Gaussian distribution:

.. math:: f_{T}(t) = \frac{1}{\sqrt{2\pi}\sigma_{T}}e^{\frac{-1}{2\sigma_{T}^{2}}\left(t - \mu_{T}\right)^{2}}
   :name: Equation 4

LogNormal distribution:

.. math:: f_{T}(t) = \frac{1}{\sqrt{2\pi}\sigma_{ln(T)}}e^{\frac{-1}{2\sigma_{ln(T)}^{2}}\left(t - \mu_{ln(T)}\right)^{2}}
   :name: Equation 5

Exponential distribution:

.. math:: f_{T}(t) = \lambda e^{- \lambda t}
   :name: Equation 6