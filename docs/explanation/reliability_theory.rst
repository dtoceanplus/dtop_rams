.. _reliability_theory: 

Theory - reliability
====================

Overview
--------

Reliability assessment is mainly based upon the fault tree (FT) and the technology of simulating the time to failures (TTF). 


Fault Tree
----------

A FT is a traditional way representing failure events connected through logic gates. In the framework of FT, every element represents a failure event caused by a unit failure. ‘Unit’ is used, because a unit may refer to a basic component, a subassembly, a subsystem or a system based upon the complexity degree of a system hierarchy. 

In most engineering applicaitons, the logical gate can be either ‘OR’, ‘AND’ or ‘k/N’ (also called vote gate). With no loss of generality, the nodes underneath the gate are called input, and the node above it is called output. ‘OR’ means that any input to the gate fails, the output fails. ‘AND’ means that if all the inputs to the gate fail, the output fails. ‘k/N’ means that if either k out of N inputs fail, the output fails. The nodes on the bottom most of a FT correspond to the failures of basic components. It is assumed that the time to failures (TTF) of basic components are statistically independent.

The FT can be used for calculating the probability of failure (PoF) and estimating the TTF of units at all level in a system. With the aim of automatic calculation, a compact data structure called hierarchy is introduced to implement the FT in the RAMS module.  

A hierarchy is a 2-D table array storing the information on the working philosophy and the interrelationship of the units at different levels reflected in a fault tree. See the template in Table XX. The first column gives the subsystem or system to be analysed. All failure events are considered nodes in the hierarchy. The second column, ‘Name of Node’, gives the names of these failure events. The third column, ‘Design Id’, gives the identification labels of the basic components and other units. The column, ‘Node Type’, defines the levels of a hierarchy. The column, ‘Node SubType’, defines the additional information the design modules use to identify the corresponding node. The column, ‘Category’, defines which levels the nodes in the ‘Name of Node’ column belong to in the fault tree. The columns ‘Parent’ and ‘Child’ define the dependencies of units at various levels. Each entry in ‘Parent’ defines the label of the higher-level unit which the current unit in the column ‘Name of Node’ belongs to. Each entry in ‘Child’ defines the labels of lower-level units which belong to the current unit. Based upon the aforementioned descriptions, the units in the column ‘Child’ are connected through a specific logic gate to the higher-level unit. The logic gates are given in the column ‘Gate Type’. The logic gate in each entry of this column is used to connect the unit in the column ‘Name of Node’ and the units in the column ‘Child’. The last two columns give the failure rates of basic components for two failure modes.

+--------+-------------+-----------+----------+-------------+----------+--------+-------+------+----------------+----------------+
| System | Name of Node| Design Id | Node Type| Node Subtype| Category | Parent | Child | Gate | Failure Rate 1 | Failure Rate 2 |
+========+=============+===========+==========+=============+==========+========+=======+======+================+================+
|        |             |           |          |             |          |        |       |      |                |                |
+--------+-------------+-----------+----------+-------------+----------+--------+-------+------+----------------+----------------+ 

"Failure Rate 1" refers to the case in which the damaged components can be repaired. "Failure Rate 2" refers to the case in which the damaged components have to be replaced;

It should be stressed that the definitions of 'AND' and 'OR' gates in the hierarchy are opposite to those in FT, as listed below. The reason is: hierarchy is used by both LMO and RAMS; the marine operations can be easily implemented according to the current defitions of logic gates. In the RAMS module the logic gates in the hierarchy are converted to the right ones in FT. 

#. 'OR' gate in the hierarchy corresponds to 'AND' gate in FT.

#. 'AND' gate in the hierarchy corresponds to 'OR' gate in FT. 


Simulation of time to failure
-----------------------------

The objectives of component-level reliability assessment are to:

#. simulate the time series of failure events of basic components under the fundamental assumption that the failures of these basic components are statistically independent.

#. estimate the mean time to failure (MTTF), the maximym time and and the standard deviations of the time to failure (TTF) of these basic components.

The Monte Carlo simulation is the basic methodology used to simulate the time to failures of basic components. The probabilistic lifetime distribution of all basic components is assumed to follow the exponential distribution. The time to failure has the following probability distribution function.

.. math:: F(t) = P(T \leq t) = 1 - e^{- \lambda t}
   :name: Equation 1

The time to failure corresponding to the probability is re-expressed by Eq.(2). 

.. math:: t = - \frac{1}{\lambda}\ln\left\lbrack 1 - \widehat{F}(t) \right\rbrack
   :name: Equation 2

where, λ is the failure rate of a basic component. The capitalised T denotes the stochastic variable of the time to failure. The variable t represents the stated time. 

The procedure for simulating failure events is summarized as follows:

#. Initiate the input parameters, e.g. the start-up time (t0), the lifetime (tL) and the failure rate

#. uniformly sample a number from [0, 1] based upon Eq.(1)

#. Obtain the i-th (i=1,2,3…) failure time tfi based upon Eq.(2) 

#. Check whether or not the termination criterion is satisfied (tfi is greater than tL); if yes, terminate this lifetime simulation; otherwise, shift t0 to tfi and go to step 2 to continue sampling.

The objectives of system-level reliability assessment are to:
#. simulate the TTF of the units other thatn the basic components through a bottom-up approach.
#. estimate the mean time to failure (MTTF), the maximum and the standard deviations of time to failure (TTF) for the Energy Delivery, Energy Transformation, Station Keeping subsystmes and the whole array.

.. image:: ./figure_reliability_1.png
  :width: 300
  :alt: Alternative text

Figure 1 Flow Chart Illustrating Simulation of System TTF

The state of an intermediate unit which is directly associated with the nodes representing the basic components can be determined according to the states of these basic components and the type of logic gate. Then, the state of a second-level intermediate unit can be determined according to the states of the 1st-level intermediate nodes and the type of logic gate. Such a recursive process can be repeated until the top node is reached. The process can be implemented in a computer code, with the flow chart illustrated in Figure XX1.