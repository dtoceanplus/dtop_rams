.. _maintainability_example: 

Example - maintainability
=========================

Overview
--------

The probabilistic distribution of repair time is assumed to follow the Gaussian distribution. The user may want to know the probability that the damage component can be successfully repaired in t=24 hours. In light of the high uncertainty in offshore on-site repair, a high standard deviation of 18 hours is assumed (in reality, this should be input by the user). The generic inputs are given in Table 1. 

Table 1 Summary of Mean Time to Failure

+-----------------+---------------------+
|        Id       |         MTTR        |
+=================+=====================+
|       ml11      |          19         |
+-----------------+---------------------+
|       ml12      |          25         |
+-----------------+---------------------+
|       ml13      |          43         |
+-----------------+---------------------+
|       ml14      |          13         |
+-----------------+---------------------+

According to the Gaussian probability distribution function, the probabilities that these damaged components can be calculated, and the minimal probability can be obtained.