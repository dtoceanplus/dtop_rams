.. _reliability_example: 

Example - reliability
=====================

Overview
--------

An energy transfer network, a subsystem in the marine energy converter farm, serves to transport the generated power to the onshore terminal. A typical topology of an energy transfer network, shown in Figure XX, is used as an example to demonstrate how a hierarchy is established. 


In Figure 1, the white bubbles with dashed boundary lines represent the marine energy converters (MECs) connected to the main cables represented by black bold lines, through the orange circles marked with numbers from 1 to 9 represent the connectors. 

.. image:: ./figure_reliability_2.png
  :width: 300
  :alt: Alternative text

Figure 1 Sketch Illustrating The Energy Delivery System Layout

Due to no tailor-made reliability database for marine energy converters, the failure rates of the basic components can be referred to some generic database for electrical components in other industrial applications and also should be subject to engineering judgement. The failure rates to be used are given in Table 1. 

Table 1 Labels and Failure Rates of Basic Components

+-----------------+--------------+------------------+
|       Item      | Labels in FT |       Value      |
+=================+==============+==================+
|       AC1       |      X1      |     2.23e-2      |
+-----------------+--------------+------------------+
|       AC2       |      X2      |     2.23e-2      |
+-----------------+--------------+------------------+
|       AC3       |      X3      |     2.23e-2      |
+-----------------+--------------+------------------+
|       AC4       |      X4      |     2.23e-2      |
+-----------------+--------------+------------------+
|       AC5       |      X5      |     2.23e-2      |
+-----------------+--------------+------------------+
|       AC6       |      X6      |     2.23e-2      |
+-----------------+--------------+------------------+
+   Connector 1   |     X11      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 2   |     X12      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 3   |     X13      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 4   |     X14      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 5   |     X15      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 6   |     X16      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 7   |     X17      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 8   |     X18      |     5.47e-3      |
+-----------------+--------------+------------------+
+   Connector 9   |     X19      |     5.47e-3      |
+-----------------+--------------+------------------+
+       CP1       |     X20      |     8.61e-3      |
+-----------------+--------------+------------------+

Note: 1) The unit of failure rates is in 1/year. 


Qualitative System Analysis
---------------------------
The working philosophy of each subsystem and the way these subsystems are aggregated to form the marine energy conversion system can be investigated through qualitative system analysis. The aim of a qualitative system analysis is to obtain the hierarchy of a unit, which could refer to either of system, subsystem, sub-assembly, and to understanding the logic interrelationship between these units. With the knowledge of the hierarchical structures, the traditional FT method will be used to assess the PoFs or reliability of the units at different levels.

Based upon the expert’s review comments, the hierarchy of the energy delivery (ED) system has been clearly defined. There are two independent energy transfer routes respectively connected to CP1 through the connectors 7 & 8 and two cables AC3 & AC6. The electricity is finally transmitted to the onshore terminal through the connector 9. Suppose that the two energy transfer routes are considered as a virtual unit denoted by T1. T1 is considered as a 1st-level sub-assembly. If either of CP1, X19 and T1 fails, the energy transfer system (T0) will be shut down (no electricity generated).

T1 is composed of two identical energy transfer routes respectively denoted by T2 and T3, which are the 2nd-level sub-assemblies. T1 fails, if both T2 and T3 fail. 

T2 comprises X17, X3, a virtual unit T4, which comprises the other connectors directly connected to MECs and the other cables connecting these connectors. If either X17, X3 or T4 fails, this energy transfer route will be shut down. T2 can be considered a series system. T3 comprises X18, X6, a virtual unit T5, which comprises the other connectors directly connected to MECs and the other cables connecting these connectors. If either X18, X6 or T5 fails, this energy transfer route will be shut down. T3 can be considered a series system. Both T4 and T5 constitutes the 3rd-level sub-assembly.

T4 comprises X2, X13 and another virtual unit T6. If either two of X2, X13 and T6 fail, T4 fails. In a similar way, T5 comprises X6, X16 and another virtual unit T7. If either two of X6, X16 and T7 fail, T5 fails. Both T6 and T7 constitutes the 4th-level sub-assembly.

T6 comprises X12 and another virtual unit T8. If either X12 or T8 fails, T6 fails. T7 comprises X15 and another virtual unit T9. If either X15 or T9 fails, T7 fails. Both T8 and T9 constitutes the 5th-level sub-assembly. 

T8 comprises X11 and X1. If either X11 or X1 fails, T8 fails. T9 comprises X14 and X4. If either X14 or X4 fails, T9 fails.

Based upon the qualitative system analysis, the hierarchy of the ED subsystem has been clearly defined, with its compact format given in Table 2. The data in Table 2 are all drawn from the hierarchy presented in Table 2, however, are renamed (some fields in Table 2 are changed to the technical terminologies in FT) and re-structured. 

Table 2 Hierarchy of Energy Delivery System

+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
| System | Name of Node| Design Id |  Node Type  | Node Subtype| Category | Parent |    Child     | Gate |    f1   |    f2   |
+========+=============+===========+=============+=============+==========+========+==============+======+=========+=========+
|   ED   |     T0      |    T0     |    System   |    System   |  Level 6 |  "NA"  | [X19,X20,T1] |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T1      |    T1     | Subassembly | Subassembly |  Level 5 |  [T0]  |    [T2,T3]   | AND  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T2      |    T2     | Subassembly | Subassembly |  Level 4 |  [T1]  |  [X17,X3,T4] |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T3      |    T3     | Subassembly | Subassembly |  Level 4 |  [T1]  |  [X18,X6,T5] |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T4      |    T4     | Subassembly | Subassembly |  Level 3 |  [T2]  | [X13,X2,T6]  | 2/3  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T5      |    T5     | Subassembly | Subassembly |  Level 3 |  [T3]  | [X16,X5,T7]  | 2/3  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T6      |    T6     | Subassembly | Subassembly |  Level 2 |  [T4]  |   [X12,T8]   |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T7      |    T7     | Subassembly | Subassembly |  Level 2 |  [T5]  |   [X15,T9]   |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T8      |    T8     | Subassembly | Subassembly |  Level 1 |  [T6]  |   [X11,X1]   |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     T9      |    T9     | Subassembly | Subassembly |  Level 1 |  [T7]  |   [X14,X4]   |  OR  |    NA   |    NA   |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X11     |    X11    |  Component  |  Component  |  Level 0 |  [T8]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X12     |    X12    |  Component  |  Component  |  Level 0 |  [T6]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X13     |    X13    |  Component  |  Component  |  Level 0 |  [T4]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X14     |    X14    |  Component  |  Component  |  Level 0 |  [T9]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X15     |    X15    |  Component  |  Component  |  Level 0 |  [T7]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X16     |    X16    |  Component  |  Component  |  Level 0 |  [T5]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X17     |    X17    |  Component  |  Component  |  Level 0 |  [T2]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X18     |    X18    |  Component  |  Component  |  Level 0 |  [T3]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X19     |    X19    |  Component  |  Component  |  Level 0 |  [T0]  |     "NA"     |  NA  | 5.47e-3 | 5.47e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X1      |     X1    |  Component  |  Component  |  Level 0 |  [T8]  |     "NA"     |  NA  | 2.23e-2 | 2.23e-2 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X2      |     X2    |  Component  |  Component  |  Level 0 |  [T4]  |     "NA"     |  NA  | 2.23e-2 | 2.23e-2 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X3      |     X3    |  Component  |  Component  |  Level 0 |  [T2]  |     "NA"     |  NA  | 2.23e-2 | 2.23e-2 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X4      |     X4    |  Component  |  Component  |  Level 0 |  [T9]  |     "NA"     |  NA  | 2.23e-2 | 2.23e-2 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X5      |     X5    |  Component  |  Component  |  Level 0 |  [T5]  |     "NA"     |  NA  | 2.23e-2 | 2.23e-2 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X6      |     X6    |  Component  |  Component  |  Level 0 |  [T2]  |     "NA"     |  NA  | 2.23e-2 | 2.23e-2 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+
|   ED   |     X20     |    X20    |  Component  |  Component  |  Level 0 |  [T0]  |     "NA"     |  NA  | 8.61e-3 | 8.61e-3 |
+--------+-------------+-----------+-------------+-------------+----------+--------+--------------+------+---------+---------+

The first column gives the subsystem to be analysed. All failure events are considered nodes in the hierarchy. The second column, ‘Name of Node’, gives the names of these failure events. The third column, ‘Design Id’, gives the identification labels of the basic components and other units. Design Ids are named according to the rules/ conventions of the ED module. The columns, ‘Node Type’ and 'Node Subtype', define the types of the units. The column, ‘Category’, defines which levels the nodes in the ‘Name of Node’ column belong to in the fault tree. The columns ‘Parent’ and ‘Child’ define the dependencies of units at various levels. Each entry in ‘Parent’ defines the label of the higher-level unit which the current unit in the column ‘Name of Node’ belongs to. Each entry in ‘Child’ defines the labels of lower-level units which belong to the current unit. Based upon the aforementioned descriptions, the units in the column ‘Child’ are connected through a specific logic gate to the higher-level unit. The logic gates are given in the column ‘Gate Type’. The logic gate in each entry of this column is used to connect the unit in the column ‘Name of Node’ and the units in the column ‘Child’. The last two columns give the failure rates of basic components for both Type 1& 2 failure modes. "f1" refers to the failure rate for the case in which the damaged components can be repaired. "f2" refers to the failure rate for the case in which the damaged components have to be replaced;

Based upon the qualitative system analysis, the fault tree can be constructed accordingly. The top event is denoted ‘Failure of ED system (T0)’. The intermediate failure events underneath the top event refer to ‘Failure of X19 (denoted X19 in the fault tree)’, ‘Failure of X20 (denoted X20 in the fault tree)’ and ‘Failure of T1’. An 'OR' gate is inserted according to the working philosophy. For simplicity, the label name is hereafter used to represent the failure event of this unit in the fault tree. 
If ‘Failure of T1’ is considered the top failure event, the intermediate failure events refer to the two ‘Failure of T2’ or ‘Failure of T3’ events. An 'AND' gate is inserted according to the working philosophy.

If ‘Failure of T2’ is considered the top failure event, the intermediate failure events refer to ‘Failure of X17 (denoted X17 in the fault tree)’, ‘Failure of X3 (denoted X3 in the fault tree)’ and ‘Failure of T4’. An 'OR' gate is inserted according to the working philosophy. If ‘Failure of T3’ is considered the top failure event, the intermediate failure events refer to ‘Failure of X18 (denoted X18 in the fault tree)’, ‘Failure of X6 (denoted X6 in the fault tree)’ and ‘Failure of T5’. An 'OR' gate is inserted according to the working philosophy.

If ‘Failure of T4’ is considered the top failure event, the intermediate failure events refer to ‘Failure of X13 (denoted X13 in the fault tree)’, ‘Failure of X2 (denoted X2 in the fault tree)’ and ‘Failure of T6)’ A '2/3' gate is inserted according to the working philosophy. If ‘Failure of T5’ is considered the top failure event, the intermediate failure events refer to ‘Failure of X16 (denoted X16 in the fault tree)’, ‘Failure of X5 (denoted X5 in the fault tree)’ and ‘Failure of T7’. A '2/3' gate is inserted according to the working philosophy.

If ‘Failure of T6’ is considered the top failure event, the intermediate failure events refer to ‘Failure of X12 (denoted X12 in the fault tree)’ and ‘Failure of T8’. An 'OR' gate is inserted according to the working philosophy. If ‘Failure of T7’ is considered the top failure event, the intermediate failure events refer to ‘Failure of X15 (denoted X15 in the fault tree)’ and ‘Failure of T9’. An 'OR' gate is inserted according to the working philosophy.

If ‘Failure of T8’ is considered the top failure event, the bottom failure events refer to ‘Failure of X11 (denoted X11 in the fault tree)’ and ‘Failure of X1 (denoted X1 in the fault tree)’. An 'OR' gate is inserted according to the working philosophy. If ‘Failure of T9’ is considered the top failure event, the bottom failure events refer to ‘Failure of X14 (denoted X14 in the fault tree)’ and ‘Failure of X4 (denoted X4 in the fault tree)’. An 'OR' gate is inserted according to the working philosophy. The fault tree of the ED subsystem is shown in Figure XX2.
