.. _availability_theory: 

Theory - availability
=====================

Overview
--------

Availability assessment is intended for calculating the time-based availability of devices and the whole array. 


Time-based Availability
-----------------------

For individual devices, the time-based availability is defined as the uptime divided by the total lifetime time, as given by the following equation. 

.. math:: \text{Availability} = \frac{t_{\text{uptime}}}{t_{\text{uptime}} + t_{\text{downtime}}}
   :name: Equation 3
