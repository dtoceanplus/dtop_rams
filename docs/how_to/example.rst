.. _rams-how-to-example:

How to do something cool with the module XX
===========================================

Give a description of what the how-to guide will achieve, and why it is important. 

Then list some specific steps the user can perform:

#. Step one is...

#. Step two is...

#. Step three...

#. Finally,...
