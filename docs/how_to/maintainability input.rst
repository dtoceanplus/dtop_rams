.. _rams-how-to-maintainability-input:

Prerequisites for maintainability assessment
============================================

The maintainability assessment mainly requires the maintenance-related data, which are the same for all complexity levels 
The input data must include the following keys:

#. "mttr" - the mean time to repair (MTTR), list

   if "Log-Normal" is chosen, the mttr and the standard deivation of repair time must be logged values. 

#. "technologies" - the repaired/replaced component names, list

The following key is optional

#. "downtime" - the downtime due to repairing/replacement, list
