.. _rams-how-to-survivability-input:

Prerequisites for survivability assessment
==========================================

Both ultimate stresses/forces, fatigue stress ranges and their probability distributions are the key inputs for survivability assessment. It is recommended that the RAMS user must well understand the following aspects:

#. the theoretical basis of probability and statistics

#. the theoretical basis of structural analysis

The reasons why these two points are recommended are: 

#. a strong technical background in structural engineering is essential for the user to understand the loads and the structural responses

#. a good command of probablity and statistics knowledge serves as a solid basis for the user to assess/ evaluate the probabilities that the critical structural members can survive the ultimate and fatigue loads. 

It should be stressed that:

#. survivability assessment is only performed for the structural and mechanical members in the station keeping and energy transformation subsystems. 

#. for the station keeping subsystem, survivabilty assessment is only performed for the floating cases;

#. for the energy transformation, the FLS survivabilty assessment is only performed for the cases where fatigue analysis is performed in the energy transformation module.

Preparation of the inputs - Station keeping subsystem
-----------------------------------------------------

Inputs related to ULS 

#. Step 1 - "mbl_uls": the maximum breaking load of mooring lines; this key must be in ["devie"][i]["uls_results"]; a 1D list with the number of entries equal to the number of mooring lines. 

#. Step 2 - "mooring_tension": the tension of mooring lines; this key must be in ["devie"][i]["uls_results"];a 4D list with each dimension explained as follows:

#. the first dimension only indicates "mooring_tension" is a list;

#. the second dimension corresponds to the weather directions, for example, 3 means there are three weather directions considered;

#. the third dimension represents the number of sea states considered for each weather direction; 

#. the fourth dimension represents the mooring tension on each mooring line; there are two values with each respectively corresponding to the fairlead and the anchor point.

Inputs related to FLS 

#. Step 3 - "ad": the S-N curve parameter a; this key must be in ["devie"][i]["fls_results"]; a 1D list with the number of entries equal to the number of mooring lines.

#. Step 4 - "m": the S-N curve parameter m; this key must be in ["devie"][i]["fls_results"]; a 1D list with the number of entries equal to the number of mooring lines.

#. Step 5 - "env_proba": a flag indicating if the FLS survivabiity assessment will be performe; this key must be in ["devie"][i]["fls_results"]; 1D list with one entry either being 0 (not performed) or 1 (performed).

#. Step 6 - "cdf": the cumulative probability of the stress ranges; this key must be in ["devie"][i]["fls_results"]; a 2D list with each dimension explained as follows:

#. the first dimension represents the sea states;

#. the second dimention represents the cumulative probability of occurrence of a specific sea state; each entry in the innter list represents the cdf for a specific mooring line; for example, "cdf" = [[0.3, 0.24],[0.35, 0.29], ...]; 0.3 is the cdf of the occurrence of the first sea state of the first moorng line; 0.35 is the cdf of the occurrence of the first and second sea states of the first moorng line;


#. Step 7 - "cdf_stress_range": the stress ranges corresponding to the probaiblities in "cdf"; this key must be in ["devie"][i]["fls_results"]; a 2D list with the relevant stress ranges corresponding to the cdf and the sea states;

Preparation of the inputs - Energy transformation subsystem
-----------------------------------------------------------

The input related to the energy transformation subsystem is a list of dictionaries. Each dictionary contains the results of one device, which is required to run the survivability assessment. [i] is used to represent the i-th dictionary. 

#. Step 1 - "Dev_E_Grid": the captured energy by each device; this key must be in each entry of the list, namely [i]; a dictionary where a key "value" is defined and contains the actual data. 

#. Step 2 - "ultimate_stress": the assumed material strength of the critical structural/ mechanical components; a dictionary where a key "value" is defined and contains the actual data; this key must be in each entry of the list, namely [i]; it is assumed that the device with the maximum Dev_E_Grid is the most critical one; RAMS only assesses the survivability for this critical device.

#. Step 3 - "stress" (for ULS survivability assessment): the maximum stresses on the critical structural/ mechanical components; this key must be in ['maximum_stress_probability']["value"]; a 1D list where each entry represents a maximum stress for a environmental condition, and the length must be the same as that of "probability".

#. Step 4 - "probability" (for ULS survivability assessment): the maximum stresses on the critical structural/ mechanical components; this key must be in ['maximum_stress_probability']["value"]. a 1D list where each entry represents the occurrence probability of the maximum stress with the same index in the list "stress", and the length must be the same as that of "stress".

#. Step 5 - "S_N": the S-N curve parameters; this key must be in each entry of the list, namely [i]; a dictionary where a key "value" is defined and contains the actual data; "value" is a 2D list, with the two 1D lists containing the m and log10(a) for a bilinear S-N curve (the first entry is m and the second entry is log10(a)). 

#. Step 6 - "stress" (for FLS survivability assessment): the fatigue stress ranges on the critical structural/ mechanical components; this key must be in ['fatigue_stress_probability']["value"]; a 1D list where each entry represents a fatigue stress range for a environmental condition, and the length must be the same as that of "probability".

#. Step 7 - "probability" (for FLS survivability assessment): the fatigue stress ranges on the critical structural/ mechanical components; this key must be in ['fatigue_stress_probability']["value"]. a 1D list where each entry represents the occurrence probability of the fatigue stress range with the same index in the list "stress", and the length must be the same as that of "stress".

#. Step 8 - "number_cycles": the number of stress range cycles; this key must be in each entry of the list, namely [i]; a dictionary where a key "value" is defined and contains the actual data; 