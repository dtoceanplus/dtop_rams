.. _rams-how-to-availability-input:

Prerequisites for availability assessment
=========================================

The availability assessment mainly requires the downtime, which is the same for all complexity levels 

The input data must include the following keys:

#. "project_life" - float

#. "n_device" - integer

#. "downtime" - an list of dictionaries

#. each entry in "downtime" inlcudes the following keys:

   #. "device_id" - string

   #. "downtime_table" - dictionary, inlcuidng the following keys:

      #. "year" - list

      #. "jan" - list (the downtime in January every year)

      #. "feb" - list (the downtime in February every year)   

      #. "mar" - list (the downtime in March every year)

      #. "apr" - list (the downtime in April every year)    

      #. "maj" - list (the downtime in May every year)

      #. "jun" - list (the downtime in June every year)  

      #. "jul" - list (the downtime in July every year)

      #. "aug" - list (the downtime in August every year)  

      #. "sep" - list (the downtime in Septermber every year)

      #. "oct" - list (the downtime in October every year)     

      #. "nov" - list (the downtime in November every year)
      
      #. "dec" - list (the downtime in December every year)  

It should be stressed that the length of the downtime for each calendar month must be equal to that of "year". 
