.. _rams-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Reliability, Availability, Maintainability and Survivability module. 
These guides are intended for users who have previously completed all the :ref:`Reliability, Availability, Maintainability and Survivability tutorials <rams-tutorials>` and have a good knowledge of the features and workings of the Reliability, Availability, Maintainability and Survivability module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

#. :ref:`rams-how-to-reliability-input`
#. :ref:`rams-how-to-availability-input`
#. :ref:`rams-how-to-maintainability-input`
#. :ref:`rams-how-to-survivability-input`
#. :ref:`rams-how-to-example`

.. toctree::
   :maxdepth: 1
   :hidden:

   reliability input
   availability input
   maintainability input
   survivability input
   example
