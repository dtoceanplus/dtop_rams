.. _rams-how-to-reliability-input:

Prerequisites for reliability assessment
========================================

Hierarchy is the key input for reliability assessment. It is recommended that the RAMS user must well understand the following aspects:

#. the theoretical basis of probability and statistics, especially Fault Tree

#. the working philosophy of the marine energy array, the devices, and the components in devices

The reasons why these two points are recommended are: 

#. a strong technical background in design of marine energy system is essential for the user to understand the logic denpendencies between units at different levels

#. a good command of probablity and statistics knowledge serves as a solid basis for the user to establish the theoretical model based upon the working philosophy

The step-by-step guidance for defining the hierarchy will be elaborated as follow.


How to prepare the hierarchy for reliability assessment
=======================================================

Break-down of a system
----------------------

The objective is to iteratively break down a system to the minimum units according to the functionalities. 
For example, an array is composed of the energy delivery (ED) subsystem, the station keeping (SK) subsystem, 
the energy transformation (ET) subsystem, etc.. Each of these subsystems can be further divided into sub-assemblies.

Demonstration of logic dependencies
-----------------------------------

In this sub-section, one simple example is used to demonstrate the logic dependencies for ED, ET and SK subsystems, respectively.

ED subsystem:

Figure 1 shows a simple energy delivery network topology. Since devices are out of work scope of the ED module, 
focus is only put on cables and connections. This subsystem contains two cables and two connections. According to the working philosophy,
there are two independent energy transfering routes. The ED subsystem can be considered a parallel system from the perspective of reliability assessment.
As long as both of the energy transfering routes fail, the ED subsystem fails. For each energy transfering route, there are two basic components, namely
a cable and a connection. Either of these components fails, the energy transfering route fails. Therefore, each energy transfering route can be considered
a series system from the perspective of reliability assessment.

.. image:: ./ED_example.png
  :width: 100
  :alt: Alternative text

Figure 1 An Example Energy Delivery Network Topology

ET subsystem:

Figure 2 shows one device with three PTOs. According to the working philosophy, three PTOs work independently. The ET subsystem can be considered a parallel system from the perspective of reliability assessment.
As long as both of the energy transfering routes fail, the ED subsystem fails. For each PTO, there are three basic components (converting the hydro power to the mechnical energy; converting the mechanical energy
to the electricity; transmitting the electricity to the grid). Either of these components fails, the PTO stops working. Therefore, each PTO can be considere a series system from the perspective of reliability assessment.

.. image:: ./ET_example.png
  :width: 500
  :alt: Alternative text

Figure 2 An Example Energy Transformation Subsystem

SK subsystem:

Figure 3 shows a simple station keeping subsystem. It contains two mooring assemblies, each of which is composed of a mooirng line and an anchor. According to the working philosophy, two mooring assemblies work independently. 
The SK subsystem can be considered a parallel system from the perspective of reliability assessment. As long as both of the mooring assemblies fail, the SK subsystem fails.For each mooring assembly, either mooring or anchor fails, 
the mooring assembly fails. Therefore, each PTO can be considere a series system from the perspective of reliability assessment.

.. image:: ./SK_example.png
  :width: 500
  :alt: Alternative text

Figure 3 An Example Station Keeping Subsystem

Steps of hierarchy definition
-----------------------------

#. Step 1 - Label the subsystem, the sub-assemblies and basic components; fill the data in the columns "System", "Name_of_node", "Design_id", "Node_type" and "Node_subtype".

#. Step 2 - a logic gate is chosen and inserted to connect the ED subsystem and the two energy transfering routes. A logic gate is chosen and inserted to connect one energy transfering route and two basic components.

#. Step 3 - according to the logic gate，define the units in the columns "Parent" and "Child" and fill in which level the corresponding units belong to in the column "Level".

#. Step 4 - fill in the failure rates of basic components in the column "failure_rate_repair" and "failure_rate_replacement". 
