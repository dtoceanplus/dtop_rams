# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop-rams",
    version="0.0.1",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    install_requires=[
        "bson",
        "flask",
        "flask-babel",
        "flask-cors",
        "flask-marshmallow",
        "flask-sqlalchemy",
        "marshmallow-sqlalchemy",
        "marshmallow",
        "matplotlib",
        "pandas",
        "requests",
        "scipy",
        "sqlalchemy",
        "xlrd",
    ],
    include_package_data=True,
    zip_safe=False,
)
