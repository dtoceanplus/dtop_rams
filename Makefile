# 'build-TEST' builds a Docker image for test
# 'TEST' runs build and then the test itself
# 'TEST-nodeps' runs test, assuming the image exists already
# 'x-TEST' is what actually runs inside the image
#
# 'TEST-src' and 'TEST-src-nodeps' run test on source tree, not on dist/*.tar.gz
# PATH=$(brew --prefix)/opt/findutils/libexec/gnubin:$PATH

SHELL = bash
.PHONY: *
export MODULE_NICKNAME = rams
export DTOP_DOMAIN = dto.test

# build-osx:
# 	docker-compose -f ci/client.docker-compose -f ci/e2e-cypress.docker-compose -f ci/backend.docker-compose build backend worker redis client nginx

# run-osx:
# 	docker-compose -f ci/client.docker-compose -f ci/e2e-cypress.docker-compose -f ci/backend.docker-compose up -d backend worker redis client nginx



all: dist pytest dredd e2e-cypress impose


dist:
	mkdir -p dist
	rm -rf dist/*
	# # This command can only be used in cmd shell!!!
	# docker run --rm -v "%cd%":/code --workdir /code python:3 python setup.py sdist clean --all
	# This command can be used in windows powershell. (Preferred!)
##	docker run --rm -v ${PWD}:/code --workdir /code python:3 python setup.py sdist clean --all
	python setup.py sdist

build:
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build

build-%:
	echo "build container for" $*
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build $*

run:
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` up client backend nginx worker redis lmo.dto.test sk.dto.test et.dto.test ed.dto.test mm

pytest: build-pytest pytest-nodeps
pytest-nodeps:
	docker-compose --file ci/pytest.docker-compose run -T pytest

x-pytest:
	pytest --verbose --capture=no --cov=dtop_rams -vv test


dev: build-backend
	docker-compose --file ci/backend.docker-compose run --rm backend-src bash

dredd: build-backend build-dredd dredd-nodeps
dredd-nodeps:
	touch dredd-hooks-output.txt
	docker-compose --file ci/backend.docker-compose --file ci/dredd.docker-compose \
	up --remove-orphans --force-recreate --always-recreate-deps --abort-on-container-exit --exit-code-from=dredd \
	backend dredd

x-dredd:
	dredd


e2e-cypress: build-backend build-client build-e2e-cypress e2e-cypress-nodeps
e2e-cypress-nodeps:
	docker-compose --file ci/backend.docker-compose --file ci/client.docker-compose --file ci/e2e-cypress.docker-compose \
	up --remove-orphans --force-recreate --always-recreate-deps --abort-on-container-exit --exit-code-from=e2e-cypress \
	nginx e2e-cypress


impose:
	docker-compose --file ci/contracts.docker-compose run --rm contracts


verify:
	docker-compose --project-name rams -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name rams -f verifiable.docker-compose stop rams
	docker cp `docker-compose --project-name rams -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify