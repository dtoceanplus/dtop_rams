from dtop_rams import business
import numpy
import pytest
import pandas as pd
import os
import math

"""
   to test the core functions for Complexity 2
"""  


def test_reliability2():
    app_rams = business.ArrayRams.calc_reliability("2")
    output1_1 = app_rams.sim_failure_event(['Id1', 'Id2', 'Id3'], [5.86e-15, 7.98e-4, 1.009e-3], 10)	
    output1_2 = app_rams.sim_failure_event(['Id1', 'Id2', 'Id3'], [5.86e-4, 7.98e-4, 1.009e-3], 10, pd=" ")	
    output1_3 = app_rams.sim_failure_event(['Id1', 'Id2', 'Id3'], [2, 10, 11], 10)	
      
    # original example  
    hierarchy_dict1 = {
     "system": {"0": 13, "1": "ED", "2": "ED", "3": "ED", "4": "ED", "5": "ED", "6": "ED",
     "7": "ED", "8": "ED", "9": "ED", "10": "ED", "11": "ED", "12": "ED", "13": "ED"},
     "name_of_node":{"0":13, "1":"ED_OEC1", "2":"ED_OEC2", "3":"ED_OEC3", 
     "4":"Route1_1", "5":"Route2_1", "6":"Route3_1", "7":"CON1", "8":"CON2",
     "9":"CON3", "10":"EC1", "11":"EC2", "12":"EC3", "13": "ED Subsystem"},
     "design_id": {"0":13, "1":"ED_OEC1", "2":"ED_OEC2", "3":"ED_OEC3", 
     "4":"Route1_1", "5":"Route2_1", "6":"Route3_1", "7":"CON1", "8":"CON2", "9":"CON3",
     "10":"EC1", "11":"EC2", "12":"EC3", "13": "ED Subsystem"},
     "node_type":{"0":"Component","1":"Subsystem", "2":"Subsystem","3":"Subsystem",
     "4":"Energy route","5":"Energy route","6":"Energy route","7":"Component",
     "8":"Component","9":"Component","10":"Component","11":"Component", "12":"Component",
     "13":"System"},
     "node_subtype":{"0":"Component","1":"Subsystem", "2":"Subsystem","3":"Subsystem",
     "4":"Energy route","5":"Energy route","6":"Energy route","7":"Component",
     "8":"Component","9":"Component","10":"Component","11":"Component", "12":"Component",
     "13":"System"},
     "category":{"0":"Level 0", "1":"Level 2","2":"Level 2","3":"Level 2",
     "4":"Level 1","5":"Level 1","6":"Level 1", "7":"Level 0","8":"Level 0",
     "9":"Level 0","10":"Level 0","11":"Level 0","12":"Level 0","13":"Level 3"},
     "parent":{"0":["ED_OEC1"], "1":"ED Subsystem","2":"ED Subsystem","3":"ED Subsystem",
     "4":"ED_OEC1","5":"ED_OEC2","6":"ED_OEC3","7":["Route1_1"],"8": ["Route2_1"],
     "9":["Route3_1"],"10":["Route1_1"],"11":["Route2_1"],"12":["Route3_1"], "13":"NA"},
     "child":{"0":"NA", "1":["Route1_1", "13"],"2":["Route2_1", "13"],
     "3":"Route3_1","4":["EC1", "CON1"],"5":["EC2", "CON2", "13"], "6":["EC3", "CON3"],
     "7":["NA"],"8":["NA"],"9":["NA"],"10":["NA"],"11":["NA"],"12":["NA"],"13":["ED_OEC1", "ED_OEC2", "ED_OEC3"]},
     "gate_type":{"0":"NA", "1":"OR","2":"AND","3":"OR","4":"AND","5":"OR","6":"1/2",
     "7":"NA","8":"NA","9":"NA", "10":"NA","11":"NA","12":"NA","13":"2/3"},
     "fFailure_rate_repair":{"0":"NA", "1":"NA","2":"NA","3":"NA","4":"NA",
     "5":"NA","6":"NA","7":"NA","8":"NA","9":"NA","10":"NA","11":"NA" ,"12":"NA",
     "13": "NA"},
     "failure_rate_replacement":{"0":5.71e-3,"1":"NA","2":"NA","3":"NA",
     "4":"NA","5":"NA","6":"NA", "7":5.71e-2, "8":5.71e-2,"9":5.71e-2,"10":5.71e-2,"11":5.71e-2,"12":5.71e-2, "13":"NA"}
    }

    # Case - Direct fixed
    hierarchy_dict2 = {
        "system": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], "name_of_node": ["ED Subsystem", "ED1", "ED2", "ED3", "ED4", "ED5", "Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1", "8", "6", "4", "2", "0", "9", "7", "5", "3", "1"], "design_id": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "8", "6", "4", "2", "0", "9", "7", "5", "3", "1"], "node_type": ["System", "System", "System", "System", "System", "System", "Energy route", "Energy route", "Energy route", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", " export", " export", " export", " export", " export", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], "category": ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], "parent": ["NA", "NA", "NA", "NA", "NA", "NA", ["ED1"], ["ED2"], ["ED3"], ["ED4"], ["ED5"], ["Route5_1"], ["Route4_1"], ["Route3_1"], ["Route2_1"], ["Route1_1"], ["Route5_1"], ["Route4_1"], ["Route3_1"], ["Route2_1"], ["Route1_1"]], "child": [["ED1", "ED2", "ED3", "ED4", "ED5"], ["Route1_1"], ["Route2_1"], ["Route3_1"], ["Route4_1"], ["Route5_1"], ["1", "0"], ["3", "2"], ["5", "4"], ["7", "6"], ["9", "8"], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], "gate_type": ["OR", "OR", "OR", "OR", "OR", "OR", "AND", "AND", "AND", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], "failure_rate_repair": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 26.15078683986118, 28.581244627348198, 25.96971929984041, 21.402187948827876, 20.808295282363776, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 26.15078683986118, 28.581244627348198, 25.96971929984041, 21.402187948827876, 20.808295282363776, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
    }

    # Case - Direct floating
    hierarchy_dict3 = {
        "system": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], "name_of_node": ["ED Subsystem", "ED1", "ED2", "ED3", "ED4", "ED5", "Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1", "18", "14", "10", "6", "2", "16", "12", "8", "4", "0", "19", "17", "15", "13", "11", "9", "7", "5", "3", "1"], "design_id": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "18", "14", "10", "6", "2", "16", "12", "8", "4", "0", "19", "17", "15", "13", "11", "9", "7", "5", "3", "1"], "node_type": ["System", "System", "System", "System", "System", "System", "Energy route", "Energy route", "Energy route", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", " umbilical", " umbilical", " umbilical", " umbilical", " umbilical", " export", " export", " export", " export", " export", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], "category": ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], "parent": ["NA", "NA", "NA", "NA", "NA", "NA", ["ED1"], ["ED2"], ["ED3"], ["ED4"], ["ED5"], ["Route5_1"], ["Route4_1"], ["Route3_1"], ["Route2_1"], ["Route1_1"], ["Route5_1"], ["Route4_1"], ["Route3_1"], ["Route2_1"], ["Route1_1"], ["Route5_1"], ["Route5_1"], ["Route4_1"], ["Route4_1"], ["Route3_1"], ["Route3_1"], ["Route2_1"], ["Route2_1"], ["Route1_1"], ["Route1_1"]], "child": [["ED1", "ED2", "ED3", "ED4", "ED5"], ["Route1_1"], ["Route2_1"], ["Route3_1"], ["Route4_1"], ["Route5_1"], ["3", "2", "1", "0"], ["7", "6", "5", "4"], ["11", "10", "9", "8"], ["15", "14", "13", "12"], ["19", "18", "17", "16"], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], "gate_type": ["OR", "OR", "OR", "OR", "OR", "OR", "AND", "AND", "AND", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], "failure_rate_repair": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.42564719753922964, 0.4200829426137176, 0.4200829426137176, 0.4576141227726697, 0.4200829426137176, 24.841464763197976, 27.37209203151919, 24.760566704011403, 20.034187948827874, 19.540464763197974, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.42564719753922964, 0.4200829426137176, 0.4200829426137176, 0.4576141227726697, 0.4200829426137176, 24.841464763197976, 27.37209203151919, 24.760566704011403, 20.034187948827874, 19.540464763197974, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
    }

    # Case - Radial fixed
    hierarchy_dict4 = {
        "system": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], 
        "name_of_node": ["ED Subsystem", "ED1", "ED2", "ED3", "ED4", "ED5", "Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1", "2", "16", "13", "10", "7", "4", "0", "1", "17", "15", "14", "12", "11", "9", "8", "6", "5", "3"], 
        "design_id": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "2", "16", "13", "10", "7", "4", "0", "1", "17", "15", "14", "12", "11", "9", "8", "6", "5", "3"], 
        "node_type": ["System", "System", "System", "System", "System", "System", "Energy route", "Energy route", "Energy route", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], 
        "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", " substation", " array", " array", " array", " array", " array", " export", " dry-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], 
        "category": ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], 
        "parent": ["NA", "NA", "NA", "NA", "NA", "NA", ["ED1"], ["ED2"], ["ED3"], ["ED4"], ["ED5"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route5_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route5_1"], ["Route5_1"], ["Route3_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route4_1"]], 
        "child": [["ED1", "ED2", "ED3", "ED4", "ED5"], ["Route1_1"], ["Route2_1"], ["Route3_1"], ["Route4_1"], ["Route5_1"], ["5", "4", "3", "2", "1", "0"], ["11", "10", "9", "2", "1", "0"], ["14", "13", "12", "11", "10", "9", "2", "1", "0"], ["8", "7", "6", "5", "4", "3", "2", "1", "0"], ["17", "16", "15", "2", "1", "0"], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
        "gate_type": ["OR", "OR", "OR", "OR", "OR", "OR", "AND", "AND", "AND", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
        "failure_rate_repair": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 2.457042, 9.7176610383316, 7.577644153326395, 5.720949344984388, 7.772949344984387, 3.273305191657993, 18.215604495807106, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], 
        "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 2.457042, 9.7176610383316, 7.577644153326395, 5.720949344984388, 7.772949344984387, 3.273305191657993, 18.215604495807106, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
    }

    # Case - Radial floating no CP (note: some gates are changed to increase the coverage rate!!)
    hierarchy_dict5 = {"system": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], "name_of_node": ["ED Subsystem", "ED1", "ED2", "ED3", "ED4", "ED5", "Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1", "22", "17", "12", "7", "2", "20", "15", "10", "5", "0", "23", "21", "19", "18", "16", "14", "13", "11", "9", "8", "6", "4", "3", "1"], "design_id": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "22", "17", "12", "7", "2", "20", "15", "10", "5", "0", "23", "21", "19", "18", "16", "14", "13", "11", "9", "8", "6", "4", "3", "1"], "node_type": ["System", "System", "System", "System", "System", "System", "Energy route", "Energy route", "Energy route", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", " umbilical", " umbilical", " umbilical", " umbilical", " umbilical", " array", " array", " array", " array", " export", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], "category": ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], "parent": ["NA", "NA", "NA", "NA", "NA", "NA", ["ED1"], ["ED2"], ["ED3"], ["ED4"], ["ED5"], ["Route4_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route4_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route4_1"], ["Route4_1"], ["Route4_1"], ["Route3_1"], ["Route3_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route5_1"], ["Route5_1"], ["Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"]], "child": [["ED1", "ED2", "ED3", "ED4", "ED5"], ["Route1_1"], ["Route2_1"], ["Route3_1"], ["Route4_1"], ["Route5_1"], ["3", "2", "1", "0"], ["13", "12", "11", "10", "9", "3", "2", "1", "0"], ["18", "17", "16", "15", "14", "13", "12", "11", "10", "9", "3", "2", "1", "0"], ["23", "22", "21", "20", "19", "3", "2", "1", "0"], ["8", "7", "6", "5", "4", "3", "2", "1", "0"], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], "gate_type": ["3/5", "OR", "OR", "OR", "OR", "OR", "AND", "6/9", "AND", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], "failure_rate_repair": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.2473381395458452, 6.392796749155391, 6.368491557497398, 6.297661038331599, 6.368491557497398, 21.15202255509104, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.2473381395458452, 6.392796749155391, 6.368491557497398, 6.297661038331599, 6.368491557497398, 21.15202255509104, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]}

    # Case - station keeping
    hierarchy_dict6 = {
    "system": ["SK","SK","SK","SK","SK","SK","SK","SK","SK","SK","SK"],
    "name_of_node": ["SK0_x","SK0_x_ml_0_seg_0","SK0_x_ml_0_anchor_n_2_0","SK0_x_ml_0","SK0_x_ml_1_seg_0","SK0_x_ml_1_anchor_n_2_0","SK0_x_ml_1","SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0","SK0_x_ml_2","SK0"],
    "design_id": ["NA","SK0_x_ml_0_seg_0","SK0_x_ml_0_anchor_n_2_0","NA","SK0_x_ml_1_seg_0","SK0_x_ml_1_anchor_n_2_0","NA","SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0","NA","NA"],
    "node_type": ["System","Component","Component","System","Component","Component","System","Component","Component","System","System"],
    "node_subtype": ["stationkeeping","line_segment","anchor","mooring_line","line_segment","anchor","mooring_line","line_segment","anchor","mooring_line","stationkeeping"],
    "category": ["Level 2","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 3"],
    "parent": ["NA","SK0_x_ml_0","SK0_x_ml_0","SK0_x","SK0_x_ml_1","SK0_x_ml_1","SK0_x","SK0_x_ml_2","SK0_x_ml_2","SK0_x","NA"],
    "child": [["SK0_x_ml_0", "SK0_x_ml_1", "SK0_x_ml_2"],["NA"],["NA"],[    "SK0_x_ml_0_seg_0", "SK0_x_ml_0_anchor_n_2_0"],["NA"],["NA"],["SK0_x_ml_1_seg_0",  "SK0_x_ml_1_anchor_n_2_0"],["NA"],["NA"],["SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0"],["SK0_x"]],
    "gate_type": ["2/3","NA","NA","AND","NA","NA","AND","NA","NA","AND","AND"],
    "failure_rate_repair": ["NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA"],
    "failure_rate_replacement": ["NA","0.00722","0.000278","NA","0.00722","0.000278","NA","0.00722","0.000278","NA","NA"]
}

    hierarchy_dict7 = {
        "system": ["ED", "ED", "ED", "ED", "ED"],
        "name_of_node": ["ED Subsystem", "ED1", "Route1_1", "0", "1"], 
        "design_id": ["NA", "NA", "NA", "0", "1"], 
        "node_type": ["System", "System", "Energy route", "Component", "Component"],
        "node_subtype": ["NA", "NA", "NA", " export", " wet-mate"],
        "category": ["Level 3", "Level 2", "Level 1", "Level 0", "Level 0"], 
        "parent": ["NA", "NA", ["ED1"], ["Route1_1"], ["Route1_1"]],
        "child": [["ED1"], ["Route1_1"], ["1", "0"], "NA", "NA"], 
        "gate_type": ["OR", "OR", "AND", "NA", "NA"], 
        "failure_rate_repair": ["NA", "NA", "NA", 4.486642423587225, 5.42244], 
        "failure_rate_replacement": ["NA", "NA", "NA", 4.486642423587225, 5.42244]
    }

    temp_hierarchy1 = pd.DataFrame(hierarchy_dict1).to_numpy()
    component_id1, failure_rate1, level1, node1, shared_component1, table_hierarchy1 = app_rams.       pre_process(temp_hierarchy1)
    output3_1 = app_rams.calc_pof_system(level1, node1, table_hierarchy1, 10)    
    temp_hierarchy2 = pd.DataFrame(hierarchy_dict2).to_numpy()
    component_id2, failure_rate2,level2, node2, shared_component2, table_hierarchy2 = app_rams.    pre_process(temp_hierarchy2)
    output3_2 = app_rams.calc_pof_system(level2, node2, table_hierarchy2, 10)    
    temp_hierarchy3 = pd.DataFrame(hierarchy_dict3).to_numpy()
    component_id3, failure_rate3, level3, node3, shared_component3, table_hierarchy3 = app_rams.       pre_process(temp_hierarchy3)
    output3_3 = app_rams.calc_pof_system(level3, node3, table_hierarchy3, 10)    
    temp_hierarchy4 = pd.DataFrame(hierarchy_dict4).to_numpy()
    component_id4, failure_rate4,level4, node4, shared_component4, table_hierarchy4 = app_rams.    pre_process(temp_hierarchy4)
    output3_4 = app_rams.calc_pof_system(level4, node4, table_hierarchy4, 10)    
    temp_hierarchy5 = pd.DataFrame(hierarchy_dict5).to_numpy()
    component_id5, failure_rate5,level5, node5, shared_component5, table_hierarchy5 = app_rams.    pre_process(temp_hierarchy5)
    output3_5 = app_rams.calc_pof_system(level5, node5, table_hierarchy5, 10)    
    temp_hierarchy6 = pd.DataFrame(hierarchy_dict6).to_numpy()
    component_id6, failure_rate6,level6, node6, shared_component6, table_hierarchy6 = app_rams.    pre_process(temp_hierarchy6)
    output3_6 = app_rams.calc_pof_system(level6, node6, table_hierarchy6, 10)    
    temp_hierarchy7 = pd.DataFrame(hierarchy_dict7).to_numpy()
    component_id7, failure_rate7,level7, node7, shared_component7, table_hierarchy7 = app_rams.    pre_process(temp_hierarchy7)
    output3_7 = app_rams.calc_pof_system(level7, node7, table_hierarchy7, 10)

    output4 = app_rams.unique([3,2,1,4,3,2,5,32,3,3,6])
	
    output5 = app_rams.k_out_of_n ([0, 1, 2, 3], [1.43e-4,2.09e-4,1.098e-4,5.98e-4], 3, 1)

    output6 = app_rams.combination([0, 1, 2, 3], [0.0]*1, 0, 3, 0, 1)
    f_exist = os.path.isfile('./summary_conbimation.txt')

    # output7_1 = app_rams.system_failure_event(shared_component1, 20, 50, table_hierarchy1)
    output7_2 = app_rams.system_failure_event(shared_component2, 20, 50, table_hierarchy2)
    output7_3 = app_rams.system_failure_event(shared_component3, 20, 50, table_hierarchy3)
    output7_4 = app_rams.system_failure_event(shared_component4, 20, 50, table_hierarchy4)
    output7_5 = app_rams.system_failure_event(shared_component5, 20, 50, table_hierarchy5)
    output7_6 = app_rams.system_failure_event(shared_component6, 20, 50, table_hierarchy6)

    downtime = ["", "", 224, ""]
    output8_1 = app_rams.pof_mttf(downtime, level2, level2, level2, node2, node2, node2, 2, shared_component2, shared_component2, shared_component2, 20, table_hierarchy2, table_hierarchy2, table_hierarchy2)

    output8_2 = app_rams.pof_mttf(downtime, level3, level3, level3, node3, node3, node3, 2, shared_component3, shared_component3, shared_component3, 20, table_hierarchy3, table_hierarchy3, table_hierarchy3)

    output8_3 = app_rams.pof_mttf(downtime, level4, level4, level4, node4, node4, node4, 100, shared_component4, shared_component4, shared_component4, 20, table_hierarchy4, table_hierarchy4, table_hierarchy4)

    output8_4 = app_rams.pof_mttf(downtime, level5, level5, level5, node5, node5, node5, 2, shared_component5, shared_component5, shared_component5, 20, table_hierarchy5, table_hierarchy5, table_hierarchy5)

    output8_5 = app_rams.pof_mttf(downtime, level6, level6, level6, node6, node6, node6, 2, shared_component6, shared_component6, shared_component6, 20, table_hierarchy6, table_hierarchy6, table_hierarchy6)

    assert type(output1_1['ts_failure_event']) == list
    assert len(output1_1['ts_failure_event']) == 3
    assert type(output1_2['ts_failure_event']) == list
    assert len(output1_2['ts_failure_event']) == 0	
    assert type(output1_3['ts_failure_event']) == list
    assert len(output1_3['ts_failure_event']) == 0		
    assert output1_1['max_ttf'][0] == -1.0
    assert output1_1['mttf'][0] > 100
    assert output1_1['std_ttf'][0] == -1.0	
    assert output1_2['ts_failure_event'] == []
    assert output1_2['max_ttf'] == []
    assert output1_2['mttf'] == []
    assert output1_2['std_ttf'] == []
    assert output1_3['ts_failure_event'] == []
    assert output1_3['max_ttf'] == []
    assert output1_3['mttf'] == []
    assert output1_3['std_ttf'] == []
    assert table_hierarchy1[:,0][0] == 'ED Subsystem'
    assert table_hierarchy1[:,5][1] == 'OR'
    assert level1 == ['Level 3', 'Level 2', 'Level 1', 'Level 0']
    assert type(output3_1) == numpy.float64
    assert type(output3_2) == numpy.float64
    assert type(output3_3) == numpy.float64
    assert type(output3_4) == numpy.float64
    assert type(output3_5) == numpy.float64
    assert type(output3_6) == numpy.float64
    assert type(output3_7) == numpy.float64
    assert output4 == [3, 2, 1, 4, 5, 32, 6]
    assert output5[1] > 0.0
    assert f_exist == True
    assert output7_2 > 0.0
    assert output7_3 > 0.0
    assert output7_4 > 0.0
    assert output7_5 > 0.0
    assert output7_6 > 0.0
    assert output8_1["std_ttf_ed"] > 0.0 
    assert output8_1["max_ttf_ed"] > 0.0  
    assert output8_2["mttf_ed"] > 0.0
    assert output8_2["std_ttf_ed"] > 0.0 
    assert output8_2["max_ttf_ed"] > 0.0 
    assert output8_3["mttf_ed"] > 0.0
    assert output8_3["std_ttf_ed"] > 0.0 
    assert output8_3["max_ttf_ed"] > 0.0 
    assert output8_4["mttf_ed"] > 0.0
    assert output8_4["std_ttf_ed"] > 0.0 
    assert output8_4["max_ttf_ed"] > 0.0 
    assert output8_5["mttf_ed"] > 0.0
    assert output8_5["std_ttf_ed"] > 0.0 
    assert output8_5["max_ttf_ed"] > 0.0


def test_availability2():

    downtime_raw = {
      "_links": {
        "self": "http://api/1234/results/downtime"
    
      },
    
      "downtime": {
        "device11": {
          "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
          "jan": [744.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "feb": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "mar": [0.0, 43.0, 0.0, 0.0, 0.0, 0.0, 432.0, 15.0, 0.0, 15.0],
          "apr": [0.0, 0.0, 0.0, 0.0, 0.0, 564.0, 0.0, 0.0, 0.0, 0.0],
          "may": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "jun": [0.0, 0.0, 196.0, 0.0, 124.0, 0.0, 532.0, 0.0, 0.0, 0.0],
          "jul": [0.0, 0.0, 0.0, 643.0, 0.0, 324.0, 0.0, 0.0, 0.0, 0.0],
          "aug": [56.0, 0.0, 0.0, 0.0, 56.0, 0.0, 56.0, 0.0, 0.0, 0.0],
          "sep": [0.0, 145.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "oct": [0.0, 0.0, 0.0, 88.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "nov": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "dec": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 159.0, 0.0, 0.0, 0.0]
        },
        "device12": {
          "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
          "jan": [744.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "feb": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "mar": [0.0, 43.0, 0.0, 0.0, 0.0, 0.0, 0.0, 15.0, 0.0, 0.0],
          "apr": [0.0, 0.0, 0.0, 0.0, 0.0, 532.0, 0.0, 0.0, 0.0, 180.0],
          "may": [0.0, 0.0, 0.0, 0.0, 0.0, 42.0, 0.0, 0.0, 0.0, 0.0],
          "jun": [0.0, 0.0, 0.0, 0.0, 124.0, 0.0, 0.0, 0.0, 0.0, 0.0],
          "jul": [0.0, 140.0, 0.0, 643.0, 0.0, 324.0, 0.0, 0.0, 0.0, 0.0],
          "aug": [56.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 720.0],
          "sep": [0.0, 0.0, 543.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 720.0],
          "oct": [0.0, 0.0, 0.0, 643.0, 0.0, 0.0, 120.0, 453.0, 0.0, 744.0],
          "nov": [0.0, 0.0, 0.0, 0.0, 47.0, 0.0, 0.0, 0.0, 0.0, 720.0],
          "dec": [0.0, 0.0, 0.0, 0.0, 96.0, 0.0, 0.0, 0.0, 0.0, 744.0]
        }
      }
    }

    list_keys1 = []
    list_keys2 = []
    list_downtime = []
    t_life = 0
    for key1 in downtime_raw['downtime']: 
        list_keys1.append(key1)
        temp1 = []
        temp2 = []
        for key2 in downtime_raw['downtime'][key1]:
            if (key2 != 'year'):
                temp1.append(key2)
                temp2.append(sum(downtime_raw['downtime'][key1][key2]))
            if (key2 == 'year'):
                t_life = len(downtime_raw['downtime'][key1][key2])
        list_keys2.append(temp1)
        list_downtime.append(sum(temp2))

    app_rams = business.ArrayRams.calc_availability("2")
    output1 = app_rams.calc_availability(list_keys1, list_downtime, t_life)
    output2 = app_rams.calc_availability(list_keys1,[], t_life)
    output3 = app_rams.calc_availability(list_keys1,list_downtime, -1)
	
    assert output1['availability_tb'][0] == 0.9521461187214612
    assert output1['availability_tb'][1] == 0.9041894977168949
    assert output2['availability_tb'] == []
    assert output3['availability_tb'] == []


def test_maintainability2():

    app_rams = business.ArrayRams.calc_maintainability("2")
    output1 = app_rams.calc_maintainability(["id1", "id2", "id4", "id1", "id11", "id2", "id4", "id1"],[12, 34, 55, 13, 40, 67, 55, 19], [10,10,10,10, 10,10,10,10], 24, 'Gaussian')
    output2 = app_rams.calc_maintainability(["id1", "id2", "id4", "id1", "id11", "id2", "id4", "id1"],[12, 34, 55, 13, 40, 67, 55, 19], [10,10,10,10, 10,10,10,10], 24, 'LogNormal')
    output3 = app_rams.calc_maintainability(["id1", "id2", "id4", "id1", "id11", "id2", "id4", "id1"],[12, 34, 55, 13, 40, 67, 55, 19], [10,10,10,10, 10,10,10,10], 24, 'Exponential')
    output4 = app_rams.calc_maintainability(['comp_Id1', 'comp_Id2'], ["NA", "NA"], [24, 37], 340, 'Gaussian')  
    output5 = app_rams.calc_maintainability([], [340, 651], [24, 37], 340, 'Gaussian')    

    u = app_rams.unique(["id1", "id2", "id4", "id1", "id11", "id2", "id4", "id1"])
    
    assert output1['probability_maintenance'][0] == 0.0009676032132183563
    assert output1['critical_component'] == 'id4'
    assert output2['probability_maintenance'][0] == 1.0964511176813948e-07
    assert output2['critical_component'] == 'id4'
    assert output3['probability_maintenance'][0] == 1.0
    assert output3['critical_component'] == 'id1'
    assert output4['probability_maintenance'][0] == 'NA'     
    assert output5['probability_maintenance'] == []  
    assert u == ["id1", "id2", "id4", "id11"] 


def test_survivability2():

    sk_raw_data = {
    "devices": [
        {
            "uls_results": {
                "mooring_tension": [
                    [
                        [
                            [
                                37435.35787709276,
                                64419.65516736676
                            ],
                            [
                                37435.35892477166,
                                64419.65469435439
                            ],
                            [
                                44570.00656000909,
                                76583.59013410198
                            ],
                            [
                                44570.00369245041,
                                76583.58878752761
                            ]
                        ],
                        [
                            [
                                36827.85783060535,
                                65065.50082856364
                            ],
                            [
                                36827.85889613669,
                                65065.50038926155
                            ],
                            [
                                46199.86850567229,
                                79312.71249213733
                            ],
                            [
                                46199.864852886836,
                                79312.71034463706
                            ]
                        ],
                        [
                            [
                                36170.36857294558,
                                65783.65696742413
                            ],
                            [
                                36170.36965784724,
                                65783.65656496542
                            ],
                            [
                                48959.48626971105,
                                83313.39282665773
                            ],
                            [
                                48959.48132696917,
                                83313.3893720024
                            ]
                        ]
                    ]
                ],	
			    "tension_versus_mbl": [
                    [
                        [
                            0.01751654091243178,
                            0.017516540783813574,
                            0.020824072813175522,
                            0.02082407244702446
                        ],
                        [
                            0.017692154735853838,
                            0.017692154616401893,
                            0.021566156627740477,
                            0.021566156043807263
                        ],
                        [
                            0.017887430717309825,
                            0.01788743060787607,
                            0.022653993570908214,
                            0.022653992631542563
                        ]
                    ]
                ]	
		  },
		"fls_results":{
            "env_proba": [1],
		    "cdf_stress_range": [
                    [
                        0.03530433733043322,
                        0.004113311227466497,
                        0.0357526183671677,
                        0.049570761686637314
                    ],
                    [
                        0.04816981615077811,
                        0.016192996694111464,
                        0.039482057998408294,
                        0.055180169984814766
                    ],
                    [
                        0.05026068881173524,
                        0.02412937305243806,
                        0.049151145581692555,
                        0.06069185676171353
                    ],
                    [
                        0.0850192515016839,
                        0.038166044361503934,
                        0.10522211138375195,
                        0.15621823301378723
                    ],
                    [
                        0.10718831438830025,
                        0.04814171982039578,
                        0.12621546774823414,
                        0.15950123927281795
                    ],
                    [
                        0.1402212660359254,
                        0.08610877852324604,
                        0.14270955086647966,
                        0.18250855738374352
                    ],
                    [
                        0.15401608191737076,
                        0.08788402286091851,
                        0.14520699149067592,
                        0.2050886634790137
                    ],
                    [
                        0.2323368205919633,
                        0.10149160003143153,
                        0.19484212533913678,
                        0.23713979281594194
                    ],
                    [
                        0.23974936048918513,
                        0.14295036729492838,
                        0.19625894152562004,
                        0.25946781427664195
                    ],
                    [
                        0.24057537100758625,
                        0.20578656203009824,
                        0.20723581258852156,
                        0.2922374477546056
                    ]
                ],
            "cdf": [
                    [
                        0.07928831669580277,
                        0.03422811116375578,
                        0.09096998709297957,
                        0.09096998709297957
                    ],
                    [
                        0.40626072180580275,
                        0.11351642785955855,
                        0.4179423922029796,
                        0.4179423922029796
                    ],
                    [
                        0.49723070889878235,
                        0.44048883296955854,
                        0.49723070889878235,
                        0.49723070889878235
                    ],
                    [
                        0.5314588200625381,
                        0.5314588200625381,
                        0.577875079640691,
                        0.639338637730452
                    ],
                    [
                        0.6566732563198289,
                        0.6566732563198289,
                        0.7199830084723604,
                        0.7199830084723604
                    ],
                    [
                        0.7987811851514987,
                        0.7144607642846041,
                        0.7542111196361162,
                        0.8451974447296513
                    ],
                    [
                        0.8794255558934071,
                        0.8565686931162739,
                        0.8794255558934071,
                        0.8794255558934071
                    ],
                    [
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9011877726333225
                    ],
                    [
                        0.9782377832530228,
                        0.9782377832530228,
                        0.9589752805980978,
                        0.9422124920281629
                    ],
                    [
                        0.9999999999929382,
                        0.9999999999929382,
                        0.9999999999929382,
                        0.9999999999929382
                    ]
                ],
            "ad": [
                    60000000000.0,
                    60000000000.0,
                    60000000000.0,
                    60000000000.0
                ],
            "m": [
                    3.0,
                    3.0,
                    3.0,
                    3.0
                ],
            "n_cycles_lifetime": [
                    95559948.11638647,
                    95559948.11638647,
                    95559948.11638647,
                    95559948.11638647
                ]
		  }
        },

        {
            "uls_results": {
                "mooring_tension": [
                    [
                        [
                            [
                                37435.35787709276,
                                64419.65516736676
                            ],
                            [
                                37435.35892477166,
                                64419.65469435439
                            ],
                            [
                                44570.00656000909,
                                76583.59013410198
                            ],
                            [
                                44570.00369245041,
                                76583.58878752761
                            ]
                        ],
                        [
                            [
                                36827.85783060535,
                                65065.50082856364
                            ],
                            [
                                36827.85889613669,
                                65065.50038926155
                            ],
                            [
                                46199.86850567229,
                                79312.71249213733
                            ],
                            [
                                46199.864852886836,
                                79312.71034463706
                            ]
                        ],
                        [
                            [
                                36170.36857294558,
                                65783.65696742413
                            ],
                            [
                                36170.36965784724,
                                65783.65656496542
                            ],
                            [
                                48959.48626971105,
                                83313.39282665773
                            ],
                            [
                                48959.48132696917,
                                83313.3893720024
                            ]
                        ]
                    ]
                ],	
			    "tension_versus_mbl": [
                    [
                        [
                            0.01751654091243178,
                            0.017516540783813574,
                            0.020824072813175522,
                            0.02082407244702446
                        ],
                        [
                            0.017692154735853838,
                            0.017692154616401893,
                            0.021566156627740477,
                            0.021566156043807263
                        ],
                        [
                            0.017887430717309825,
                            0.01788743060787607,
                            0.022653993570908214,
                            0.022653992631542563
                        ]
                    ]
                ]	
		  },
		"fls_results":{
            "env_proba": [1],
		    "cdf_stress_range": [
                    [
                        1.03530433733043322,
                        1.004113311227466497,
                        1.0357526183671677,
                        1.049570761686637314
                    ],
                    [
                        1.04816981615077811,
                        1.016192996694111464,
                        1.039482057998408294,
                        1.055180169984814766
                    ],
                    [
                        1.05026068881173524,
                        1.02412937305243806,
                        1.049151145581692555,
                        1.06069185676171353
                    ],
                    [
                        1.0850192515016839,
                        1.038166044361503934,
                        1.10522211138375195,
                        1.15621823301378723
                    ],
                    [
                        1.10718831438830025,
                        1.04814171982039578,
                        1.12621546774823414,
                        1.15950123927281795
                    ],
                    [
                        1.1402212660359254,
                        1.08610877852324604,
                        1.14270955086647966,
                        1.18250855738374352
                    ],
                    [
                        1.15401608191737076,
                        1.08788402286091851,
                        1.14520699149067592,
                        1.2050886634790137
                    ],
                    [
                        1.2323368205919633,
                        1.10149160003143153,
                        1.19484212533913678,
                        1.23713979281594194
                    ],
                    [
                        1.23974936048918513,
                        1.14295036729492838,
                        1.19625894152562004,
                        1.25946781427664195
                    ],
                    [
                        1.24057537100758625,
                        1.20578656203009824,
                        1.20723581258852156,
                        1.2922374477546056
                    ]
                ],
            "cdf": [
                    [
                        0.07928831669580277,
                        0.03422811116375578,
                        0.09096998709297957,
                        0.09096998709297957
                    ],
                    [
                        0.40626072180580275,
                        0.11351642785955855,
                        0.4179423922029796,
                        0.4179423922029796
                    ],
                    [
                        0.49723070889878235,
                        0.44048883296955854,
                        0.49723070889878235,
                        0.49723070889878235
                    ],
                    [
                        0.5314588200625381,
                        0.5314588200625381,
                        0.577875079640691,
                        0.639338637730452
                    ],
                    [
                        0.6566732563198289,
                        0.6566732563198289,
                        0.7199830084723604,
                        0.7199830084723604
                    ],
                    [
                        0.7987811851514987,
                        0.7144607642846041,
                        0.7542111196361162,
                        0.8451974447296513
                    ],
                    [
                        0.8794255558934071,
                        0.8565686931162739,
                        0.8794255558934071,
                        0.8794255558934071
                    ],
                    [
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9011877726333225
                    ],
                    [
                        0.9782377832530228,
                        0.9782377832530228,
                        0.9589752805980978,
                        0.9422124920281629
                    ],
                    [
                        1.0,
                        1.0,
                        1.0,
                        1.0
                    ]
                ],
            "ad": [
                    40000000000.0,
                    40000000000.0,
                    40000000000.0,
                    40000000000.0
                ],
            "m": [
                    5.0,
                    5.0,
                    5.0,
                    5.0
                ],
            "n_cycles_lifetime": [
                    85559948.11638647,
                    85559948.11638647,
                    85559948.11638647,
                    85559948.11638647
                ]
		  }
        }
	]
}

    fatigue_input_et = {
                "description": "Fatigue Stress Probability",
                "label": "Fatigue Stress Probability",
                "unit": "-",
                "value": {
                    "probability": [
                        1.5206049678508205e-08,
                        3.0407537884231846e-08,
                        4.559990542487577e-08,
                        6.077859766752728e-08,
                        7.593906681435863e-08,
                        9.107677417354688e-08,
                        1.0618719242382392e-07,
                        1.21265807871073e-07,
                        1.3630812269540208e-07,
                        1.5130965718712085e-07,
                        1.6626595197007078e-07,
                        1.8117257021076361e-07,
                        1.960250998117898e-07,
                        2.1081915558798836e-07,
                        2.255503814238678e-07,
                        2.4021445241079934e-07,
                        2.5480707696250304e-07,
                        2.693239989073973e-07,
                        2.8376099955636943e-07,
                        2.981138997445583e-07,
                        3.123785618457884e-07,
                        3.265508917582762e-07,
                        3.4062684086027586e-07,
                        3.5460240793438074e-07,
                        3.684736410591807e-07,
                        3.8223663946702437e-07,
                        3.958875553666936e-07,
                        4.094225957297704e-07,
                        4.228380240395563e-07,
                        4.361301620014324e-07,
                        4.4929539121352127e-07,
                        4.623301547966765e-07,
                        4.7523095898270135e-07,
                        4.879943746598625e-07,
                        5.006170388747334e-07,
                        5.130956562894634e-07,
                        5.254270005936209e-07,
                        5.376079158697219e-07,
                        5.496353179117512e-07,
                        5.615061954958308e-07,
                        5.732176116023825e-07,
                        5.847667045890971e-07,
                        5.961506893141092e-07,
                        6.073668582087254e-07,
                        6.184125822992809e-07,
                        6.292853121775112e-07,
                        6.399825789190542e-07,
                        6.505019949496442e-07,
                        6.608412548586594e-07,
                        6.709981361596233e-07,
                        6.809704999975055e-07,
                        6.907562918024628e-07,
                        7.003535418898967e-07,
                        7.097603660066647e-07,
                        7.189749658233333e-07,
                        7.279956293724174e-07,
                        7.368207314325895e-07,
                        7.454487338588709e-07,
                        7.538781858589358e-07,
                        7.621077242154633e-07,
                        7.701360734549813e-07,
                        7.779620459631059e-07,
                        7.855845420465912e-07,
                        7.930025499424044e-07,
                        8.002151457741559e-07,
                        8.072214934562425e-07,
                        8.140208445461004e-07,
                        8.206125380450052e-07,
                        8.269960001478955e-07,
                        8.331707439427284e-07,
                        8.39136369059911e-07,
                        8.448925612724563e-07,
                        8.504390920472892e-07,
                        8.557758180486822e-07,
                        8.609026805941851e-07,
                        8.658197050639658e-07,
                        8.705270002642497e-07,
                        8.750247577456519e-07,
                        8.793132510772164e-07,
                        8.833928350770067e-07,
                        8.87263945000119e-07,
                        8.909270956850176e-07,
                        8.943828806591169e-07,
                        8.976319712045505e-07,
                        9.006751153851737e-07,
                        9.035131370355809e-07,
                        9.061469347135152e-07,
                        9.085774806163534e-07,
                        9.108058194629385e-07,
                        9.128330673417688e-07,
                        9.14660410526649e-07,
                        9.16289104260912e-07,
                        9.17720471511344e-07,
                        9.1895590169295e-07,
                        9.199968493657157e-07,
                        9.208448329045191e-07,
                        9.215014331434429e-07,
                        9.219682919954576e-07,
                        9.222471110490248e-07,
                        9.22339650142468e-07
                    ],
                    "stress": [
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        0.1,
                        1.6468789096280935e-05,
                        3.481680736811738e-05,
                        5.316468133310006e-05,
                        7.151241099122798e-05,
                        8.98599963425015e-05,
                        0.00010820743738691949,
                        0.00012655473412448364,
                        0.0001449018865551922,
                        0.0001632488946790467,
                        0.00018159575849604638,
                        0.0001999424780061912,
                        0.000218289053209482
                    ]
                }
            }

    device_number = len(sk_raw_data['devices'])
    device_id = []
    a = []
    m = []
    cycles = []
    cdf = []
    stress_range = []
    for i in range(device_number):
        device_id.append('Device'+str(i))
        a.append(sk_raw_data['devices'][i]['fls_results']['ad'])
        m.append(sk_raw_data['devices'][i]['fls_results']['m'])
        cycles.append((sk_raw_data['devices'][i]['fls_results']
        ['n_cycles_lifetime']))
        stress_range.append(sk_raw_data['devices'][i]['fls_results']
        ['cdf_stress_range'])
        cdf.append(sk_raw_data['devices'][i]['fls_results']['cdf'])

    app_rams = business.ArrayRams.calc_survivability("2")

    output1_uls = app_rams.calc_survivability_uls([1], 46.75, 60, 1.09, 0.96, 10000, 7, 1.5, 0.2, 0.05, 
                                                  'Monte Carlo', 'Gaussian', 'Gaussian', 'Gaussian', 'Gaussian')
										
    output2_uls = app_rams.calc_survivability_uls([1], 46.75, 60, 1.09, 0.96, 10000, 7, 1.5, 0.2, 0.05,
                                                  'Monte Carlo','LogNormal', 'LogNormal', 'LogNormal',     'LogNormal')	
										
    output3_uls = app_rams.calc_survivability_uls([1], 46.75, 60, 1.09, 0.96, 10000, 7, 1.5, 0.2, 0.05, 
                                                  'Monte Carlo', 'Weibull', 'Weibull', 'Weibull', 
                                                  'Weibull')

    output4_uls = app_rams.calc_survivability_uls([1], 46.75, 60, 1.09, 0.96, 10000, 7, 1.5, 0.2, 0.05,
                                                 'Monte Carlo', 'Exponential', 'Exponential',  'Exponential','Exponential')
										
    output5_uls = app_rams.calc_survivability_uls([1], 46.75, 60, 1.09, 0.96, 10000, 7, 1.5, 0.2, 0.05,
                                                  'Monte Carlo', 'Gumbel', 'Gumbel', 'Gumbel', 'Gumbel')																				
    output6_uls = app_rams.calc_survivability_uls([1], 46.75, 60, 1.09, 0.96, 10000, 7, 1.5, 0.2, 0.05,
                                                 ' ', 'Gaussian', 'Gaussian', 'Gaussian', 'Gaussian')

    output7_uls = app_rams.calc_survivability_uls([1], 83313, 270000, 1.0, 1.0, 10000, 0.2396, 0.1980, 
                                                  0.3, 0.2,  option='FORM', pd_l='Gaussian', pd_r='Gaussian', pd_ufl='Gaussian',
                                                   pd_ufr='Gaussian', error=1e-3, num_iteration=20)

    output8_uls = app_rams.calc_survivability_uls([1], 83313, 270000, 1.0, 1.0, 10000, 24994, 81000, 
                                                  0.3, 0.2,  option='FORM', pd_l='LogNormal', pd_r='LogNormal', pd_ufl='LogNormal',
                                                pd_ufr='LogNormal',error=1e-3, num_iteration=20)       

    output9_uls = app_rams.calc_survivability_uls([1], 83313, 270000, 1.0, 1.0, 10000, 24994, 81000, 
                                                  0.3, 0.2,  option='FORM', pd_l='Exponential', pd_r='Exponential', pd_ufl='Exponential',
                                                   pd_ufr='Exponential', error=1e-3, num_iteration=20)

    output10_uls = app_rams.calc_survivability_uls([1], 83313, 270000, 1.0, 1.0, 10000, 24994, 81000, 
                                                  0.3, 0.2,  option='FORM', pd_l='Gumbel', pd_r='Gumbel', pd_ufl='Gumbel',
                                                   pd_ufr='Gumbel', error=1e-3, num_iteration=20)

    output1_fls = app_rams.calc_survivability_fls([1], math.pow(10, 12.164), 1, 5.0, 1e8, 4.368, 
                                              10000, 'Gaussian', 'Gaussian', 0.1*math.pow(10, 12.164), '', '', '', 0.3)

    output2_fls = app_rams.calc_survivability_fls([1], 12.164, 1, 5.0, 1e8, 4.348, 
                                              10000, 'LogNormal', 'LogNormal', 1.2164, '', '', '', 0.3)

    output3_fls = app_rams.calc_survivability_fls([1], 12.164, 1, 5.0, 1e8, 4.348, 
                                              10000, 'Weibull', 'Weibull', 1.2164, '', '', '', 0.3)

    output4_fls = app_rams.calc_survivability_fls([1], 12.164, 1, 5.0, 1e8, 4.348, 
                                              10000, 'Weibull', 'Weibull', 1.2164, '', '', '', 0.3, option=' ', pd_h='constant',
                                              pd_m='constant', pd_n='constant')

    output5_fls = app_rams.calc_survivability_fls([1], 5.076e10, 1.786, 3.0, 1e8, 0.115, 
                                              10000, 'Gaussian', 'LogNormal', 1.0153e10, '', '', '', 0.0575, option='FORM', pd_h='constant',
                                              pd_m='constant', pd_n='constant')

    output6_fls = app_rams.calc_survivability_fls([1], 5.076e10, 1.786, 3.0, 1e8, 0.115, 
                                              10000, 'Gaussian', 'Gaussian', 1.0153e10, '', '', '', 0.0575, option='FORM', pd_h='constant',
                                              pd_m='constant', pd_n='constant')

    a_unique, m_unique, cdf_unique, cycles_unique, device_unique, stress_range_unique, shape, scale= app_rams.pre_process_sk(a, cdf,cycles, device_id, m, stress_range)

    survival_prob_uls1 = app_rams.survivability_uls(["Device1"], ["Device1"], 270000, 
	    270000, 83313, 1.0, 1.0, 100, 0.2*270000, 0.2*270000, 0.3*83313, 0.1, 
		0.1, [83313, 83313,83313, 83313], [0.25,0.25, 0.25, 0.25], 'Monte Carlo', 'Gaussian', 'Gaussian', 'Gaussian', 'Gaussian', "ULS")
    survival_prob_uls2 = app_rams.survivability_uls(["Device1"], ["Device1"], 270000, 
	    270000, 83313, 1.0, 1.0, 100, 0.2*270000, 0.2*270000, 0.3*83313, 0.1, 
		0.1, [83313, 83313,83313, 83313], [0.25,0.25, 0.25, 0.25], 'FORM', 'Gaussian', 'Gaussian', 'Gaussian', 'Gaussian', "ULS")

    survival_prob_fls1 = app_rams.survivability_fls(a, cdf, 0.25, 0.5, cycles, 100000000.0, ["Device1"], ["Device1"], [1], fatigue_input_et["value"]["stress"], fatigue_input_et["value"]["probability"], m, 50763336419, 3.0, 100, "Monte Carlo", "Gaussian", "Gaussian", "constant", "constant", "constant", "FLS", 10153000000,'','','', stress_range)
    survival_prob_fls2 = app_rams.survivability_fls(a, cdf, 0.25, 0.5, cycles, 100000000.0, ["Device1"], ["Device1"], [1], fatigue_input_et["value"]["stress"], fatigue_input_et["value"]["probability"], m, 50763336419, 3.0, 100, "FORM", "Gaussian", "LogNormal", "constant", "constant", "constant", "FLS", 10153000000,'','','', stress_range)

    # ULS
    assert output1_uls['survival_uls'][0] > 0.0
    assert output2_uls['survival_uls'][0] > 0.0
    assert output3_uls['survival_uls'][0] > 0.0
    assert output4_uls['survival_uls'][0] > 0.0
    assert output5_uls['survival_uls'][0] > 0.0	
    assert output6_uls['survival_uls'] == []
    n7 = len(output7_uls['survival_uls'])
    assert output7_uls['survival_uls'][n7-1] > 0.0
    n8 = len(output8_uls['survival_uls'])
    assert output8_uls['survival_uls'][n8-1] > 0.0
    n9 = len(output9_uls['survival_uls'])
    assert output9_uls['survival_uls'][n9-1] > 0.0
    n10 = len(output10_uls['survival_uls'])
    assert output10_uls['survival_uls'][n10-1] > 0.0
    assert survival_prob_uls1['SK Subsystem']["survival_uls"][0] > 0.0
    assert survival_prob_uls1['ET Subsystem']["survival_uls"][0] > 0.0
    assert survival_prob_uls2['SK Subsystem']["survival_uls"][0] > 0.0
    assert survival_prob_uls2['ET Subsystem']["survival_uls"][0] > 0.0 

    # FLS
    assert output1_fls['survival_fls'][0] == 0.0
    assert output2_fls['survival_fls'][0] == 0.0
    assert output3_fls['survival_fls'][0] > 0.0
    assert output4_fls['survival_fls'] == None
    assert output5_fls['survival_fls'][0] > 0.0
    assert output6_fls['survival_fls'] == None

    assert a_unique[0] == 60000000000.0
    assert m_unique[0] == 3.0
    assert cdf_unique[0][0][0] == 0.07928831669580277
    assert cycles_unique[0][0] == 95559948.11638647
    assert stress_range_unique[0][0][0] == 0.03530433733043322
    assert shape[0][0] >0.0
    assert scale[0][0] >0.0
    assert type(survival_prob_fls1['SK Subsystem']) == dict
    assert type(survival_prob_fls1['ET Subsystem']) == dict
    assert type(survival_prob_fls2['SK Subsystem']) == dict
    assert type(survival_prob_fls2['ET Subsystem']) == dict