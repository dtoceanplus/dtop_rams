from dtop_rams import business

"""
   to test the __init__ function
"""


def test_init():
    app_err_reliability = business.ArrayRams.calc_reliability("4")
    app_err_availability = business.ArrayRams.calc_availability("4")
    app_err_maintainability = business.ArrayRams.calc_maintainability("4")
    app_err_survivability = business.ArrayRams.calc_survivability("4")

    assert app_err_reliability == None
    assert app_err_availability == None
    assert app_err_maintainability == None
    assert app_err_survivability == None