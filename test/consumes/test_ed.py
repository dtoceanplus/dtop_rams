import atexit
import pytest
from pact import Consumer, Like, Provider, Term, EachLike
import pytest
import numpy as np
import os
import requests
import json
from dtop_rams import service


@pytest.fixture
def app():
    settings_override = {
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": "/////db/rams.db", 
    }
    app = service.create_app(settings_override)
    yield app
@pytest.fixture
def client(app):
    client = app.test_client()
    yield client
pact = Consumer("rams").has_pact_with(Provider("ed"), port=1231)
pact.start_service()
atexit.register(pact.stop_service)


def test_ed_hierarchy1(client):
    pact.given("ed 1 exists at cpx 3").upon_receiving(
        "a request for hierarchy"
    ).with_request(
        "GET", Term(r"/api/energy-deliv-studies/\d+/results", "/api/energy-deliv-studies/1/results")
    ).will_respond_with(
        200,
        body=Like(
            {
                "hierarchy_new": {'system': ['ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED'], 'name_of_node': ['ED Subsystem', 'ED1', 'ED2', 'Route1_1', 'Route2_1', '11', '6', '2', '9', '4', '0', '8', '3', '1', '12', '10', '7', '5'], 'design_id': ['NA', 'NA', 'NA', 'NA', 'NA', '11', '6', '2', '9', '4', '0', '8', '3', '1', '12', '10', '7', '5'], 'node_type': ['System', 'System', 'System', 'Energy route', 'Energy route', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component'], 'node_subtype': ['NA', 'NA', 'NA', 'NA', 'NA', ' umbilical', ' umbilical', ' substation', ' array', ' array', ' export', ' dry-mate', ' dry-mate', ' dry-mate', ' wet-mate', ' wet-mate', ' wet-mate', ' wet-mate'], 'category': ['Level 3', 'Level 2', 'Level 2', 'Level 1', 'Level 1', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0'], 'parent': ['NA', 'ED Subsystem', 'ED Subsystem', ['ED1'], ['ED2'], ['Route2_1'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route2_1'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route2_1'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route2_1'], ['Route2_1'], ['Route1_1'], ['Route1_1']], 'child': [['ED1', 'ED2'], ['Route1_1'], ['Route2_1'], ['7', '6', '5', '4', '3', '2', '1', '0'], ['12', '11', '10', '9', '8', '2', '1', '0'], 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA'], 'gate_type': ['OR', 'OR', 'OR', 'AND', 'AND', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA'], 'failure_rate_repair': ['NA', 'NA', 'NA', 'NA', 'NA', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 'failure_rate_replacement': ['NA', 'NA', 'NA', 'NA', 'NA', 0.0009786441030323538, 0.0009786441030323538, 0.0215, 0.00054696, 0.0019143600000000004, 0.00920716, 0.0475, 0.0475, 0.0475, 0.0475, 0.0475, 0.0475, 0.0475]}
            }
    

        ),
    )
    with pact:
        response = requests.get(f"{pact.uri}/api/energy-deliv-studies/1/results")
        assert response.json() == {
                "hierarchy_new": {'system': ['ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED'], 'name_of_node': ['ED Subsystem', 'ED1', 'ED2', 'Route1_1', 'Route2_1', '11', '6', '2', '9', '4', '0', '8', '3', '1', '12', '10', '7', '5'], 'design_id': ['NA', 'NA', 'NA', 'NA', 'NA', '11', '6', '2', '9', '4', '0', '8', '3', '1', '12', '10', '7', '5'], 'node_type': ['System', 'System', 'System', 'Energy route', 'Energy route', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component', 'Component'], 'node_subtype': ['NA', 'NA', 'NA', 'NA', 'NA', ' umbilical', ' umbilical', ' substation', ' array', ' array', ' export', ' dry-mate', ' dry-mate', ' dry-mate', ' wet-mate', ' wet-mate', ' wet-mate', ' wet-mate'], 'category': ['Level 3', 'Level 2', 'Level 2', 'Level 1', 'Level 1', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0', 'Level 0'], 'parent': ['NA', 'ED Subsystem', 'ED Subsystem', ['ED1'], ['ED2'], ['Route2_1'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route2_1'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route2_1'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route2_1'], ['Route2_1'], ['Route1_1'], ['Route1_1']], 'child': [['ED1', 'ED2'], ['Route1_1'], ['Route2_1'], ['7', '6', '5', '4', '3', '2', '1', '0'], ['12', '11', '10', '9', '8', '2', '1', '0'], 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA'], 'gate_type': ['OR', 'OR', 'OR', 'AND', 'AND', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA'], 'failure_rate_repair': ['NA', 'NA', 'NA', 'NA', 'NA', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 'failure_rate_replacement': ['NA', 'NA', 'NA', 'NA', 'NA', 0.0009786441030323538, 0.0009786441030323538, 0.0215, 0.00054696, 0.0019143600000000004, 0.00920716, 0.0475, 0.0475, 0.0475, 0.0475, 0.0475, 0.0475, 0.0475]}
            }


def test_ed_hierarchy2(client):
    pact.given("ed 2 exists at cpx 1").upon_receiving(
        "a request for hierarchy"
    ).with_request(
        "GET", Term(r"/api/energy-deliv-studies/\d+/results", "/api/energy-deliv-studies/1/results")
    ).will_respond_with(
        200,
        body=Like(
            {
    "hierarchy_new": {'system': ['ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED'], 'name_of_node': ['ED Subsystem', 'ED1', 'ED2', 'Route1_1', 'Route2_1', 'AC1', 'AC2', 'CP1', 'EC1'], 'design_id': ['NA', 'NA', 'NA', 'NA', 'NA', 'AC1', 'AC2', 'CP1', 'EC1'], 'node_type': ['System', 'System', 'System', 'Energy route', 'Energy route', 'Component', 'Component', 'Component', 'Component'], 'node_subtype': ['NA', 'NA', 'NA', 'NA', 'NA', 'array', 'array', 'substation', 'export'], 'category': ['Level 3', 'Level 2', 'Level 2', 'Level 1', 'Level 1', 'Level 0', 'Level 0', 'Level 0', 'Level 0'], 'parent': ['NA', 'ED Subsystem', 'ED Subsystem', ['ED1'], ['ED2'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route1_1', 'Route2_1'], ['Route1_1', 'Route2_1']], 'child': [['ED1', 'ED2'], ['Route1_1'], ['Route2_1'], ['AC1', 'AC2', 'CP1', 'EC1'], ['AC2', 'CP1', 'EC1'], 'NA', 'NA', 'NA', 'NA'], 'gate_type': ['OR', 'OR', 'OR', 'AND', 'AND', 'NA', 'NA', 'NA', 'NA'], 'failure_rate_repair': ['NA', 'NA', 'NA', 'NA', 'NA', 0.00023999999999999998, 0.0004, 0.03, 0.00096], 'failure_rate_replacement': ['NA', 'NA', 'NA', 'NA', 'NA', 0.00023999999999999998, 0.0004, 0.03, 0.00096]}
}

        ),
    )
    with pact:
        response = requests.get(f"{pact.uri}/api/energy-deliv-studies/1/results")
        assert response.json()["hierarchy_new"]["category"] == ['Level 3', 'Level 2', 'Level 2', 'Level 1', 'Level 1', 'Level 0', 'Level 0', 'Level 0', 'Level 0']
        assert response.json()["hierarchy_new"]["parent"] == ['NA', 'ED Subsystem', 'ED Subsystem', ['ED1'], ['ED2'], ['Route1_1'], ['Route1_1', 'Route2_1'], ['Route1_1', 'Route2_1'], ['Route1_1', 'Route2_1']]
        assert response.json()["hierarchy_new"]["child"] == [['ED1', 'ED2'], ['Route1_1'], ['Route2_1'], ['AC1', 'AC2', 'CP1', 'EC1'], ['AC2', 'CP1', 'EC1'], 'NA', 'NA', 'NA', 'NA']
        assert response.json()["hierarchy_new"]["gate_type"] == ['OR', 'OR', 'OR', 'AND', 'AND', 'NA', 'NA', 'NA', 'NA']
        assert response.json()["hierarchy_new"]["failure_rate_repair"] == ['NA', 'NA', 'NA', 'NA', 'NA', 0.00023999999999999998, 0.0004, 0.03, 0.00096]
        assert response.json()["hierarchy_new"]["failure_rate_replacement"] == ['NA', 'NA', 'NA', 'NA', 'NA', 0.00023999999999999998, 0.0004, 0.03, 0.00096]
        assert response.json()["hierarchy_new"]["system"] == ['ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED', 'ED']
        assert response.json()["hierarchy_new"]["design_id"] == ['NA', 'NA', 'NA', 'NA', 'NA', 'AC1', 'AC2', 'CP1', 'EC1']
        assert response.json()["hierarchy_new"]["name_of_node"] == ['ED Subsystem', 'ED1', 'ED2', 'Route1_1', 'Route2_1', 'AC1', 'AC2', 'CP1', 'EC1']
        assert response.json()["hierarchy_new"]["node_subtype"] == ['NA', 'NA', 'NA', 'NA', 'NA', 'array', 'array', 'substation', 'export']
        assert response.json()["hierarchy_new"]["node_type"] == ['System', 'System', 'System', 'Energy route', 'Energy route', 'Component', 'Component', 'Component', 'Component']