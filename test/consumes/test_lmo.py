import atexit
import pytest
from pact import Consumer, Like, Provider, Term, EachLike
from dtop_rams.service import create_app
import requests


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


data_downtime = {
    "project_life": Like(10),
    "n_devices": Like(2),
    "downtime": EachLike(
        {
            "device_id": Like("device11"),
            "downtime_table": Like(
                {
                    "year": EachLike(1),
                    "jan": EachLike(1.5),
                    "feb": EachLike(1.5),
                    "mar": EachLike(1.5),
                    "apr": EachLike(1.5),
                    "may": EachLike(1.5),
                    "jun": EachLike(1.5),
                    "jul": EachLike(1.5),
                    "aug": EachLike(1.5),
                    "sep": EachLike(1.5),
                    "oct": EachLike(1.5),
                    "nov": EachLike(1.5),
                    "dec": EachLike(1.5),
                }
            ),
        }
    ),
}

data_maintenance = {
    "operation_id": EachLike("op01_00"),
    "name": EachLike("foundation installation"),
    "tech_group": EachLike("station keeping"),
    "technologies": EachLike("ed_0,"),
    "operation_type": EachLike("installation"),
    "date_start": EachLike("2022/05/01 8"),
    "date_end": EachLike("2022/05/08 20"),
    "duration_total": EachLike(180.2338),
    "waiting_to_start": EachLike(0.0),
    # "duration_at_port": EachLike(52.0),
    # "duration_at_sea": EachLike(18.1169),
    # "waiting_port": EachLike(0.0),
    # "waiting_sea": EachLike(0.0),
    # "duration_transit": EachLike(14.116900000000001),
    # "duration_mobilization": EachLike(96.0),
    # "activity_sequence": EachLike("OP01_A0, OP01_A1, T_A0, T_A3, T_A8, OP01_A2, OP01_A13, OP01_A14, OP01_A16, OP01_A17,"),
    "vessel_consumption": EachLike(620.043923436),
    # "vessel_equip_combination": EachLike("vec_033"),
    "vessels": EachLike("v11_1"),
    "equipment": EachLike("rov_001"),
    "terminal": EachLike("t008"),
    "cost": EachLike(2499219.990872388),
    "cost_vessel": EachLike(2426466.06056954),
    "cost_terminal": EachLike(12433.9303028477),
    "downtime": EachLike(52),
    "fail_date": EachLike("dd-mm-yy"),
    "mttr": EachLike(35),
    "replacement_parts": EachLike("bom"),
    "replacement_costs": EachLike(0.0),
    "cost_label": EachLike("CAPEX")
}


pact = Consumer("rams").has_pact_with(Provider("lmo"), port=1234)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_lmo_downtime(client):
    pact.given("lmo 1 exists and it has downtime").upon_receiving(
        "a request for lmo"
    ).with_request(
        "GET",
        Term(
            r"/api/\d+/phases/maintenance/downtime",
            "/api/1/phases/maintenance/downtime",
        ),
    ).will_respond_with(
        200, body=Like(data_downtime)
    )

    with pact:
        response = requests.get(f"{pact.uri}/api/1/phases/maintenance/downtime")
        assert response.json()["project_life"] == 10
        assert response.json()["downtime"][0]["downtime_table"]["year"] == [1]
        assert response.json()["downtime"][0]["downtime_table"]["jan"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["feb"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["mar"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["apr"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["may"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["jun"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["jul"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["aug"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["sep"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["oct"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["nov"] == [1.5]
        assert response.json()["downtime"][0]["downtime_table"]["dec"] == [1.5]


def test_lmo_maintenance(client):
    pact.given("lmo 1 exists and it has maintenance plan").upon_receiving(
        "a request for lmo"
    ).with_request(
        "GET",
        Term(
            r"/api/\d+/phases/maintenance/plan",
            "/api/1/phases/maintenance/plan",
        ),
    ).will_respond_with(
        200, body=Like(data_maintenance)
    )

    with pact:
        response = requests.get(f"{pact.uri}/api/1/phases/maintenance/plan")
        assert response.json()["technologies"] == ["ed_0,"]
        assert response.json()["downtime"] == [52]
        assert response.json()["mttr"] == [35]