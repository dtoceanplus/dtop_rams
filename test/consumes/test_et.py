import atexit
import pytest
from pact import Consumer, Like, Provider, Term, EachLike
import pytest
import numpy as np
import os
import requests
from dtop_rams import service

@pytest.fixture
def app():
    settings_override = {
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": "/////db/rams.db", 
    }
    app = service.create_app(settings_override)
    yield app
@pytest.fixture
def client(app):
    client = app.test_client()
    yield client
pact = Consumer("rams").has_pact_with(Provider("et"), port=1232)
pact.start_service()
atexit.register(pact.stop_service)


def test_et_hierarchy(client):
    pact.given("et 1 exists and has hierarchy").upon_receiving(
        "a request for hierarchy"
    ).with_request(
        "GET", Term(r"/energy_transf/\d+/array", "/energy_transf/1/array")
    ).will_respond_with(
        200,
        body=Like(
            {
              "Hierarchy": {
                "description": "Hierarchy of the Energy Transformation subsystem ", 
                "label": "Hierarchy", 
                "unit": "-", 
                "value": {
                           "category": ["Level 3", "Level 2", "Level 1", "Level 0", "Level 0", "Level 0", "Level 2", "Level 1", "Level 0", "Level 0", "Level 0"], 
                           "child": [["ET0", "ET1"], ["ET0_PTO_0_0"], ["ET0_PTO_0_0_MechT", "ET0_PTO_0_0_ElectT", "ET0_PTO_0_0_GridC"], "NA", "NA", "NA", ["ET1_PTO_0_0"], ["ET1_PTO_0_0_MechT", "ET1_PTO_0_0_ElectT", "ET1_PTO_0_0_GridC"], "NA", "NA", "NA"], 
                           "design_id": ["Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01"], 
                           "failure_rate_repair": ["NA", "NA", "NA", 1.443, 0.002, 10.508, "NA", "NA", 1.334, 0.002, 9.774], 
                           "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
                           "gate_type": ["AND", "1/1", "AND", "AND", "AND", "AND", "1/1", "AND", "AND", "AND", "AND"], 
                           "name_of_node": ["Array_01", "ET0", "ET0_PTO_0_0", "ET0_PTO_0_0_MechT", "ET0_PTO_0_0_ElectT", "ET0_PTO_0_0_GridC", "ET1", "ET1_PTO_0_0", "ET1_PTO_0_0_MechT", "ET1_PTO_0_0_ElectT", "ET1_PTO_0_0_GridC"], 
                           "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
                           "node_type": ["System", "Device", "PTO", "Component", "Component", "Component", "Device", "PTO", "Component", "Component", "Component"], 
                           "parent": ["NA", "Array_01", "ET0", "ET0_PTO_0_0", "ET0_PTO_0_0", "ET0_PTO_0_0", "Array_01", "ET1", "ET1_PTO_0_0", "ET1_PTO_0_0", "ET1_PTO_0_0"], 
                           "system": ["ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET"]
                        }
                }
            }
        ),
    )
    with pact:
        response = requests.get(f"{pact.uri}/energy_transf/1/array")
        assert response.json()["Hierarchy"]["value"]["category"] == ["Level 3", "Level 2", "Level 1", "Level 0", "Level 0", "Level 0", "Level 2", "Level 1", "Level 0", "Level 0", "Level 0"]
        assert response.json()["Hierarchy"]["value"]["parent"] == ["NA", "Array_01", "ET0", "ET0_PTO_0_0", "ET0_PTO_0_0", "ET0_PTO_0_0", "Array_01", "ET1", "ET1_PTO_0_0", "ET1_PTO_0_0", "ET1_PTO_0_0"]
        assert response.json()["Hierarchy"]["value"]["child"] == [["ET0", "ET1"], ["ET0_PTO_0_0"], ["ET0_PTO_0_0_MechT", "ET0_PTO_0_0_ElectT", "ET0_PTO_0_0_GridC"], "NA", "NA", "NA", ["ET1_PTO_0_0"], ["ET1_PTO_0_0_MechT", "ET1_PTO_0_0_ElectT", "ET1_PTO_0_0_GridC"], "NA", "NA", "NA"]
        assert response.json()["Hierarchy"]["value"]["gate_type"] == ["AND", "1/1", "AND", "AND", "AND", "AND", "1/1", "AND", "AND", "AND", "AND"]
        assert response.json()["Hierarchy"]["value"]["failure_rate_repair"] == ["NA", "NA", "NA", 1.443, 0.002, 10.508, "NA", "NA", 1.334, 0.002, 9.774]
        assert response.json()["Hierarchy"]["value"]["failure_rate_replacement"] == ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
        assert response.json()["Hierarchy"]["value"]["system"] == ["ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET"]
        assert response.json()["Hierarchy"]["value"]["design_id"] == ["Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01", "Array_01"]
        assert response.json()["Hierarchy"]["value"]["name_of_node"] == ["Array_01", "ET0", "ET0_PTO_0_0", "ET0_PTO_0_0_MechT", "ET0_PTO_0_0_ElectT", "ET0_PTO_0_0_GridC", "ET1", "ET1_PTO_0_0", "ET1_PTO_0_0_MechT", "ET1_PTO_0_0_ElectT", "ET1_PTO_0_0_GridC"]
        assert response.json()["Hierarchy"]["value"]["node_subtype"] == ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
        assert response.json()["Hierarchy"]["value"]["node_type"] == ["System", "Device", "PTO", "Component", "Component", "Component", "Device", "PTO", "Component", "Component", "Component"]



def test_et_stress(client):
    pact.given("et 1 exists and has stress").upon_receiving(
        "a request for stress"
    ).with_request(
        "GET", Term(r"/energy_transf/\d+/devices/\d+/ptos", "/energy_transf/1/devices/2/ptos")
    ).will_respond_with(
        200,
body=EachLike(
            {
                "S_N": Like({
                    "description": "SN curve",
                    "label": "SN curve energy",
                    "unit": "-",
                    "value": EachLike(EachLike(10.97))
                }),
                "fatigue_stress_probability": Like({
                    "description": "Fatigue Stress Probability",
                    "label": "Fatigue Stress Probability",
                    "unit": "-",
                    "value": {
                        "probability": EachLike(1.5206049678508205e-08),
                        "stress": EachLike(1.6468789096280935e-05)
                    }
                }),
                "maximum_stress_probability": Like({
                    "description": "Stress Probability",
                    "label": "Maximum stress Probability",
                    "unit": "-",
                    "value": {
                        "probability": EachLike(1.2132672269276997e-06),
                        "stress": EachLike(1.6468789096280935e-05)
                    }
                }),
                "number_cycles": Like({
                    "description": "Fatigue Stress Probability",
                    "label": "NUme",
                    "unit": "-",
                    "value": EachLike(4.1399967481973125e+3)
                }),
                "ultimate_stress": Like({
                    "description": "Ultimate Stress",
                    "label": "Ultimate stress",
                    "unit": "-",
                    "value": 503.9925969532528
                })
            }
        ),
    )
    with pact:
        response = requests.get(f"{pact.uri}/energy_transf/1/devices/2/ptos")
        assert response.status_code == 200