import atexit
import pytest
from pact import Consumer, Like, Provider, Term, EachLike
import pytest
import numpy as np
import os
import requests
from dtop_rams import service


@pytest.fixture
def app():
    settings_override = {
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": "/////db/rams.db", 
    }
    app = service.create_app(settings_override)
    yield app
@pytest.fixture
def client(app):
    client = app.test_client()
    yield client
pact = Consumer("rams").has_pact_with(Provider("sk"), port=1233)
pact.start_service()
atexit.register(pact.stop_service)


def test_sk_hierarchy(client):
    pact.given("sk 1 exists and has hierarchy").upon_receiving(
        "a request for hierarchy"
    ).with_request(
        "GET", Term(r"/sk/\d+/hierarchy", "/sk/1/hierarchy")
    ).will_respond_with(
        200,
        body=Like(
            {
                "system": ["SK","SK","SK"],
                "name_of_node": ["SK0_x","SK0_x_pile_foundation1","SK0"],
                "design_id": ["NA","SK0_x_pile_foundation1","NA"],
                "node_type": ["System","Component","System"],
                "node_subtype": ["stationkeeping","foundation","stationkeeping"],
                "category": ["Level 1","Level 0","Level 2"],
                "parent": ["NA","SK0_x","NA"],
                "child": [["SK0_x_pile_foundation1"],["NA"],["SK0_x"]],
                "gate_type": ["AND","NA","AND"],
                "failure_rate_repair": ["NA",0.001876,"NA"],
                "failure_rate_replacement": ["NA",0.001876,"NA"]
            }
        ),
    )
    with pact:
        response = requests.get(f"{pact.uri}/sk/1/hierarchy")
        assert response.json()["category"] == ["Level 1","Level 0","Level 2"]
        assert response.json()["parent"] == ["NA","SK0_x","NA"]
        assert response.json()["child"] == [["SK0_x_pile_foundation1"],["NA"],["SK0_x"]]
        assert response.json()["gate_type"] == ["AND","NA","AND"]
        assert response.json()["failure_rate_repair"] == ["NA",0.001876,"NA"]
        assert response.json()["failure_rate_replacement"] == ["NA",0.001876,"NA"]
        assert response.json()["system"] == ["SK", "SK", "SK"]
        assert response.json()["design_id"] == ["NA","SK0_x_pile_foundation1","NA"]
        assert response.json()["name_of_node"] == ["SK0_x","SK0_x_pile_foundation1","SK0"]
        assert response.json()["node_subtype"] == ["stationkeeping","foundation","stationkeeping"]
        assert response.json()["node_type"] == ["System","Component","System"]


def test_sk_stress(client):
    pact.given("sk 1 exists and has stress").upon_receiving(
        "a request for stress"
    ).with_request(
        "GET", Term(r"/sk/\d+/design_assessment", "/sk/1/design_assessment")
    ).will_respond_with(
        200,
        body=Like(
            {
                "devices": [
                    {
                        "uls_results": {
                            "mooring_tension": [[  [ [37435.35787709276,64419.65516736676]  ] ] ],
		            	    "tension_versus_mbl": [
                                [  [0.01751654091243178,0.017516540783813574,0.020824072813175522,0.02082407244702446] ]
                            ],
                            "mbl_uls": [9650000.0]        
		              },
		            "fls_results":{
		                "cdf_stress_range": [  [0.03530433733043322,0.004113311227466497,0.0357526183671677,0.049570761686637314] ],
                        "cdf": [ [0.07928831669580277,0.03422811116375578,0.09096998709297957,0.09096998709297957] ],
                        "ad": [60000000000.0],
                        "m": [3.0],
                        "n_cycles_lifetime": [95559948.11638647]
		              }
                    }
                    ],
                "master_structure": {
                    "uls_results": {
                        "mooring_tension": [[[  [0.0,245013.04723266925  ], ]  ]  ],
		                "mbl_uls": [9650000.0]
		            }    
                }
            }
        ),
    )
    with pact:
        response = requests.get(f"{pact.uri}/sk/1/design_assessment")
        assert response.json() == {
                "devices": [
                    {
                        "uls_results": {
                            "mooring_tension": [[  [ [37435.35787709276,64419.65516736676]  ] ] ],
		            	    "tension_versus_mbl": [
                                [  [0.01751654091243178,0.017516540783813574,0.020824072813175522,0.02082407244702446] ]
                            ],
                            "mbl_uls": [9650000.0]        
		              },
		            "fls_results":{
		                "cdf_stress_range": [  [0.03530433733043322,0.004113311227466497,0.0357526183671677,0.049570761686637314] ],
                        "cdf": [ [0.07928831669580277,0.03422811116375578,0.09096998709297957,0.09096998709297957] ],
                        "ad": [60000000000.0],
                        "m": [3.0],
                        "n_cycles_lifetime": [95559948.11638647]
		              }
                    }
                    ],
                "master_structure": {
                    "uls_results": {
                        "mooring_tension": [[[  [0.0,245013.04723266925  ], ]  ]  ],
		                "mbl_uls": [9650000.0]
		            }    
                }
            }