import json
import pytest
from dtop_rams import service


"""
   to test the services provided by RAMS
"""

# IMPORTANT -
# The keys in hierarchy_ed.json and hierarchy_sk.json are reordered according to
# the alphabetic sequence, after running client.post(...).
# So, an addtional letter is added to each key name to force Python to put them in
# the same order as defined in
hierarchy_sk = {
            "asystem": ["SK","SK","SK","SK","SK","SK","SK","SK","SK","SK","SK"],
            "bname_of_node": ["SK0_x","SK0_x_ml_0_seg_0","SK0_x_ml_0_anchor_n_2_0","SK0_x_ml_0","SK0_x_ml_1_seg_0","SK0_x_ml_1_anchor_n_2_0","SK0_x_ml_1","SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0","SK0_x_ml_2","SK0"],
            "cdesign_id": ["NA","SK0_x_ml_0_seg_0","SK0_x_ml_0_anchor_n_2_0","NA","SK0_x_ml_1_seg_0","SK0_x_ml_1_anchor_n_2_0","NA","SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0","NA","NA"],
            "dnode_type": ["System","Component","Component","System","Component","Component","System","Component","Component","System","System"],
            "enode_subtype": ["stationkeeping","line_segment","anchor","mooring_line","line_segment","anchor","mooring_line","line_segment","anchor","mooring_line","stationkeeping"],
            "fcategory": ["Level 2","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 3"],
            "gparent": ["NA","SK0_x_ml_0","SK0_x_ml_0","SK0_x","SK0_x_ml_1","SK0_x_ml_1","SK0_x","SK0_x_ml_2","SK0_x_ml_2","SK0_x","NA"],
            "hchild": [["SK0_x_ml_0", "SK0_x_ml_1", "SK0_x_ml_2"],["NA"],["NA"],[    "SK0_x_ml_0_seg_0", "SK0_x_ml_0_anchor_n_2_0"],["NA"],["NA"],["SK0_x_ml_1_seg_0",  "SK0_x_ml_1_anchor_n_2_0"],["NA"],["NA"],["SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0"],["SK0_x"]],
            "igate_type": ["AND","NA","NA","AND","NA","NA","AND","NA","NA","AND","AND"],
            "jfailure_rate_repair": ["NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA"],
            "kfailure_rate_replacement": ["NA","0.00722","0.000278","NA","0.00722","0.000278","NA","0.00722","0.000278","NA","NA"]
        }


body_sk_hierarchy = {
            "system": ["SK","SK","SK","SK","SK","SK","SK","SK","SK","SK","SK"],
            "name_of_node": ["SK0_x","SK0_x_ml_0_seg_0","SK0_x_ml_0_anchor_n_2_0","SK0_x_ml_0","SK0_x_ml_1_seg_0","SK0_x_ml_1_anchor_n_2_0","SK0_x_ml_1","SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0","SK0_x_ml_2","SK0"],
            "design_id": ["NA","SK0_x_ml_0_seg_0","SK0_x_ml_0_anchor_n_2_0","NA","SK0_x_ml_1_seg_0","SK0_x_ml_1_anchor_n_2_0","NA","SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0","NA","NA"],
            "node_type": ["System","Component","Component","System","Component","Component","System","Component","Component","System","System"],
            "node_subtype": ["stationkeeping","line_segment","anchor","mooring_line","line_segment","anchor","mooring_line","line_segment","anchor","mooring_line","stationkeeping"],
            "category": ["Level 2","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 3"],
            "parent": ["NA","SK0_x_ml_0","SK0_x_ml_0","SK0_x","SK0_x_ml_1","SK0_x_ml_1","SK0_x","SK0_x_ml_2","SK0_x_ml_2","SK0_x","NA"],
            "child": [["SK0_x_ml_0", "SK0_x_ml_1", "SK0_x_ml_2"],["NA"],["NA"],[    "SK0_x_ml_0_seg_0", "SK0_x_ml_0_anchor_n_2_0"],["NA"],["NA"],["SK0_x_ml_1_seg_0",  "SK0_x_ml_1_anchor_n_2_0"],["NA"],["NA"],["SK0_x_ml_2_seg_0","SK0_x_ml_2_anchor_n_2_0"],["SK0_x"]],
            "gate_type": ["AND","NA","NA","AND","NA","NA","AND","NA","NA","AND","AND"],
            "failure_rate_repair": ["NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA"],
            "failure_rate_replacement": ["NA","0.00722","0.000278","NA","0.00722","0.000278","NA","0.00722","0.000278","NA","NA"]
}


body_sk_hierarchy_url = {
            "system": ["SK"],
            "name_of_node": ["SK0_x"],
            "design_id": ["NA"],
            "node_type": ["System"],
            "node_subtype": ["stationkeeping"],
            "category": ["Level 2"],
            "parent": ["NA"],
            "child": [["SK0_x_ml_0", "SK0_x_ml_1", "SK0_x_ml_2"]],
            "gate_type": ["AND"],
            "failure_rate_repair": ["NA"],
            "failure_rate_replacement": ["NA"],
            "url_sk_hierarchy": "127.0.0.1:5000/api/sk/1/hierarchy"
        }


hierarchy_et={
    "asystem": ["ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET", "ET"], 
    "bname_of_node": ["ET Subsystem", "ET1", "ET2", "Route1_1", "Route2_1", '11', '6', '2', '9', '4', '0', '1', '12', '10', '8', '7', '5', '3'], 
    "cdesign_id": ["NA", "NA", "NA", "NA", "NA", '11', '6', '2', '9', '4', '0', '1', '12', '10', '8', '7', '5', '3'], 
    "dnode_type": ["System", "System", "System", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], 
    "enode_subtype": ["System", "NA", "NA", "NA", "NA", " umbilical", " umbilical", " passive hub", " array", " array", " export", " dry-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], 
    "fcategory": ["Level 3", "Level 2", "Level 2", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], 
    "gparent": ["NA", "NA", "NA", ["ED1"], ["ED2"], ["Route1_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1"], ["Route1_1"], ["Route1_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"]], 
    "hchild": [["ED1", "ED2"], ["Route1_1"], ["Route2_1"], ['12', '11', '10', '9', '8', '7', '6', '5', '4', '3', '2', '1', '0'], ['7', '6', '5', '4', '3', '2', '1', '0'], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
    "igate_type": ["OR", "OR", "OR", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
    "jfailure_rate_repair": ["NA", "NA", "NA", "NA", "NA", 0.4576141227726697, 0.4576141227726697, 2.457042, 2.052, 0.171, 17.271, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], "kfailure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", 0.4576141227726697, 0.4576141227726697, 2.457042, 2.052, 0.171, 17.271, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
    }


body_et_hierarchy ={"Hierarchy":{
    "value": {
    "system": ["ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET"],
    "name_of_node": ["Array_01","DEV_0","DEV_0_PTO_0_0","DEV_0_PTO_0_0_MechT","DEV_0_PTO_0_0_ElectT","DEV_0_PTO_0_0_GridC","DEV_0_PTO_1_0","DEV_0_PTO_1_0_MechT","DEV_0_PTO_1_0_ElectT","DEV_0_PTO_1_0_GridC","DEV_0_PTO_2_0","DEV_0_PTO_2_0_MechT","DEV_0_PTO_2_0_ElectT","DEV_0_PTO_2_0_GridC","DEV_1","DEV_1_PTO_0_0","DEV_1_PTO_0_0_MechT","DEV_1_PTO_0_0_ElectT","DEV_1_PTO_0_0_GridC","DEV_1_PTO_1_0","DEV_1_PTO_1_0_MechT","DEV_1_PTO_1_0_ElectT","DEV_1_PTO_1_0_GridC","DEV_1_PTO_2_0","DEV_1_PTO_2_0_MechT","DEV_1_PTO_2_0_ElectT","DEV_1_PTO_2_0_GridC"],
    "design_id": ["Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01","Array_01"],
    "node_type": ["System","Device","PTO","Component","Component","Component","PTO","Component","Component","Component","PTO","Component","Component","Component","Device","PTO","Component","Component","Component","PTO","Component","Component","Component","PTO","Component","Component","Component"],
    "node_subtype": ["NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA"],
    "category": ["Level 3","Level 2","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 2","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0"],
    "parent": ["NA","Array_01","DEV_0","DEV_0_PTO_0_0","DEV_0_PTO_0_0","DEV_0_PTO_0_0","DEV_0","DEV_0_PTO_1_0","DEV_0_PTO_1_0","DEV_0_PTO_1_0","DEV_0","DEV_0_PTO_2_0","DEV_0_PTO_2_0","DEV_0_PTO_2_0","Array_01","DEV_1","DEV_1_PTO_0_0","DEV_1_PTO_0_0","DEV_1_PTO_0_0","DEV_1","DEV_1_PTO_1_0","DEV_1_PTO_1_0","DEV_1_PTO_1_0","DEV_1","DEV_1_PTO_2_0","DEV_1_PTO_2_0","DEV_1_PTO_2_0"],
    "child": [ ["DEV_0","DEV_1"], ["DEV_0_PTO_0_0","DEV_0_PTO_1_0","DEV_0_PTO_2_0"], ["DEV_0_PTO_0_0_MechT","DEV_0_PTO_0_0_ElectT","DEV_0_PTO_0_0_GridC"],"NA","NA","NA", ["DEV_0_PTO_1_0_MechT","DEV_0_PTO_1_0_ElectT","DEV_0_PTO_1_0_GridC"],"NA","NA","NA", ["DEV_0_PTO_2_0_MechT","DEV_0_PTO_2_0_ElectT","DEV_0_PTO_2_0_GridC"],"NA","NA","NA", ["DEV_1_PTO_0_0","DEV_1_PTO_1_0","DEV_1_PTO_2_0"], ["DEV_1_PTO_0_0_MechT","DEV_1_PTO_0_0_ElectT","DEV_1_PTO_0_0_GridC"],"NA","NA","NA", ["DEV_1_PTO_1_0_MechT","DEV_1_PTO_1_0_ElectT","DEV_1_PTO_1_0_GridC"],"NA","NA","NA", ["DEV_1_PTO_2_0_MechT","DEV_1_PTO_2_0_ElectT","DEV_1_PTO_2_0_GridC"],"NA","NA","NA" ],
    "gate_type": ["AND","1/3","AND","AND","AND","AND","AND","AND","AND","AND","AND","AND","AND","AND","1/3","AND","AND","AND","AND","AND","AND","AND","AND","AND","AND","AND","AND"],
    "failure_rate_repair": ["NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA"],
    "failure_rate_replacement": ["NA","NA","NA",2.517894594637399e-4,1.2815349288344602e-3,5.994451644910028e-4,"NA",2.517894594637399e-4,1.2815349288344602e-3,5.994451644910028e-4,"NA",2.517894594637399e-4,1.2815349288344602e-3,5.994451644910028e-4,"NA","NA",2.517894594637399e-4,1.2815349288344602e-3,5.994451644910028e-4,"NA",2.517894594637399e-4,1.2815349288344602e-3,5.994451644910028e-4,"NA",2.517894594637399e-4,1.2815349288344602e-3,5.994451644910028e-4]
}
    }
}


body_et_hierarchy_url = {
    "system": ["SK"],
    "name_of_node": ["SK0_x"],
    "design_id": ["NA"],
    "node_type": ["System"],
    "node_subtype": ["ED"],
    "category": ["Level 2"],
    "parent": ["NA"],
    "child": [["ED"]],
    "gate_type": ["AND"],
    "failure_rate_repair": ["NA"],
    "failure_rate_replacement": ["NA"],
    "url_et_hierarcy": "127.0.0.1:5000/api/et/1/hierarchy"    
}


hierarchy_ed={
    "asystem": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], 
    "bname_of_node": ["ED Subsystem", "ED1", "ED2", "Route1_1", "Route2_1", '11', '6', '2', '9', '4', '0', '1', '12', '10', '8', '7', '5', '3'], 
    "cdesign_id": ["NA", "NA", "NA", "NA", "NA", '11', '6', '2', '9', '4', '0', '1', '12', '10', '8', '7', '5', '3'], 
    "dnode_type": ["System", "System", "System", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], 
    "enode_subtype": ["System", "NA", "NA", "NA", "NA", " umbilical", " umbilical", " passive hub", " array", " array", " export", " dry-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], 
    "fcategory": ["Level 3", "Level 2", "Level 2", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], 
    "gparent": ["NA", "NA", "NA", ["ED1"], ["ED2"], ["Route1_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1"], ["Route1_1"], ["Route1_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"], ["Route1_1", "Route2_1"]], 
    "hchild": [["ED1", "ED2"], ["Route1_1"], ["Route2_1"], ['12', '11', '10', '9', '8', '7', '6', '5', '4', '3', '2', '1', '0'], ['7', '6', '5', '4', '3', '2', '1', '0'], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
    "igate_type": ["OR", "OR", "OR", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
    "jfailure_rate_repair": ["NA", "NA", "NA", "NA", "NA", 0.4576141227726697, 0.4576141227726697, 2.457042, 2.052, 0.171, 17.271, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], 
	"kfailure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", 0.4576141227726697, 0.4576141227726697, 2.457042, 2.052, 0.171, 17.271, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
    }


body_ed = {
    "hierarchy_new":{
        "system": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], 
        "name_of_node": ["ED Subsystem", "ED1", "ED2", "ED3", "ED4", "ED5", "Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1", "26", "21", "16", "11", "6", "2", "24", "19", "14", "9", "4", "0", "1", "27", "25", "23", "22", "20", "18", "17", "15", "13", "12", "10", "8", "7", "5", "3"], 
        "design_id": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "26", "21", "16", "11", "6", "2", "24", "19", "14", "9", "4", "0", "1", "27", "25", "23", "22", "20", "18", "17", "15", "13", "12", "10", "8", "7", "5", "3"], 
        "node_type": ["System", "System", "System", "System", "System", "System", "Energy route", "Energy route", "Energy route", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], 
        "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", " umbilical", " umbilical", " umbilical", " umbilical", " umbilical", " substation", " array", " array", " array", " array", " array", " export", " dry-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], 
        "category": ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], 
        "parent": ["NA", "NA", "NA", "NA", "NA", "NA", ["ED1"], ["ED2"], ["ED3"], ["ED4"], ["ED5"], ["Route5_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route5_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route5_1"], ["Route5_1"], ["Route5_1"], ["Route3_1"], ["Route3_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route4_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route4_1"]], 
        "child": [["ED1", "ED2", "ED3", "ED4", "ED5"], ["Route1_1"], ["Route2_1"], ["Route3_1"], ["Route4_1"], ["Route5_1"], ["7", "6", "5", "4", "3", "2", "1", "0"], ["17", "16", "15", "14", "13", "2", "1", "0"], ["22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "2", "1", "0"], ["12", "11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1", "0"], ["27", "26", "25", "24", "23", "2", "1", "0"], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
        "gate_type": ["OR", "OR", "OR", "OR", "OR", "OR", "AND", "AND", "AND", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
        "failure_rate_repair": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.4284022243429648, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 2.457042, 8.379, 6.368491557497398, 4.5117967491553905, 6.563796749155391, 2.064152595828997, 18.215604495807106, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], 
        "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.4284022243429648, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 2.457042, 8.379, 6.368491557497398, 4.5117967491553905, 6.563796749155391, 2.064152595828997, 18.215604495807106, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
    }
}


body_ed_url = {
    "url_ed": "127.0.0.1:5000/api/energy-deliv-studies/ed/1/results"
}


downtime= {
  "project_life": 20,
  "n_devices": 1,
  "downtime": [
    {
      "device_id": "device_1",
      "downtime_table": {
        "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
        "jan": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "feb": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,176.409],
        "mar": [0.0,0.0,0.0,0.0,95.7045,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,197.409,0.0,0.0,0.0,0.0,0.0,135.409,22.182],
        "apr": [0.0,0.0,0.0,0.0,3.591,62.295500000000004,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.182,0.0,0.0,0.0,0.0,0.0,63.182,0.0],
        "may": [0.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0],
        "jun": [0.0,0.0,0.0,24.7045,0.0,0.0,0.0,0.0,0.0,124.59100000000001,0.0,0.0,124.59100000000001,0.0,0.0,0.0,0.0,0.0,124.59100000000001,0.0,0.0],
        "jul": [0.0,0.0,0.0,37.591,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,105.40899999999999,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "aug": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,19.182,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "sep": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "oct": [0.0,0.0,83.29549999999999,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,0.0,0.0,0.0,0.0,184.59099999999998,0.0],
        "nov": [0.0,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,39.409,0.0,56.409,166.59099999999998,0.0,0.0],
        "dec": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,127.182,0.0,110.182,186.59099999999998,0.0,0.0]
      }
    }
  ]
}


downtime_input = {
  "project_life": 20,
  "n_devices": 1,
  "downtime": [
    {
      "device_id": "device_1",
      "downtime_table": {
        "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
        "jan": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "feb": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,176.409],
        "mar": [0.0,0.0,0.0,0.0,95.7045,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,197.409,0.0,0.0,0.0,0.0,0.0,135.409,22.182],
        "apr": [0.0,0.0,0.0,0.0,3.591,62.295500000000004,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.182,0.0,0.0,0.0,0.0,0.0,63.182,0.0],
        "may": [0.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0,24.0,0.0,0.0],
        "jun": [0.0,0.0,0.0,24.7045,0.0,0.0,0.0,0.0,0.0,124.59100000000001,0.0,0.0,124.59100000000001,0.0,0.0,0.0,0.0,0.0,124.59100000000001,0.0,0.0],
        "jul": [0.0,0.0,0.0,37.591,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,105.40899999999999,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "aug": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,19.182,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "sep": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
        "oct": [0.0,0.0,83.29549999999999,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,0.0,0.0,0.0,0.0,184.59099999999998,0.0],
        "nov": [0.0,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,0.0,0.0,166.59099999999998,0.0,0.0,0.0,39.409,0.0,56.409,166.59099999999998,0.0,0.0],
        "dec": [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,127.182,0.0,110.182,186.59099999999998,0.0,0.0]
      }
    }
  ]
}


maintenance = {
        "operation_id": ["op13_1", "op12_1", "op13_2", "op12_2"],
        "name": [
          "repair/replacement",
          "inspection",
          "repair/replacement",
          "inspection"
        ],
        "tech_group": [
          "electrical",
          "station keeping",
          "station keeping",
          "electrical"
        ],
        "operation_type": [
          "planned maintenance",
          "planned maintenance",
          "unplanned maintenance",
          "planned maintenance"
        ],
        "technologies": [
          "[ac1]",
          "[ml11,ml12,ml13,ml14]",
          "[ml21]",
          "[ac1,ac2,ec1]"
        ],
        "date_start": [
          "01/06/2021 8",
          "07/06/2021 8",
          "23/11/2021 4",
          "04/06/2021 8"
        ],
        "date_end": [
          "06/06/2021 14",
          "09/06/2021 16",
          "25/11/2021 1",
          "01/06/2021 8"
        ],
        "duration_total": [126, 56, 45, 27],
        "duration_at_sea": [38, 10, 26, 10],
        "duration_at_port": [54, 5, 5, 5],
        "duration_waiting_port": [20, 10, 8, 7],
        "duration_waiting_sea": [10, 5, 2, 3],
        "duration_transit_site": [2, 1, 2, 1],
        "duration_transit_port": [2, 1, 2, 1],
        "duration_mobilization": [0, 24, 0, 0],
        "activity_sequence": [
          "[op2_a1,op2_a2…]",
          "[op3_a1,op3_a2…]",
          "[op9_a1,op9_a2…]",
          "[op9_a1,op9_a2…]"
        ],
        "vessel_consumption": [40000, 25000, 180000, 8000],
        "vessel_equip_combination": [
          "vec_021",
          "vec_032",
          "vec_033",
          "vec_023"
        ],
        "cost_p25": [10000000, 1000000, 500000, 500000],
        "cost_p50": [10500000, 1020000, 700000, 700000],
        "cost_p75": [12000000, 1320000, 1070000, 1070000],
        "terminal_id": ["t103", "t103", "t103", "t103"],
        "downtime": ["", "", 224, ""],
        "fail_date": ["", "", "15/11/2021 15", ""],
        "mttr": ["", "", 43, ""],
        "replacement_parts": ["", "", "D11_ML1", ""],
        "replacement_cost": ["", "", 9000, ""],
        "cost_label": ["opex", "opex", "opex", "opex"]
}

stress_sk = {
        "devices": [
        {
            "uls_results": {
                "mooring_tension": [
                    [
                        [
                            [
                                37435.35787709276,
                                64419.65516736676
                            ],
                            [
                                37435.35892477166,
                                64419.65469435439
                            ],
                            [
                                44570.00656000909,
                                76583.59013410198
                            ],
                            [
                                44570.00369245041,
                                76583.58878752761
                            ]
                        ],
                        [
                            [
                                36827.85783060535,
                                65065.50082856364
                            ],
                            [
                                36827.85889613669,
                                65065.50038926155
                            ],
                            [
                                46199.86850567229,
                                79312.71249213733
                            ],
                            [
                                46199.864852886836,
                                79312.71034463706
                            ]
                        ],
                        [
                            [
                                36170.36857294558,
                                65783.65696742413
                            ],
                            [
                                36170.36965784724,
                                65783.65656496542
                            ],
                            [
                                48959.48626971105,
                                83313.39282665773
                            ],
                            [
                                48959.48132696917,
                                83313.3893720024
                            ]
                        ]
                    ]
                ],
			    "tension_versus_mbl": [
                    [
                        [
                            0.01751654091243178,
                            0.017516540783813574,
                            0.020824072813175522,
                            0.02082407244702446
                        ],
                        [
                            0.017692154735853838,
                            0.017692154616401893,
                            0.021566156627740477,
                            0.021566156043807263
                        ],
                        [
                            0.017887430717309825,
                            0.01788743060787607,
                            0.022653993570908214,
                            0.022653992631542563
                        ]
                    ]
                ],
                "mbl_uls": [
                    9650000.0,
                    9650000.0,
                    9650000.0
                ]        
		  },
		"fls_results":{
            "env_proba": [0],
		    "cdf_stress_range": [
                    [
                        0.03530433733043322,
                        0.004113311227466497,
                        0.0357526183671677,
                        0.049570761686637314
                    ],
                    [
                        0.04816981615077811,
                        0.016192996694111464,
                        0.039482057998408294,
                        0.055180169984814766
                    ],
                    [
                        0.05026068881173524,
                        0.02412937305243806,
                        0.049151145581692555,
                        0.06069185676171353
                    ],
                    [
                        0.0850192515016839,
                        0.038166044361503934,
                        0.10522211138375195,
                        0.15621823301378723
                    ],
                    [
                        0.10718831438830025,
                        0.04814171982039578,
                        0.12621546774823414,
                        0.15950123927281795
                    ],
                    [
                        0.1402212660359254,
                        0.08610877852324604,
                        0.14270955086647966,
                        0.18250855738374352
                    ],
                    [
                        0.15401608191737076,
                        0.08788402286091851,
                        0.14520699149067592,
                        0.2050886634790137
                    ],
                    [
                        0.2323368205919633,
                        0.10149160003143153,
                        0.19484212533913678,
                        0.23713979281594194
                    ],
                    [
                        0.23974936048918513,
                        0.14295036729492838,
                        0.19625894152562004,
                        0.25946781427664195
                    ],
                    [
                        0.24057537100758625,
                        0.20578656203009824,
                        0.20723581258852156,
                        0.2922374477546056
                    ]
                ],
            "cdf": [
                    [
                        0.07928831669580277,
                        0.03422811116375578,
                        0.09096998709297957,
                        0.09096998709297957
                    ],
                    [
                        0.40626072180580275,
                        0.11351642785955855,
                        0.4179423922029796,
                        0.4179423922029796
                    ],
                    [
                        0.49723070889878235,
                        0.44048883296955854,
                        0.49723070889878235,
                        0.49723070889878235
                    ],
                    [
                        0.5314588200625381,
                        0.5314588200625381,
                        0.577875079640691,
                        0.639338637730452
                    ],
                    [
                        0.6566732563198289,
                        0.6566732563198289,
                        0.7199830084723604,
                        0.7199830084723604
                    ],
                    [
                        0.7987811851514987,
                        0.7144607642846041,
                        0.7542111196361162,
                        0.8451974447296513
                    ],
                    [
                        0.8794255558934071,
                        0.8565686931162739,
                        0.8794255558934071,
                        0.8794255558934071
                    ],
                    [
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9011877726333225
                    ],
                    [
                        0.9782377832530228,
                        0.9782377832530228,
                        0.9589752805980978,
                        0.9422124920281629
                    ],
                    [
                        0.9999999999929382,
                        0.9999999999929382,
                        0.9999999999929382,
                        0.9999999999929382
                    ]
                ],
            "ad": [
                    60000000000.0,
                    60000000000.0,
                    60000000000.0,
                    60000000000.0
                ],
            "m": [
                    3.0,
                    3.0,
                    3.0,
                    3.0
                ],
            "n_cycles_lifetime": [
                    95559948.11638647,
                    95559948.11638647,
                    95559948.11638647,
                    95559948.11638647
                ]
		  }
        },

        {
            "uls_results": {
                "mooring_tension": [
                    [
                        [
                            [
                                37435.35787709276,
                                64419.65516736676
                            ],
                            [
                                37435.35892477166,
                                64419.65469435439
                            ],
                            [
                                44570.00656000909,
                                76583.59013410198
                            ],
                            [
                                44570.00369245041,
                                76583.58878752761
                            ]
                        ],
                        [
                            [
                                36827.85783060535,
                                65065.50082856364
                            ],
                            [
                                36827.85889613669,
                                65065.50038926155
                            ],
                            [
                                46199.86850567229,
                                79312.71249213733
                            ],
                            [
                                46199.864852886836,
                                79312.71034463706
                            ]
                        ],
                        [
                            [
                                36170.36857294558,
                                65783.65696742413
                            ],
                            [
                                36170.36965784724,
                                65783.65656496542
                            ],
                            [
                                48959.48626971105,
                                83313.39282665773
                            ],
                            [
                                48959.48132696917,
                                83313.3893720024
                            ]
                        ]
                    ]
                ],
			    "tension_versus_mbl": [
                    [
                        [
                            0.01751654091243178,
                            0.017516540783813574,
                            0.020824072813175522,
                            0.02082407244702446
                        ],
                        [
                            0.017692154735853838,
                            0.017692154616401893,
                            0.021566156627740477,
                            0.021566156043807263
                        ],
                        [
                            0.017887430717309825,
                            0.01788743060787607,
                            0.022653993570908214,
                            0.022653992631542563
                        ]
                    ]
                ],
                "mbl_uls": [
                    9650000.0,
                    9650000.0,
                    9650000.0
                ]
		  },
		"fls_results":{
            "env_proba": [0],
		    "cdf_stress_range": [
                    [
                        1.03530433733043322,
                        1.004113311227466497,
                        1.0357526183671677,
                        1.049570761686637314
                    ],
                    [
                        1.04816981615077811,
                        1.016192996694111464,
                        1.039482057998408294,
                        1.055180169984814766
                    ],
                    [
                        1.05026068881173524,
                        1.02412937305243806,
                        1.049151145581692555,
                        1.06069185676171353
                    ],
                    [
                        1.0850192515016839,
                        1.038166044361503934,
                        1.10522211138375195,
                        1.15621823301378723
                    ],
                    [
                        1.10718831438830025,
                        1.04814171982039578,
                        1.12621546774823414,
                        1.15950123927281795
                    ],
                    [
                        1.1402212660359254,
                        1.08610877852324604,
                        1.14270955086647966,
                        1.18250855738374352
                    ],
                    [
                        1.15401608191737076,
                        1.08788402286091851,
                        1.14520699149067592,
                        1.2050886634790137
                    ],
                    [
                        1.2323368205919633,
                        1.10149160003143153,
                        1.19484212533913678,
                        1.23713979281594194
                    ],
                    [
                        1.23974936048918513,
                        1.14295036729492838,
                        1.19625894152562004,
                        1.25946781427664195
                    ],
                    [
                        1.24057537100758625,
                        1.20578656203009824,
                        1.20723581258852156,
                        1.2922374477546056
                    ]
                ],
            "cdf": [
                    [
                        0.07928831669580277,
                        0.03422811116375578,
                        0.09096998709297957,
                        0.09096998709297957
                    ],
                    [
                        0.40626072180580275,
                        0.11351642785955855,
                        0.4179423922029796,
                        0.4179423922029796
                    ],
                    [
                        0.49723070889878235,
                        0.44048883296955854,
                        0.49723070889878235,
                        0.49723070889878235
                    ],
                    [
                        0.5314588200625381,
                        0.5314588200625381,
                        0.577875079640691,
                        0.639338637730452
                    ],
                    [
                        0.6566732563198289,
                        0.6566732563198289,
                        0.7199830084723604,
                        0.7199830084723604
                    ],
                    [
                        0.7987811851514987,
                        0.7144607642846041,
                        0.7542111196361162,
                        0.8451974447296513
                    ],
                    [
                        0.8794255558934071,
                        0.8565686931162739,
                        0.8794255558934071,
                        0.8794255558934071
                    ],
                    [
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9372130638581824,
                        0.9011877726333225
                    ],
                    [
                        0.9782377832530228,
                        0.9782377832530228,
                        0.9589752805980978,
                        0.9422124920281629
                    ],
                    [
                        0.9999999999929382,
                        0.9999999999929382,
                        0.9999999999929382,
                        0.9999999999929382
                    ]
                ],
            "ad": [
                    40000000000.0,
                    40000000000.0,
                    40000000000.0,
                    40000000000.0
                ],
            "m": [
                    5.0,
                    5.0,
                    5.0,
                    5.0
                ],
            "n_cycles_lifetime": [
                    85559948.11638647,
                    85559948.11638647,
                    85559948.11638647,
                    85559948.11638647
                ]
		  }
        }
	],
"master_structure": {
        "uls_results": {
            "mooring_tension": [
                [
                    [
                        [
                            0.0,
                            245013.04723266925
                        ],
                        [
                            0.0,
                            236072.98474651977
                        ],
                        [
                            0.0,
                            245013.05221602484
                        ],
                        [
                            0.0,
                            272442.22758307157
                        ],
                        [
                            0.0,
                            311771.52753289614
                        ],
                        [
                            0.0,
                            333009.4482197404
                        ],
                        [
                            0.0,
                            311771.51731848036
                        ],
                        [
                            0.0,
                            272442.21772205405
                        ]
                    ],
                    [
                        [
                            0.0,
                            241972.72757755624
                        ],
                        [
                            0.0,
                            232294.99242808914
                        ],
                        [
                            0.0,
                            241972.7331313091
                        ],
                        [
                            0.0,
                            272684.25540541374
                        ],
                        [
                            0.0,
                            319059.823178776
                        ],
                        [
                            0.0,
                            345170.39432560315
                        ],
                        [
                            0.0,
                            319059.81054824934
                        ],
                        [
                            0.0,
                            272684.2438796134
                        ]
                    ],
                    [
                        [
                            0.0,
                            238357.82995546074
                        ],
                        [
                            0.0,
                            227882.2961167658
                        ],
                        [
                            0.0,
                            238357.83595259217
                        ],
                        [
                            0.0,
                            273036.22921102884
                        ],
                        [
                            0.0,
                            328965.7599880596
                        ],
                        [
                            0.0,
                            362203.5789261152
                        ],
                        [
                            0.0,
                            328965.7444234613
                        ],
                        [
                            0.0,
                            273036.21600457845
                        ]
                    ]
                ]
            ],
		    "mbl_uls": [
                    9650000.0,
                    9650000.0,
                    9650000.0
            ]
		}    
    }
}


stress_sk_url = {
    "devices": [
        { "uls_results": {},
	      "fls_results":{}
        }],
    "master_structure":
        { "uls_results": {},
	      "fls_results":{}
        },
    "url_sk_stress": "127.0.0.1:5000/sk/1/stress"
}

fatigue_input_et = {
            "description": "Fatigue Stress Probability",
            "label": "Fatigue Stress Probability",
            "unit": "-",
            "value": {
                "probability": [
                    1.5206049678508205e-08,
                    3.0407537884231846e-08,
                    4.559990542487577e-08,
                    6.077859766752728e-08,
                    7.593906681435863e-08,
                    9.107677417354688e-08,
                    1.0618719242382392e-07,
                    1.21265807871073e-07,
                    1.3630812269540208e-07,
                    1.5130965718712085e-07,
                    1.6626595197007078e-07,
                    1.8117257021076361e-07,
                    1.960250998117898e-07,
                    2.1081915558798836e-07,
                    2.255503814238678e-07,
                    2.4021445241079934e-07,
                    2.5480707696250304e-07,
                    2.693239989073973e-07,
                    2.8376099955636943e-07,
                    2.981138997445583e-07,
                    3.123785618457884e-07,
                    3.265508917582762e-07,
                    3.4062684086027586e-07,
                    3.5460240793438074e-07,
                    3.684736410591807e-07,
                    3.8223663946702437e-07,
                    3.958875553666936e-07,
                    4.094225957297704e-07,
                    4.228380240395563e-07,
                    4.361301620014324e-07,
                    4.4929539121352127e-07,
                    4.623301547966765e-07,
                    4.7523095898270135e-07,
                    4.879943746598625e-07,
                    5.006170388747334e-07,
                    5.130956562894634e-07,
                    5.254270005936209e-07,
                    5.376079158697219e-07,
                    5.496353179117512e-07,
                    5.615061954958308e-07,
                    5.732176116023825e-07,
                    5.847667045890971e-07,
                    5.961506893141092e-07,
                    6.073668582087254e-07,
                    6.184125822992809e-07,
                    6.292853121775112e-07,
                    6.399825789190542e-07,
                    6.505019949496442e-07,
                    6.608412548586594e-07,
                    6.709981361596233e-07,
                    6.809704999975055e-07,
                    6.907562918024628e-07,
                    7.003535418898967e-07,
                    7.097603660066647e-07,
                    7.189749658233333e-07,
                    7.279956293724174e-07,
                    7.368207314325895e-07,
                    7.454487338588709e-07,
                    7.538781858589358e-07,
                    7.621077242154633e-07,
                    7.701360734549813e-07,
                    7.779620459631059e-07,
                    7.855845420465912e-07,
                    7.930025499424044e-07,
                    8.002151457741559e-07,
                    8.072214934562425e-07,
                    8.140208445461004e-07,
                    8.206125380450052e-07,
                    8.269960001478955e-07,
                    8.331707439427284e-07,
                    8.39136369059911e-07,
                    8.448925612724563e-07,
                    8.504390920472892e-07,
                    8.557758180486822e-07,
                    8.609026805941851e-07,
                    8.658197050639658e-07,
                    8.705270002642497e-07,
                    8.750247577456519e-07,
                    8.793132510772164e-07,
                    8.833928350770067e-07,
                    8.87263945000119e-07,
                    8.909270956850176e-07,
                    8.943828806591169e-07,
                    8.976319712045505e-07,
                    9.006751153851737e-07,
                    9.035131370355809e-07,
                    9.061469347135152e-07,
                    9.085774806163534e-07,
                    9.108058194629385e-07,
                    9.128330673417688e-07,
                    9.14660410526649e-07,
                    9.16289104260912e-07,
                    9.17720471511344e-07,
                    9.1895590169295e-07,
                    9.199968493657157e-07,
                    9.208448329045191e-07,
                    9.215014331434429e-07,
                    9.219682919954576e-07,
                    9.222471110490248e-07,
                    9.22339650142468e-07
                ],
                "stress": [
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    1.6468789096280935e-05,
                    3.481680736811738e-05,
                    5.316468133310006e-05,
                    7.151241099122798e-05,
                    8.98599963425015e-05,
                    0.00010820743738691949,
                    0.00012655473412448364,
                    0.0001449018865551922,
                    0.0001632488946790467,
                    0.00018159575849604638,
                    0.0001999424780061912,
                    0.000218289053209482
                ]
            }
        }

uls_stress_et = {
            "description": "Stress Probability",
            "label": "Maximum stress Probability",
            "unit": "-",
            "value": {
                "probability": [
                    1.2132672269276997e-06,
                    1.2130852504922345e-06,
                    1.2127820170853677e-06,
                    1.212357617653621e-06,
                    1.2118121794584752e-06,
                    1.2111458660127666e-06,
                    1.2103588769989998e-06,
                    1.2094514481695964e-06,
                    1.2084238512292227e-06,
                    1.2072763936991832e-06
                ],
                "stress": [
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1,
                    0.1
                ]
            }
        }

n_cycles_et = {
            "description": "Fatigue Stress Probability",
            "label": "NUme",
            "unit": "-",
            "value": [
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3,
                4.1399967481973125e+3
            ]
        }

s_n_et = {
            "description": "SN curve",
            "label": "SN curve energy",
            "unit": "-",
            "value": [
                [
                    3,
                    10.97
                ],
                [
                    5,
                    13.617
                ]
            ]
        }

ultimate_stess_et = {
            "description": "Ultimate Stress",
            "label": "Ultimate stress",
            "unit": "-",
            "value": 503.9925969532528
        }

stress_et = [{
        "Captured_Power": {
            "description": "Captured Power",
            "label": "Captured Power",
            "unit": "kW",
            "value": [120.11403822021823]
        },
        "S_N": s_n_et,
        "maximum_stress_probability": uls_stress_et,
        "fatigue_stress_probability": fatigue_input_et,
        "number_cycles": n_cycles_et,
        "ultimate_stress": ultimate_stess_et
    }
]

stress_et_url = {
    "device_id": "Device 1", 
    "mu_a": 50763336419,
    "std_a": 10153000000, 
    "h": 1.786,
    "mu_l": 83313,
    "std_l": 24994, 
    "m": 3.0,
    "n": 100000000.0,
    "mu_q": 0.115,
    "std_q": 0.0575,
    "mu_r": 270000,
    "std_r": 81000,
    "url_et_stress": "127.0.0.1:5000/et/1/stress"
}


rams_prj = {
    "title": "DTOceanPlus assessment - RAMS",
    "desc": "Metrics of R.A.M.S. for a marine energy farm",
    "complexity":1,
    "status": 15,
    "tags": "RAMS_general_inputs",
    "lifetime": 20
}


rams_prj_update = {
    "title": "DTOceanPlus assessment - RAMS",
    "desc": "Metrics of R.A.M.S. for a marine energy farm",
    "complexity":3,
    "status": 35,
    "tags": "RAMS_general_inputs",
    "lifetime": 20
}


data_r_component = {
    "title": "DTOceanPlus assessment - RAMS",
    "desc": "Metrics of R.A.M.S. for a marine energy farm",
    "complexity":1,
    "status": 15,
    "tags": "RAMS_general_inputs",
    "lifetime": 20,
    "downtime": 8,
    "n_sim_reliability": 2,
    "hierarchy_ed": hierarchy_ed,
    "hierarchy_et": hierarchy_et,
    "hierarchy_sk": hierarchy_sk,
    "lmo_maintenance": maintenance      
    }

data_a = {
    "title": "DTOceanPlus assessment - RAMS",
    "desc": "Metrics of R.A.M.S. for a marine energy farm",
    "complexity":1,
    "status": 15,
    "tags": "RAMS_general_inputs",
    "lifetime": 20,
    "lmo_downtime": downtime
    }


data_m = {
    "title": "DTOceanPlus assessment - RAMS",
    "desc": "Metrics of R.A.M.S. for a marine energy farm",
    "complexity":1,
    "status": 15,
    "tags": "RAMS_general_inputs",
    "lifetime": 20,
    "lmo_maintenance": maintenance,
    "pd_t_repair": "Gaussian",
    "std_t_repair": 24,
    "t_ava_repair": 8    
    }

user_defined_r = {
    "downtime": 8.0,
    "n_sim_reliability": 2
    }


user_defined_m = {
    "pd_t_repair": "Gaussian",
    "std_t_repair": 24.0,
    "t_ava_repair": 8.0
    }


user_defined_s = {
    "cov_a": 0.25,
    "cov_l": 0.3,
    "cov_q": 0.5,
    "cov_r": 0.2,
    "cov_ufl": 0.1,
    "cov_ufr": 0.1,    
    "mu_ufl": 1.0,
    "mu_ufr": 1.0,
    "n_sim_fls": 10000,
    "n_sim_uls": 10000,
    "option_fls": "Monte Carlo", 
    "option_uls": "Monte Carlo", 
    "pd_a": "Gaussian",
    "pd_h": "constant",
    "pd_l": "Gaussian",
    "pd_m": "constant",
    "pd_n": "constant", 
    "pd_q": "Gaussian",
    "pd_r": "Gaussian", 
    "pd_ufl": "Gaussian", 
    "pd_ufr": "Gaussian"
    }


@pytest.fixture
def app():
    settings_override = {
        "TESTING": True,
        "SQLALCHEMY_DATABASE_URI": "/////db/rams.db", #"sqlite://"
    }
    app = service.create_app(settings_override)
    yield app


@pytest.fixture(scope="function")
def init_db(app):
    from dtop_rams.service import db
    app_current = app.app_context()
    app_current.push()
    db.init_db()

    yield db


@pytest.fixture
def client(app):
    client = app.test_client()

    yield client


def test_get_all_inputs(client, init_db):
    response = client.get('/rams')
    projects = json.loads(response.data)
    assert type(projects) is list


def test_create_project(client):
    response = client.post('/rams')
    if response.status_code == 404:
        response = client.post('/rams', json=rams_prj)
        assert response.status_code == 201


def test_get_all_projects(client, init_db):
    # response = client.get('/rams')
    # assert response.status_code == 404    
    response = client.post('/rams', json=rams_prj)
    response = client.get(f'/rams')
    assert response.status_code == 200


def test_get_single_project(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.get(f'/rams/{ramsId}')
    assert response.status_code == 200
    response = client.get('/rams/-1')
    assert response.status_code == 404


def test_post_ed_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed)
    assert response_ed.status_code == 201
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity2', json=body_ed)
    assert response_ed.status_code == 201
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity3', json=body_ed)
    assert response_ed.status_code == 201

    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=None)
    assert response_ed.status_code == 400
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity2', json=None)
    assert response_ed.status_code == 400
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity3', json=None)
    assert response_ed.status_code == 400

    body_ed.pop("hierarchy_new")    
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed)
    assert response_ed.status_code == 400	
    body_ed1 = {
        "hierarchy_new":{
            "system": ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED"], 
            "name_of_node": ["ED Subsystem", "ED1", "ED2", "ED3", "ED4", "ED5", "Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1", "26", "21", "16", "11", "6", "2", "24", "19", "14", "9", "4", "0", "1", "27", "25", "23", "22", "20", "18", "17", "15", "13", "12", "10", "8", "7", "5", "3"], 
            "design_id": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "26", "21", "16", "11", "6", "2", "24", "19", "14", "9", "4", "0", "1", "27", "25", "23", "22", "20", "18", "17", "15", "13", "12", "10", "8", "7", "5", "3"], 
            "node_type": ["System", "System", "System", "System", "System", "System", "Energy route", "Energy route", "Energy route", "Energy route", "Energy route", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component", "Component"], 
            "node_subtype": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", " umbilical", " umbilical", " umbilical", " umbilical", " umbilical", " substation", " array", " array", " array", " array", " array", " export", " dry-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate", " wet-mate"], 
            "category": ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], 
            "parent": ["NA", "NA", "NA", "NA", "NA", "NA", ["ED1"], ["ED2"], ["ED3"], ["ED4"], ["ED5"], ["Route5_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route5_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route1_1", "Route2_1", "Route3_1", "Route4_1", "Route5_1"], ["Route5_1"], ["Route5_1"], ["Route5_1"], ["Route3_1"], ["Route3_1"], ["Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route2_1", "Route3_1"], ["Route4_1"], ["Route4_1"], ["Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route4_1"], ["Route1_1", "Route4_1"]], 
            "child": [["ED1", "ED2", "ED3", "ED4", "ED5"], ["Route1_1"], ["Route2_1"], ["Route3_1"], ["Route4_1"], ["Route5_1"], ["7", "6", "5", "4", "3", "2", "1", "0"], ["17", "16", "15", "14", "13", "2", "1", "0"], ["22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "2", "1", "0"], ["12", "11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1", "0"], ["27", "26", "25", "24", "23", "2", "1", "0"], "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
            "gate_type": ["OR", "OR", "OR", "OR", "OR", "OR", "AND", "AND", "AND", "AND", "AND", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"], 
            "failure_rate_repair": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.4284022243429648, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 2.457042, 8.379, 6.368491557497398, 4.5117967491553905, 6.563796749155391, 2.064152595828997, 18.215604495807106, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244], 
            "failure_rate_replacement": ["NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", 0.4284022243429648, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 0.4200829426137176, 2.457042, 8.379, 6.368491557497398, 4.5117967491553905, 6.563796749155391, 2.064152595828997, 18.215604495807106, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244, 5.42244]
        }
    }
    body_ed1["hierarchy_new"].pop('system')    
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed1)
    assert response_ed.status_code == 400	

    body_ed1["hierarchy_new"]['system'] = ["ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED",
	                                      "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", 
										  "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", 
										  "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", "ED", 
										  "ED", "ED", "ED"]
    body_ed1["hierarchy_new"]['child'][0] = ["ED1", "ED2", "ED3", "ED4", "ED"]
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed1)
    assert response_ed.status_code == 400

    body_ed1["hierarchy_new"]['child'][0] = ["ED1", "ED2", "ED3", "ED4", "ED5"]
    body_ed1["hierarchy_new"]['parent'][1] = "ED Subsystem120"
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed1)
    assert response_ed.status_code == 400
	
    body_ed1["hierarchy_new"]['parent'][1] = "ED Subsystem"
    body_ed1["hierarchy_new"]['parent'][6] = ["ED1212"]
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed1)
    assert response_ed.status_code == 400
    body_ed1["hierarchy_new"]['parent'][6] = ["ED1"]

    body_ed1["hierarchy_new"]['category'] = ["Level 3", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"], 
    response_ed = client.post(f'/rams/{ramsId}/inputs/ed/complexity1', json=body_ed1)
    assert response_ed.status_code == 400
    body_ed1["hierarchy_new"]['category'] = ["Level 3", "Level 2", "Level 2", "Level 2", "Level 2", "Level 2", "Level 1", "Level 1", "Level 1", "Level 1", "Level 1", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0", "Level 0"]

    response = client.post('/rams/-1/inputs/ed/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/ed/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/ed/complexity3')
    assert response.status_code == 404


def test_post_et_hierarchy_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=body_et_hierarchy)
    assert response_et.status_code == 201
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity2', json=body_et_hierarchy)
    assert response_et.status_code == 201
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity3', json=body_et_hierarchy)
    assert response_et.status_code == 201

    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=None)
    assert response_et.status_code == 400
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity2', json=None)
    assert response_et.status_code == 400
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity3', json=None)
    assert response_et.status_code == 400

    body_et_hierarchy["Hierarchy"]["value"].pop('system')   
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=body_et_hierarchy)
    assert response_et.status_code == 400

    body_et_hierarchy["Hierarchy"]["value"]['system'] = ["ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET",
	                               "ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET","ET"]
    body_et_hierarchy["Hierarchy"]["value"]['child'][0] = ["DEV_0","DEV_190"]
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=body_et_hierarchy)
    assert response_et.status_code == 400

    body_et_hierarchy["Hierarchy"]["value"]['child'][0] = ["DEV_0","DEV_1"]								  
    body_et_hierarchy["Hierarchy"]["value"]['parent'][2] = "DEV_190" 								 
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=body_et_hierarchy)
    assert response_et.status_code == 400

    body_et_hierarchy["Hierarchy"]["value"]['parent'][2] = "DEV_0"
    body_et_hierarchy["Hierarchy"]["value"]['parent'][1] = ["Array_0109"]	 								 
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=body_et_hierarchy)
    assert response_et.status_code == 400
    body_et_hierarchy["Hierarchy"]["value"]['parent'][1] = ["Array_01"]	
 
    body_et_hierarchy["Hierarchy"]["value"]['category'] = ["Level 3","Level 1","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0"]
    response_et = client.post(f'/rams/{ramsId}/inputs/et/hierarchy/complexity1', json=body_et_hierarchy)
    assert response_et.status_code == 400
    body_et_hierarchy["Hierarchy"]["value"]['category'] = ["Level 3","Level 2","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 2","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0","Level 1","Level 0","Level 0","Level 0"]

    response = client.post('/rams/-1/inputs/et/hierarchy/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/et/hierarchy/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/et/hierarchy/complexity3')
    assert response.status_code == 404


def test_post_et_stress_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    # response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    # assert response_et.status_code == 201
    # response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity2', json=stress_et)
    # assert response_et.status_code == 201
    # response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity3', json=stress_et)
    # assert response_et.status_code == 201
    
    stress_et[0].pop("S_N")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400
    stress_et[0]["S_N"] = s_n_et
    stress_et[0]["S_N"].pop("value")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400
    stress_et[0]["S_N"] = s_n_et
    stress_et[0].pop("maximum_stress_probability")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    stress_et[0]["maximum_stress_probability"] = uls_stress_et
    stress_et[0].pop("fatigue_stress_probability")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    stress_et[0]["fatigue_stress_probability"] = fatigue_input_et
    stress_et[0].pop("number_cycles")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    stress_et[0]["number_cycles"] = n_cycles_et
    stress_et[0].pop("ultimate_stress")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400
    stress_et[0]["ultimate_stress"] = ultimate_stess_et

    stress_et[0]["maximum_stress_probability"]["value"].pop("stress")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    stress_et[0]["maximum_stress_probability"] = uls_stress_et
    stress_et[0]["maximum_stress_probability"]["value"].pop("probability")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    stress_et[0]["maximum_stress_probability"] = uls_stress_et
    stress_et[0]["fatigue_stress_probability"]["value"].pop("stress")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    stress_et[0]["fatigue_stress_probability"]= fatigue_input_et
    stress_et[0]["fatigue_stress_probability"]["value"].pop("probability")
    response_et = client.post(f'/rams/{ramsId}/inputs/et/stress/complexity1', json=stress_et)
    assert response_et.status_code == 400

    response = client.post('/rams/-1/inputs/et/stress/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/et/stress/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/et/stress/complexity3')
    assert response.status_code == 404


def test_post_sk_hierarchy_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=body_sk_hierarchy)
    assert response_sk.status_code == 201
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity2', json=body_sk_hierarchy)
    assert response_sk.status_code == 201
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity3', json=body_sk_hierarchy)
    assert response_sk.status_code == 201

    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=None)
    assert response_sk.status_code == 400
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity2', json=None)
    assert response_sk.status_code == 400
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity3', json=None)
    assert response_sk.status_code == 400

    body_sk_hierarchy.pop('system')    
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=body_sk_hierarchy)
    assert response_sk.status_code == 400

    body_sk_hierarchy['system'] = ["SK","SK","SK","SK","SK","SK","SK","SK","SK","SK","SK"]
    body_sk_hierarchy['child'][0][2] = "SK0_x_ml_200"
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=body_sk_hierarchy)
    assert response_sk.status_code == 400

    body_sk_hierarchy['child'][0][2] = "SK0_x_ml_2"
    body_sk_hierarchy['parent'][1] = "SK0_x_ml_0320"
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=body_sk_hierarchy)
    assert response_sk.status_code == 400

    body_sk_hierarchy['parent'][1] = ["SK0_x_ml_0320"]
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=body_sk_hierarchy)
    assert response_sk.status_code == 400

    body_sk_hierarchy['parent'] = ["NA","SK0_x_ml_0","SK0_x_ml_0","SK0_x","SK0_x_ml_1","SK0_x_ml_1","SK0_x","SK0_x_ml_2","SK0_x_ml_2","SK0_x","NA"]
    body_sk_hierarchy['category'] = ["Level 1","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 3"]
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/hierarchy/complexity1', json=body_sk_hierarchy)
    assert response_sk.status_code == 400
    body_sk_hierarchy['category'] = ["Level 2","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 0","Level 0","Level 1","Level 3"]

    response = client.post('/rams/-1/inputs/sk/hierarchy/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/sk/hierarchy/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/sk/hierarchy/complexity3')
    assert response.status_code == 404


def test_post_sk_stress_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 201
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity2', json=stress_sk)
    assert response_sk.status_code == 201
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity3', json=stress_sk)
    assert response_sk.status_code == 201

    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=None)
    assert response_sk.status_code == 400
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity2', json=None)
    assert response_sk.status_code == 400
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity3', json=None)
    assert response_sk.status_code == 400

    stress_sk['devices_temp'] = stress_sk['devices'] 
    stress_sk.pop('devices')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'] = stress_sk['devices_temp'] 
    stress_sk.pop('devices_temp')

    stress_sk['temp'] = stress_sk['devices'][0]['uls_results'] 
    stress_sk['devices'][0].pop('uls_results')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['uls_results'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['uls_results']['mooring_tension'] 
    stress_sk['devices'][0]['uls_results'].pop('mooring_tension')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['uls_results']['mooring_tension']  = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['uls_results']['mbl_uls'] 
    stress_sk['devices'][0]['uls_results'].pop('mbl_uls')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['uls_results']['mbl_uls']  = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['fls_results'] 
    stress_sk['devices'][0].pop('fls_results')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['fls_results'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['fls_results']['ad'] 
    stress_sk['devices'][0]['fls_results'].pop('ad')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['fls_results']['ad'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['fls_results']['m'] 
    stress_sk['devices'][0]['fls_results'].pop('m')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['fls_results']['m'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['fls_results']['n_cycles_lifetime'] 
    stress_sk['devices'][0]['fls_results'].pop('n_cycles_lifetime')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['fls_results']['n_cycles_lifetime'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['fls_results']['cdf'] 
    stress_sk['devices'][0]['fls_results'].pop('cdf')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['fls_results']['cdf'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['temp'] = stress_sk['devices'][0]['fls_results']['cdf_stress_range'] 
    stress_sk['devices'][0]['fls_results'].pop('cdf_stress_range')
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'][0]['fls_results']['cdf_stress_range'] = stress_sk['temp']
    stress_sk.pop('temp')

    stress_sk['devices_temp'] = stress_sk['devices'] 
    stress_sk['devices'] = ""
    response_sk = client.post(f'/rams/{ramsId}/inputs/sk/stress/complexity1', json=stress_sk)
    assert response_sk.status_code == 400
    stress_sk['devices'] = stress_sk['devices_temp']
    stress_sk.pop('devices_temp')

    response = client.post('/rams/-1/inputs/sk/stress/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/sk/stress/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/sk/stress/complexity3')
    assert response.status_code == 404


def test_post_lmo_downtime_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response_downtime = client.post(f'/rams/{ramsId}/inputs/lmo/downtime/complexity1', json=downtime_input)
    assert response_downtime.status_code == 201
    response_downtime = client.post(f'/rams/{ramsId}/inputs/lmo/downtime/complexity2', json=downtime_input)
    assert response_downtime.status_code == 201
    response_downtime = client.post(f'/rams/{ramsId}/inputs/lmo/downtime/complexity3', json=downtime_input)
    assert response_downtime.status_code == 201

    response_downtime = client.post(f'/rams/{ramsId}/inputs/lmo/downtime/complexity1', json=None)
    assert response_downtime.status_code == 400
    response_downtime = client.post(f'/rams/{ramsId}/inputs/lmo/downtime/complexity2', json=None)
    assert response_downtime.status_code == 400
    response_downtime = client.post(f'/rams/{ramsId}/inputs/lmo/downtime/complexity3', json=None)
    assert response_downtime.status_code == 400

    downtime_input['downtime_temp'] = downtime_input['downtime']
    downtime_input.pop('downtime')
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=downtime_input)
    assert response_maintenance.status_code == 400
    downtime_input['downtime'] = downtime_input['downtime_temp']
    downtime_input.pop('downtime_temp')

    downtime_input['downtime_temp'] = downtime_input['downtime']
    downtime_input['downtime'] = {}
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=downtime_input)
    assert response_maintenance.status_code == 400
    downtime_input['downtime'] = downtime_input['downtime_temp']
    downtime_input.pop('downtime_temp')

    response = client.post('/rams/-1/inputs/lmo/downtime/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/lmo/downtime/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/lmo/downtime/complexity3')
    assert response.status_code == 404


def test_post_lmo_maintenance_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=maintenance)
    assert response_maintenance.status_code == 201
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity2', json=maintenance)
    assert response_maintenance.status_code == 201
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity3', json=maintenance)
    assert response_maintenance.status_code == 201

    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=None)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity2', json=None)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity3', json=None)
    assert response_maintenance.status_code == 400

    maintenance['mttr'] = 8
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=maintenance)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity2', json=maintenance)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity3', json=maintenance)
    assert response_maintenance.status_code == 400
    maintenance['mttr'] = ["", "", 224, ""]
    maintenance['downtime'] = 10
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=maintenance)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity2', json=maintenance)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity3', json=maintenance)
    assert response_maintenance.status_code == 400
    maintenance['mttr'] = ["", "", 43, ""]
    maintenance['downtime'] = ["", "", 224, ""]
    maintenance['technologies'] = 8
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity1', json=maintenance)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity2', json=maintenance)
    assert response_maintenance.status_code == 400
    response_maintenance = client.post(f'/rams/{ramsId}/inputs/lmo/maintenance/complexity3', json=maintenance)
    assert response_maintenance.status_code == 400

    response = client.post('/rams/-1/inputs/lmo/maintenance/complexity1')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/lmo/maintenance/complexity2')
    assert response.status_code == 404
    response = client.post('/rams/-1/inputs/lmo/maintenance/complexity3')
    assert response.status_code == 404


def test_reliability_user_defined(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=user_defined_r)   
    assert response.status_code == 201
    
    response = client.get(f'/rams/{ramsId}/inputs/reliability/user_defined')  
    assert response.status_code == 200

    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=None)   
    assert response.status_code == 400

    user_defined_r['n_sim_reliability'] = 1   
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=user_defined_r)   
    assert response.status_code == 400
    user_defined_r.pop('n_sim_reliability')
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=user_defined_r)   
    assert response.status_code == 400
    user_defined_r['n_sim_reliability'] = 2
    user_defined_r.pop('downtime')
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=user_defined_r)   
    assert response.status_code == 400

    user_defined_r['n_sim_reliability'] = ""
    user_defined_r['downtime'] = 24.0
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=user_defined_r)   
    assert response.status_code == 400
    user_defined_r['n_sim_reliability'] = 2
    user_defined_r['downtime'] = ""
    response = client.post(f'/rams/{ramsId}/inputs/reliability/user_defined', json=user_defined_r)   
    assert response.status_code == 400

    response = client.post('/rams/-1/inputs/reliability/user_defined')
    assert response.status_code == 404    

    response = client.get('/rams/-1/inputs/reliability/user_defined')
    assert response.status_code == 404 


def test_maintainability_user_defined(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 201

    response = client.get(f'/rams/{ramsId}/inputs/maintainability/user_defined')  
    assert response.status_code == 200

    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=None)   
    assert response.status_code == 400

    user_defined_m["pd_t_repair"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 400
    user_defined_m["pd_t_repair"] = 'Gaussian'
    user_defined_m["std_t_repair"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 400
    user_defined_m["std_t_repair"] = 2.98
    user_defined_m["t_ava_repair"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 400

    user_defined_m.pop("pd_t_repair")
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 400
    user_defined_m["pd_t_repair"] = 'Gaussian'
    user_defined_m.pop("std_t_repair")
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 400
    user_defined_m["pd_t_repair"] = 'Gaussian'
    user_defined_m["std_t_repair"] = 2.98
    user_defined_m.pop("t_ava_repair")
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m)   
    assert response.status_code == 400

    user_defined_m["t_ava_repair"] = 24.0
    user_defined_m["pd_t_repair"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/maintainability/user_defined', json=user_defined_m) 
	
    assert response.status_code == 400
    response = client.post('/rams/-1/inputs/maintainability/user_defined')
    assert response.status_code == 404  

    response = client.get('/rams/-1/inputs/maintainability/user_defined')
    assert response.status_code == 404 


def test_survivability_user_defined(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 201

    response = client.get(f'/rams/{ramsId}/inputs/survivability/user_defined')  
    assert response.status_code == 200

    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=None)   
    assert response.status_code == 400

    user_defined_s.pop("cov_a")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400    
    user_defined_s["cov_a"] = 0.25
    user_defined_s.pop("cov_l")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400    
    user_defined_s["cov_l"] = 0.3
    user_defined_s.pop("cov_q")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400    
    user_defined_s["cov_q"] = 0.5
    user_defined_s.pop("cov_r")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400    
    user_defined_s["cov_r"] = 0.2
    user_defined_s.pop("cov_ufl")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["cov_ufl"] = 0.1
    user_defined_s.pop("cov_ufr")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["cov_ufr"] = 0.1
    user_defined_s.pop("mu_ufl")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["mu_ufl"] = 1.0
    user_defined_s.pop("mu_ufr")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["mu_ufr"] = 1.0
    user_defined_s.pop("n_sim_fls")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["n_sim_fls"] = 10000
    user_defined_s.pop("n_sim_uls")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["n_sim_uls"] = 10000
    user_defined_s.pop("option_fls")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["option_fls"] = 'Monte Carlo'
    user_defined_s.pop("option_uls")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["option_uls"] = 'Monte Carlo'
    user_defined_s.pop("pd_a")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["pd_a"] = 'Gaussian'
    user_defined_s.pop("pd_h")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["pd_h"] = 'constant'    
    user_defined_s.pop("pd_l")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400     
    user_defined_s["pd_l"] = 'Gaussian'
    user_defined_s.pop("pd_m")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["pd_m"] = 'constant'
    user_defined_s.pop("pd_n")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["pd_n"] = 'constant'
    user_defined_s.pop("pd_q")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_q"] = 'Gaussian'
    user_defined_s.pop("pd_r")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["pd_r"] = 'Gaussian'
    user_defined_s.pop("pd_ufl")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["pd_ufl"] = 'Gaussian'
    user_defined_s.pop("pd_ufr")
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 

    user_defined_s["pd_ufr"] = 'Gaussian'
    user_defined_s["cov_a"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400   
    user_defined_s["cov_a"] = 0.25
    user_defined_s["cov_l"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["cov_l"] = 0.3
    user_defined_s["cov_q"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["cov_q"] = 0.5
    user_defined_s["cov_r"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["cov_r"] = 0.2
    user_defined_s["cov_ufl"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400 
    user_defined_s["cov_ufl"] = 0.1
    user_defined_s["cov_ufr"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["cov_ufr"] = 0.1
    user_defined_s["mu_ufl"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["mu_ufl"] = 1.0
    user_defined_s["mu_ufr"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["mu_ufr"] = 1.0
    user_defined_s["n_sim_fls"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["n_sim_fls"] = 10000
    user_defined_s["n_sim_uls"] = ""
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["n_sim_uls"] = 10000
    user_defined_s["option_fls"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["option_fls"] = 'Monte Carlo'
    user_defined_s["option_uls"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["option_uls"] = 'Monte Carlo'
    user_defined_s["pd_a"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_a"] = 'Gaussian'
    user_defined_s["pd_h"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_h"] = 'constant'
    user_defined_s["pd_l"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_l"] = 'Gaussian'
    user_defined_s["pd_m"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_m"] = 'constant'
    user_defined_s["pd_n"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_n"] = 'constant'
    user_defined_s["pd_q"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_q"] = 'Gaussian'
    user_defined_s["pd_r"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_r"] = 'Gaussian'
    user_defined_s["pd_ufl"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_ufl"] = 'Gaussian'
    user_defined_s["pd_ufr"] = 1
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["pd_ufr"] = 'Gaussian'
    user_defined_s["cov_a"] = 2.0
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["cov_a"] = 0.25
    user_defined_s["cov_l"] = 2.0
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["cov_l"] = 0.3
    user_defined_s["cov_q"] = 2.0
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["cov_q"] = 0.5
    user_defined_s["cov_r"] = 2.0
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["cov_r"] = 0.2
    user_defined_s["cov_ufl"] = 2.0
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["cov_ufl"] = 0.1
    user_defined_s["cov_ufr"] = 2.0
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400

    user_defined_s["cov_ufr"] = 0.2
    user_defined_s["option_fls"] = "FORM"
    response = client.post(f'/rams/{ramsId}/inputs/survivability/user_defined', json=user_defined_s)   
    assert response.status_code == 400
    user_defined_s["option_fls"] = "Monte Carlo"

    response = client.post('/rams/-1/inputs/survivability/user_defined')
    assert response.status_code == 404  

    response = client.get('/rams/-1/inputs/survivability/user_defined')
    assert response.status_code == 404 


def test_get_reliability_component(client, init_db):
    response = client.post('/rams', json=data_r_component)
    ramsId = response.get_json()['id']
    lifetime = data_r_component['lifetime']
    response = client.get(f'/rams/{ramsId}/reliability_component/{lifetime}')
    assert response.status_code == 200

    data_r_component["hierarchy_ed"] = {}
    response = client.post('/rams', json=data_r_component)
    ramsId = response.get_json()['id']
    response = client.get(f'/rams/{ramsId}/reliability_component/{lifetime}')
    assert response.status_code == 500

    response = client.get(f'/rams/-1/reliability_component/{lifetime}')
    assert response.status_code == 404



def test_get_availability(client, init_db):
    response = client.post('/rams', json=data_a)
    ramsId = response.get_json()['id']
    response = client.get(f'/rams/{ramsId}/availability')
    assert response.status_code == 200

    data_a["lmo_downtime"] = {}
    response = client.post('/rams', json=data_a)
    ramsId = response.get_json()['id']
    response = client.get(f'/rams/{ramsId}/availability')
    assert response.status_code == 500

    response = client.get(f'/rams/-1/availability')
    assert response.status_code == 404


def test_get_maintainability(client, init_db):
    maintenance['technologies'] = ["[ac1]", "[ml11,ml12,ml13,ml14]","[ml21]","[ac1,ac2,ec1]"]
    response = client.post('/rams', json=data_m)
    ramsId = response.get_json()['id']
    t_ava_repair = data_m['t_ava_repair']
    response = client.get(f'/rams/{ramsId}/maintainability/{t_ava_repair}')
    assert response.status_code == 200

    data_m["lmo_maintenance"] = {}
    response = client.post('/rams', json=data_m)
    ramsId = response.get_json()['id']
    response = client.get(f'/rams/{ramsId}/maintainability/2')
    assert response.status_code == 500

    response = client.get(f'/rams/-1/maintainability/2')
    assert response.status_code == 404


def test_update_specific_inputs(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.put(f'/rams/{ramsId}', json = rams_prj_update)
    assert response.status_code == 201    
    response = client.put('/rams/-1', json = rams_prj_update)
    assert response.status_code == 404


def test_delete_rams_project(client, init_db):
    response = client.post('/rams', json=rams_prj)
    ramsId = response.get_json()['id']
    response = client.delete(f'/rams/{ramsId}')
    assert response.status_code == 200
    response = client.delete('/rams/-1')
    assert response.status_code == 404
