"""
"""

import requests


def test_rams_ed():
    resp = requests.get('http://ed/api/energy-deliv-studies/ed/1/results')
    assert resp.status_code == 200, resp.text


def test_rams_et_hierarchy():
    resp = requests.get('http://et/energy_transf/1/array')
    assert resp.status_code == 200, resp.text


def test_rams_lmo_downtime():
    resp = requests.get('http://lmo/api/1/phases/maintenance/downtime')
    assert resp.status_code == 200, resp.text


def test_rams_lmo_maintenance():
    resp = requests.get('http://lmo/api/1/phases/maintenance/plan')
    assert resp.status_code == 200, resp.text


def test_rams_sk_hierarchy():
    resp = requests.get('http://sk/1/hierarchy')
    assert resp.status_code == 200, resp.text


def test_rams_sk_stress():
    resp = requests.get('http://sk/1/design_assessment')
    assert resp.status_code == 200, resp.text