# RAMS Overview

The RAMS Module assesses the reliability, availability, maintainability and survivability of
marine energy conversion system.


# Start service
## in Docker Container (PREFERRED METHOD)
preparation:

 - Install [Docker](https://www.docker.com/products/docker-desktop)

command:

1) make dist

2) to build the rams service(s)

    make build-backend build-client

3) to start the rams service

    make run

The FE will be available at 0.0.0.0

notes:
1) This will run the server (flask) on port 5000.
2) The BackEnd can be naviated through the paths/ routes defined in OpenAPI


## in a Virtual Environment
Cleanup - deactivate / remove conda environment if necessary :
```bash
conda deactivate
conda remove --name dtop_rams --all
```

Create and activate conda environment :
```bash
conda env create --file environment.yml
conda activate dtop_rams
```
Install the module
```bash
pip install -e .
```

Install `python-dotenv` for taking service default configuration from `.flaskenv`:
```bash
pip install python-dotenv
```
with `.flaskenv`:
```bash
FLASK_APP=dtop_energycapt.service
FLASK_ENV=development
FLASK_RUN_PORT=5000
```
without `.flaskenv`:
```bash
set FLASK_APP=dtop_rams.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```
and issue the following command
```bash
export FLASK_APP=src.dtop_rams.service
```

Initialize database:
```bash
flask init-db
```

Start the service:
```bash
flask run
```


# Stop service and Clean Docker
## in Docker Container (PREFERRED METHOD)
preparation:
Install [Docker]
[Docker]: https://www.docker.com/products/docker-desktop
command:
docker-compose down
docker system prune --volumes (delete all images)
docker image prune -a


# Tests

## Pytest

Option 1 (in Docker Container)

    make pytest

Option 2 (in Virtual Environment)

    python -m pytest -rxXs --capture=no --cov=dtop_rams --junitxml=report.xml test


## Dredd

Option 1 (in Docker Container)

    make dredd

Option 2 (in Virtual Environment)

    make x-dredd


## E2E tests

    make e2e-cypress


# GitLab CI
[`Makefile`](Makefile), [`ci.makefile`](ci.makefile), [`.gitlab-ci.yml`](.gitlab-ci.yml) and files in directory [`ci`](ci) provide various tests for the application. They are organized in such a way that maximizes reusability of Docker images, so that GitLab CI can run the whole pipeline faster. File `ci.makefile` contains entry points for GitLab CI and deals with pulling and pushing images in a Docker registry, while `Makefile` is responsible for the actual builds and actual tests.

To run a single test locally, use a target from `Makefile`, e.g.:

    make pytest


# Test Environment

## Windows

### OS version
```bash
Windows 10 Pro Version 1903
```
### Docker Version
```bash
Docker version 19.03.5, build 633a0ea
```
### Miniconda3 Version
```bash
conda 4.8.2
