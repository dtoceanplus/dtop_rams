import json
import os
import traceback
from functools import partial
from dredd_data import *
import dredd_hooks as hooks
import requests
import sys
import requests
import binascii


# filename = "hooks-output.txt"
# if os.path.exists(filename):
#     os.remove(filename)

# file = open(filename, "w")
# print = partial(print, file=file)

# def print_log(func):
#     def wrapper(transaction):
#         try:
#             func(transaction)
#         except:
#             traceback.print_exc(file=file)
#         finally:
#             file.flush()

#     return wrapper

sys.stdout = sys.stderr = open("dredd-hooks-output.txt", "w")
def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction['skip']:
            func(transaction)
    return wrapper


def clear_db(tr):
    base_url = f"{tr['protocol']}//{tr['host']}:{tr['port']}/rams"
    response = requests.get(base_url)
    if response.status_code == 200:
        existing_project = response.json()
        for project in existing_project:
            response = requests.delete(f'{base_url}/{project["id"]}')


def remove_all_projects(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    projects_ids = get_project_id(tr)
    for ramsId in projects_ids:
        requests.delete(f'{protocol}//{host}:{port}/rams/{ramsId}')


def get_project_id(tr):
    existing_project = check_project(tr)
    ids = [pr['id'] for pr in existing_project]
    return list(set(ids))


def get_a_project(tr):
    existing_project = check_project(tr)
    if existing_project: 
        project = existing_project[0]
    else:
        post_project(tr)
        projects = check_project(tr)
        if projects:
            project = projects[0]
        else:
            print('ERROR NO PROJECT AVAILABLE IN THE DB')

    return project


def check_project(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    existing_project = requests.get(f'{protocol}//{host}:{port}/rams').json()
    return existing_project


def post_project(tr):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/rams', json=rams_example)


def post_ud_reliability(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/rams/{ramsId}/inputs/reliability/user_defined', json=rams_ud_reliability)


def post_ud_maintainability(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/rams/{ramsId}/inputs/maintainability/user_defined', json=rams_ud_maintainability)

def post_ud_survivability(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.post(f'{protocol}//{host}:{port}/rams/{ramsId}/inputs/survivability/user_defined', json=rams_ud_survivability)


def reliability_assessment(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']

    r = requests.put(f'{protocol}//{host}:{port}/rams/{ramsId}', json=input_reliability)


def availability_assessment(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    
    r = requests.put(f'{protocol}//{host}:{port}/rams/{ramsId}', json=input_availability)


def maintainability_assessment(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    
    r = requests.put(f'{protocol}//{host}:{port}/rams/{ramsId}', json=input_maintainability)


def survivability_assessment(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    
    r = requests.put(f'{protocol}//{host}:{port}/rams/{ramsId}', json=input_survivability)


def post_task_reliability(tr, ramsId):
    protocol = tr['protocol']
    host = tr['host']
    port = tr['port']
    r = requests.post(f'{protocol}//{host}:{port}/rams/{ramsId}/reliability_system/task/10', json=input_reliability)
    
    return r
