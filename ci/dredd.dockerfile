FROM apiaryio/dredd

RUN apk add --no-cache py3-pip bash
RUN pip3 install --no-cache-dir dredd-hooks requests
RUN wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh \
    --directory-prefix=/usr/bin/ && chmod +x /usr/bin/wait-for-it.sh
