FROM continuumio/miniconda3:latest

COPY environment.yml .
RUN conda env create --file environment.yml
ENV PATH /opt/conda/envs/dtop_rams/bin:$PATH

RUN conda install nodejs
RUN npm install -g dredd
RUN pip --no-cache-dir install dredd-hooks
