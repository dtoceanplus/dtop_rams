FROM continuumio/miniconda3:latest
COPY environment.yml .
RUN conda env create --name dtop_rams --file environment.yml
ENV PATH /opt/conda/envs/dtop_rams/bin:$PATH
COPY manage.py .
COPY setup.py .
COPY src/dtop_rams/service ./src/dtop_rams/service
COPY src/dtop_rams/business ./src/dtop_rams/business
COPY .flaskenv .
RUN pip install -e . && pip install pact-python
EXPOSE 5000
ENV DATABASE_URL sqlite://///db/rams.db