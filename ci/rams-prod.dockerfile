FROM python:3.6-slim-buster

WORKDIR /app

COPY requirements.txt .
COPY setup.py .
COPY manage.py .
COPY src/ ./src/

RUN apt-get update && \
    apt-get install --yes --no-install-recommends gcc libc6-dev && \
    pip install --requirement requirements.txt && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes gcc libc6-dev

ENV FLASK_APP dtop_rams.service

EXPOSE 5000
