FROM continuumio/miniconda3:latest

COPY environment.yml .
RUN conda env create --file environment.yml
ENV PATH /opt/conda/envs/dtop_rams/bin:$PATH

COPY setup.py .
RUN pip --no-cache-dir install pytest pytest-cov pact-python
RUN mkdir src
RUN pip install -e .