# This file is intended primarily for CI.
# Unlike default Makefile which works like this: 'build, test',
# this file performs more steps: 'pull, build, push, test'.

SHELL = bash
.PHONY: *

export MODULE_NICKNAME = rams
REGISTRY_PORT = 5500

CI_REGISTRY_IMAGE ?= localhost:$(REGISTRY_PORT)/$(MODULE_NICKNAME)
DTOP_BASIC_AUTH=Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE=

# Convenient default target that simulates
all: \
	registry-start \
	update-dind update-backend update-client update-dredd update-dredd-src update-e2e-cypress update-pytest \
	dist dredd-nodeps pytest-nodeps e2e-cypress

clean: registry-remove
	docker rmi --force $(shell docker images --filter "reference=${CI_REGISTRY_IMAGE}" --quiet) | true
	docker rmi --force $(shell docker images --filter "reference=${MODULE_NICKNAME}" --quiet) | true

# Start local Docker registry if it is not yet started.
registry-start:
	if [ ! $(shell docker ps --filter "name=registry-$(MODULE_NICKNAME)" --format="{{.ID}}" --no-trunc) ]; then \
		docker run --detach --publish $(REGISTRY_PORT):5000 --restart always --name registry-$(MODULE_NICKNAME) registry:2; \
	fi

# Stop local Docker registry if it is running.
registry-remove:
	docker container stop registry-$(MODULE_NICKNAME) | true
	docker container rm --volumes registry-$(MODULE_NICKNAME) | true

# Convenient target for preparation before running a test.
update-%:
	$(MAKE) -f ci.makefile pull-$*
	$(MAKE) -f ci.makefile build-$*
	$(MAKE) -f ci.makefile push-$*

# Pull image from Docker registry and tag it according to a certain convention.
# If pull fails, it is not an error: maybe we are just working with a fresh registry.
pull-%:
	docker pull $(CI_REGISTRY_IMAGE):$* \
	&& docker tag $(CI_REGISTRY_IMAGE):$* $(MODULE_NICKNAME):$* \
	|| true

# Run 'build' step from Makefile.
# It is expected that 'pull' is done before this, so Docker Compose will reuse some or all layers.
build-%:
	$(MAKE) $@

# Special case for building Docker-in-Docker image for CI
build-dind:
	docker build --cache-from $(MODULE_NICKNAME):dind --tag $(MODULE_NICKNAME):dind --file ci/dind.dockerfile .

# Push image to Docker registry.
push-%:
	docker tag $(MODULE_NICKNAME):$* $(CI_REGISTRY_IMAGE):$*
	docker push $(CI_REGISTRY_IMAGE):$*


dist:
	$(MAKE) dist

dredd: pull-backend pull-dredd
	$(MAKE) dredd-nodeps

e2e-cypress: pull-backend pull-client pull-e2e-cypress
	$(MAKE) e2e-cypress-nodeps

pytest: pull-pytest
	$(MAKE) pytest-nodeps

# Pull image and immediately run the actual test.
%: pull-% %-nodeps

# Any target with suffix '-nodeps' is redirected to Makefile without changes.
# it is expected that CI has done all preparations before that,
# and it is expected that this target won't cause any pulls or builds inside.
%-nodeps:
	$(MAKE) $@


## !
## Set the required module nickname
MODULE_SHORT_NAME=rams

## !
## Set the required docker image TAG
MODULE_TAG=v1.0.2

## !
## Update the values of CI_REGISTRY_IMAGE to your module registry
CI_REGISTRY_IMAGE?=registry.gitlab.com/dtoceanplus/dtop_rams                                        

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --file ./ci/${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
		--cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
	  	--tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
	  	--file ./src/dtop_rams/gui/frontend-prod.dockerfile \
	  	./src/dtop_rams
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images

help:
	@echo URLs for access to properly deployed module ${MODULE_SHORT_NAME} on Staging server :
	@echo "       " - https://${MODULE_SHORT_NAME}.dto.opencascade.com
	@echo "       " - https://${MODULE_SHORT_NAME}.dto.opencascade.com/api
